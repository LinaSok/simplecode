## Getting Started

This project is backend application for prasac micro-fiance bank.

### Prerequisites
* Git
* Jdk 8
* springboot + spring5
* MariaDB as database

### Clone

```
git clone ssh://your-domain-name@gerrit.dminc-gtc.com:29418/prasac-loan-backend
cd prasac-loan-backend
```

### run database MariaDb with docker

docker run --name  prasac-mariadb -p 3306:3306 -e MYSQL_ROOT_PASSWORD=password mariadb:10.3

#### create database only first time 
mysql -uroot -ppassword -h127.0.0.1
create database PRASAC_LOAN  CHARACTER SET utf8 COLLATE utf8_general_ci;

#### start mariaDB in docker
docker start prasac-mariadb

### Run local environment
./gradlew clean bootRun

### open browser 
http://localhost:8080

API : http://localhost:8080/actuator/info

## install redis cache
sudo apt-get install redis-server

# ***** Test coverage ***** 
* Add your APIs to coverage test in api.js and and api.tests.js
* Before commit the code or release the project please run test in http://localhost:8080/test/v1.html 