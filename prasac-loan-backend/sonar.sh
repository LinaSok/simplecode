#!/bin/bash

currentVersion=$(grep "version" gradle.properties|cut -d'=' -f2  | sed  's/-SNAPSHOT//g')
versionMinor=$(echo ${currentVersion} | rev | cut -d\. -f1 | rev)
versionMajor=${currentVersion: 0: -${#versionMinor} }
finalVersion=${versionMajor}$((versionMinor-1))

echo ${finalVersion}

git checkout tags/${finalVersion}

currentVersion=${finalVersion}
cp  ~/*.jar build/libs/
sonar-scanner -Dsonar.projectName="Prasac-Backend" -Dsonar.projectVersion=$currentVersion -Dsonar.sourceEncoding="UTF-8" -Dsonar.tests="src/test/java" -Dsonar.projectDescription="Prasac-Backend" -Dsonar.projectKey="com.dminc.prasac:prasac-backend" -Dsonar.java.binaries=build -Dsonar.java.libraries=build/libs/*.jar -Dsonar.sources="src/main/java"  -Dsonar.java.source=1.8 -Dsonar.jacoco.reportPath="build/jacoco/test.exec" -Dsonar.login=a974162d83f2a22c22d6b8f5716b6041fa301254

