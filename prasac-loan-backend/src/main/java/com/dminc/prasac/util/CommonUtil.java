package com.dminc.prasac.util;

import java.util.concurrent.atomic.AtomicLong;

public final class CommonUtil {
    private static final AtomicLong LAST_TIME_MS = new AtomicLong();

    private CommonUtil() {
        // Nothing to do
    }

    public static Long uniqueCurrentTimeMS() {
        long now = System.currentTimeMillis();
        while (true) {
            long lastTime = LAST_TIME_MS.get();
            if (lastTime >= now) {
                now = lastTime + 1;
            }
            if (LAST_TIME_MS.compareAndSet(lastTime, now)) {
                return now;
            }
        }
    }

}
