package com.dminc.prasac.util;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmailTemplate {
    private Template createdNewUser;
    private Template loanProcess;

    @Data
    @Builder
    public static class Template {
        private String title;
        private String content;
    }
}
