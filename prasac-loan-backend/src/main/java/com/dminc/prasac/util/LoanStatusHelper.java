package com.dminc.prasac.util;

import java.util.HashMap;
import java.util.Map;

import com.dminc.prasac.model.entity.UserRole;
import com.dminc.prasac.model.entity.loan.LoanStatusEntity;
import com.dminc.prasac.model.misc.LoanStatusEnum;

public final class LoanStatusHelper {

    private static final Map<String, String> STATUS_CODE_TO_STATUS_LABEL = new HashMap<>();

    static {
        STATUS_CODE_TO_STATUS_LABEL.put("N", "Normal");
        STATUS_CODE_TO_STATUS_LABEL.put("R", "Reject");
        STATUS_CODE_TO_STATUS_LABEL.put("S", "Special Mention");
        STATUS_CODE_TO_STATUS_LABEL.put("U", "Substandard");
        STATUS_CODE_TO_STATUS_LABEL.put("D", "Doubtful");
        STATUS_CODE_TO_STATUS_LABEL.put("L", "Loss");
        STATUS_CODE_TO_STATUS_LABEL.put("W", "Write Off");
        STATUS_CODE_TO_STATUS_LABEL.put("C", "Closed");
    }

    private LoanStatusHelper() {

    }

    public static String getLoanStatusLabel(String statusCode) {
        return STATUS_CODE_TO_STATUS_LABEL.getOrDefault(statusCode, "--");
    }

    public static String getStatusText(final LoanStatusEntity loanStatusEntity, final UserRole group) {

        if (group == null) {
            return loanStatusEntity.getStatus();
        }
        final String name = group.getName();
        switch (LoanStatusEnum.getInstance(loanStatusEntity.getId())) {
            case PENDING:
                return String.format("PENDING AT %s", name);
            case SUSPENDED:
                return String.format("SUSPENDED BY %s", name);
            case APPROVED:
                return String.format("APPROVED BY %s", name);
            case REJECTED:
                return String.format("REJECTED BY %s", name);
            case IN_PROGRESS:
                return String.format("IN PROGRESS AT %s", name);
            default:
                return loanStatusEntity.getStatus();
        }
    }
}
