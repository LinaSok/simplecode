package com.dminc.prasac.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.ClassUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.dminc.prasac.model.entity.User;
import com.dminc.prasac.model.entity.UserPermission;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.service.BusinessError;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuppressWarnings({"PMD.CyclomaticComplexity", "PMD.NPathComplexity", "PMD.UnnecessaryLocalBeforeReturn"})
public final class PrasacHelper {

    private PrasacHelper() {
    }

    public static double getTotal(Double... vars) {
        return Arrays.stream(vars).mapToDouble(var -> getDouble(var)).sum();
    }

    private static double getDouble(Double value) {
        return Optional.ofNullable(value).orElse(0.0);
    }

    public static HttpServletRequest getCurrentRequest() {
        final RequestAttributes requestObj = RequestContextHolder.getRequestAttributes();
        return requestObj == null ? null : ((ServletRequestAttributes) requestObj).getRequest();
    }

    public static Class<?>[] asValidationGroups(Object... validationHints) {
        final Set<Class<?>> groups = new LinkedHashSet<>(4);
        for (Object hint : validationHints) {
            if (hint instanceof Class) {
                groups.add((Class<?>) hint);
            }
        }
        return ClassUtils.toClassArray(groups);
    }

    public static Long getCurrentUserId() {

        final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = null;
        if (principal != null) {
            if (principal instanceof UserDetails) {
                userId = ((User) principal).getId();
            } else {
                userId = -1L;
            }
        }

        return userId;
    }

    public static User getCurrentUser() {
        final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            return ((User) principal);
        }
        return null;
    }

    public static String format(Number n, int digit) {
        final String format = String.format("%%0%dd", digit);
        return String.format(format, n);
    }

    public static boolean hasViewInBranchPermission() {
        final User user = PrasacHelper.getCurrentUser();
        if (user == null) {
            BusinessError.UNAUTHORIZED_REQUEST.throwExceptionHttpStatus403();
        }
        final List<UserPermission> permissions = user.getPermissions();
        final Optional<UserPermission> loanInBranch = permissions.stream()
                .filter(userPermission -> userPermission.getName().equals(LocalConstants.PERMISSION_VIEW_LOAN_IN_BRANCH))
                .findFirst();

        return loanInBranch.isPresent();

    }

    public static boolean hasBMPermission() {
        final User user = PrasacHelper.getCurrentUser();
        if (user == null) {
            BusinessError.UNAUTHORIZED_REQUEST.throwExceptionHttpStatus403();
        }
        final List<UserPermission> permissions = user.getPermissions();
        final Optional<UserPermission> receivePrescreening = permissions.stream()
                .filter(userPermission -> userPermission.getName().equals(LocalConstants.PERMISSION_RECEIVE_PRE_SCREENING))
                .findFirst();
        return receivePrescreening.isPresent();
    }

    public static boolean hasBRReviewPermission() {
        final User user = PrasacHelper.getCurrentUser();
        if (user == null) {
            BusinessError.UNAUTHORIZED_REQUEST.throwExceptionHttpStatus403();
        }
        final List<UserPermission> permissions = user.getPermissions();
        final Optional<UserPermission> brReviewPermission = permissions.stream()
                .filter(userPermission -> userPermission.getName().equals(LocalConstants.PERMISSION_REVIEW_LEVEL_1))
                .findFirst();
        return brReviewPermission.isPresent();
    }

    public static boolean hasViewAllBranchPermission() {
        final User user = PrasacHelper.getCurrentUser();
        if (user == null) {
            BusinessError.UNAUTHORIZED_REQUEST.throwExceptionHttpStatus403();
        }
        //If user has permission to view loan from all branch or not
        final List<UserPermission> permissions = user.getPermissions();
        final Optional<UserPermission> permission = permissions.stream()
                .filter(userPermission -> userPermission.getName().equals(LocalConstants.PERMISSION_VIEW_LOAN_ALL_BRANCH))
                .findFirst();

        return permission.isPresent();
    }

    public static boolean hasROPermission() {
        final User user = PrasacHelper.getCurrentUser();
        return hasROPermission(user);
    }

    public static boolean hasROPermission(final User user) {
        if (user == null) {
            BusinessError.UNAUTHORIZED_REQUEST.throwExceptionHttpStatus403();
        }
        final List<UserPermission> permissions = user.getPermissions();
        final Optional<UserPermission> permission = permissions.stream()
                .filter(userPermission -> userPermission.getName().equals(LocalConstants.PERMISSION_RO))
                .findFirst();

        return permission.isPresent();
    }

    /**
     * @return 1: REVIEW LEVEL 1
     * 2: REVIEW LEVEL 2
     * 3: REVIEW LEVEL 3
     * 4: REVIEW LEVEL 4
     * 5: DECISION MAKER (CC2/CC3 has only DECISION MAKING permission)
     */
    public static int getUserCommitteeLevel() {
        int committeeLevel = 0;
        final User user = PrasacHelper.getCurrentUser();
        if (user == null) {
            BusinessError.UNAUTHORIZED_REQUEST.throwExceptionHttpStatus403();
        }
        final List<UserPermission> permissions = user.getPermissions();
        final Optional<UserPermission> reviewLevelOne = getReviewLevel(permissions, LocalConstants.PERMISSION_REVIEW_LEVEL_1);
        if (reviewLevelOne.isPresent()) {
            committeeLevel = 1;
        }
        final Optional<UserPermission> reviewLevel2 = getReviewLevel(permissions, LocalConstants.PERMISSION_REVIEW_LEVEL_2);
        final Optional<UserPermission> decisionMaking = getReviewLevel(permissions, LocalConstants.PERMISSION_DECISION_MAKING);
        //Review Level 2 or CC1 must have both permission
        if (reviewLevel2.isPresent() && decisionMaking.isPresent()) {
            committeeLevel = 2;
        }
        final Optional<UserPermission> reviewLevel3 = getReviewLevel(permissions, LocalConstants.PERMISSION_REVIEW_LEVEL_3);
        if (reviewLevel3.isPresent()) {
            committeeLevel = 3;
        }
        final Optional<UserPermission> reviewLevel4 = getReviewLevel(permissions, LocalConstants.PERMISSION_REVIEW_LEVEL_4);
        if (reviewLevel4.isPresent()) {
            committeeLevel = 4;
        }

        //CC2/CC3 has only decision making role
        if (decisionMaking.isPresent() && !(reviewLevelOne.isPresent() || reviewLevel2.isPresent() || reviewLevel3.isPresent() || reviewLevel4.isPresent())) {
            committeeLevel = 5;
        }

        log.info("Current user committee level: {}", committeeLevel);
        return committeeLevel;
    }

    public static List<Integer> getUserLevels() {
        final List<Integer> userCommitteeLevel = new ArrayList<>();
        userCommitteeLevel.add(LocalConstants.NON_COMMITTEE_LEVEL);

        final User user = PrasacHelper.getCurrentUser();
        if (user == null) {
            BusinessError.UNAUTHORIZED_REQUEST.throwExceptionHttpStatus403();
        }
        final List<UserPermission> permissions = user.getPermissions();
        final Optional<UserPermission> reviewLevelOne;
        reviewLevelOne = getReviewLevel(permissions, LocalConstants.PERMISSION_REVIEW_LEVEL_1);
        if (reviewLevelOne.isPresent()) {
            userCommitteeLevel.add(LocalConstants.COMMITTEE_LEVEL_1);
        }
        final Optional<UserPermission> reviewLevel2 = getReviewLevel(permissions, LocalConstants.PERMISSION_REVIEW_LEVEL_2);
        final Optional<UserPermission> decisionMaking = getReviewLevel(permissions, LocalConstants.PERMISSION_DECISION_MAKING);
        //Review Level 2 or CC1 must have both permission
        if (reviewLevel2.isPresent() && decisionMaking.isPresent()) {
            userCommitteeLevel.add(LocalConstants.COMMITTEE_LEVEL_2);
        }
        final Optional<UserPermission> reviewLevel3 = getReviewLevel(permissions, LocalConstants.PERMISSION_REVIEW_LEVEL_3);
        if (reviewLevel3.isPresent()) {
            userCommitteeLevel.add(LocalConstants.COMMITTEE_LEVEL_3);
        }
        final Optional<UserPermission> reviewLevel4 = getReviewLevel(permissions, LocalConstants.PERMISSION_REVIEW_LEVEL_4);
        if (reviewLevel4.isPresent()) {
            userCommitteeLevel.add(LocalConstants.LEVEL_LAO_REVIEW);
        }

        //CC2/CC3 has only decision making role
        if (decisionMaking.isPresent() && !(reviewLevelOne.isPresent() || reviewLevel2.isPresent() || reviewLevel3.isPresent() || reviewLevel4.isPresent())) {
            userCommitteeLevel.add(LocalConstants.LEVEL_CC2);
        }

        return userCommitteeLevel;
    }

    private static Optional<UserPermission> getReviewLevel(final List<UserPermission> permissions, final String permission) {
        final Optional<UserPermission> level = permissions.stream()
                .filter(userPermission -> userPermission.getName().equals(permission))
                .findFirst();
        return level;
    }
}
