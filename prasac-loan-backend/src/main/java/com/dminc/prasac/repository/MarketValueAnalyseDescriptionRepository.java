package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.MarketValueAnalyseDescriptionEntity;

@Repository
public interface MarketValueAnalyseDescriptionRepository extends AuditRepository<MarketValueAnalyseDescriptionEntity> {
}

