package com.dminc.prasac.repository.document;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.document.CollateralDocumentEntity;

@Repository
public interface CollateralDocumentRepository extends CrudRepository<CollateralDocumentEntity, Long> {
    Optional<CollateralDocumentEntity> findTopByDocument_Id(Long documentId);

    List<CollateralDocumentEntity> findByCollateral_Id(Long collateralId);
}
