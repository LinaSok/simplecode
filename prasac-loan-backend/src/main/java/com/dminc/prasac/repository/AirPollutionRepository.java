package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.AirPollutionEntity;

@Repository
public interface AirPollutionRepository extends AuditRepository<AirPollutionEntity> {
}

