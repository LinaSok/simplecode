package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.CollateralRegistrationEntity;

@Repository
public interface CollateralRegistrationRepository extends AuditRepository<CollateralRegistrationEntity> {
}

