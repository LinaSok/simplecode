package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.WaterPollutionEntity;

@Repository
public interface WaterPollutionRepository extends AuditRepository<WaterPollutionEntity> {
}

