package com.dminc.prasac.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

import com.dminc.prasac.model.misc.AuditEntity;

@NoRepositoryBean
public interface AuditRepository<T extends AuditEntity> extends JpaRepository<T, Long> {

    @Query("SELECT t FROM #{#entityName} t WHERE t.updatedDate > ?1")
    List<T> findAllByUpdatedDateAfter(LocalDateTime lastModified);
}
