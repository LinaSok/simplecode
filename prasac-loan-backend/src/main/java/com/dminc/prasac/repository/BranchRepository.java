package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.BranchEntity;

@Repository
public interface BranchRepository extends AuditRepository<BranchEntity> {
}
