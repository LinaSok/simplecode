package com.dminc.prasac.repository;

import com.dminc.prasac.model.entity.UserPermission;
import com.dminc.prasac.model.entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
    Optional<UserRole> findOneByName(final String name);

    @Modifying
    @Query(value = "DELETE FROM ps_user_role_user WHERE user_role_id = ?1", nativeQuery = true)
    void deleteUserRoleUser(Long id);

    @Modifying
    @Query(value = "DELETE FROM ps_user_role_user_permission WHERE user_role_id = ?1", nativeQuery = true)
    void deleteUserRoleUserPermission(Long id);

    List<UserRole> findDistinctByPermissionsIn(List<UserPermission> singletonList);

    List<UserRole> findDistinctByPermissionsInAndIdNotIn(List<UserPermission> singletonList, List<Long> notIn);

    @Query(value = "Select r from UserRole r inner join r.permissions p where p.id = ?1 and p.id = ?2")
    List<UserRole> getUserRolesByPermission(Long reviewerOne, Long reviewerTwo);
}
