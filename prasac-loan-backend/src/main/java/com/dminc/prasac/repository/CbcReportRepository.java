package com.dminc.prasac.repository;

import com.dminc.prasac.model.entity.CbcReportEntity;
import com.dminc.prasac.model.entity.loan.LoanEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CbcReportRepository extends JpaRepository<CbcReportEntity, Long> {

    Optional<CbcReportEntity> findTopByLoan(LoanEntity loanEntity);
}
