package com.dminc.prasac.repository.selection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.selection.CountryEntity;
import com.dminc.prasac.model.entity.selection.ProvinceEntity;

@Repository
public interface ProvinceRepository extends JpaRepository<ProvinceEntity, Long> {

    Page<ProvinceEntity> findByCountry(CountryEntity country, Pageable pageable);

}
