package com.dminc.prasac.repository;

import com.dminc.prasac.model.entity.UserPermissionGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserPermissionGroupRepository extends JpaRepository<UserPermissionGroup, Long> {
}
