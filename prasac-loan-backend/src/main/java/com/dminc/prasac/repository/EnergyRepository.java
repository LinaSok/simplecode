package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.EnergyEntity;

@Repository
public interface EnergyRepository extends AuditRepository<EnergyEntity> {
}

