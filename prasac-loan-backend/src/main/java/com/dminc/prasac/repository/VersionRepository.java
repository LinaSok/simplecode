package com.dminc.prasac.repository;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.VersionEntity;

@Repository
public interface VersionRepository extends PagingAndSortingRepository<VersionEntity, Long> {

    Optional<VersionEntity> findFirstByNameAndUpdatedDateAfter(String name, LocalDateTime lastModified);

}
