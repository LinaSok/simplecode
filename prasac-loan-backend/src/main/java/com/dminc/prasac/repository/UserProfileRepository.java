package com.dminc.prasac.repository;

import com.dminc.prasac.model.entity.BranchEntity;
import com.dminc.prasac.model.entity.User;
import com.dminc.prasac.model.entity.UserProfile;
import com.dminc.prasac.model.entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserProfileRepository extends JpaRepository<UserProfile, Long>, JpaSpecificationExecutor {
    UserProfile findByUser(User user);

    UserProfile findByStaffId(String staffId);

    UserProfile findByEmail(String email);

    List<UserProfile> findByUser_RolesInAndBranch(List<UserRole> userRoles, BranchEntity branch);

    List<UserProfile> findByUser_RolesIn(List<UserRole> userRoles);
}