package com.dminc.prasac.repository.document;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.document.LiabilityDocumentEntity;

@Repository
public interface LiabilityDocumentRepository extends CrudRepository<LiabilityDocumentEntity, Long> {
    Optional<LiabilityDocumentEntity> findTopByDocument_Id(Long documentId);

    List<LiabilityDocumentEntity> findByLiability_Id(Long liabilityId);
}
