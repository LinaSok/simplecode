package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.ClientBusinessLocationStatusEntity;

@Repository
public interface ClientBusinessLocationStatusRepository extends AuditRepository<ClientBusinessLocationStatusEntity> {
}

