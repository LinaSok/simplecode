package com.dminc.prasac.repository;

import com.dminc.prasac.model.entity.LoanTypeEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface LoanTypeRepository extends AuditRepository<LoanTypeEntity> {

    LoanTypeEntity findTopByCode(String code);
}

