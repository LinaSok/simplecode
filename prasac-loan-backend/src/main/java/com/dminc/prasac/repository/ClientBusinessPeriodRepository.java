package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.ClientBusinessPeriodEntity;

@Repository
public interface ClientBusinessPeriodRepository extends AuditRepository<ClientBusinessPeriodEntity> {
}

