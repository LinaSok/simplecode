package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.HealthSafetyEntity;

@Repository
public interface HealthSafetyRepository extends AuditRepository<HealthSafetyEntity> {
}

