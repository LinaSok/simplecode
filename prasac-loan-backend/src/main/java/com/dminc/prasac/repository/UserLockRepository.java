package com.dminc.prasac.repository;

import com.dminc.prasac.model.entity.UserLock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserLockRepository extends JpaRepository<UserLock, Long> {

    Optional<UserLock> findByUsername(String userName);

    void deleteByUsername(String userName);
}
