package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.BusinessExperienceEntity;

@Repository
public interface BusinessExperienceRepository extends AuditRepository<BusinessExperienceEntity> {
}

