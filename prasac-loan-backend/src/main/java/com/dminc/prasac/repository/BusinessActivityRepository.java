package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.BusinessActivityEntity;

@Repository
public interface BusinessActivityRepository extends AuditRepository<BusinessActivityEntity> {
}

