package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.CollateralTitleTypeEntity;

/**
 * Generated by Spring Data Generator on 12/02/2019
 */
@Repository
public interface CollateralTitleTypeRepository extends AuditRepository<CollateralTitleTypeEntity> {

    CollateralTitleTypeEntity findTopByCode(String code);
}
