package com.dminc.prasac.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.CollateralMarketValueAnalyseEntity;

@Repository
public interface CollateralMarketValueAnalyseRepository extends JpaRepository<CollateralMarketValueAnalyseEntity, Long> {
    Long deleteAllByIdNotIn(List<Long> ids);
}
