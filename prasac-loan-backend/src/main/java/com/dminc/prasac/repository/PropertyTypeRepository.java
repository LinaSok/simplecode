package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.PropertyTypeEntity;

@Repository
public interface PropertyTypeRepository extends AuditRepository<PropertyTypeEntity> {
}

