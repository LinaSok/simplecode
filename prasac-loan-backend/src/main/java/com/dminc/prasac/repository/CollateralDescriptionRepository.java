package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.CollateralDescriptionEntity;

@Repository
public interface CollateralDescriptionRepository extends AuditRepository<CollateralDescriptionEntity> {
}

