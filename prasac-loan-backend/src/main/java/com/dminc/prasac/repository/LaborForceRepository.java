package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.LaborForceEntity;

@Repository
public interface LaborForceRepository extends AuditRepository<LaborForceEntity> {
}

