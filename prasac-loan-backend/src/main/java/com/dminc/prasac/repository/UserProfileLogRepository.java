package com.dminc.prasac.repository;

import com.dminc.prasac.model.entity.UserProfileLogEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @see UserProfileLogEntity
 */
@Repository
public interface UserProfileLogRepository extends JpaRepository<UserProfileLogEntity, Long> {

    List<UserProfileLogEntity> findByUserProfileIdAndIsRO(Long userProfileId, boolean isRo);
}
