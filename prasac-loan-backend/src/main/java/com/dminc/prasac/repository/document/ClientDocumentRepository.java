package com.dminc.prasac.repository.document;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.document.ClientDocumentEntity;

@Repository
public interface ClientDocumentRepository extends CrudRepository<ClientDocumentEntity, Long> {
    Optional<ClientDocumentEntity> findTopByDocument_Id(Long documentId);

    List<ClientDocumentEntity> findByClient_Id(Long clientId);
}
