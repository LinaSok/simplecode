package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.LoanPurposeEntity;

@Repository
public interface LoanPurposeRepository extends AuditRepository<LoanPurposeEntity> {
    LoanPurposeEntity findByCode(String code);

    LoanPurposeEntity findTopByCbcCode(String code);
}

