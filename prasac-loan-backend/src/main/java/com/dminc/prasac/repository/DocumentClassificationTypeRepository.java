package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.DocumentClassificationTypeEntity;

@Repository
public interface DocumentClassificationTypeRepository extends AuditRepository<DocumentClassificationTypeEntity> {
}
