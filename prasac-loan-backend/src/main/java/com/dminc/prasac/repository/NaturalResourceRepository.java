package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.NaturalResourceEntity;

@Repository
public interface NaturalResourceRepository extends AuditRepository<NaturalResourceEntity> {
}

