package com.dminc.prasac.repository;

import com.dminc.prasac.model.entity.UserPermission;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PermissionRepository extends JpaRepository<UserPermission, Long> {
}
