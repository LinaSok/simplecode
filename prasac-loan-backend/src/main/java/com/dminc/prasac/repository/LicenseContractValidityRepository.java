package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.LicenseContractValidityEntity;

@Repository
public interface LicenseContractValidityRepository extends AuditRepository<LicenseContractValidityEntity> {
}

