package com.dminc.prasac.repository;

import com.dminc.prasac.model.entity.ApprovalState;
import com.dminc.prasac.model.entity.LoanApprovalState;
import com.dminc.prasac.model.entity.loan.LoanEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LoanApprovalStateRepository extends JpaRepository<LoanApprovalState, Long> {
    List<LoanApprovalState> findByLoan(LoanEntity loan);

    LoanApprovalState findByLoanAndCreatedBy(LoanEntity loan, String username);

    List<LoanApprovalState> findByCreatedByAndLoanInAndStateNot(String username, List<LoanEntity> loanEntities, ApprovalState state);

    void deleteByLoan_Id(Long id);
}
