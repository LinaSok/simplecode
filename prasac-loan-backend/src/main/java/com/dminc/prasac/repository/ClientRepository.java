package com.dminc.prasac.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.client.ClientEntity;


@Repository
public interface ClientRepository extends JpaRepository<ClientEntity, Long> {

    Optional<ClientEntity> findById(Long id);

    Optional<ClientEntity> findFirstByCif(String cif);

    @Query("SELECT e FROM ClientEntity e "
            + " LEFT JOIN ClientIdentificationEntity i on i.client = e.id "
            + " LEFT JOIN ClientLoan cl on cl.client = e.id "
            + " WHERE "
            + " (:fullName = '' OR lower(e.fullName) LIKE lower(:fullName)) AND "
            + " (:khmerFullName = '' OR e.khmerFullName LIKE :khmerFullName) AND "
            + " (:dob is NULL OR e.dob = :dob) AND "
            + " (:idNumber = '' OR i.idNumber LIKE :idNumber) AND "
            + " e.cif is NULL AND "
            + " cl.loan.loanStatus.id <> :loanStatus "
            + " group by e.id ")
    List<ClientEntity> searchClients(String fullName, String khmerFullName, LocalDate dob, String idNumber, Long loanStatus);

    void deleteAllByIdIn(List<Long> clientIds);
}
