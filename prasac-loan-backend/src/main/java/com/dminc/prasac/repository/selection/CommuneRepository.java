package com.dminc.prasac.repository.selection;

import com.dminc.prasac.model.entity.selection.CommuneEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CommuneRepository extends JpaRepository<CommuneEntity, Long> {

    @Query(value = "select c from CommuneEntity c join c.district d join d.province p where "
            + "c.code=:ccode "
            + "and d.code=:dcode "
            + "and p.code=:pcode")
    CommuneEntity findByCodes(Long ccode, Long dcode, Long pcode);
}
