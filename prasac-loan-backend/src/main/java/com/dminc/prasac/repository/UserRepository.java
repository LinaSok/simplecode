package com.dminc.prasac.repository;

import com.dminc.prasac.model.entity.User;
import com.dminc.prasac.model.entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findOneByUsername(String username);

    List<User> findDistinctByRolesInAndEnabledIs(List<UserRole> roles, boolean isEanbled);
}
