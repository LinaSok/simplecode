package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.ProductWasteEntity;

@Repository
public interface ProductWasteRepository extends AuditRepository<ProductWasteEntity> {
}

