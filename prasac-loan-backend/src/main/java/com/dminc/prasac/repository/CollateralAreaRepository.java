package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.CollateralAreaEntity;

@Repository
public interface CollateralAreaRepository extends AuditRepository<CollateralAreaEntity> {
}

