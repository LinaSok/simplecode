package com.dminc.prasac.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.DocumentEntity;

@Repository
public interface DocumentRepository extends CrudRepository<DocumentEntity, Long> {
    List<DocumentEntity> findAllByIdInAndLoan_Id(List<Long> documentIds, Long loanId);
}
