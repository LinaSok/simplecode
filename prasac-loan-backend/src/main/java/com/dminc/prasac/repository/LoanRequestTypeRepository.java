package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.LoanRequestTypeEntity;

@Repository
public interface LoanRequestTypeRepository extends AuditRepository<LoanRequestTypeEntity> {
}

