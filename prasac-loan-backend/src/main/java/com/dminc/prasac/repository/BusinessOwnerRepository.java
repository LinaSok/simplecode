package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.BusinessOwnerEntity;

@Repository
public interface BusinessOwnerRepository extends AuditRepository<BusinessOwnerEntity> {
}

