package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.ClientBusinessCashFlowEntity;

@Repository
public interface ClientBusinessCashFlowRepository extends AuditRepository<ClientBusinessCashFlowEntity> {
}

