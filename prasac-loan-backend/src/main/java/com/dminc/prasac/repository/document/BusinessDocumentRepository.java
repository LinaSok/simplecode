package com.dminc.prasac.repository.document;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.document.BusinessDocumentEntity;

@Repository
public interface BusinessDocumentRepository extends CrudRepository<BusinessDocumentEntity, Long> {
    Optional<BusinessDocumentEntity> findTopByDocument_Id(Long documentId);

    List<BusinessDocumentEntity> findByBusiness_Id(Long businessId);
}
