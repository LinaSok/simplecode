package com.dminc.prasac.repository.document;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.document.LoanDocumentEntity;

@Repository
public interface LoanDocumentRepository extends CrudRepository<LoanDocumentEntity, Long> {
    Optional<LoanDocumentEntity> findTopByDocument_Id(Long loanId);

    List<LoanDocumentEntity> findByLoan_Id(long loanId);
}
