package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.ClientOccupationEntity;

@Repository
public interface ClientOccupationRepository extends AuditRepository<ClientOccupationEntity> {
}
