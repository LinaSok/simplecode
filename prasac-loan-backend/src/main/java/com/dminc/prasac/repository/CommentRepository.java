package com.dminc.prasac.repository;

import com.dminc.prasac.model.entity.loan.CommentEntity;
import com.dminc.prasac.model.entity.loan.LoanEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<CommentEntity, Long> {

    List<CommentEntity> getAllByLoanOrderByCreatedDateDesc(final LoanEntity loan);

    List<CommentEntity> getAllByLoanAndCreatedDateAfterOrderByCreatedDateDesc(final LoanEntity loan, final LocalDateTime lastModified);

    @Modifying
    void deleteByUserRole_Id(Long userRoleId);
}
