package com.dminc.prasac.repository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.model.entity.ClientBusinessProgressEntity;

@Repository
public interface ClientBusinessProgressRepository extends AuditRepository<ClientBusinessProgressEntity> {
}

