package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.FinancialStatementIncomeEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToFinancialStatementIncomeEntityConverter extends BidirectionalConverter<Long, FinancialStatementIncomeEntity> {

    @Override
    public FinancialStatementIncomeEntity convertTo(Long source, Type<FinancialStatementIncomeEntity> destinationType,
                                                    MappingContext mappingContext) {
        return source == null ? null : FinancialStatementIncomeEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(FinancialStatementIncomeEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
