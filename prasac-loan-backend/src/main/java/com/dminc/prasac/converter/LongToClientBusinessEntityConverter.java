package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.ClientBusinessEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToClientBusinessEntityConverter extends BidirectionalConverter<Long, ClientBusinessEntity> {

    @Override
    public ClientBusinessEntity convertTo(Long source, Type<ClientBusinessEntity> destinationType,
                                          MappingContext mappingContext) {
        return source == null ? null : ClientBusinessEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(ClientBusinessEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
