package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.FinancialStatementBalanceEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToFinancialStatementBalanceEntityConverter
        extends BidirectionalConverter<Long, FinancialStatementBalanceEntity> {

    @Override
    public FinancialStatementBalanceEntity convertTo(Long source, Type<FinancialStatementBalanceEntity> destinationType,
                                                     MappingContext mappingContext) {
        return source == null ? null : FinancialStatementBalanceEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(FinancialStatementBalanceEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
