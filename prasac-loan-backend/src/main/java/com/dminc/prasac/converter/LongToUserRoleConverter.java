package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.UserRole;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToUserRoleConverter extends BidirectionalConverter<Long, UserRole> {

    @Override
    public UserRole convertTo(Long source, Type<UserRole> destinationType, MappingContext mappingContext) {
        return source == null ? null : UserRole.builder().id(source).build();
    }

    @Override
    public Long convertFrom(UserRole source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
