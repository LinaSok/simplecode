package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.PositionEntity;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToPositionEntityConverter extends BidirectionalConverter<Long, PositionEntity> {

    @Override
    public PositionEntity convertTo(Long source, Type<PositionEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : PositionEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(PositionEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
