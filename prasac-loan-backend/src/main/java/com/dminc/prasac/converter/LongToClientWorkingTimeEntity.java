package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.ClientWorkingTimeEntity;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

public class LongToClientWorkingTimeEntity extends BidirectionalConverter<Long, ClientWorkingTimeEntity> {

    @Override
    public ClientWorkingTimeEntity convertTo(Long source, Type<ClientWorkingTimeEntity> destinationType,
                                             MappingContext mappingContext) {
        return source == null ? null : ClientWorkingTimeEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(ClientWorkingTimeEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}

