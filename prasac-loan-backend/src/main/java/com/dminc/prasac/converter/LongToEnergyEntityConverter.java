package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.EnergyEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToEnergyEntityConverter extends BidirectionalConverter<Long, EnergyEntity> {

    @Override
    public EnergyEntity convertTo(Long source, Type<EnergyEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : EnergyEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(EnergyEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
