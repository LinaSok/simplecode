package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.CollateralDescriptionEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToCollateralDescriptionEntityConverter extends BidirectionalConverter<Long, CollateralDescriptionEntity> {

    @Override
    public CollateralDescriptionEntity convertTo(Long source, Type<CollateralDescriptionEntity> destinationType,
                                                 MappingContext mappingContext) {
        return source == null ? null : CollateralDescriptionEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(CollateralDescriptionEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
