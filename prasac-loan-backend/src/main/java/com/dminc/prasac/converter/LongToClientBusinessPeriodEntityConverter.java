package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.ClientBusinessPeriodEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToClientBusinessPeriodEntityConverter extends BidirectionalConverter<Long, ClientBusinessPeriodEntity> {

    @Override
    public ClientBusinessPeriodEntity convertTo(Long source, Type<ClientBusinessPeriodEntity> destinationType,
                                                MappingContext mappingContext) {
        return source == null ? null : ClientBusinessPeriodEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(ClientBusinessPeriodEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
