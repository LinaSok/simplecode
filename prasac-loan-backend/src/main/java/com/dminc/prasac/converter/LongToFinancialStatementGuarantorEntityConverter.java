package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.FinancialStatementGuarantorEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToFinancialStatementGuarantorEntityConverter
        extends BidirectionalConverter<Long, FinancialStatementGuarantorEntity> {

    @Override
    public FinancialStatementGuarantorEntity convertTo(Long source, Type<FinancialStatementGuarantorEntity> destinationType,
                                                       MappingContext mappingContext) {
        return source == null ? null : FinancialStatementGuarantorEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(FinancialStatementGuarantorEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
