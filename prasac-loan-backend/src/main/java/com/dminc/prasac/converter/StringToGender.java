package com.dminc.prasac.converter;

import com.dminc.prasac.model.dto.misc.Gender;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

import static com.dminc.prasac.model.dto.misc.Gender.FEMALE;
import static com.dminc.prasac.model.dto.misc.Gender.MALE;

public class StringToGender extends BidirectionalConverter<String, Gender> {

    public static final String STRING_FEMALE = "F";
    public static final String STRING_MALE = "M";

    @Override
    public Gender convertTo(String source, Type<Gender> destinationType, MappingContext mappingContext) {
        switch (source) {
            case STRING_FEMALE:
                return FEMALE;
            case STRING_MALE:
                return MALE;
            default:
                return null;
        }
    }

    @Override
    public String convertFrom(Gender source, Type<String> destinationType, MappingContext mappingContext) {
        switch (source) {
            case MALE:
                return STRING_MALE;
            case FEMALE:
                return STRING_FEMALE;
            default:
                return null;
        }
    }
}
