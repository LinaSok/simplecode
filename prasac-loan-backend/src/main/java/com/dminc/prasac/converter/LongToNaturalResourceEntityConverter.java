package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.NaturalResourceEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToNaturalResourceEntityConverter extends BidirectionalConverter<Long, NaturalResourceEntity> {

    @Override
    public NaturalResourceEntity convertTo(Long source, Type<NaturalResourceEntity> destinationType,
                                           MappingContext mappingContext) {
        return source == null ? null : NaturalResourceEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(NaturalResourceEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
