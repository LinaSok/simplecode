package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.loan.ClientLoan;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToClientLoanConverter extends BidirectionalConverter<Long, ClientLoan> {

    @Override
    public ClientLoan convertTo(Long source, Type<ClientLoan> destinationType, MappingContext mappingContext) {
        return source == null ? null : ClientLoan.builder().build();
    }

    @Override
    public Long convertFrom(ClientLoan source, Type<Long> destinationType, MappingContext mappingContext) {
        return null;
    }
}
