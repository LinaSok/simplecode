package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.loan.LoanEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToLoanEntityConverter extends BidirectionalConverter<Long, LoanEntity> {

    @Override
    public LoanEntity convertTo(Long source, Type<LoanEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : LoanEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(LoanEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
