package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.selection.CurrencyEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToCurrencyEntityConverter extends BidirectionalConverter<Long, CurrencyEntity> {

    @Override
    public CurrencyEntity convertTo(Long source, Type<CurrencyEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : CurrencyEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(CurrencyEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
