package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.LoanPurposeEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToLoanPurposeEntityConverter extends BidirectionalConverter<Long, LoanPurposeEntity> {

    @Override
    public LoanPurposeEntity convertTo(Long source, Type<LoanPurposeEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : LoanPurposeEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(LoanPurposeEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
