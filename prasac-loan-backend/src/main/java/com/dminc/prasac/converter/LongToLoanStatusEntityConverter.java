package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.loan.LoanStatusEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToLoanStatusEntityConverter extends BidirectionalConverter<Long, LoanStatusEntity> {

    @Override
    public LoanStatusEntity convertTo(Long source, Type<LoanStatusEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : LoanStatusEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(LoanStatusEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
