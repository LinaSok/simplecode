package com.dminc.prasac.converter;

import com.dminc.prasac.model.dto.misc.Document;
import com.dminc.prasac.model.entity.DocumentEntity;
import com.dminc.prasac.service.DocumentService;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class DocumentEntityToDocument extends CustomConverter<DocumentEntity, Document> {
    @Override
    public Document convert(DocumentEntity source, Type<? extends Document> destinationType, MappingContext mappingContext) {
        return Document.builder().id(source.getId()).description(source.getDescription())
                .image(DocumentService.getImageUrl(source.getId())).imageType(source.getImageType())
                .documentType(source.getDocumentType().getId()).loan(source.getLoan().getId()).build();
    }
}
