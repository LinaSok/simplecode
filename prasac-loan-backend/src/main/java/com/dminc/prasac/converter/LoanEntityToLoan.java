package com.dminc.prasac.converter;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections4.CollectionUtils;

import com.dminc.prasac.model.dto.CommentResponse;
import com.dminc.prasac.model.dto.address.Address;
import com.dminc.prasac.model.dto.loan.Loan;
import com.dminc.prasac.model.dto.misc.Client;
import com.dminc.prasac.model.dto.misc.Document;
import com.dminc.prasac.model.dto.misc.FinancialStatementIncomeForecast;
import com.dminc.prasac.model.dto.misc.LoanCollateral;
import com.dminc.prasac.model.entity.User;
import com.dminc.prasac.model.entity.UserRole;
import com.dminc.prasac.model.entity.client.ClientEntity;
import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.entity.loan.LoanStatusEntity;
import com.dminc.prasac.model.entity.selection.VillageEntity;
import com.dminc.prasac.repository.UserRepository;
import com.dminc.prasac.repository.document.BusinessDocumentRepository;
import com.dminc.prasac.repository.document.ClientDocumentRepository;
import com.dminc.prasac.repository.document.CollateralDocumentRepository;
import com.dminc.prasac.repository.document.LiabilityDocumentRepository;
import com.dminc.prasac.repository.document.LoanDocumentRepository;
import com.dminc.prasac.repository.selection.VillageRepository;
import com.dminc.prasac.util.LoanStatusHelper;
import com.dminc.prasac.util.PrasacHelper;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.metadata.Type;

@Slf4j
@SuppressWarnings("PMD.TooManyMethods")
public class LoanEntityToLoan extends CustomConverter<LoanEntity, Loan> {
    private static final int NUMBER_OF_MONTHS = 12;

    private final ClientDocumentRepository clientDocRepo;
    private final LoanDocumentRepository loanDocRepo;
    private final CollateralDocumentRepository collateralDocRepo;
    private final BusinessDocumentRepository businessDocRepo;
    private final LiabilityDocumentRepository liabilityDocRepo;
    private final VillageRepository villageRepository;
    private final UserRepository userRepository;

    public LoanEntityToLoan(ClientDocumentRepository clientDocRepo, LoanDocumentRepository loanDocRepo,
                            CollateralDocumentRepository collateralDocRepo, BusinessDocumentRepository businessDocRepo,
                            LiabilityDocumentRepository liabilityDocRepo, VillageRepository villageRepository, UserRepository userRepository) {
        this.clientDocRepo = clientDocRepo;
        this.loanDocRepo = loanDocRepo;
        this.collateralDocRepo = collateralDocRepo;
        this.businessDocRepo = businessDocRepo;
        this.liabilityDocRepo = liabilityDocRepo;
        this.villageRepository = villageRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Loan convert(LoanEntity source, Type<? extends Loan> destinationType, MappingContext mappingContext) {

        Loan loan = new Loan();
        mapperFacade.map(source, loan);

        List<Client> clients = CollectionUtils.emptyIfNull(source.getClients()).stream().map(clientEntity -> {
            Client client = new Client();
            mapperFacade.map(clientEntity, client);
            ClientEntityToClientResponse.populateAddresses(clientEntity, client, villageRepository);
            populateClientType(loan, clientEntity, client);
            return client;
        }).collect(Collectors.toList());

        List<CommentResponse> comments = CollectionUtils.emptyIfNull(source.getComments()).stream()
                .map(commentEntity -> mapperFacade.map(commentEntity, CommentResponse.class)).collect(Collectors.toList());

        loan.setComments(setGroupName(comments));
        loan.setClients(clients);
        final LoanStatusEntity loanStatusEntity = source.getLoanStatus();
        final UserRole group = source.getUserRole();
        this.setLoanStatusText(loan, group, loanStatusEntity);

        populateFinancialStatements(loan);
        populateDocuments(loan);
        populateAddresses(loan);

        return loan;
    }

    private void populateAddress(Address address) {
        Optional.ofNullable(address).ifPresent(a -> {
            Optional.ofNullable(a.getVillage()).ifPresent(villageId -> {
                VillageEntity v = villageRepository.getOne(villageId);
                address.setCommune(ClientEntityToClientResponse.getCommuneId(v, address.getCommune()));
                address.setDistrict(ClientEntityToClientResponse.getDistrictId(v, address.getDistrict()));
                address.setProvince(ClientEntityToClientResponse.getProvinceId(v, address.getProvince()));
            });
        });
    }

    private void populateAddresses(Loan loan) {
        CollectionUtils.emptyIfNull(loan.getBusinesses()).forEach(business -> populateAddress(business));
        CollectionUtils.emptyIfNull(loan.getCollateralList()).forEach(collateral -> populateAddress(collateral));
    }

    private void populateDocuments(Loan loan) {
        Long loanId = loan.getId();

        CollectionUtils.emptyIfNull(loan.getClients()).forEach(e -> {
            final Stream<Document> documentStream = clientDocRepo.findByClient_Id(e.getId()).stream()
                    .map(entity -> mapperFacade.map(entity.getDocument(), Document.class));
            e.setDocuments(getDocuments(loanId, documentStream));
        });

        CollectionUtils.emptyIfNull(loan.getCollateralList()).forEach(e -> populateCollateralDocs(loanId, e));

        CollectionUtils.emptyIfNull(loan.getOtherAssetsList()).forEach(e -> populateCollateralDocs(loanId, e));

        CollectionUtils.emptyIfNull(loan.getBusinesses()).forEach(e -> {
            final Stream<Document> documentStream = businessDocRepo.findByBusiness_Id(e.getId()).stream()
                    .map(entity -> mapperFacade.map(entity.getDocument(), Document.class));
            e.setDocuments(getDocuments(loanId, documentStream));
        });

        CollectionUtils.emptyIfNull(loan.getLiabilityCredits()).forEach(e -> {
            final Stream<Document> documentStream = liabilityDocRepo.findByLiability_Id(e.getId()).stream()
                    .map(entity -> mapperFacade.map(entity.getDocument(), Document.class));
            e.setDocuments(getDocuments(loanId, documentStream));
        });

        final List<Document> documents = loanDocRepo.findByLoan_Id(loan.getId()).stream()
                .map(entity -> mapperFacade.map(entity.getDocument(), Document.class)).collect(Collectors.toList());
        loan.setDocuments(documents);
    }

    private void populateCollateralDocs(Long loanId, LoanCollateral e) {
        final Stream<Document> documentStream = collateralDocRepo.findByCollateral_Id(e.getId()).stream()
                .map(entity -> mapperFacade.map(entity.getDocument(), Document.class));
        e.setDocuments(getDocuments(loanId, documentStream));
    }

    private List<Document> getDocuments(Long loanId, Stream<Document> documentStream) {
        return documentStream.filter(document -> document.getLoan().equals(loanId)).collect(Collectors.toList());
    }

    private void populateFinancialStatements(Loan loan) {
        populateFinancialStatementIncomeBankPayment(loan);
        populateFinancialStatementIncomeRevenue(loan);
        populateFinancialStatementIncomeExpense(loan);
        populateFinancialStatementBalanceLiability(loan);
        populateFinancialStatementBalanceAsset(loan);
    }

    private void populateFinancialStatementBalanceLiability(Loan loan) {
        Optional.ofNullable(loan.getFinancialStatementBalance()).ifPresent(financialStatementBalance -> {
            Optional.ofNullable(financialStatementBalance.getFinancialStatementBalanceLiability()).ifPresent(liability -> {
                final double currentYearTotal = PrasacHelper.getTotal(liability.getCurrentYearAccountPayable(),
                        liability.getCurrentYearAdvanceDeposit(), liability.getCurrentYearInformalMoneyLenders(),
                        liability.getCurrentYearShortTermLoan());

                final double forecastYearTotal = PrasacHelper.getTotal(liability.getForecastYearAccountPayable(),
                        liability.getForecastYearAdvanceDeposit(), liability.getForecastYearInformalMoneyLenders(),
                        liability.getForecastYearShortTermLoan());

                liability.setCurrentYear(currentYearTotal);
                liability.setForecastYear(forecastYearTotal);
            });
        });
    }

    private void populateFinancialStatementBalanceAsset(Loan loan) {
        Optional.ofNullable(loan.getFinancialStatementBalance()).ifPresent(financialStatementBalance -> {
            Optional.ofNullable(financialStatementBalance.getFinancialStatementBalanceAsset()).ifPresent(asset -> {
                final double currentYearTotal = PrasacHelper.getTotal(asset.getCurrentYearAccountReceivable(), asset.getCurrentYearCash(),
                        asset.getCurrentYearMaterialInventory(), asset.getCurrentYearOthers());

                final double forecastYearTotal = PrasacHelper.getTotal(asset.getForecastYearAccountReceivable(), asset.getForecastYearCash(),
                        asset.getForecastYearMaterialInventory(), asset.getForecastYearOthers());

                asset.setCurrentYear(currentYearTotal);
                asset.setForecastYear(forecastYearTotal);
            });
        });
    }

    private void populateFinancialStatementIncomeExpense(Loan loan) {
        Optional.ofNullable(loan.getFinancialStatementIncome()).ifPresent(financialStatementIncome -> {
            Optional.ofNullable(financialStatementIncome.getFinancialStatementIncomeOperatingExpense()).ifPresent(expense -> {
                final double currentMonthTotal = PrasacHelper.getTotal(expense.getCurrentMonthAdministrativeExpense(),
                        expense.getCurrentMonthDepreciationExpense(), expense.getCurrentMonthFamilyExpense(),
                        expense.getCurrentMonthMaintenanceExpense(), expense.getCurrentMonthOthersExpense(),
                        expense.getCurrentMonthRentalExpense(), expense.getCurrentMonthSalaryExpense(),
                        expense.getCurrentMonthUtilityExpense());

                final double forecastMonthTotal = PrasacHelper.getTotal(expense.getForecastMonthAdministrativeExpense(),
                        expense.getForecastMonthDepreciationExpense(), expense.getForecastMonthFamilyExpense(),
                        expense.getForecastMonthMaintenanceExpense(), expense.getForecastMonthOthersExpense(),
                        expense.getForecastMonthRentalExpense(), expense.getForecastMonthSalaryExpense(),
                        expense.getForecastMonthUtilityExpense());

                expense.setCurrentMonthTotal(currentMonthTotal);
                expense.setNextMonthTotal(forecastMonthTotal);
            });
        });
    }

    private void populateFinancialStatementIncomeRevenue(Loan loan) {
        CollectionUtils.emptyIfNull(loan.getBusinesses()).stream().forEach(biz -> {
            Optional.ofNullable(biz.getFinancialStatementIncomeRevenue())
                    .ifPresent(revenue -> populateFinancialStatementIncomeForecast(revenue));
        });
    }

    private void populateFinancialStatementIncomeForecast(FinancialStatementIncomeForecast forecast) {
        final double total = PrasacHelper.getTotal(forecast.getFirst(), forecast.getSecond(), forecast.getThird(), forecast.getFourth(),
                forecast.getFifth(), forecast.getSixth(), forecast.getSeventh(), forecast.getEighth(), forecast.getNinth(),
                forecast.getTenth(), forecast.getEleventh(), forecast.getTwelfth());

        forecast.setTotal(total);
        forecast.setAverage(Math.floor(total / NUMBER_OF_MONTHS));
    }

    private void populateFinancialStatementIncomeBankPayment(Loan loan) {
        Optional.ofNullable(loan.getFinancialStatementIncome()).ifPresent(financialStatementIncome -> {
            Optional.ofNullable(financialStatementIncome.getFinancialStatementIncomeBankPayment())
                    .ifPresent(payment -> populateFinancialStatementIncomeForecast(payment));
        });
    }

    private void populateClientType(Loan loan, ClientEntity clientEntity, Client client) {
        CollectionUtils.emptyIfNull(clientEntity.getClientLoans()).stream().filter(clientLoan -> {
            final Long loanId = clientLoan.getLoan().getId();
            final Long clientId = clientLoan.getClient().getId();

            return loanId.equals(loan.getId()) && clientId.equals(client.getId());
        }).findFirst().ifPresent(clientLoan -> {
            final Long clientTypeId = clientLoan.getClientType().getId();
            client.setClientType(clientTypeId);
        });
    }

    /**
     * @param comments
     * @return
     */
    private List<CommentResponse> setGroupName(final List<CommentResponse> comments) {
        return comments.stream().map(response -> {
            com.dminc.prasac.model.dto.user.UserRole role = response.getUserRole();
            if (role == null) {
                log.debug("===== Should reach this but somehow current comment has no user group ====");
                final String username = response.getCreatedBy();
                final User user = userRepository.findOneByUsername(username);
                final UserRole userRole = user.getRoles().get(0);
                role = mapperFacade.map(userRole, com.dminc.prasac.model.dto.user.UserRole.class);
                log.debug("===== Role selected from user ====");
            }
            final String userGroup = role.getName();
            response.setGroupName(userGroup);
            return response;
        }).collect(Collectors.toList());
    }

    private void setLoanStatusText(Loan loan, UserRole group, LoanStatusEntity loanStatusEntity) {
        if (Objects.nonNull(group)) {
            loan.setLoanStatusText(LoanStatusHelper.getStatusText(loanStatusEntity, group));
        }
    }
}
