package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.selection.ClientTypeEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToClientTypeEntityConverter extends BidirectionalConverter<Long, ClientTypeEntity> {

    @Override
    public ClientTypeEntity convertTo(Long source, Type<ClientTypeEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : ClientTypeEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(ClientTypeEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
