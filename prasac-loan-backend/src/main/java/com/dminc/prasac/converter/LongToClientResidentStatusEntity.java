package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.ClientResidentStatusEntity;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

public class LongToClientResidentStatusEntity extends BidirectionalConverter<Long, ClientResidentStatusEntity> {

    @Override
    public ClientResidentStatusEntity convertTo(Long source, Type<ClientResidentStatusEntity> destinationType,
                                                MappingContext mappingContext) {
        return source == null ? null : ClientResidentStatusEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(ClientResidentStatusEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}

