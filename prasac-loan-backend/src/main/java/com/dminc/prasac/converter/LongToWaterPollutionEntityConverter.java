package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.WaterPollutionEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToWaterPollutionEntityConverter extends BidirectionalConverter<Long, WaterPollutionEntity> {

    @Override
    public WaterPollutionEntity convertTo(Long source, Type<WaterPollutionEntity> destinationType,
                                          MappingContext mappingContext) {
        return source == null ? null : WaterPollutionEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(WaterPollutionEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
