package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.ClientBusinessProgressEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToClientBusinessProgressEntityConverter extends BidirectionalConverter<Long, ClientBusinessProgressEntity> {

    @Override
    public ClientBusinessProgressEntity convertTo(Long source, Type<ClientBusinessProgressEntity> destinationType,
                                                  MappingContext mappingContext) {
        return source == null ? null : ClientBusinessProgressEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(ClientBusinessProgressEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
