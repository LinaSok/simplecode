package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.LoanRequestTypeEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToLoanRequestTypeEntityConverter extends BidirectionalConverter<Long, LoanRequestTypeEntity> {

    @Override
    public LoanRequestTypeEntity convertTo(Long source, Type<LoanRequestTypeEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : LoanRequestTypeEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(LoanRequestTypeEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
