package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.BranchEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToBranchEntityConverter extends BidirectionalConverter<Long, BranchEntity> {

    @Override
    public BranchEntity convertTo(Long source, Type<BranchEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : BranchEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(BranchEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
