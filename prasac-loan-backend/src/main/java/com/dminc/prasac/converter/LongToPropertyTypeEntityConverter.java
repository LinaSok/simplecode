package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.PropertyTypeEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToPropertyTypeEntityConverter extends BidirectionalConverter<Long, PropertyTypeEntity> {

    @Override
    public PropertyTypeEntity convertTo(Long source, Type<PropertyTypeEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : PropertyTypeEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(PropertyTypeEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
