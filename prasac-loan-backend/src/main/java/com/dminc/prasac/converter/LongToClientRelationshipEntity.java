package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.ClientRelationshipEntity;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

public class LongToClientRelationshipEntity extends BidirectionalConverter<Long, ClientRelationshipEntity> {

    @Override
    public ClientRelationshipEntity convertTo(Long source, Type<ClientRelationshipEntity> destinationType,
                                              MappingContext mappingContext) {
        return source == null ? null : ClientRelationshipEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(ClientRelationshipEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}

