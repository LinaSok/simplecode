package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.selection.CommuneEntity;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

public class LongToCommuneEntityConverter extends BidirectionalConverter<Long, CommuneEntity> {
    @Override
    public CommuneEntity convertTo(Long source, Type<CommuneEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : CommuneEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(CommuneEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
