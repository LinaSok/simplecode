package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.CollateralAreaEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToCollateralAreaEntityConverter extends BidirectionalConverter<Long, CollateralAreaEntity> {

    @Override
    public CollateralAreaEntity convertTo(Long source, Type<CollateralAreaEntity> destinationType,
                                          MappingContext mappingContext) {
        return source == null ? null : CollateralAreaEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(CollateralAreaEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
