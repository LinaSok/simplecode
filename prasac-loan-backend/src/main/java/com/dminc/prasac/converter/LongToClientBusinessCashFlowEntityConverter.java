package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.ClientBusinessCashFlowEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToClientBusinessCashFlowEntityConverter extends BidirectionalConverter<Long, ClientBusinessCashFlowEntity> {

    @Override
    public ClientBusinessCashFlowEntity convertTo(Long source, Type<ClientBusinessCashFlowEntity> destinationType,
                                                  MappingContext mappingContext) {
        return source == null ? null : ClientBusinessCashFlowEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(ClientBusinessCashFlowEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
