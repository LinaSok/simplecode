package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.CollateralRegistrationEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToCollateralRegistrationEntityConverter extends BidirectionalConverter<Long, CollateralRegistrationEntity> {

    @Override
    public CollateralRegistrationEntity convertTo(Long source, Type<CollateralRegistrationEntity> destinationType,
                                                  MappingContext mappingContext) {
        return source == null ? null : CollateralRegistrationEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(CollateralRegistrationEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
