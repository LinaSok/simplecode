package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.BusinessExperienceEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToBusinessExperienceEntityConverter extends BidirectionalConverter<Long, BusinessExperienceEntity> {

    @Override
    public BusinessExperienceEntity convertTo(Long source, Type<BusinessExperienceEntity> destinationType,
                                              MappingContext mappingContext) {
        return source == null ? null : BusinessExperienceEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(BusinessExperienceEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
