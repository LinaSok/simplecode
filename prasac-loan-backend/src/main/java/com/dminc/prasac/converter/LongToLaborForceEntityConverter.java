package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.LaborForceEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToLaborForceEntityConverter extends BidirectionalConverter<Long, LaborForceEntity> {

    @Override
    public LaborForceEntity convertTo(Long source, Type<LaborForceEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : LaborForceEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(LaborForceEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
