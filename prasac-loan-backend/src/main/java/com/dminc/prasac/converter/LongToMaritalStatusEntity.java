package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.MaritalStatusEntity;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

public class LongToMaritalStatusEntity extends BidirectionalConverter<Long, MaritalStatusEntity> {

    @Override
    public MaritalStatusEntity convertTo(Long source, Type<MaritalStatusEntity> destinationType,
                                         MappingContext mappingContext) {
        return source == null ? null : MaritalStatusEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(MaritalStatusEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}

