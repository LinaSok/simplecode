package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.ClientBusinessLocationStatusEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToClientBusinessLocationStatusEntityConverter
        extends BidirectionalConverter<Long, ClientBusinessLocationStatusEntity> {

    @Override
    public ClientBusinessLocationStatusEntity convertTo(Long source, Type<ClientBusinessLocationStatusEntity> destinationType,
                                                        MappingContext mappingContext) {
        return source == null ? null : ClientBusinessLocationStatusEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(ClientBusinessLocationStatusEntity source, Type<Long> destinationType,
                            MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
