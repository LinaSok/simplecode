package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.User;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToUserEntityConverter extends BidirectionalConverter<Long, User> {


    @Override
    public User convertTo(Long source, Type<User> destinationType, MappingContext mappingContext) {
        return source == null ? null : User.builder().id(source).build();
    }

    @Override
    public Long convertFrom(User source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
