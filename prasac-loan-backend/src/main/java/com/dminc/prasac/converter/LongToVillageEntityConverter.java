package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.selection.VillageEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToVillageEntityConverter extends BidirectionalConverter<Long, VillageEntity> {

    @Override
    public VillageEntity convertTo(Long source, Type<VillageEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : VillageEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(VillageEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
