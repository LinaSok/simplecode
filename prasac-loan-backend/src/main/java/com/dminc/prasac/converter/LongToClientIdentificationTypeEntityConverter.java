package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.ClientIdentificationTypeEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToClientIdentificationTypeEntityConverter extends BidirectionalConverter<Long, ClientIdentificationTypeEntity> {

    @Override
    public ClientIdentificationTypeEntity convertTo(Long source, Type<ClientIdentificationTypeEntity> destinationType,
                                                    MappingContext mappingContext) {
        return source == null ? null : ClientIdentificationTypeEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(ClientIdentificationTypeEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
