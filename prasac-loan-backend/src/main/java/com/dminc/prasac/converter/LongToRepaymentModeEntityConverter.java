package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.RepaymentModeEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToRepaymentModeEntityConverter extends BidirectionalConverter<Long, RepaymentModeEntity> {

    @Override
    public RepaymentModeEntity convertTo(Long source, Type<RepaymentModeEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : RepaymentModeEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(RepaymentModeEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
