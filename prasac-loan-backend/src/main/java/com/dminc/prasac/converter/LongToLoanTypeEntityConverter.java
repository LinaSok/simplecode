package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.LoanTypeEntity;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToLoanTypeEntityConverter extends BidirectionalConverter<Long, LoanTypeEntity> {

    @Override
    public LoanTypeEntity convertTo(Long source, Type<LoanTypeEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : LoanTypeEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(LoanTypeEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
