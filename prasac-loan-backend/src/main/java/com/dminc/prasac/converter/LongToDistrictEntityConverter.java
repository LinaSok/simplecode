package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.selection.DistrictEntity;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

public class LongToDistrictEntityConverter extends BidirectionalConverter<Long, DistrictEntity> {
    @Override
    public DistrictEntity convertTo(Long source, Type<DistrictEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : DistrictEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(DistrictEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
