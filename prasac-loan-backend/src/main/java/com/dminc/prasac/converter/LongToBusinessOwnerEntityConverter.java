package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.BusinessOwnerEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToBusinessOwnerEntityConverter extends BidirectionalConverter<Long, BusinessOwnerEntity> {

    @Override
    public BusinessOwnerEntity convertTo(Long source, Type<BusinessOwnerEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : BusinessOwnerEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(BusinessOwnerEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
