package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.HealthSafetyEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToHealthSafetyEntityConverter extends BidirectionalConverter<Long, HealthSafetyEntity> {

    @Override
    public HealthSafetyEntity convertTo(Long source, Type<HealthSafetyEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : HealthSafetyEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(HealthSafetyEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
