package com.dminc.prasac.converter;

import com.dminc.prasac.model.dto.misc.Client;
import com.dminc.prasac.model.entity.client.ClientEntity;
import com.dminc.prasac.model.entity.selection.VillageEntity;
import com.dminc.prasac.repository.selection.VillageRepository;

import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.metadata.Type;

public class ClientEntityToClientResponse extends CustomConverter<ClientEntity, Client> {

    private final VillageRepository villageRepository;

    public ClientEntityToClientResponse(VillageRepository villageRepository) {
        this.villageRepository = villageRepository;
    }

    @Override
    public Client convert(ClientEntity source, Type<? extends Client> destinationType, MappingContext mappingContext) {
        Client client = new Client();
        mapperFacade.map(source, client);
        return populateAddresses(source, client, villageRepository);
    }

    public static Client populateAddresses(ClientEntity source, Client client, VillageRepository villageRepository) {
        populateCurrentAddress(source, client, villageRepository);
        populateWorkplaceAddress(source, client, villageRepository);
        populateBirthAddress(source, client, villageRepository);

        return client;
    }

    private static void populateBirthAddress(ClientEntity source, Client client, VillageRepository villageRepository) {
        final VillageEntity village = source.getBirthVillage();
        if (village != null) {
            VillageEntity v = villageRepository.getOne(village.getId());
            client.setBirthCommune(getCommuneId(v, client.getBirthCommune()));
            client.setBirthDistrict(getDistrictId(v, client.getBirthDistrict()));
            client.setBirthProvince(getProvinceId(v, client.getBirthProvince()));
        }
    }

    private static void populateWorkplaceAddress(ClientEntity source, Client client, VillageRepository villageRepository) {
        final VillageEntity village = source.getWorkplaceVillage();
        if (village != null) {
            VillageEntity v = villageRepository.getOne(village.getId());
            client.setWorkplaceCommune(getCommuneId(v, client.getWorkplaceCommune()));
            client.setWorkplaceDistrict(getDistrictId(v, client.getWorkplaceDistrict()));
            client.setWorkplaceProvince(getProvinceId(v, client.getWorkplaceProvince()));
        }
    }

    private static void populateCurrentAddress(ClientEntity source, Client client, VillageRepository villageRepository) {
        final VillageEntity village = source.getCurrentVillage();
        if (village != null) {
            VillageEntity v = villageRepository.getOne(village.getId());
            client.setCurrentCommune(getCommuneId(v, client.getCurrentCommune()));
            client.setCurrentDistrict(getDistrictId(v, client.getCurrentDistrict()));
            client.setCurrentProvince(getProvinceId(v, client.getCurrentProvince()));
        }
    }

    public static Long getProvinceId(VillageEntity village, Long defaultValue) {
        return validCommuneAndDistrict(village) && village.getCommune().getDistrict().getProvince() != null ? village.getCommune().getDistrict().getProvince().getId() : defaultValue;
    }

    private static boolean validCommuneAndDistrict(VillageEntity village) {
        return isValidCommune(village) && village.getCommune().getDistrict() != null;
    }

    private static boolean isValidCommune(VillageEntity village) {
        return village.getCommune() != null;
    }

    public static Long getDistrictId(VillageEntity village, Long defaultValue) {
        return validCommuneAndDistrict(village) ? village.getCommune().getDistrict().getId() : defaultValue;
    }

    public static Long getCommuneId(VillageEntity village, Long defaultValue) {
        return isValidCommune(village) ? village.getCommune().getId() : defaultValue;
    }
}
