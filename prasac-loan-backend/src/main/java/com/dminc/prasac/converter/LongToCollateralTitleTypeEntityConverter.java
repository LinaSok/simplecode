package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.CollateralTitleTypeEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToCollateralTitleTypeEntityConverter extends BidirectionalConverter<Long, CollateralTitleTypeEntity> {

    @Override
    public CollateralTitleTypeEntity convertTo(Long source, Type<CollateralTitleTypeEntity> destinationType,
                                               MappingContext mappingContext) {
        return source == null ? null : CollateralTitleTypeEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(CollateralTitleTypeEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
