package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.selection.NationalityEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToNationalityEntityConverter extends BidirectionalConverter<Long, NationalityEntity> {

    @Override
    public NationalityEntity convertTo(Long source, Type<NationalityEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : NationalityEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(NationalityEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
