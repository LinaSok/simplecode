package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.DocumentTypeEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToDocumentTypeEntityConverter extends BidirectionalConverter<Long, DocumentTypeEntity> {

    @Override
    public DocumentTypeEntity convertTo(Long source, Type<DocumentTypeEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : DocumentTypeEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(DocumentTypeEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
