package com.dminc.prasac.converter;

import com.dminc.prasac.model.dto.user.Branch;
import com.dminc.prasac.model.dto.user.Position;
import com.dminc.prasac.model.dto.user.UserPermission;
import com.dminc.prasac.model.dto.user.UserRole;
import com.dminc.prasac.model.entity.UserProfile;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.metadata.Type;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class UserProfileToProfileResponse extends CustomConverter<UserProfile, com.dminc.prasac.model.dto.user.UserProfile> {

    @Override
    public com.dminc.prasac.model.dto.user.UserProfile convert(UserProfile source, Type<? extends com.dminc.prasac.model.dto.user.UserProfile> destinationType, MappingContext mappingContext) {
        if (source == null) {
            return null;
        }

        final Position position = Position.builder().id(source.getPosition().getId()).name(source.getPosition().getName()).build();

        final List<UserRole> userRoles = source.getUser().getRoles().stream().map(roles -> {
            UserRole role = UserRole.builder().id(roles.getId()).name(roles.getName()).build();
            if (CollectionUtils.isNotEmpty(roles.getPermissions())) {
                List<UserPermission> userPermissions = roles.getPermissions().stream().map(p -> UserPermission.builder().id(p.getId()).name(p.getName()).build()).collect(Collectors.toList());
                role.setPermissions(userPermissions);
            }
            return role;
        }).collect(Collectors.toList());

        return com.dminc.prasac.model.dto.user.UserProfile.builder().id(source.getUser().getId())
                .profileId(source.getId())
                .staffId(source.getStaffId())
                .branch(mapperFacade.map(source.getBranch(), Branch.class))
                .position(position)
                .dateOfBirth(source.getDateOfBirth())
                .firstName(source.getFirstName())
                .lastName(source.getLastName())
                .roles(userRoles)
                .username(source.getUser().getUsername())
                .email(source.getEmail())
                .isEnabled(source.getUser().isEnabled())
                .createdDate(source.getCreatedDate())
                .gender(source.getGender().name())
                .build();
    }
}
