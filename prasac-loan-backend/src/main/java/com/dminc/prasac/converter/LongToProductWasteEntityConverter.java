package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.ProductWasteEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToProductWasteEntityConverter extends BidirectionalConverter<Long, ProductWasteEntity> {

    @Override
    public ProductWasteEntity convertTo(Long source, Type<ProductWasteEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : ProductWasteEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(ProductWasteEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
