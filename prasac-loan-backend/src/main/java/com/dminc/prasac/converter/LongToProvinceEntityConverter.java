package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.selection.ProvinceEntity;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

public class LongToProvinceEntityConverter extends BidirectionalConverter<Long, ProvinceEntity> {
    @Override
    public ProvinceEntity convertTo(Long source, Type<ProvinceEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : ProvinceEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(ProvinceEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
