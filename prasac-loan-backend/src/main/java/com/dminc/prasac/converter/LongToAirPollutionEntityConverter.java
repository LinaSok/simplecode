package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.AirPollutionEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToAirPollutionEntityConverter extends BidirectionalConverter<Long, AirPollutionEntity> {

    @Override
    public AirPollutionEntity convertTo(Long source, Type<AirPollutionEntity> destinationType, MappingContext mappingContext) {
        return source == null ? null : AirPollutionEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(AirPollutionEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
