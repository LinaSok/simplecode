package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.ClientOccupationEntity;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

public class LongToClientOccupationEntity extends BidirectionalConverter<Long, ClientOccupationEntity> {

    @Override
    public ClientOccupationEntity convertTo(Long source, Type<ClientOccupationEntity> destinationType,
                                            MappingContext mappingContext) {
        return source == null ? null : ClientOccupationEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(ClientOccupationEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}

