package com.dminc.prasac.converter;

import com.dminc.prasac.model.entity.LoanCollateralEntity;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Slf4j
public class LongToLoanCollateralEntityConverter extends BidirectionalConverter<Long, LoanCollateralEntity> {

    @Override
    public LoanCollateralEntity convertTo(Long source, Type<LoanCollateralEntity> destinationType,
                                          MappingContext mappingContext) {
        return source == null ? null : LoanCollateralEntity.builder().id(source).build();
    }

    @Override
    public Long convertFrom(LoanCollateralEntity source, Type<Long> destinationType, MappingContext mappingContext) {
        return source == null ? null : source.getId();
    }
}
