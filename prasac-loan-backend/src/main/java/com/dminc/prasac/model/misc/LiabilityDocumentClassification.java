package com.dminc.prasac.model.misc;

import java.util.Arrays;

public enum LiabilityDocumentClassification {
    LOAN_AGREEMENT(7L), REPAYMENT_SCHEDULE(8L), BANK_STATEMENT(9L), PASSBOOK(10L), COLLATERAL_RECIEPT(11L), REPAYMENT_SLIP(
            12L), OTHERS(14L);

    private final Long value;

    LiabilityDocumentClassification(Long value) {
        this.value = value;
    }

    public Long getValue() {
        return this.value;
    }

    public static boolean contains(Long classification) {
        return Arrays.stream(LiabilityDocumentClassification.values()).anyMatch(c -> c.getValue().equals(classification));
    }

}
