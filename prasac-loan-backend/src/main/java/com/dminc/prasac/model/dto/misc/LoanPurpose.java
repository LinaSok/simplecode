package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanPurpose implements Serializable {

    private static final long serialVersionUID = 8079677458381468378L;
    private Long id;

    private String purpose;

    private String khmerPurpose;

    private String code;

    private String cbcCode;

    private String cbcDesc;
}