package com.dminc.prasac.model.entity.selection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.dminc.prasac.model.misc.AuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = NationalityEntity.REGION)
@Table(name = "ps_nationality")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NationalityEntity extends AuditEntity {
    public static final String REGION = "nationality";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String code;

    private String nationality;

    @Column(name = "nationality_in_khmer")
    private String khmerNationality;

    @Column(name = "cbc_code")
    private String cbcCode;
}