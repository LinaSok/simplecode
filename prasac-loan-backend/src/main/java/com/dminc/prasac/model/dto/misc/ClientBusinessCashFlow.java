package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientBusinessCashFlow implements Serializable {

    private static final long serialVersionUID = -5890220866571964977L;
    private Long id;

    private String cashFlow;

    private String khmerCashFlow;

    private String code;
}
