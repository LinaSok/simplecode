package com.dminc.prasac.model.entity;

import com.dminc.prasac.model.dto.misc.Gender;
import com.dminc.prasac.model.misc.AuditEntity;
import com.dminc.prasac.model.misc.LocalConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

/**
 * Entity class to store the user profile before updating.
 * This main purpose is for the RO user.
 * RO users can move branches but he/she still be able to view loan in his/her previous branch
 * <p>
 * Also this class used for preparing ahead in case we want to track user moving branches/position or update profile
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = UserProfileLogEntity.TABLE_NAME)
public class UserProfileLogEntity extends AuditEntity {

    public static final String TABLE_NAME = "ps_user_profile_log";

    public static final String REGION = "userProfileLog";

    @Id
    @GenericGenerator(name = REGION, strategy = LocalConstants.ID_GENERATOR)
    @GeneratedValue(generator = REGION)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "position_id")
    private Long positionId;

    @Column(name = "branch_id")
    private Long branchId;

    @Column(name = "staff_id")
    private String staffId;

    @Column(name = "email")
    private String email;

    @Column(name = "user_profile_id")
    private Long userProfileId;

    /**
     * Only RO user has the value of pending loan
     */
    @Column(name = "pending_loan_id")
    @Type(type = "text")
    private String pendingLoanId;

    @Column(name = "is_ro")
    private Boolean isRO;
}
