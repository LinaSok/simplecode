package com.dminc.prasac.model.misc;

import java.util.Arrays;

public enum LoanDocumentClassification {
    APPLICATION_FORM(3L), LOAN_AGREEMENT(4L), COLLATERAL_AGREEMENT(5L), GUARANTEE_AGREEMENT(6L);

    private final Long value;

    LoanDocumentClassification(Long value) {
        this.value = value;
    }

    public Long getValue() {
        return this.value;
    }

    public static boolean contains(Long classification) {
        return Arrays.stream(LoanDocumentClassification.values()).anyMatch(c -> c.getValue().equals(classification));
    }

}
