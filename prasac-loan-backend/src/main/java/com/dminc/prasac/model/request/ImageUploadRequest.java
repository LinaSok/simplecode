package com.dminc.prasac.model.request;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.dminc.prasac.model.misc.DocumentRequestType;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ImageUploadRequest {
    @ApiModelProperty(required = true)
    @Size(min = 1)
    @NotNull
    @Valid
    private List<DocumentRequest> documentRequests;

    @ApiModelProperty(required = true)
    @NotNull
    private Long loan;

    @ApiModelProperty(hidden = true)
    private Long client;

    @ApiModelProperty(hidden = true)
    private Long liability;

    @ApiModelProperty(hidden = true)
    private Long business;

    @ApiModelProperty(hidden = true)
    private Long collateral;

    @ApiModelProperty(hidden = true)
    private DocumentRequestType documentRequestType;

}