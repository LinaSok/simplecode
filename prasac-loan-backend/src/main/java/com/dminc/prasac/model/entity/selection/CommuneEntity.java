package com.dminc.prasac.model.entity.selection;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.dminc.prasac.model.misc.AuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = CommuneEntity.REGION)
@Table(name = "ps_commune")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CommuneEntity extends AuditEntity implements Serializable {
    public static final String REGION = "commune";
    private static final long serialVersionUID = 6406638828238057303L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long code;

    @Column(name = "khmer_name")
    private String khmerName;

    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "district_id")
    private DistrictEntity district;

    @OneToMany(mappedBy = REGION)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = VillageEntity.REGION)
    @LazyCollection(LazyCollectionOption.TRUE)
    private List<VillageEntity> villages;

}