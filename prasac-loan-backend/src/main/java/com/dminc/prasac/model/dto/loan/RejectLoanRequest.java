package com.dminc.prasac.model.dto.loan;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class RejectLoanRequest extends ApprovalRequest {
    private boolean byClient;
}
