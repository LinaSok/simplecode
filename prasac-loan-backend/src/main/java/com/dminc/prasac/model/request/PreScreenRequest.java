package com.dminc.prasac.model.request;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.service.validator.constraint.AtLeastOneBorrower;
import com.dminc.prasac.service.validator.constraint.Decision;
import com.dminc.prasac.service.validator.constraint.FailReason;
import com.dminc.prasac.service.validator.constraint.ValidCIFs;
import com.dminc.prasac.service.validator.constraint.group.COConstraint;
import com.dminc.prasac.service.validator.constraint.group.Default;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@FailReason(groups = Default.class)
public class PreScreenRequest {
    private Long id;

    @ApiModelProperty(value = LocalConstants.CO_REQUIRED_BO_OPTIONAL)
    @NotNull(groups = {COConstraint.class})
    private Double loanAmount;

    @ApiModelProperty(value = "Required when decision is 'failed' at Pre-screening")
    private String preScreeningFailReason;

    @ApiModelProperty(value = LocalConstants.CO_OPTIONAL_BO_OPTIONAL)
    private Long branch;

    @ApiModelProperty(value = LocalConstants.CO_REQUIRED_BO_OPTIONAL)
    @NotNull(groups = {COConstraint.class})
    private Long currency;

    @ApiModelProperty(value = LocalConstants.CO_REQUIRED_BO_OPTIONAL)
    @NotNull(groups = {COConstraint.class})
    @Decision(groups = Default.class)
    private Long loanStatus;

    @Valid
    @Size(min = 1, groups = Default.class)
    @ApiModelProperty(value = LocalConstants.CO_REQUIRED_BO_REQUIRED)
    @NotNull(groups = {Default.class})
    @AtLeastOneBorrower(groups = Default.class)
    @ValidCIFs(groups = Default.class)
    private List<ClientPreScreenRequest> clients;

}
