package com.dminc.prasac.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.dminc.prasac.model.misc.AuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "ps_schema_version")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = true)
public class VersionEntity extends AuditEntity implements Serializable {
    private static final long serialVersionUID = -6197357857309514029L;

    public static final String ADDRESS_VALUE_OF_NAME_FIELD = "address";

    @Id
    @GenericGenerator(name = "version_id", strategy = "increment")
    @GeneratedValue(generator = "version_id")
    @Column(nullable = false, updatable = false)
    private Long id;

    private String name;

    @Type(type = "text")
    private String description;

}
