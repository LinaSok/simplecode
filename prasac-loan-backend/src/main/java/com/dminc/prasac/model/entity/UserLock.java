package com.dminc.prasac.model.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.dminc.prasac.model.misc.AuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Table(name = "ps_user_locked")
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class UserLock extends AuditEntity {

    @Id
    @GenericGenerator(name = "user_lock_id", strategy = "increment")
    @GeneratedValue(generator = "user_lock_id")
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "user_name")
    private String username;

    @Column(name = "start_locked_period")
    private LocalDateTime startLockedPeriod;

    @Column(name = "attempt_count")
    private Integer attemptCount;

}
