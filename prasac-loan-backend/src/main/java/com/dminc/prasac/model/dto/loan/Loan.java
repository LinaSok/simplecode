package com.dminc.prasac.model.dto.loan;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.format.annotation.DateTimeFormat;

import com.dminc.common.tools.Constants;
import com.dminc.prasac.model.dto.CommentResponse;
import com.dminc.prasac.model.dto.misc.Client;
import com.dminc.prasac.model.dto.misc.ClientBusiness;
import com.dminc.prasac.model.dto.misc.ClientLiabilityCredit;
import com.dminc.prasac.model.dto.misc.CollateralDescription;
import com.dminc.prasac.model.dto.misc.Document;
import com.dminc.prasac.model.dto.misc.FinancialStatementBalance;
import com.dminc.prasac.model.dto.misc.FinancialStatementGuarantor;
import com.dminc.prasac.model.dto.misc.FinancialStatementIncome;
import com.dminc.prasac.model.dto.misc.LoanCollateral;
import com.dminc.prasac.model.dto.misc.LoanProject;
import com.dminc.prasac.model.dto.misc.SocialEnvironmentImpactAssessment;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@SuppressWarnings("PMD.TooManyFields")
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
public class Loan implements Serializable {
    private static final long serialVersionUID = 5948584780482386802L;
    private Long id;

    private Long loanStatus;

    private String loanStatusText;

    private Long branch;

    private Long currency;

    private Long loanRequestType;

    private Long repaymentMode;

    private Integer cycle;

    private Double loanAmount;

    private String preScreeningFailReason;

    private Integer gracePeriod;

    private Double loanFee;

    private Integer lockInPeriod;

    private Double monthlyInterestRate;

    private Double refinancingFee;

    private Double adminFee;

    private Double registrationFee;

    private Double paidOffFee;

    private Integer tenure;

    private String conclusion;

    private Long assignedUser;

    private Integer resubmitCounter;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_MINUTE_FORMAT)
    @DateTimeFormat(pattern = Constants.DATE_MINUTE_FORMAT)
    private LocalDateTime createdDate;

    private SocialEnvironmentImpactAssessment socialEnvironmentImpactAssessment;

    private FinancialStatementBalance financialStatementBalance;

    private FinancialStatementIncome financialStatementIncome;

    private FinancialStatementGuarantor financialStatementGuarantor;

    private List<ClientBusiness> businesses;

    private List<LoanProject> projects;

    private List<LoanCollateral> collateralList;

    private List<Client> clients;

    private List<ClientLiabilityCredit> liabilityCredits;

    private List<Long> imageIds;

    private int committeeLevel;

    private List<CommentResponse> comments;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_MINUTE_FORMAT)
    @DateTimeFormat(pattern = Constants.DATE_MINUTE_FORMAT)
    private LocalDateTime updatedDate;

    private List<Document> documents;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_MINUTE_FORMAT)
    @DateTimeFormat(pattern = Constants.DATE_MINUTE_FORMAT)
    private LocalDateTime requestDate;

    //filter out other assets
    public List<LoanCollateral> getCollateralList() {

        if (collateralList == null) {
            return Collections.emptyList();
        }
        return collateralList.stream().filter(loanCollateral -> CollateralDescription.COLLATERAL_DESC.equals(loanCollateral.getCollateralDescription())).collect(Collectors.toList());
    }

    public List<LoanCollateral> getOtherAssetsList() {
        if (collateralList == null) {
            return Collections.emptyList();
        }
        // 2L is other assets
        return collateralList.stream().filter(loanCollateral -> CollateralDescription.OTHER_ASSETS_DESC.equals(loanCollateral.getCollateralDescription())).collect(Collectors.toList());
    }
}
