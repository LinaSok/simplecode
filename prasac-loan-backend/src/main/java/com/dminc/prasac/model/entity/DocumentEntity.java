package com.dminc.prasac.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.misc.AuditEntity;
import com.dminc.prasac.model.misc.LocalConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = DocumentEntity.REGION)
@Table(name = "ps_document")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DocumentEntity extends AuditEntity {
    public static final String REGION = "document";
    public static final String DOCUMENT_ID = "document_id";

    @Id
    @GenericGenerator(name = REGION, strategy = LocalConstants.ID_GENERATOR)
    @GeneratedValue(generator = REGION)
    private Long id;

    private String description;

    @Lob
    private byte[] image;

    @Column(name = "image_type")
    private String imageType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "document_type_id")
    private DocumentTypeEntity documentType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "loan_id")
    private LoanEntity loan;
}