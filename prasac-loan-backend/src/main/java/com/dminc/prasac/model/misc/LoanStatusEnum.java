package com.dminc.prasac.model.misc;

import org.apache.logging.log4j.util.Strings;

import java.util.Arrays;
import java.util.Optional;

public enum LoanStatusEnum {
    NEW_PRE_SCREENING(3L),
    NEWLY_ASSIGNED(4L),
    TO_BE_CORRECTED(19L),
    PASSED_PRE_SCREENING(1L),
    FAIL_PRE_SCREENING(2L),
    SUSPENDED(20L),
    IN_PROGRESS(23L),
    REJECTED(16L),
    APPROVED(12L),
    PENDING(5L),
    TO_BE_SUBMITTED(24L),
    REJECTED_BY_CLIENT(18L),
    COMPLETE_REJECTED(25L);

    private final Long value;

    LoanStatusEnum(Long value) {
        this.value = value;
    }

    public static LoanStatusEnum getInstance(Long value) {
        Optional<LoanStatusEnum> status = Arrays.stream(LoanStatusEnum.values()).filter(loanStatusEnum -> loanStatusEnum.value.equals(value)).findFirst();
        return status.orElse(null);
    }

    public Long getValue() {
        return this.value;
    }

    public static String name(final Long value) {
        for (LoanStatusEnum val : LoanStatusEnum.values()) {
            if (val.getValue().equals(value)) {
                return val.name();
            }
        }
        return Strings.EMPTY;
    }
}
