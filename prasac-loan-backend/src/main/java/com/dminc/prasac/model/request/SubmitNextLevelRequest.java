package com.dminc.prasac.model.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SubmitNextLevelRequest extends AssignBackRequest {
    private Long userRoleId;
}
