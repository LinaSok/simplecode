package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.dminc.prasac.service.validator.constraint.group.Default;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FinancialStatementGuarantorEmployment implements Serializable {
    private static final long serialVersionUID = 8941400358363788039L;

    private Long id;

    @ApiModelProperty(required = true)
    @NotBlank(groups = Default.class)
    private String activity;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double annualIncome;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double annualExpense;

}