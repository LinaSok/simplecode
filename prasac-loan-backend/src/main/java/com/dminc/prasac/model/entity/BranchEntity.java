package com.dminc.prasac.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.dminc.prasac.model.misc.AuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "ps_branch")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = true)
public class BranchEntity extends AuditEntity implements Serializable {

    private static final long serialVersionUID = -6197357857309514029L;

    @Id
    @GenericGenerator(name = "branch_id", strategy = "increment")
    @GeneratedValue(generator = "branch_id")
    @Column(name = "branch_id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "branch_code")
    private String code;

    @Column(name = "branch_name")
    private String name;
}
