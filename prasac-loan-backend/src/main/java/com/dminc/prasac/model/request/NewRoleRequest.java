package com.dminc.prasac.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewRoleRequest {
    private String name;
    private List<Long> permissions;
}
