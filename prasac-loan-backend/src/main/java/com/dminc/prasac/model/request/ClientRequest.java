package com.dminc.prasac.model.request;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.dminc.prasac.model.dto.misc.ClientIdentification;
import com.dminc.prasac.service.validator.constraint.group.COConstraint;
import com.dminc.prasac.service.validator.constraint.group.Default;
import com.dminc.prasac.service.validator.constraint.group.Update;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ClientRequest {
    private Long id;

    @Size(groups = Default.class, max = 100)
    private String cif;

    @NotNull(groups = {COConstraint.class, Update.class})
    private Long clientType;

    @Valid
    @NotNull(groups = {COConstraint.class, Update.class})
    @Size(min = 1, groups = {COConstraint.class, Update.class})
    private List<ClientIdentification> identifications;
}
