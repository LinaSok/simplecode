package com.dminc.prasac.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;

import com.dminc.prasac.model.misc.AuditEntity;
import com.dminc.prasac.model.misc.LocalConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = FinancialStatementIncomeEntity.REGION)
@Table(name = "ps_financial_statement_income")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FinancialStatementIncomeEntity extends AuditEntity {
    public static final String REGION = "financialStatementIncome";

    @Id
    @GenericGenerator(name = REGION, strategy = LocalConstants.ID_GENERATOR)
    @GeneratedValue(generator = REGION)
    private Long id;

    @Column(name = "tax_expenses")
    private Double taxExpenses;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "income_bank_payment_id")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = FinancialStatementIncomeBankPaymentEntity.REGION)
    @Cascade({CascadeType.ALL})
    private FinancialStatementIncomeBankPaymentEntity financialStatementIncomeBankPayment;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "income_operating_expense_id")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = FinancialStatementIncomeOperatingExpenseEntity.REGION)
    @Cascade({CascadeType.ALL})
    private FinancialStatementIncomeOperatingExpenseEntity financialStatementIncomeOperatingExpense;

}