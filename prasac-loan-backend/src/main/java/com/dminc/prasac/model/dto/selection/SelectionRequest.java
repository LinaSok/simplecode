package com.dminc.prasac.model.dto.selection;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.dminc.common.tools.Constants;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SelectionRequest {
    @ApiModelProperty(value = Constants.DATE_MINUTE_FORMAT)
    @DateTimeFormat(pattern = Constants.DATE_MINUTE_FORMAT)
    @NotNull
    private LocalDateTime lastModified;
}
