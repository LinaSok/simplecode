package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientBusinessPeriod implements Serializable {

    private static final long serialVersionUID = -9120667685707497660L;
    private Long id;

    private String period;

    private String khmerPeriod;

    private String code;
}
