package com.dminc.prasac.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import com.dminc.prasac.model.misc.AuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = ClientIdentificationTypeEntity.REGION)
@Table(name = ClientIdentificationTypeEntity.PS_CLIENT_IDENTIFICATION_TYPE)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientIdentificationTypeEntity extends AuditEntity {
    public static final String REGION = "clientIdentificationType";
    public static final String PS_CLIENT_IDENTIFICATION_TYPE = "ps_client_identification_type";

    @Id
    @GenericGenerator(name = "client_identification_type_id", strategy = "increment")
    @GeneratedValue(generator = "client_identification_type_id")
    private Long id;

    private String code;

    private String type;

    @Column(name = "type_in_khmer")
    private String khmerType;

    @Column(name = "cbc_code")
    private String cbcCode;

}