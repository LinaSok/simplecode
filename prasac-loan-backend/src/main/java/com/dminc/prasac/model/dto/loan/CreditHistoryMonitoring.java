package com.dminc.prasac.model.dto.loan;


import com.dminc.prasac.model.dto.misc.Currency;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreditHistoryMonitoring implements Serializable {

    private static final long serialVersionUID = -8892290193520204072L;

    private String clientCif;

    private LocalDate enqDate;

    private LocalDate date;

    private String event;

    private Currency currency;

    private Double amount;

    private String lender;

    private String reference;
}
