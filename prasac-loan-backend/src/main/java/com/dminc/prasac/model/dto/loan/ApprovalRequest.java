package com.dminc.prasac.model.dto.loan;

import lombok.Data;

@Data
public class ApprovalRequest {
    private String comment;
}
