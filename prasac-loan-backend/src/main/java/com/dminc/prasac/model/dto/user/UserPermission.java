package com.dminc.prasac.model.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserPermission implements Serializable {
    private static final long serialVersionUID = 4035427091929211401L;
    private Long id;
    private String name;
    private String displayName;
}
