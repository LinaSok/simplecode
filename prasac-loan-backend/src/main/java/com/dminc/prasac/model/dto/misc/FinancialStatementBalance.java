package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.dminc.prasac.service.validator.constraint.group.Default;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FinancialStatementBalance implements Serializable {
    private static final long serialVersionUID = 983555844706715639L;

    private Long id;

    @Valid
    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private FinancialStatementBalanceAsset financialStatementBalanceAsset;

    @Valid
    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private FinancialStatementBalanceLiability financialStatementBalanceLiability;
}
