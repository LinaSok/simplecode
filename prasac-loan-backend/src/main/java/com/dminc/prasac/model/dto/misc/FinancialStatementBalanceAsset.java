package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.dminc.prasac.service.validator.constraint.group.Default;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FinancialStatementBalanceAsset implements Serializable {
    private static final long serialVersionUID = -5614488408871218003L;

    private Long id;

    private Double currentYear;

    private Double forecastYear;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double currentYearAccountReceivable;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double currentYearCash;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double currentYearMaterialInventory;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double currentYearOthers;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double forecastYearAccountReceivable;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double forecastYearCash;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double forecastYearMaterialInventory;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double forecastYearOthers;

}
