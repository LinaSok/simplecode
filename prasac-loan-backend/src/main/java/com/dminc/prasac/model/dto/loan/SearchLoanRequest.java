package com.dminc.prasac.model.dto.loan;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.data.domain.Sort;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SearchLoanRequest implements Serializable {

    private static final long serialVersionUID = -594117311714824314L;

    private String searchText;
    private Long branch;
    private Long status;
    private SortingField sortingField;
    private Sort.Direction direction;

    @JsonIgnore
    private Long borrowerType;
}
