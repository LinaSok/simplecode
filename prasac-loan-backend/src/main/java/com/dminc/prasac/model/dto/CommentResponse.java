package com.dminc.prasac.model.dto;

import com.dminc.common.tools.Constants;
import com.dminc.prasac.model.dto.user.UserRole;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class CommentResponse implements Serializable {
    private static final long serialVersionUID = -6283593958053538759L;

    private String groupName;
    private String comment;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_MINUTE_FORMAT)
    @DateTimeFormat(pattern = Constants.DATE_MINUTE_FORMAT)
    private LocalDateTime createdDate;

    @JsonIgnore
    private String createdBy;

    @JsonIgnore
    private UserRole userRole;
}
