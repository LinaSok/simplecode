package com.dminc.prasac.model.request;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.dminc.prasac.model.dto.misc.ClientBusiness;
import com.dminc.prasac.model.dto.misc.ClientLiabilityCredit;
import com.dminc.prasac.model.dto.misc.FinancialStatementBalance;
import com.dminc.prasac.model.dto.misc.FinancialStatementGuarantor;
import com.dminc.prasac.model.dto.misc.FinancialStatementIncome;
import com.dminc.prasac.model.dto.misc.LoanCollateral;
import com.dminc.prasac.model.dto.misc.LoanProject;
import com.dminc.prasac.model.dto.misc.SocialEnvironmentImpactAssessment;
import com.dminc.prasac.service.validator.constraint.AtLeastOneBorrower;
import com.dminc.prasac.service.validator.constraint.LoanExists;
import com.dminc.prasac.service.validator.constraint.ValidCIFs;
import com.dminc.prasac.service.validator.constraint.group.Default;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("PMD.TooManyFields")
public class LoanAppraisalRequest {

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    @LoanExists(groups = {Default.class})
    private Long id;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long loanRequestType;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long repaymentMode;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double loanAmount;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long currency;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Integer cycle;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Integer gracePeriod;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Integer lockInPeriod;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double monthlyInterestRate;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double refinancingFee;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double adminFee;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double registrationFee;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double paidOffFee;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Integer tenure;

    private String conclusion;

    @Valid
    @Size(min = 1, groups = Default.class)
    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    @AtLeastOneBorrower(groups = Default.class)
    @ValidCIFs(groups = Default.class)
    private List<LoanAppraisalClientRequest> clients;

    @Valid
    @Size(min = 1, groups = Default.class)
    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private List<LoanProject> projects;

    private List<ClientLiabilityCredit> liabilityCredits;

    @Valid
    @Size(min = 1, groups = Default.class)
    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private List<ClientBusiness> businesses;

    @Valid
    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private FinancialStatementIncome financialStatementIncome;

    @Valid
    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private FinancialStatementBalance financialStatementBalance;

    private FinancialStatementGuarantor financialStatementGuarantor;

    @Valid
    private List<LoanCollateral> collateralList;

    @Valid
    private List<LoanCollateral> otherAssetsList;

    @Valid
    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private SocialEnvironmentImpactAssessment socialEnvironmentImpactAssessment;

}
