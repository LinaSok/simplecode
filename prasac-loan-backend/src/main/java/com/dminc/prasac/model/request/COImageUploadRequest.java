package com.dminc.prasac.model.request;

import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.MultipartFile;

import com.dminc.prasac.model.misc.DocumentRequestType;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class COImageUploadRequest {

    private MultipartFile[] images;

    @ApiModelProperty(required = true)
    @NotNull
    private Long loan;

    @ApiModelProperty(required = true)
    @NotNull
    private Long documentType;

    @ApiModelProperty(hidden = true)
    private Long client;

    @ApiModelProperty(hidden = true)
    private Long liability;

    @ApiModelProperty(hidden = true)
    private Long business;

    @ApiModelProperty(hidden = true)
    private Long collateral;

    @ApiModelProperty(hidden = true)
    private DocumentRequestType documentRequestType;

}