package com.dminc.prasac.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.dminc.prasac.model.misc.AuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = ClientBusinessCashFlowEntity.REGION)
@Table(name = "ps_client_business_cash_flow")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClientBusinessCashFlowEntity extends AuditEntity {
    public static final String REGION = "clientBusinessCashFlow";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "cash_flow")
    private String cashFlow;

    @Column(name = "cash_flow_in_khmer")
    private String khmerCashFlow;

    private String code;
}
