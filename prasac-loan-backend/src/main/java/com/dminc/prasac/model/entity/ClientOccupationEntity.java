package com.dminc.prasac.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Builder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.dminc.prasac.model.misc.AuditEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = ClientOccupationEntity.REGION)
@Table(name = "ps_client_occupation")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClientOccupationEntity extends AuditEntity {
    public static final String REGION = "clientOccupation";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String code;

    private String occupation;

    @Column(name = "occupation_in_khmer")
    private String khmerOccupation;

    @Column(name = "cbc_code")
    private String cbcCode;

}