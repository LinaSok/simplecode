package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.dminc.prasac.service.validator.constraint.group.Default;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CollateralMarketValueAnalyse implements Serializable {
    private static final long serialVersionUID = -6141203050766652212L;

    private Long id;

    @ApiModelProperty(required = true)
    private String floorNo;

    @ApiModelProperty(required = true)
    private Double length;

    @ApiModelProperty(required = true)
    private Double size;

    @ApiModelProperty(required = true)
    private Double width;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long marketValueAnalyseDescription;

    @ApiModelProperty(required = true)
    private Double landPrice;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double marketValue;

}
