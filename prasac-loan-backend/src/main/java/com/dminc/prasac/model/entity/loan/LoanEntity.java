package com.dminc.prasac.model.entity.loan;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dminc.prasac.model.misc.FlexCubeIntegrationStatus;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;

import com.dminc.prasac.model.entity.BranchEntity;
import com.dminc.prasac.model.entity.ClientBusinessEntity;
import com.dminc.prasac.model.entity.ClientLiabilityCreditEntity;
import com.dminc.prasac.model.entity.FinancialStatementBalanceEntity;
import com.dminc.prasac.model.entity.FinancialStatementGuarantorEntity;
import com.dminc.prasac.model.entity.FinancialStatementIncomeEntity;
import com.dminc.prasac.model.entity.LoanCollateralEntity;
import com.dminc.prasac.model.entity.LoanProjectEntity;
import com.dminc.prasac.model.entity.LoanRequestTypeEntity;
import com.dminc.prasac.model.entity.RepaymentModeEntity;
import com.dminc.prasac.model.entity.SocialEnvironmentImpactAssessmentEntity;
import com.dminc.prasac.model.entity.User;
import com.dminc.prasac.model.entity.UserRole;
import com.dminc.prasac.model.entity.client.ClientEntity;
import com.dminc.prasac.model.entity.selection.CurrencyEntity;
import com.dminc.prasac.model.misc.AuditEntity;
import com.dminc.prasac.model.misc.LocalConstants;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = LoanEntity.REGION)
@Table(name = LoanEntity.TABLE_NAME)
@EntityListeners(LoanEntityListener.class)
@SuppressWarnings({"PMD.TooManyFields", "CPD-START", "PMD.FieldDeclarationsShouldBeAtStartOfClass"})
public class LoanEntity extends AuditEntity {

    public static final String REGION = "loan";
    public static final String LOAN_ID = "loan_id";
    public static final String TABLE_NAME = "ps_loan";

    @Id
    @GenericGenerator(name = REGION, strategy = LocalConstants.ID_GENERATOR)
    @GeneratedValue(generator = REGION)
    private Long id;

    private Integer cycle;

    @Column(name = "loan_amount")
    private Double loanAmount;

    @Column(name = "resubmit_counter")
    private Integer resubmitCounter;

    @Column(name = "grace_period")
    private Integer gracePeriod;

    @Column(name = "loan_fee")
    private Double loanFee;

    @Column(name = "lock_in_period")
    private Integer lockInPeriod;

    @Column(name = "monthly_interest_rate")
    private Double monthlyInterestRate;

    @Column(name = "refinancing_fee")
    private Double refinancingFee;

    @Column(name = "admin_fee")
    private Double adminFee;

    @Column(name = "registration_fee")
    private Double registrationFee;

    @Column(name = "paid_off_fee")
    private Double paidOffFee;

    private Integer tenure;

    @Type(type = "text")
    private String conclusion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "currency_id")
    private CurrencyEntity currency;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "status_id")
    private LoanStatusEntity loanStatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "assigned_user_id")
    private User assignedUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "assigned_branch_id")
    private BranchEntity branch;

    @Column(name = "pre_screening_fail_reason")
    @Type(type = "text")
    private String preScreeningFailReason;

    @Column(name = "loan_number")
    private String loanNumber; // flexcube loan number

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "loan_request_type_id")
    private LoanRequestTypeEntity loanRequestType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "repayment_mode_id")
    private RepaymentModeEntity repaymentMode;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "financial_statement_balance_id")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = FinancialStatementBalanceEntity.REGION)

    @Cascade({CascadeType.ALL})
    private FinancialStatementBalanceEntity financialStatementBalance;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "social_environment_impact_assessment_id")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = SocialEnvironmentImpactAssessmentEntity.REGION)

    @Cascade({CascadeType.ALL})
    private SocialEnvironmentImpactAssessmentEntity socialEnvironmentImpactAssessment;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "financial_statement_income_id")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = FinancialStatementIncomeEntity.REGION)

    @Cascade({CascadeType.ALL})
    private FinancialStatementIncomeEntity financialStatementIncome;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "financial_statement_guarantor_id")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = FinancialStatementGuarantorEntity.REGION)

    @Cascade({CascadeType.ALL})
    private FinancialStatementGuarantorEntity financialStatementGuarantor;

    @OneToMany(mappedBy = REGION, orphanRemoval = true)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = ClientBusinessEntity.REGION)
    @LazyCollection(LazyCollectionOption.TRUE)
    @Cascade({CascadeType.ALL})
    @Setter(AccessLevel.NONE)
    private List<ClientBusinessEntity> businesses;

    public void setBusinesses(List<ClientBusinessEntity> businesses) {
        businesses.forEach(entity -> entity.setLoan(this));
        this.businesses = businesses;
    }

    @OneToMany(mappedBy = REGION, orphanRemoval = true)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = LoanProjectEntity.REGION)
    @LazyCollection(LazyCollectionOption.TRUE)
    @Cascade({CascadeType.ALL})
    @Setter(AccessLevel.NONE)
    private List<LoanProjectEntity> projects;

    public void setProjects(List<LoanProjectEntity> projects) {
        projects.forEach(entity -> entity.setLoan(this));
        this.projects = projects;
    }

    @OneToMany(mappedBy = REGION, orphanRemoval = true)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = LoanCollateralEntity.REGION)
    @LazyCollection(LazyCollectionOption.TRUE)
    @Cascade({CascadeType.ALL})
    @Setter(AccessLevel.NONE)
    private List<LoanCollateralEntity> collateralList;

    public void setCollateralList(List<LoanCollateralEntity> collateralList) {
        collateralList.forEach(entity -> entity.setLoan(this));
        this.collateralList = collateralList;
    }

    @OneToMany(mappedBy = REGION, orphanRemoval = true)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = ClientLiabilityCreditEntity.REGION)
    @LazyCollection(LazyCollectionOption.TRUE)
    @Cascade({CascadeType.ALL})
    @Setter(AccessLevel.NONE)
    private List<ClientLiabilityCreditEntity> liabilityCredits;

    public void setLiabilityCredits(List<ClientLiabilityCreditEntity> liabilityCredits) {
        liabilityCredits.forEach(entity -> entity.setLoan(this));
        this.liabilityCredits = liabilityCredits;
    }

    @ManyToMany(mappedBy = "loans")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = ClientEntity.REGION)
    @LazyCollection(LazyCollectionOption.TRUE)
    @Cascade({CascadeType.REFRESH, CascadeType.DETACH})
    private List<ClientEntity> clients;

    @Column(name = "committee_level")
    private Integer committeeLevel;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "assigned_user_group_id")
    private UserRole userRole;

    @OneToMany(mappedBy = REGION)
    @Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = ClientLoan.REGION)
    @LazyCollection(LazyCollectionOption.TRUE)
    @Cascade({CascadeType.REFRESH, CascadeType.DETACH})
    private List<ClientLoan> clientLoans;

    @OneToMany(mappedBy = REGION)
    @LazyCollection(LazyCollectionOption.TRUE)
    private List<CommentEntity> comments;

    @Column(name = "requested_date")
    private LocalDateTime requestDate;

    @Column(name = "integration_status")
    @Enumerated(EnumType.STRING)
    private FlexCubeIntegrationStatus integrationStatus;
}