package com.dminc.prasac.model.misc;

import java.util.Arrays;

public enum ClientDocumentClassification {
    DOCUMENTS(18L), PICTURES(19L);

    private final Long value;

    ClientDocumentClassification(Long value) {
        this.value = value;
    }

    public Long getValue() {
        return this.value;
    }

    public static boolean contains(Long classification) {
        return Arrays.stream(ClientDocumentClassification.values()).anyMatch(c -> c.getValue().equals(classification));
    }

}
