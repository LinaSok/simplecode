package com.dminc.prasac.model.entity.client;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.dminc.prasac.model.dto.misc.Gender;
import com.dminc.prasac.model.entity.BranchEntity;
import com.dminc.prasac.model.entity.ClientOccupationEntity;
import com.dminc.prasac.model.entity.ClientRelationshipEntity;
import com.dminc.prasac.model.entity.ClientResidentStatusEntity;
import com.dminc.prasac.model.entity.ClientWorkingTimeEntity;
import com.dminc.prasac.model.entity.MaritalStatusEntity;
import com.dminc.prasac.model.entity.loan.ClientLoan;
import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.entity.selection.NationalityEntity;
import com.dminc.prasac.model.entity.selection.VillageEntity;
import com.dminc.prasac.model.misc.AuditEntity;
import com.dminc.prasac.model.misc.LocalConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = ClientEntity.REGION)
@Table(name = "ps_client")
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings({"PMD.TooManyFields", "PMD.FieldDeclarationsShouldBeAtStartOfClass"})
@Builder
public class ClientEntity extends AuditEntity {
    public static final String REGION = "client";
    public static final String CLIENT_ID = "client_id";

    @Id
    @GenericGenerator(name = REGION, strategy = LocalConstants.ID_GENERATOR)
    @GeneratedValue(generator = REGION)
    private Long id;

    private String cif;

    @Column(name = "current_address_group_no")
    private Integer currentAddressGroupNo;

    @Column(name = "current_address_house_no")
    private String currentAddressHouseNo;

    @Column(name = "current_address_street")
    private String currentAddressStreet;

    private LocalDate dob;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "income_amount")
    private Double incomeAmount;

    @Column(name = "khmer_full_name")
    private String khmerFullName;

    @Column(name = "no_of_active_children")
    private Integer noOfActiveChildren;

    @Column(name = "no_of_children")
    private Integer noOfChildren;

    @Column(name = "no_of_children_age_greater_than_18")
    private Integer noOfChildrenAgeGreaterThan18;

    @Column(name = "period_in_current_address")
    private Double periodInCurrentAddress;

    private String phone;

    @Column(name = "short_name")
    private String shortName;

    @Column(name = "working_period")
    private Double workingPeriod;

    @Column(name = "workplace_group_no")
    private Integer workplaceGroupNo;

    @Column(name = "workplace_house_no")
    private String workplaceHouseNo;

    @Column(name = "workplace_name")
    private String workplaceName;

    @Column(name = "workplace_street")
    private String workplaceStreet;

    @Column(name = "liability_no")
    private String liabilityNo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "occupation_id")
    private ClientOccupationEntity clientOccupation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_relationship_id")
    private ClientRelationshipEntity clientRelationship;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "resident_status_id")
    private ClientResidentStatusEntity clientResidentStatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "working_time_id")
    private ClientWorkingTimeEntity clientWorkingTime;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "marital_status_id")
    private MaritalStatusEntity maritalStatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "nationality_id")
    private NationalityEntity nationality;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "birth_village_id")
    private VillageEntity birthVillage;

    @Column(name = "saving_account_number")
    private String savingAccountNumber;

    @Column(name = "collateral_pool_code")
    private String collateralPoolCode;

    @Transient
    private Long birthCommune;

    @Transient
    private Long birthDistrict;

    @Transient
    private Long birthProvince;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "current_address_village_id")
    private VillageEntity currentVillage;

    @Transient
    private Long clientType;

    @Transient
    private Long currentCommune;

    @Transient
    private Long currentDistrict;

    @Transient
    private Long currentProvince;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "workplace_village_id")
    private VillageEntity workplaceVillage;

    @ManyToOne
    @JoinColumn(name = "branch_id")
    private BranchEntity branch;

    @Transient
    private Long workplaceCommune;

    @Transient
    private Long workplaceDistrict;

    @Transient
    private Long workplaceProvince;

    @OneToMany(mappedBy = REGION, orphanRemoval = true)
    @Cascade({CascadeType.ALL})
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = ClientIdentificationEntity.REGION)
    @LazyCollection(LazyCollectionOption.TRUE)
    private List<ClientIdentificationEntity> identifications;

    public void setIdentifications(List<ClientIdentificationEntity> identifications) {
        identifications.forEach(entity -> entity.setClient(this));
        this.identifications = identifications;
    }

    @ManyToMany
    @JoinTable(//
            name = ClientLoan.TABLE_PS_CLIENT_LOAN, //
            joinColumns = {@JoinColumn(name = CLIENT_ID)}, //
            inverseJoinColumns = {@JoinColumn(name = LoanEntity.LOAN_ID)}, //
            uniqueConstraints = @UniqueConstraint(columnNames = {CLIENT_ID, LoanEntity.LOAN_ID}))
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = LoanEntity.REGION)
    @LazyCollection(LazyCollectionOption.TRUE)
    @Cascade({CascadeType.ALL})
    private List<LoanEntity> loans;

    @OneToMany(mappedBy = REGION)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = ClientLoan.REGION)
    @LazyCollection(LazyCollectionOption.TRUE)
    @Cascade({CascadeType.REFRESH, CascadeType.DETACH})
    private List<ClientLoan> clientLoans;

}