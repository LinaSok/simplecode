package com.dminc.prasac.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.entity.selection.VillageEntity;
import com.dminc.prasac.model.misc.AuditEntity;
import com.dminc.prasac.model.misc.LocalConstants;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = LoanCollateralEntity.REGION)
@Table(name = "ps_loan_collateral")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@SuppressWarnings({"PMD.TooManyFields", "PMD.FieldDeclarationsShouldBeAtStartOfClass"})
public class LoanCollateralEntity extends AuditEntity {
    public static final String REGION = "loanCollateral";
    public static final String COLLATERAL_ID = "collateral_id";

    @Id
    @GenericGenerator(name = REGION, strategy = LocalConstants.ID_GENERATOR)
    @GeneratedValue(generator = REGION)
    private Long id;

    @Column(name = "collateral_group_no")
    private Integer collateralGroupNo;

    @Column(name = "house_number")
    private String houseNumber;

    private String latitude;

    private String longitude;

    private String east;

    private String north;

    private String south;

    private String west;

    @Column(name = "property_owner")
    private String propertyOwner;

    private String street;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "village_id")
    private VillageEntity village;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "loan_id")
    private LoanEntity loan;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "collateral_area_id")
    private CollateralAreaEntity collateralArea;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "collateral_description_id")
    private CollateralDescriptionEntity collateralDescription;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "collateral_registration_id")
    private CollateralRegistrationEntity collateralRegistration;

    @Column(name = "collateral_title_deed_no")
    private String collateralTitleDeedNo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "collateral_title_type_id")
    private CollateralTitleTypeEntity collateralTitleType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "property_type_id")
    private PropertyTypeEntity propertyType;

    @OneToMany(mappedBy = REGION, orphanRemoval = true)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = CollateralMarketValueAnalyseEntity.REGION)
    @LazyCollection(LazyCollectionOption.TRUE)
    @Cascade({CascadeType.ALL})
    @Setter(AccessLevel.NONE)
    private List<CollateralMarketValueAnalyseEntity> marketValueAnalyses;

    @Column(name = "uuid")
    private String uuid;

    public void setMarketValueAnalyses(List<CollateralMarketValueAnalyseEntity> markets) {
        markets.forEach(entity -> entity.setLoanCollateral(this));
        this.marketValueAnalyses = markets;
    }

}