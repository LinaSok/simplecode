package com.dminc.prasac.model.entity;

public enum ApprovalState {
    SUSPENDED, APPROVED, REJECTED
}
