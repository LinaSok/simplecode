package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;
import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.dminc.common.tools.Constants;
import com.dminc.prasac.service.validator.constraint.group.Default;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("PMD.TooManyFields")
public class FinancialStatementIncomeForecast implements Serializable {
    private static final long serialVersionUID = 2496294002517846493L;

    private Long id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @NotNull(groups = Default.class)
    private LocalDate startDate;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double currentMonth;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double eighth;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double eleventh;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double fifth;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double first;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double fourth;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double ninth;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double second;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double seventh;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double sixth;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double tenth;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double third;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double twelfth;

    private Double total;

    private Double average;

}
