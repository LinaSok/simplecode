package com.dminc.prasac.model.dto.loan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClientRequest implements Serializable {
    private static final long serialVersionUID = -5225283469879534380L;

    private Long clientId;
    private String clientCif;
}
