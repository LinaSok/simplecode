package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BusinessOwner implements Serializable {

    private static final long serialVersionUID = 3004101745890115418L;
    private Long id;

    private String name;

    private String khmerName;

    private String code;
}