package com.dminc.prasac.model.dto.misc;

import javax.validation.constraints.NotNull;

import com.dminc.prasac.service.validator.constraint.group.Default;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FinancialStatementIncomeRevenue extends FinancialStatementIncomeForecast {

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double costOfGoodsSold;

}
