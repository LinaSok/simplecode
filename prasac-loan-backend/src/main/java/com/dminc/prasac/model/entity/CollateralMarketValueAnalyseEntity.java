package com.dminc.prasac.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import com.dminc.prasac.model.misc.AuditEntity;
import com.dminc.prasac.model.misc.LocalConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = CollateralMarketValueAnalyseEntity.REGION)
@Table(name = "ps_collateral_market_value_analyse")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CollateralMarketValueAnalyseEntity extends AuditEntity {
    public static final String REGION = "collateralMarketValueAnalyse";

    @Id
    @GenericGenerator(name = REGION, strategy = LocalConstants.ID_GENERATOR)
    @GeneratedValue(generator = REGION)
    private Long id;

    @Column(name = "floor_no")
    private String floorNo;

    @Column(name = "land_price")
    private Double landPrice;

    private Double length;

    private Double size;

    private Double width;

    @Column(name = "market_value")
    private Double marketValue;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "collateral_id")
    private LoanCollateralEntity loanCollateral;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "description_id")
    private MarketValueAnalyseDescriptionEntity marketValueAnalyseDescription;
}