package com.dminc.prasac.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.dminc.prasac.model.misc.AuditEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Builder
@Entity
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = DocumentTypeEntity.REGION)
@Table(name = "ps_document_type")

public class DocumentTypeEntity extends AuditEntity {
    public static final String REGION = "documentType";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "document_classification_type_id")
    private DocumentClassificationTypeEntity documentClassificationType;

    @Column(name = "type_in_khmer")
    private String khmerType;

}