package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.dminc.prasac.service.validator.constraint.group.Default;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SocialEnvironmentImpactAssessment implements Serializable {
    private static final long serialVersionUID = 7401669716456659811L;

    private Long id;

    @ApiModelProperty(required = true)
    @NotBlank(groups = Default.class)
    private String adviceConsultation;

    @ApiModelProperty(required = true)
    @NotBlank(groups = Default.class)
    private String conversationWithBorrower;

    @ApiModelProperty(required = true)
    @NotBlank(groups = Default.class)
    private String customerImprovementNotice;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long businessActivity;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long energy;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long healthSafety;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long laborForce;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long naturalResource;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long productWaste;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long waterPollution;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long airPollution;

    @ApiModelProperty(hidden = true)
    private Long loan;
}