package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientBusinessProgress implements Serializable {

    private static final long serialVersionUID = -7848228902712988951L;
    private Long id;

    private String progress;

    private String khmerProgress;

    private String code;
}
