package com.dminc.prasac.model.entity;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(User.class)
public final class User_ {
    public static volatile ListAttribute<User, UserRole> roles;
    public static volatile SingularAttribute<User, Boolean> enabled;

    private User_() {
        // empty
    }
}
