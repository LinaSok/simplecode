package com.dminc.prasac.model.entity;

import com.dminc.prasac.model.misc.AuditEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Builder
@Table(name = "ps_user_permission")
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class UserPermission extends AuditEntity implements GrantedAuthority, Serializable {

    private static final long serialVersionUID = -1669358881681063697L;

    public static final String USER_PERMISSION_ID = "user_permission_id";

    @Id
    @GenericGenerator(name = USER_PERMISSION_ID, strategy = "increment")
    @GeneratedValue(generator = USER_PERMISSION_ID)
    @Column(name = USER_PERMISSION_ID)
    private Long id;

    @Column(name = "permission_name")
    private String name;

    @Column(name = "display_name")
    private String displayName;

    @Override
    public String getAuthority() {
        return name;
    }
}
