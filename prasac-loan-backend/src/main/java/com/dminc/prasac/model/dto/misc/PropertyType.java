package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PropertyType implements Serializable {

    private static final long serialVersionUID = -4446311165572083995L;
    private Long id;

    private String type;

    private String khmerType;

    private String code;
}