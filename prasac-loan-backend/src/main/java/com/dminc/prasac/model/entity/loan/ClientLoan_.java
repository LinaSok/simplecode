package com.dminc.prasac.model.entity.loan;

import com.dminc.prasac.model.entity.selection.ClientTypeEntity;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(ClientLoan.class)
public final class ClientLoan_ {
    public static volatile SingularAttribute<ClientLoan, ClientTypeEntity> clientType;

    private ClientLoan_() {
        // empty
    }
}
