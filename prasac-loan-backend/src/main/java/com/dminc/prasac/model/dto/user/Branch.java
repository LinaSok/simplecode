package com.dminc.prasac.model.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Branch implements Serializable {
    private static final long serialVersionUID = 8532151212151150206L;
    private Long id;
    private String code;
    private String name;
}
