package com.dminc.prasac.model.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.dminc.prasac.model.misc.AuditEntity;
import com.dminc.prasac.model.misc.LocalConstants;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = FinancialStatementGuarantorEntity.REGION)
@Table(name = "ps_financial_statement_guarantor")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FinancialStatementGuarantorEntity extends AuditEntity {
    public static final String REGION = "financialStatementGuarantor";

    @Id
    @GenericGenerator(name = REGION, strategy = LocalConstants.ID_GENERATOR)
    @GeneratedValue(generator = REGION)
    private Long id;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "guarantor_expense_id")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = FinancialStatementGuarantorExpenseEntity.REGION)
    @Cascade({CascadeType.ALL})
    private FinancialStatementGuarantorExpenseEntity financialStatementGuarantorExpense;

    @OneToMany(mappedBy = REGION, orphanRemoval = true)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = FinancialStatementGuarantorEmploymentEntity.REGION)
    @LazyCollection(LazyCollectionOption.TRUE)
    @Cascade({CascadeType.ALL})
    @Setter(AccessLevel.NONE)
    private List<FinancialStatementGuarantorEmploymentEntity> financialStatementGuarantorEmployments;

    public void setFinancialStatementGuarantorEmployments(List<FinancialStatementGuarantorEmploymentEntity> employments) {
        employments.forEach(entity -> entity.setFinancialStatementGuarantor(this));
        this.financialStatementGuarantorEmployments = employments;
    }

}
