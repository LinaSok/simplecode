package com.dminc.prasac.model.dto.pdf;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PDF implements Serializable {
    private String url;
    private String description;
}
