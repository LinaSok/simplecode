package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.dminc.prasac.service.validator.constraint.group.Default;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanProject implements Serializable {
    private static final long serialVersionUID = -782457988278834283L;

    private Long id;

    private String uuid;

    @ApiModelProperty(required = true)
    @NotBlank(groups = Default.class)
    @Size(groups = Default.class, max = 200)
    private String loanUtilizationProject;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long loanPurpose;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Integer unit;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double pricePerUnit;

    @ApiModelProperty(hidden = true)
    private Long loan;

}