package com.dminc.prasac.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import com.dminc.prasac.model.misc.AuditEntity;
import com.dminc.prasac.model.misc.LocalConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = FinancialStatementGuarantorExpenseEntity.REGION)
@Table(name = "ps_financial_statement_guarantor_expense")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FinancialStatementGuarantorExpenseEntity extends AuditEntity {
    public static final String REGION = "financialStatementGuarantorExpense";

    @Id
    @GenericGenerator(name = REGION, strategy = LocalConstants.ID_GENERATOR)
    @GeneratedValue(generator = REGION)
    private Long id;

    @Column(name = "utility_expense")
    private Double utilityExpense;

    @Column(name = "family_expense")
    private Double familyExpense;

    @Column(name = "other_expense")
    private Double otherExpense;

    @OneToOne(mappedBy = REGION)
    private FinancialStatementGuarantorEntity financialStatementGuarantor;
}
