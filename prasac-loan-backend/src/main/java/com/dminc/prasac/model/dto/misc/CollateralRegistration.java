package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CollateralRegistration implements Serializable {

    private static final long serialVersionUID = -9184409324442467732L;
    private Long id;

    private String registration;

    private String khmerRegistration;

    private String code;
}
