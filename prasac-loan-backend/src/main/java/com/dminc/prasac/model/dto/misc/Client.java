package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.dminc.common.tools.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings({"PMD.TooManyFields", "PMD.ImmutableField"})
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
public class Client implements Serializable {

    private static final long serialVersionUID = -7165432802540815110L;
    private Long id;

    private String cif;

    private Long clientType;

    private Integer currentAddressGroupNo;

    private String currentAddressHouseNo;

    private String currentAddressStreet;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate dob;

    private String fullName;

    private Double incomeAmount;

    private String khmerFullName;

    private Integer noOfActiveChildren;

    private Integer noOfChildren;

    private Integer noOfChildrenAgeGreaterThan18;

    private Double periodInCurrentAddress;

    private String phone;

    private Double workingPeriod;

    private Integer workplaceGroupNo;

    private String workplaceHouseNo;

    private String workplaceName;

    private String workplaceStreet;

    private Long clientOccupation;

    private Long clientRelationship;

    private Long clientResidentStatus;

    private Long clientWorkingTime;

    private Gender gender;

    private Long maritalStatus;

    private Long nationality;

    private Long birthVillage;

    private Long birthCommune;

    private Long birthDistrict;

    private Long birthProvince;

    private Long currentVillage;

    private Long currentCommune;

    private Long currentDistrict;

    private Long currentProvince;

    private Long workplaceVillage;

    private Long workplaceCommune;

    private Long workplaceDistrict;

    private Long workplaceProvince;

    private List<ClientIdentification> identifications;

    private List<Long> loans;

    private String shortName;

    private List<Document> documents;

    public Client(String cif, String fullName, String khmerFullName, String phone, Gender gender, LocalDate dob, String shortName) {
        this.cif = cif;
        this.fullName = fullName;
        this.khmerFullName = khmerFullName;
        this.phone = phone;
        this.dob = dob;
        this.gender = gender;
        this.shortName = shortName;
    }
}
