package com.dminc.prasac.model.dto.loan;

public enum SortingField {
    REQUESTED_DATE, FULL_NAME
}
