package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BusinessExperience implements Serializable {

    private static final long serialVersionUID = -2902504520810805936L;
    private Long id;

    private String experience;

    private String khmerExperience;
    private String code;
}
