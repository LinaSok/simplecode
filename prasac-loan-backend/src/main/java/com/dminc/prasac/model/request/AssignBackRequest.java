package com.dminc.prasac.model.request;

import lombok.Data;

@Data
public class AssignBackRequest {

    private String comment;
}
