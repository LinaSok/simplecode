package com.dminc.prasac.model.dto.selection;

import java.io.Serializable;
import java.util.List;

import com.dminc.prasac.model.dto.misc.AirPollution;
import com.dminc.prasac.model.dto.misc.BusinessActivity;
import com.dminc.prasac.model.dto.misc.BusinessExperience;
import com.dminc.prasac.model.dto.misc.BusinessOwner;
import com.dminc.prasac.model.dto.misc.ClientBusinessCashFlow;
import com.dminc.prasac.model.dto.misc.ClientBusinessLocationStatus;
import com.dminc.prasac.model.dto.misc.ClientBusinessPeriod;
import com.dminc.prasac.model.dto.misc.ClientBusinessProgress;
import com.dminc.prasac.model.dto.misc.ClientIdentificationType;
import com.dminc.prasac.model.dto.misc.ClientOccupation;
import com.dminc.prasac.model.dto.misc.ClientRelationship;
import com.dminc.prasac.model.dto.misc.ClientResidentStatus;
import com.dminc.prasac.model.dto.misc.ClientType;
import com.dminc.prasac.model.dto.misc.ClientWorkingTime;
import com.dminc.prasac.model.dto.misc.CollateralArea;
import com.dminc.prasac.model.dto.misc.CollateralDescription;
import com.dminc.prasac.model.dto.misc.CollateralRegistration;
import com.dminc.prasac.model.dto.misc.CollateralTitleType;
import com.dminc.prasac.model.dto.misc.Currency;
import com.dminc.prasac.model.dto.misc.DocumentClassificationType;
import com.dminc.prasac.model.dto.misc.DocumentType;
import com.dminc.prasac.model.dto.misc.Energy;
import com.dminc.prasac.model.dto.misc.HealthSafety;
import com.dminc.prasac.model.dto.misc.LaborForce;
import com.dminc.prasac.model.dto.misc.LicenseContractValidity;
import com.dminc.prasac.model.dto.misc.LoanPurpose;
import com.dminc.prasac.model.dto.misc.LoanStatus;
import com.dminc.prasac.model.dto.misc.LoanRequestType;
import com.dminc.prasac.model.dto.misc.LoanType;
import com.dminc.prasac.model.dto.misc.MaritalStatus;
import com.dminc.prasac.model.dto.misc.MarketValueAnalyseDescription;
import com.dminc.prasac.model.dto.misc.Nationality;
import com.dminc.prasac.model.dto.misc.NaturalResource;
import com.dminc.prasac.model.dto.misc.ProductWaste;
import com.dminc.prasac.model.dto.misc.PropertyType;
import com.dminc.prasac.model.dto.misc.RepaymentMode;
import com.dminc.prasac.model.dto.misc.WaterPollution;
import com.dminc.prasac.model.dto.user.Branch;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@SuppressWarnings("PMD.TooManyFields")
public class Selection implements Serializable {
    private static final long serialVersionUID = 1L;

    private List<CollateralTitleType> collateralTitleTypes;
    private List<Currency> currencies;
    private List<RepaymentMode> repaymentModes;
    private List<ClientRelationship> clientRelationships;
    private List<ClientResidentStatus> clientResidentStatuses;
    private List<ClientType> clientTypes;
    private List<ClientWorkingTime> clientWorkingTimes;
    private List<MaritalStatus> maritalStatuses;
    private List<Nationality> nationalities;
    private List<ClientOccupation> clientOccupations;
    private List<ClientIdentificationType> clientIdentificationTypes;
    private List<DocumentType> documentTypes;
    private List<DocumentClassificationType> documentClassificationTypes;
    private List<LoanStatus> loanStatuses;

    private List<BusinessActivity> businessActivities;
    private List<BusinessExperience> businessExperiences;
    private List<ClientBusinessCashFlow> clientBusinessCashFlows;
    private List<Branch> branches;
    private List<ClientBusinessLocationStatus> clientBusinessLocationStatuses;
    private List<ClientBusinessPeriod> clientBusinessPeriods;
    private List<ClientBusinessProgress> clientBusinessProgresses;
    private List<CollateralArea> collateralAreas;
    private List<CollateralDescription> collateralDescriptions;
    private List<CollateralRegistration> collateralRegistrations;
    private List<Energy> energies;
    private List<HealthSafety> healthSafeties;
    private List<LaborForce> laborForces;
    private List<LicenseContractValidity> licenseContractValidities;
    private List<LoanPurpose> loanPurposes;
    private List<LoanRequestType> loanRequestTypes;
    private List<MarketValueAnalyseDescription> marketValueAnalyseDescriptions;
    private List<NaturalResource> naturalResources;
    private List<ProductWaste> productWastes;
    private List<PropertyType> propertyTypes;
    private List<WaterPollution> waterPollutions;
    private List<AirPollution> airPollutions;
    private List<BusinessOwner> businessOwners;
    private List<LoanType> loanTypes;

}
