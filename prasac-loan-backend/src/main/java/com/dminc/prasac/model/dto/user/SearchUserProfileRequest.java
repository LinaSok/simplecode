package com.dminc.prasac.model.dto.user;

import com.dminc.prasac.model.dto.loan.SortingField;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Sort;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SearchUserProfileRequest {
    private String staffIdOrfullName;
    private Long branchId;
    private Long roleId;
    private Boolean isEnabled;
    private SortingField sortingField;
    private Sort.Direction direction;
}