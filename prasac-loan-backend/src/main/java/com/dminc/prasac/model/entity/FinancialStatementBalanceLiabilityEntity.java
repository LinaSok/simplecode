package com.dminc.prasac.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import com.dminc.prasac.model.misc.AuditEntity;
import com.dminc.prasac.model.misc.LocalConstants;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = FinancialStatementBalanceLiabilityEntity.REGION)
@Table(name = "ps_financial_statement_balance_liability")
@AllArgsConstructor
@NoArgsConstructor
public class FinancialStatementBalanceLiabilityEntity extends AuditEntity {
    public static final String REGION = "financialStatementBalanceLiability";

    @Id
    @GenericGenerator(name = REGION, strategy = LocalConstants.ID_GENERATOR)
    @GeneratedValue(generator = REGION)
    private Long id;

    @Column(name = "capital_investment_to_loan_utilization_project")
    private Double capitalInvestmentToLoanUtilizationProject;

    @Column(name = "current_year_account_payable")
    private Double currentYearAccountPayable;

    @Column(name = "current_year_advance_deposit")
    private Double currentYearAdvanceDeposit;

    @Column(name = "current_year_informal_money_lenders")
    private Double currentYearInformalMoneyLenders;

    @Column(name = "current_year_short_term_loan")
    private Double currentYearShortTermLoan;

    @Column(name = "forecast_year_account_payable")
    private Double forecastYearAccountPayable;

    @Column(name = "forecast_year_advance_deposit")
    private Double forecastYearAdvanceDeposit;

    @Column(name = "forecast_year_informal_money_lenders")
    private Double forecastYearInformalMoneyLenders;

    @Column(name = "forecast_year_short_term_loan")
    private Double forecastYearShortTermLoan;

    @OneToOne(mappedBy = REGION)
    private FinancialStatementBalanceEntity financialStatementBalance;
}