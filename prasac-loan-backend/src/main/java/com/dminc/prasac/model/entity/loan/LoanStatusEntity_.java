package com.dminc.prasac.model.entity.loan;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(LoanStatusEntity.class)
public final class LoanStatusEntity_ {

    public static volatile SingularAttribute<LoanStatusEntity, Long> id;

    private LoanStatusEntity_() {
        //empty
    }
}
