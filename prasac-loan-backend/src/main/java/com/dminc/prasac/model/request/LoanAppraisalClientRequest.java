package com.dminc.prasac.model.request;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.dminc.common.tools.Constants;
import com.dminc.prasac.model.dto.misc.Gender;
import com.dminc.prasac.service.validator.constraint.group.Default;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("PMD.TooManyFields")
@EqualsAndHashCode(callSuper = true)
public class LoanAppraisalClientRequest extends ClientRequest {

    private Integer currentAddressGroupNo;

    @Size(groups = Default.class, max = 15)
    private String currentAddressHouseNo;

    @Size(groups = Default.class, max = 45)
    private String currentAddressStreet;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate dob;

    @ApiModelProperty(required = true)
    @NotBlank(groups = Default.class)
    @Size(groups = Default.class, max = 70)
    private String fullName;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double incomeAmount;

    @ApiModelProperty(required = true)
    @NotBlank(groups = Default.class)
    @Size(groups = Default.class, max = 100)
    private String khmerFullName;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Integer noOfActiveChildren;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Integer noOfChildren;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Integer noOfChildrenAgeGreaterThan18;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double periodInCurrentAddress;

    @ApiModelProperty(required = true)
    @NotBlank(groups = Default.class)
    @Size(groups = Default.class, max = 20)
    private String phone;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double workingPeriod;

    private Integer workplaceGroupNo;

    @Size(groups = Default.class, max = 15)
    private String workplaceHouseNo;

    @ApiModelProperty(required = true)
    @NotBlank(groups = Default.class)
    @Size(groups = Default.class, max = 70)
    private String workplaceName;

    @Size(groups = Default.class, max = 45)
    private String workplaceStreet;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long clientOccupation;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long clientRelationship;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long clientResidentStatus;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long clientWorkingTime;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Gender gender;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long maritalStatus;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long nationality;

    private Long birthVillage;

    private Long birthCommune;

    private Long birthDistrict;

    private Long birthProvince;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long currentVillage;

    private Long currentCommune;

    private Long currentDistrict;

    private Long currentProvince;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long workplaceVillage;

    private Long workplaceCommune;

    private Long workplaceDistrict;

    private Long workplaceProvince;

}
