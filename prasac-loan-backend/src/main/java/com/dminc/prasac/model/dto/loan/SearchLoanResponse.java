package com.dminc.prasac.model.dto.loan;

import com.dminc.prasac.model.dto.misc.Currency;
import com.dminc.prasac.model.dto.misc.LoanStatus;
import com.dminc.prasac.model.dto.user.Branch;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class SearchLoanResponse implements Serializable {

    private static final long serialVersionUID = 8716651021739481649L;

    private long loanId;
    private String clientCif;
    private String clientFullname;
    private String clientKhmerFullname;
    private Branch branch;
    private LocalDate requestDate;
    private Double loanAmount;
    private LoanStatus loanStatus;
    private Currency currency;
    private String loanStatusText;
    private LocalDate createdDate;
    private LocalDate updatedDate;
}
