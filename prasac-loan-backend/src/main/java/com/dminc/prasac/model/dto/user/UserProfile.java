package com.dminc.prasac.model.dto.user;

import com.dminc.common.tools.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserProfile implements Serializable {
    private static final long serialVersionUID = -3945098383623551792L;
    private Long id;
    private Long profileId;
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    private Position position;
    private Branch branch;
    private List<UserRole> roles;
    private String staffId;
    private String username;
    private String email;
    private Boolean isEnabled;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_MINUTE_FORMAT)
    @DateTimeFormat(pattern = Constants.DATE_MINUTE_FORMAT)
    private LocalDateTime createdDate;
    private String gender;
}
