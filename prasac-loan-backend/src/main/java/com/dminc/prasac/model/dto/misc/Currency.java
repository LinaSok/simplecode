package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Currency implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    private String currency;

    private String khmerCurrency;

    private String code;
}
