package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientBusinessLocationStatus implements Serializable {

    private static final long serialVersionUID = -6559911459631962769L;
    private Long id;

    private String status;

    private String khmerStatus;

    private String code;
}
