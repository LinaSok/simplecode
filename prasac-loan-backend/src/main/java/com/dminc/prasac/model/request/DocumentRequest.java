package com.dminc.prasac.model.request;

import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DocumentRequest {
    @ApiModelProperty(required = true)
    @NotNull
    private MultipartFile image;

    @ApiModelProperty(required = true)
    @NotNull
    private Long documentType;

}