package com.dminc.prasac.model.request;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.dminc.common.tools.Constants;
import com.dminc.prasac.model.dto.misc.Gender;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.service.validator.constraint.group.COConstraint;
import com.dminc.prasac.service.validator.constraint.group.Default;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("PMD.TooManyFields")
@EqualsAndHashCode(callSuper = true)
public class ClientPreScreenRequest extends ClientRequest {

    @ApiModelProperty(value = LocalConstants.CO_OPTIONAL_BO_OPTIONAL)
    private Integer currentAddressGroupNo;

    @ApiModelProperty(value = LocalConstants.CO_OPTIONAL_BO_OPTIONAL)
    @Size(max = 15, groups = Default.class)
    private String currentAddressHouseNo;

    @ApiModelProperty(value = LocalConstants.CO_OPTIONAL_BO_OPTIONAL)
    @Size(max = 45, groups = Default.class)
    private String currentAddressStreet;

    @ApiModelProperty(value = LocalConstants.CO_REQUIRED_BO_OPTIONAL)
    @NotNull(groups = {COConstraint.class})
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate dob;

    @ApiModelProperty(value = LocalConstants.CO_REQUIRED_BO_REQUIRED)
    @NotBlank(groups = Default.class)
    @Size(max = 70, groups = Default.class)
    private String fullName;

    @ApiModelProperty(value = LocalConstants.CO_REQUIRED_BO_REQUIRED)
    @NotBlank(groups = Default.class)
    @Size(max = 100, groups = Default.class)
    private String khmerFullName;

    @ApiModelProperty(value = LocalConstants.CO_REQUIRED_BO_REQUIRED)
    @NotBlank(groups = Default.class)
    @Size(max = 20, groups = Default.class)
    private String phone;

    @ApiModelProperty(value = LocalConstants.CO_REQUIRED_BO_REQUIRED)
    @NotNull(groups = Default.class)
    private Gender gender;

    @ApiModelProperty(value = LocalConstants.CO_REQUIRED_BO_REQUIRED)
    @NotNull(groups = Default.class)
    private Long nationality;

    @ApiModelProperty(value = LocalConstants.CO_REQUIRED_BO_OPTIONAL)
    @NotNull(groups = {COConstraint.class})
    private Long currentVillage;

    private Long currentCommune;

    private Long currentDistrict;

    private Long currentProvince;

}
