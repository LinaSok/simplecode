package com.dminc.prasac.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import com.dminc.prasac.model.misc.AuditEntity;
import com.dminc.prasac.model.misc.LocalConstants;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = FinancialStatementIncomeOperatingExpenseEntity.REGION)
@Table(name = "ps_financial_statement_income_operating_expense")
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("PMD.TooManyFields")
public class FinancialStatementIncomeOperatingExpenseEntity extends AuditEntity {
    public static final String REGION = "financialStatementIncomeOperatingExpense";

    @Id
    @GenericGenerator(name = REGION, strategy = LocalConstants.ID_GENERATOR)
    @GeneratedValue(generator = REGION)
    private Long id;

    @Column(name = "current_month_administrative_expense")
    private Double currentMonthAdministrativeExpense;

    @Column(name = "current_month_depreciation_expense")
    private Double currentMonthDepreciationExpense;

    @Column(name = "current_month_family_expense")
    private Double currentMonthFamilyExpense;

    @Column(name = "current_month_maintenance_expense")
    private Double currentMonthMaintenanceExpense;

    @Column(name = "current_month_others_expense")
    private Double currentMonthOthersExpense;

    @Column(name = "current_month_rental_expense")
    private Double currentMonthRentalExpense;

    @Column(name = "current_month_salary_expense")
    private Double currentMonthSalaryExpense;

    @Column(name = "current_month_utility_expense")
    private Double currentMonthUtilityExpense;

    @Column(name = "forecast_month_administrative_expense")
    private Double forecastMonthAdministrativeExpense;

    @Column(name = "forecast_month_depreciation_expense")
    private Double forecastMonthDepreciationExpense;

    @Column(name = "forecast_month_family_expense")
    private Double forecastMonthFamilyExpense;

    @Column(name = "forecast_month_maintenance_expense")
    private Double forecastMonthMaintenanceExpense;

    @Column(name = "forecast_month_others_expense")
    private Double forecastMonthOthersExpense;

    @Column(name = "forecast_month_rental_expense")
    private Double forecastMonthRentalExpense;

    @Column(name = "forecast_month_salary_expense")
    private Double forecastMonthSalaryExpense;

    @Column(name = "forecast_month_utility_expense")
    private Double forecastMonthUtilityExpense;

    @OneToOne(mappedBy = REGION)
    private FinancialStatementIncomeEntity financialStatementIncome;
}