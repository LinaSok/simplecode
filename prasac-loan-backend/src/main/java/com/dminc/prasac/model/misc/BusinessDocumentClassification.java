package com.dminc.prasac.model.misc;

import java.util.Arrays;

public enum BusinessDocumentClassification {
    DOCUMENTS(1L), PICTURES(2L), OTHERS(13L);

    private final Long value;

    BusinessDocumentClassification(Long value) {
        this.value = value;
    }

    public Long getValue() {
        return this.value;
    }

    public static boolean contains(Long classification) {
        return Arrays.stream(BusinessDocumentClassification.values()).anyMatch(c -> c.getValue().equals(classification));
    }

}
