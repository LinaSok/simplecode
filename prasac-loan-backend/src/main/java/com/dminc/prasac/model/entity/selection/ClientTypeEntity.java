package com.dminc.prasac.model.entity.selection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.dminc.prasac.model.misc.AuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = ClientTypeEntity.REGION)
@Table(name = "ps_client_type")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientTypeEntity extends AuditEntity {
    public static final String REGION = "clientType";
    public static final Long TYPE_BORROWER_ID = 1L;
    public static final Long TYPE_CO_BORROWER_ID = 2L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String code;

    private String type;

    @Column(name = "type_in_khmer")
    private String khmerType;

}