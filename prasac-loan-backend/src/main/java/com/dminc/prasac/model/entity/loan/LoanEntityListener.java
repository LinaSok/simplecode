package com.dminc.prasac.model.entity.loan;

import com.dminc.prasac.model.entity.ActivityLog;
import com.dminc.prasac.model.misc.LoanStatusEnum;
import com.dminc.prasac.service.BeanUtil;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;

@Slf4j
public class LoanEntityListener {

    @PostPersist
    public void performCreate(LoanEntity target) {
        doPerform(target, "create");
    }

    @PostUpdate
    public void performUpdate(LoanEntity target) {
        doPerform(target, "update");
    }

    @PostRemove
    public void performRemove(LoanEntity target) {
        doPerform(target, "remove");
    }

    private void doPerform(LoanEntity target, String actionDescription) {
        final EntityManagerFactory factory = BeanUtil.getBean(EntityManagerFactory.class);
        final EntityManager entityManager = factory.createEntityManager();
        entityManager.getTransaction().begin();

        final ActivityLog log = this.buildLog(target, actionDescription);

        entityManager.persist(log);
        entityManager.getTransaction().commit();
        entityManager.close();
    }


    private ActivityLog buildLog(LoanEntity target, String actionDescription) {
        final Long statusId = target.getLoanStatus().getId();
        final String loanAction = LoanStatusEnum.name(statusId);
        return ActivityLog.builder().entityName(target.TABLE_NAME).action(loanAction).entityId(target.getId()).description(actionDescription).build();
    }
}
