package com.dminc.prasac.model.entity;

import com.dminc.prasac.model.misc.AuditEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "ps_position")
@EqualsAndHashCode(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PositionEntity extends AuditEntity implements Serializable {

    private static final long serialVersionUID = 3727001761321241661L;

    @Id
    @GenericGenerator(name = "position_id", strategy = "increment")
    @GeneratedValue(generator = "position_id")
    @Column(name = "position_id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "position_name")
    private String name;
}
