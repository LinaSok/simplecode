package com.dminc.prasac.model.dto.misc;

import lombok.Builder;
import lombok.Data;


//Indicate whether something changed (inserted, updated, deleted)

@Data
@Builder
public class Version {

    //In CountryEntity, ProvinceEntity, DistrictEntity, CommuneEntity and VillageEntity
    private boolean address;
}
