package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssignLoanRequest implements Serializable {
    private static final long serialVersionUID = -7817650700247306909L;
    private Long userId;
    private Long branchId;
}
