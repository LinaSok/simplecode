package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.dminc.prasac.service.validator.constraint.group.Default;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FinancialStatementIncome implements Serializable {
    private static final long serialVersionUID = -3530176162255460170L;

    private Long id;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double taxExpenses;

    @Valid
    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private FinancialStatementIncomeBankPayment financialStatementIncomeBankPayment;

    @Valid
    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private FinancialStatementIncomeOperatingExpense financialStatementIncomeOperatingExpense;

}
