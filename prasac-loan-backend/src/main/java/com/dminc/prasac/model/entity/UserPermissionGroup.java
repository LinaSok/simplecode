package com.dminc.prasac.model.entity;

import com.dminc.prasac.model.misc.AuditEntity;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@Entity
@Table(name = "ps_user_permission_group")
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class UserPermissionGroup extends AuditEntity {

    private static final String USER_PERMISSION_GROUP_ID = "group_id";

    @Id
    @GenericGenerator(name = USER_PERMISSION_GROUP_ID, strategy = "increment")
    @GeneratedValue(generator = USER_PERMISSION_GROUP_ID)
    @Column(name = USER_PERMISSION_GROUP_ID, nullable = false, updatable = false)
    private Long id;

    @Column(name = "group_name")
    private String groupName;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "group_id")
    private List<UserPermission> userPermissions;
}
