package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.dminc.prasac.model.dto.address.Address;
import com.dminc.prasac.service.validator.constraint.group.Default;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("PMD.TooManyFields")
public class LoanCollateral extends Address implements Serializable {
    private static final long serialVersionUID = 292649387762360967L;

    private Long id;

    private String uuid;

    private String east;

    private String north;

    private String south;

    private String west;

    private Integer collateralGroupNo;

    @ApiModelProperty(required = true)
    private String latitude;

    @ApiModelProperty(required = true)
    private String longitude;

    private String propertyOwner;

    private String street;
    private String houseNumber;

    @ApiModelProperty(hidden = true)
    private Long loan;

    @ApiModelProperty(required = true)
    private Long collateralArea;

    @ApiModelProperty(required = true)
    private Long collateralDescription;

    @ApiModelProperty(required = true)
    private Long collateralRegistration;

    @ApiModelProperty(required = true)
    private String collateralTitleDeedNo;

    @ApiModelProperty(required = true)
    private Long collateralTitleType;

    @ApiModelProperty(required = true)
    private Long propertyType;

    @Valid
    @Size(min = 1, groups = Default.class)
    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private List<CollateralMarketValueAnalyse> marketValueAnalyses;

    private List<Document> documents;

}