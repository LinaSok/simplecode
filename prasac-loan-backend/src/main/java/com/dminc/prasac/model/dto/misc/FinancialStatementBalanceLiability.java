package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.dminc.prasac.service.validator.constraint.group.Default;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FinancialStatementBalanceLiability implements Serializable {
    private static final long serialVersionUID = -2455001556262888067L;

    private Long id;

    private Double currentYear;

    private Double forecastYear;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double capitalInvestmentToLoanUtilizationProject;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double currentYearAccountPayable;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double currentYearAdvanceDeposit;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double currentYearInformalMoneyLenders;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double currentYearShortTermLoan;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double forecastYearAccountPayable;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double forecastYearAdvanceDeposit;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double forecastYearInformalMoneyLenders;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double forecastYearShortTermLoan;

}
