package com.dminc.prasac.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.dminc.prasac.model.misc.AuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = MarketValueAnalyseDescriptionEntity.REGION)
@Table(name = "ps_market_value_analyse_description")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MarketValueAnalyseDescriptionEntity extends AuditEntity {
    public static final String REGION = "marketValueAnalyseDescription";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String description;

    private String code;

    /**
     * 1 - collateral, 2- other assets
     */
    private Long type;

}