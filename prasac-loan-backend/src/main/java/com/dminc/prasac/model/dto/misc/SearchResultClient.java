package com.dminc.prasac.model.dto.misc;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuppressWarnings({"PMD.ImmutableField"})
public class SearchResultClient extends Client {

    private static final long serialVersionUID = 7412551217900947013L;
    private String idNumber;
    private String address;
    private String branch;
    private String idType;
    private String rawMaritalStatus;

    @SuppressWarnings({"PMD.ExcessiveMethodLength", "PMD.ExcessiveParameterList"})
    @Builder
    public SearchResultClient(String cif, String fullName, String khmerFullName, String phone, LocalDate dob, Gender gender, String idNumber, String address, String branch, String idType, String rawMaritalStatus, String shortName) {
        super(cif, fullName, khmerFullName, phone, gender, dob, shortName);
        this.idNumber = idNumber;
        this.address = address;
        this.branch = branch;
        this.idType = idType;
        this.rawMaritalStatus = rawMaritalStatus;
    }
}
