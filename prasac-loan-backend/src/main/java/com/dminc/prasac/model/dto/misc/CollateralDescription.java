package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CollateralDescription implements Serializable {
    public static final Long COLLATERAL_DESC = 1L;
    public static final Long OTHER_ASSETS_DESC = 2L;

    private static final long serialVersionUID = -7941446665622824566L;
    private Long id;

    private String description;

    private String khmerDescription;

    private String code;
}
