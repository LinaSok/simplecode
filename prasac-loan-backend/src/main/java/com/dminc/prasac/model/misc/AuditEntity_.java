package com.dminc.prasac.model.misc;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.time.LocalDateTime;

@StaticMetamodel(AuditEntity.class)
public final class AuditEntity_ {
    public static volatile SingularAttribute<AuditEntity, LocalDateTime> createdDate;

    private AuditEntity_() {
        // empty
    }
}
