package com.dminc.prasac.model.entity.loan;

import com.dminc.prasac.model.entity.BranchEntity;
import com.dminc.prasac.model.entity.client.ClientEntity;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(LoanEntity.class)
public final class LoanEntity_ {

    public static volatile SingularAttribute<LoanEntity, Long> id;
    public static volatile SingularAttribute<LoanEntity, BranchEntity> branch;
    public static volatile SingularAttribute<LoanEntity, LoanStatusEntity> loanStatus;
    public static volatile ListAttribute<LoanEntity, ClientEntity> clients;
    public static volatile ListAttribute<LoanEntity, ClientLoan> clientLoans;

    private LoanEntity_() {
        // empty
    }
}
