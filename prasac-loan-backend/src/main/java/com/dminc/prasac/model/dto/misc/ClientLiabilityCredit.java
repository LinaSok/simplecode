package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.dminc.common.tools.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("PMD.TooManyFields")
public class ClientLiabilityCredit implements Serializable {
    private static final long serialVersionUID = 1247133448639351214L;

    private Long id;

    private String uuid;

    @ApiModelProperty(required = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate disbursementDate;

    @ApiModelProperty(required = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.DATE_FORMAT)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate maturityDate;

    private Double firstPrincipalRepayment;

    private String institutionName;

    private Double interestRateMonthly;

    private Double loanAmount;

    private Integer loanCycle;

    private Double loanLimit;

    private Double lockInPeriodMonthly;

    private Double monthlyInterestRepayment;

    private String mortgagedCollateralNumber;

    private Integer numberOfLateDays;

    private Integer numberOfLateInstallments;

    private Boolean payOffAfterNewLoan;

    private Double penaltyAmountForPaidOff;

    @ApiModelProperty(hidden = true)
    private Double tenureMonthly;

    private Long repaymentMode;

    private Long currency;

    private Long loanPurpose;

    private Long loanType;

    private Integer lengthOfUsing;

    @ApiModelProperty(hidden = true)
    private Long loan;

    @ApiModelProperty(hidden = true)
    private String accountNumber;

    private List<Document> documents;
}
