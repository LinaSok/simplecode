package com.dminc.prasac.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.dminc.prasac.model.misc.AuditEntity;
import com.dminc.prasac.model.misc.LocalConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Table(name = "ps_user")
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User extends AuditEntity implements UserDetails, Serializable {

    public static final String REGION = "user";
    public static final String USER_ID = "user_id";
    private static final long serialVersionUID = 2132636482484828059L;
    public static final String USER_ROLE_JOIN_TABLE = "ps_user_role_user";

    @Id
    @GenericGenerator(name = USER_ID, strategy = "increment")
    @GeneratedValue(generator = USER_ID)
    @Column(name = USER_ID, nullable = false, updatable = false)
    private Long id;

    @Column(name = "user_name")
    private String username;

    @Column(name = "password")
    private String password;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = USER_ROLE_JOIN_TABLE, joinColumns = {@JoinColumn(name = USER_ID)}, inverseJoinColumns = {
            @JoinColumn(name = "user_role_id")})
    private List<UserRole> roles;

    @Column(name = "is_enabled")
    private boolean enabled;

    @Column(name = "is_locked")
    private boolean locked;

    @OneToOne(mappedBy = REGION, cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    private UserProfile userProfile;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles.stream().flatMap(role -> {
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(buildAuthority(UserRole.ROLE_USER));
            authorities.add(buildAuthority(LocalConstants.ROLE_PREFIX + role.getName()));
            authorities.addAll(CollectionUtils.emptyIfNull(role.getPermissions()).stream()
                    .map(permission -> buildAuthority(permission.getName())).collect(Collectors.toList()));
            return authorities.stream();
        }).collect(Collectors.toList());
    }

    private SimpleGrantedAuthority buildAuthority(String authority) {
        return new SimpleGrantedAuthority(authority);
    }

    public List<UserPermission> getPermissions() {
        return roles.stream().flatMap(role -> CollectionUtils.emptyIfNull(role.getPermissions()).stream()).collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !locked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

}
