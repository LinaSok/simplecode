package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.dminc.prasac.service.validator.constraint.group.Default;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FinancialStatementGuarantorExpense implements Serializable {
    private static final long serialVersionUID = 8941400358363788039L;

    private Long id;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double utilityExpense;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double familyExpense;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double otherExpense;

}
