package com.dminc.prasac.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.dminc.prasac.model.misc.AuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = LoanPurposeEntity.REGION)
@Table(name = "ps_loan_purpose")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoanPurposeEntity extends AuditEntity {
    public static final String REGION = "loanPurpose";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String code;

    private String purpose;

    @Column(name = "purpose_in_khmer")
    private String khmerPurpose;

    @Column(name = "cbc_code")
    private String cbcCode;

    @Column(name = "cbc_desc")
    private String cbcDesc;

}