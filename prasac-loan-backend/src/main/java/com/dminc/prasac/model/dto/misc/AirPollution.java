package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AirPollution implements Serializable {

    private static final long serialVersionUID = -837805210869627083L;
    private Long id;

    private String name;

    private String khmerName;

    private String code;
}