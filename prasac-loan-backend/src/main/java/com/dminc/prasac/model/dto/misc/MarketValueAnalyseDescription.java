package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketValueAnalyseDescription implements Serializable {

    private static final long serialVersionUID = 7409883337785736362L;
    private Long id;

    private String description;

    /**
     * 1 - collateral, 2- other assets
     */
    private Long type;

}