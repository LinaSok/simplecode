package com.dminc.prasac.model.dto.loan;

import com.dminc.prasac.model.dto.misc.ClientType;
import com.dminc.prasac.model.dto.misc.CollateralTitleType;
import com.dminc.prasac.model.dto.misc.Currency;
import com.dminc.prasac.model.dto.misc.LoanPurpose;
import com.dminc.prasac.model.dto.misc.RepaymentMode;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@Builder
@AllArgsConstructor
@SuppressWarnings("PMD.TooManyFields")
public class LoanHistory implements Serializable {

    private static final long serialVersionUID = 6053107138653241932L;
    private String accountNumber;
    private String clientCif;
    private String requestStatus;
    private ClientType clientType;
    private LocalDate disbursementDate;
    private Double loanFee;
    private Currency currency;
    private Double interestRate;
    private Integer loanTerm;
    private Integer usedTerm;
    private String loanStatus;
    private Integer lockInPeriod;
    private RepaymentMode repaymentMode;
    private Integer lateInstallment;
    private String loanClassification;
    private Double paidOffAmount;
    private LoanPurpose loanPurpose;
    private Integer totalCollateral;
    private CollateralTitleType collateralType;
    private String prasacScoring;
    private String cbcScoring;
    private String otherApplicants;
    private Integer loanCycle;
    private Integer aging;
    private String multipleLoanCode;
    private String multipleLoan;
    private String rawCollateralType;
    private String rawLoanPurpose;
    private String otherApplicants1;
    private String no;
}
