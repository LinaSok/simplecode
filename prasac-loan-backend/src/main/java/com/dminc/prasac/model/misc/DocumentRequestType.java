package com.dminc.prasac.model.misc;

public enum DocumentRequestType {
    LOAN, CLIENT, LIABILITY, BUSINESS, COLLATERAL, OTHER_ASSET
}
