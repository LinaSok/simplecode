package com.dminc.prasac.model.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import com.dminc.prasac.model.misc.AuditEntity;
import com.dminc.prasac.model.misc.LocalConstants;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = FinancialStatementIncomeBankPaymentEntity.REGION)
@Table(name = "ps_financial_statement_income_bank_payment")
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings({"PMD.TooManyFields", "CPD-START"})
public class FinancialStatementIncomeBankPaymentEntity extends AuditEntity {
    public static final String REGION = "financialStatementIncomeBankPayment";

    @Id
    @GenericGenerator(name = REGION, strategy = LocalConstants.ID_GENERATOR)
    @GeneratedValue(generator = REGION)
    private Long id;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "current_month")
    private Double currentMonth;

    private Double eighth;

    private Double eleventh;

    private Double fifth;

    private Double first;

    private Double fourth;

    private Double ninth;

    private Double second;

    private Double seventh;

    private Double sixth;

    private Double tenth;

    private Double third;

    private Double twelfth;

    @OneToOne(mappedBy = REGION)
    private FinancialStatementIncomeEntity financialStatementIncome;
}