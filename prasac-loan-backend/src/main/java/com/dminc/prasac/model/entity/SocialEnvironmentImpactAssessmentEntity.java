package com.dminc.prasac.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.dminc.prasac.model.misc.AuditEntity;
import com.dminc.prasac.model.misc.LocalConstants;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = SocialEnvironmentImpactAssessmentEntity.REGION)
@Table(name = "ps_social_environment_impact_assessment")
@AllArgsConstructor
@NoArgsConstructor
public class SocialEnvironmentImpactAssessmentEntity extends AuditEntity {
    public static final String REGION = "socialEnvironmentImpactAssessment";

    @Id
    @GenericGenerator(name = REGION, strategy = LocalConstants.ID_GENERATOR)
    @GeneratedValue(generator = REGION)
    private Long id;

    @Column(name = "advice_consultation")
    @Type(type = "text")
    private String adviceConsultation;

    @Column(name = "conversation_with_borrower")
    @Type(type = "text")
    private String conversationWithBorrower;

    @Column(name = "customer_improvement_notice")
    @Type(type = "text")
    private String customerImprovementNotice;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "business_activity_id")
    private BusinessActivityEntity businessActivity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "energy_id")
    private EnergyEntity energy;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "health_safety_id")
    private HealthSafetyEntity healthSafety;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "labor_force_id")
    private LaborForceEntity laborForce;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "natural_resource_id")
    private NaturalResourceEntity naturalResource;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_waste_id")
    private ProductWasteEntity productWaste;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "water_pollution_id")
    private WaterPollutionEntity waterPollution;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "air_pollution_id")
    private AirPollutionEntity airPollution;
}