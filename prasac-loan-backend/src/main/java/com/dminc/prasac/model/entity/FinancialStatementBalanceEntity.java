package com.dminc.prasac.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;

import com.dminc.prasac.model.misc.AuditEntity;
import com.dminc.prasac.model.misc.LocalConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = FinancialStatementBalanceEntity.REGION)
@Table(name = "ps_financial_statement_balance")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FinancialStatementBalanceEntity extends AuditEntity {
    public static final String REGION = "financialStatementBalance";

    @Id
    @GenericGenerator(name = REGION, strategy = LocalConstants.ID_GENERATOR)
    @GeneratedValue(generator = REGION)
    private Long id;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "balance_assets_id")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = FinancialStatementBalanceAssetEntity.REGION)
    @Cascade({CascadeType.ALL})
    private FinancialStatementBalanceAssetEntity financialStatementBalanceAsset;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "balance_liability_id")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = FinancialStatementBalanceLiabilityEntity.REGION)
    @Cascade({CascadeType.ALL})
    private FinancialStatementBalanceLiabilityEntity financialStatementBalanceLiability;
}