package com.dminc.prasac.model.entity;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(UserRole.class)
public final class UserRole_ {
    public static volatile SingularAttribute<UserRole, Long> id;

    private UserRole_() {
        // empty
    }
}
