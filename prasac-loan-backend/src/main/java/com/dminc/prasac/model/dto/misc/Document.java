package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
public class Document implements Serializable {
    private static final long serialVersionUID = 6814772868048828191L;

    private Long id;

    private String description;

    private String image;

    private String imageType;

    private Long documentType;

    private Long loan;

}
