package com.dminc.prasac.model.entity;

import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.misc.AuditEntity;
import com.dminc.prasac.model.misc.LocalConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = LoanApprovalState.TABLE_NAME)
public class LoanApprovalState extends AuditEntity {

    public static final String TABLE_NAME = "ps_committee_loan_approval";
    public static final String REGION = "loanApprovalState";

    @Id
    @GenericGenerator(name = REGION, strategy = LocalConstants.ID_GENERATOR)
    @GeneratedValue(generator = REGION)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "loan_id")
    private LoanEntity loan;

    @Column(name = "approval_state")
    @Enumerated(EnumType.STRING)
    private ApprovalState state;

}
