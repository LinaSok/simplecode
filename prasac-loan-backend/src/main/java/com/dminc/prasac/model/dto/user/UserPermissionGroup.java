package com.dminc.prasac.model.dto.user;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserPermissionGroup implements Serializable {
    private static final long serialVersionUID = 2429952065518178067L;
    private Long id;
    private String groupName;
    private List<UserPermission> userPermissions;
}
