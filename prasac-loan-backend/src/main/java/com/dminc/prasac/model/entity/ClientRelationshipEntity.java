package com.dminc.prasac.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Builder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.dminc.prasac.model.misc.AuditEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = ClientRelationshipEntity.REGION)
@Table(name = "ps_client_relationship")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClientRelationshipEntity extends AuditEntity {
    public static final String REGION = "clientRelationship";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String code;

    private String relationship;

    @Column(name = "relationship_in_khmer")
    private String khmerRelationship;

}