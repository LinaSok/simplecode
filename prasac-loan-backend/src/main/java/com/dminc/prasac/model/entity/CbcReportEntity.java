package com.dminc.prasac.model.entity;

import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.misc.AuditEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = CbcReportEntity.REGION)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "ps_cbc_report")
public class CbcReportEntity extends AuditEntity {
    public static final String REGION = "cbcReport";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private CbcStatus status;

    @Column(name = "report")
    @Type(type = "text")
    private String report;

    @Column(name = "request")
    @Type(type = "text")
    private String request;

    @ManyToOne
    @JoinColumn(name = "loan_id")
    private LoanEntity loan;
}
