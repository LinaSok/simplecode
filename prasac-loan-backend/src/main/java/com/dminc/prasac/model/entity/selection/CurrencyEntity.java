package com.dminc.prasac.model.entity.selection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.dminc.prasac.model.misc.AuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = CurrencyEntity.REGION)
@Table(name = "ps_currency")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyEntity extends AuditEntity {
    public static final String REGION = "currency";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String code;

    private String currency;

    @Column(name = "class_code")
    private String classCode;

    @Column(name = "currency_in_khmer")
    private String khmerCurrency;
}