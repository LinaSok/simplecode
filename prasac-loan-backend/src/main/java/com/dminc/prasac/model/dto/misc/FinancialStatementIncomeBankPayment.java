package com.dminc.prasac.model.dto.misc;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class FinancialStatementIncomeBankPayment extends FinancialStatementIncomeForecast {

}
