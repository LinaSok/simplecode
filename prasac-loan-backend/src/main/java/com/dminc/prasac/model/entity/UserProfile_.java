package com.dminc.prasac.model.entity;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(UserProfile.class)
public final class UserProfile_ {
    public static volatile SingularAttribute<UserProfile, String> firstName;
    public static volatile SingularAttribute<UserProfile, String> lastName;
    public static volatile SingularAttribute<UserProfile, String> staffId;
    public static volatile SingularAttribute<UserProfile, BranchEntity> branch;
    public static volatile SingularAttribute<UserProfile, User> user;

    private UserProfile_() {
        // empty
    }
}
