package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.dminc.prasac.model.dto.address.Address;
import com.dminc.prasac.service.validator.constraint.group.Default;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("PMD.TooManyFields")
public class ClientBusiness extends Address implements Serializable {
    private static final long serialVersionUID = 2350724829322044982L;

    private Long id;

    private String uuid;

    private Integer addressGroupNo;

    private String businessActivitiesDetail;

    private String houseNo;

    @ApiModelProperty(required = true)
    @NotBlank(groups = Default.class)
    private String latitude;

    @ApiModelProperty(required = true)
    @NotBlank(groups = Default.class)
    private String longitude;

    @ApiModelProperty(required = true)
    @NotBlank(groups = Default.class)
    private String managerName;

    @ApiModelProperty(required = true)
    @NotBlank(groups = Default.class)
    private String name;

    private Integer numberOfManager;

    private String street;

    @ApiModelProperty(hidden = true)
    private Long loan;

    private Long businessExperience;

    private Long clientBusinessCashFlow;

    private Long clientBusinessLocationStatus;

    private Long clientBusinessPeriod;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long clientBusinessProgress;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long licenseContractValidity;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Long businessOwner;

    @Valid
    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private FinancialStatementIncomeRevenue financialStatementIncomeRevenue;

    private List<Document> documents;
}
