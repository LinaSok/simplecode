package com.dminc.prasac.model.dto.address;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Commune implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String khmerName;
    private String name;
    private List<Village> villages;
    private Long code;
}
