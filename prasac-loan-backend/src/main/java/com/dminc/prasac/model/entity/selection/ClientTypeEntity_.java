package com.dminc.prasac.model.entity.selection;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(ClientTypeEntity.class)
public final class ClientTypeEntity_ {

    public static volatile SingularAttribute<ClientTypeEntity, Long> id;

    private ClientTypeEntity_() {
        // empty
    }
}
