package com.dminc.prasac.model.misc;

@SuppressWarnings({"PMD.AvoidDuplicateLiterals"})
public final class LocalConstants {

    public static final String API_V1 = "/api/v1";
    public static final String RESULT_SIZE_0 = "#result.size() == 0";
    public static final String RESULT_NULL = "#result == null";
    public static final String TRUE = "true";
    public static final String FALSE = "false";

    public static final String LIST = "LIST";
    public static final String ID_GENERATOR = "com.dminc.prasac.util.EntityIdGenerator";
    public static final int ONE_DAY_IN_SECONDS = 86400;
    public static final String INTEGER = "integer";
    public static final String QUERY = "query";
    public static final String CO_REQUIRED_BO_OPTIONAL = "CO: <span style='color: red;'>required</span>, BO: optional";
    public static final String REQUIRED_ON_UPDATE = "<span style='color: orange;'>Required on update</span>";
    public static final String CO_REQUIRED_BO_REQUIRED = "CO: <span style='color: red;'>required</span>, BO: <span style='color: red;'>required</span>";
    public static final String CO_OPTIONAL_BO_OPTIONAL = "CO: optional, BO: optional";

    public static final String PERMISSION_CREATE_PRE_SCREENING = "CREATE_PRE_SCREENING";
    public static final String PERMISSION_RECEIVE_PRE_SCREENING = "RECEIVE_PRE_SCREENING";
    public static final String PERMISSION_PRINT_TEMPLATES = "PRINT_TEMPLATES";
    public static final String PERMISSION_TABLET_USER = "TABLET_USER";
    public static final String PERMISSION_VIEW_LOAN_IN_BRANCH = "VIEW_LOAN_IN_BRANCH";
    public static final String PERMISSION_VIEW_LOAN_ALL_BRANCH = "VIEW_LOAN_ALL_BRANCH";
    public static final String PERMISSION_EDIT_LOAN = "EDIT_LOAN";
    public static final String PERMISSION_REVIEW_LEVEL_1 = "REVIEW_LEVEL_1";
    public static final String PERMISSION_REVIEW_LEVEL_2 = "REVIEW_LEVEL_2";
    public static final String PERMISSION_REVIEW_LEVEL_3 = "REVIEW_LEVEL_3";
    public static final String PERMISSION_REVIEW_LEVEL_4 = "REVIEW_LEVEL_4";
    public static final String PERMISSION_ASSIGN_BACK = "ASSIGN_BACK";
    public static final String PERMISSION_CHECK_CLIENT_CREDIT_HISTORY = "CHECK_CLIENT_CREDIT_HISTORY";
    public static final String PERMISSION_ADD_VIEW_COMMENT = "ADD_VIEW_COMMENT";
    public static final String PERMISSION_RO = "RO";
    public static final String PERMISSION_DECISION_MAKING = "DECISION_MAKING";

    public static final String PERMISSION_CREATE_USER = "CREATE_USER";
    public static final String PERMISSION_EDIT_USER = "EDIT_USER";
    public static final String PERMISSION_DEACTIVATE_USER = "DEACTIVATE_USER";
    public static final String PERMISSION_CREATE_ROLE = "CREATE_ROLE";
    public static final String PERMISSION_EDIT_ROLE = "EDIT_ROLE";
    public static final String PERMISSION_DELETE_ROLE = "DELETE_ROLE";

    public static final String TABLET_USER = "hasAuthority('" + PERMISSION_TABLET_USER + "')";
    public static final String CAN_SUBMIT_LOAN_APPRAISAL = "hasAnyAuthority('" + PERMISSION_TABLET_USER + "', '" + PERMISSION_EDIT_LOAN + "')";
    public static final String CAN_CREATE_PRE_SCREENING = "hasAnyAuthority('" + PERMISSION_CREATE_PRE_SCREENING + "', '" + PERMISSION_TABLET_USER + "')";
    public static final String CAN_ASSIGN_LOAN = "hasAnyAuthority('" + PERMISSION_CREATE_PRE_SCREENING + "', '" + PERMISSION_RECEIVE_PRE_SCREENING + "')";
    public static final String CAN_RECEIVE_PRE_SCREENING = "hasAnyAuthority('" + PERMISSION_RECEIVE_PRE_SCREENING + "')";
    public static final String CAN_VIEW_USERS = "hasAnyAuthority('" + PERMISSION_CREATE_USER + "', '" + PERMISSION_EDIT_USER + "', '" + PERMISSION_DEACTIVATE_USER + "')";
    public static final String CAN_VIEW_USER_GROUP = "hasAnyAuthority('" + PERMISSION_CREATE_ROLE + "', '" + PERMISSION_EDIT_ROLE + "', '" + PERMISSION_DELETE_ROLE + "')";
    public static final String CAN_VIEW_LOAN = "hasAnyAuthority('" + PERMISSION_VIEW_LOAN_IN_BRANCH + "', '" + PERMISSION_VIEW_LOAN_ALL_BRANCH + "', '" + PERMISSION_RECEIVE_PRE_SCREENING + "')";
    public static final String CAN_ASSIGN_BACK = "hasAuthority('" + PERMISSION_ASSIGN_BACK + "')";
    public static final String CAN_VIEW_COMMENTS = "hasAnyAuthority('" + PERMISSION_TABLET_USER + "', '" + PERMISSION_ADD_VIEW_COMMENT + "')";
    public static final String CAN_VIEW_DECISION_MAKER_GROUP = "hasAuthority('" + PERMISSION_REVIEW_LEVEL_3 + "')";


    public static final String CAN_VIEW_LOAN_REQUEST_HISTORY = "hasAnyAuthority('" + PERMISSION_TABLET_USER + "', '" + PERMISSION_CHECK_CLIENT_CREDIT_HISTORY + "')";

    public static final String ALL_REVIEW_LEVEL_TABLET_USER_DECISION_MAKING = "hasAnyAuthority('"
            + PERMISSION_REVIEW_LEVEL_1 + "', '"
            + PERMISSION_REVIEW_LEVEL_2 + "', '"
            + PERMISSION_REVIEW_LEVEL_3 + "', '"
            + PERMISSION_REVIEW_LEVEL_4 + "', '"
            + PERMISSION_TABLET_USER + "', '"
            + PERMISSION_DECISION_MAKING + "', '"
            + PERMISSION_RECEIVE_PRE_SCREENING + "', '"
            + PERMISSION_EDIT_LOAN + "')";

    public static final String ALL_REVIEW_LEVEL_DECISION_MAKING = "hasAnyAuthority('"
            + PERMISSION_REVIEW_LEVEL_1 + "', '"
            + PERMISSION_REVIEW_LEVEL_2 + "', '"
            + PERMISSION_REVIEW_LEVEL_3 + "', '"
            + PERMISSION_REVIEW_LEVEL_4 + "', '"
            + PERMISSION_DECISION_MAKING + "')";

    public static final String ROLE_PREFIX = "ROLE_";
    public static final String MULTIPART_FORM_DATA = "multipart/form-data";
    public static final String CAN_REJECT_LOAN = "hasAnyAuthority('" + PERMISSION_DECISION_MAKING + "', '" + PERMISSION_TABLET_USER + "')";
    public static final String CAN_MAKE_DECISION = "hasAuthority('" + PERMISSION_DECISION_MAKING + "')";
    public static final String ROLE_MOBILE_CLIENT = "ROLE_MOBILE_CLIENT";
    public static final String ROLE_USER = "ROLE_USER";

    //These are the ID in database ps_user_permission table
    public static final Long REVIEW_LEVEL_1 = 8L;
    public static final Long REVIEW_LEVEL_2 = 9L;
    public static final Long REVIEW_LEVEL_3 = 10L;
    public static final Long REVIEW_LEVEL_4 = 11L;
    public static final Long PRINT_TEMPLATE = 3L; //Only TL will have this permission
    public static final Long RECIEVE_PRESCREENING_PERMISSION_ID = 2L;
    public static final Long DECISION_MAKER = 16L;

    //Email template placeholder
    public static final String EMAIL_CONTENT_EMAIL_USER_NAME = "${userName}";
    public static final String EMAIL_CONTENT_ASSIGNED_FROM_USER = "${assignedFromUser}";
    public static final String EMAIL_CONTENT_LOAN_ID = "${loanId}";
    public static final String EMAIL_CONTENT_LOAN_STATUS = "${loanStatus}";
    public static final String EMAIL_CONTENT_BORROWER_NAME = "${borrowerName}";
    public static final String EMAIL_CONTENT_CO_BORROWER_NAME = "${coBorrowerName}";
    public static final String EMAIL_CONTENT_BORROWER_KH_NAME = "${borrowerKHName}";
    public static final String EMAIL_CONTENT_CO_BORROWER_KH_NAME = "${coBorrowerKHName}";
    public static final String EMAIL_CONTENT_LOAN_CURRENCY = "${currency}";
    public static final String EMAIL_CONTENT_LOAN_AMOUNT = "${amount}";
    public static final String EMAIL_CONTENT_CURRENT_ADDRESS = "${currentAddress}";
    public static final String EMAIL_CONTENT_COMMENT = "${comment}";

    //Committee level used for making decision on loan status
    public static final int LEVEL_CC1 = 2;
    public static final int LEVEL_CC2 = 5;
    public static final int COMMITTEE_LEVEL_2 = 2;
    public static final int COMMITTEE_LEVEL_3 = 3;
    public static final int LEVEL_LAO_REVIEW = 4;
    public static final int NON_COMMITTEE_LEVEL = 0;
    public static final int COMMITTEE_LEVEL_1 = 1;

    private LocalConstants() {
        //to prevent creating instances
    }

}
