package com.dminc.prasac.model.entity.selection;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.dminc.prasac.model.misc.AuditEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = CountryEntity.REGION)
@Table(name = "ps_country")
public class CountryEntity extends AuditEntity implements Serializable {
    public static final String REGION = "country";
    public static final Long CAMBODIA_ID = 1L;
    private static final long serialVersionUID = -1407536507903531532L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String code;

    @Column(name = "khmer_name")
    private String khmerName;

    private String name;

    @Column(name = "cbc_code")
    private String cbcCode;

    @OneToMany(mappedBy = REGION)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = ProvinceEntity.REGION)
    @LazyCollection(LazyCollectionOption.TRUE)
    private List<ProvinceEntity> provinces;

}