package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LicenseContractValidity implements Serializable {

    private static final long serialVersionUID = -152523212553672688L;
    private Long id;

    private String validity;

    private String khmerValidity;

    private String code;
}
