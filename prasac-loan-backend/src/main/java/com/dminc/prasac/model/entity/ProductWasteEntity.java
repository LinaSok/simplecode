package com.dminc.prasac.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.dminc.prasac.model.misc.AuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = ProductWasteEntity.REGION)
@Table(name = "ps_product_waste")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductWasteEntity extends AuditEntity {
    public static final String REGION = "productWaste";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String code;

    private String name;

    @Column(name = "name_in_khmer")
    private String khmerName;

}