package com.dminc.prasac.model.entity;

import com.dminc.prasac.model.misc.AuditEntity;
import com.dminc.prasac.model.misc.LocalConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "ps_user_role")
@EqualsAndHashCode(callSuper = false)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRole extends AuditEntity implements Serializable {
    private static final long serialVersionUID = -6598608911428727519L;

    public static final String ROLE_USER = LocalConstants.ROLE_PREFIX + "USER";
    public static final String USER_ROLE_ID = "user_role_id";

    @Id
    @GenericGenerator(name = USER_ROLE_ID, strategy = "increment")
    @GeneratedValue(generator = USER_ROLE_ID)
    @Column(name = USER_ROLE_ID)
    private Long id;

    private String name;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ps_user_role_user_permission", joinColumns = @JoinColumn(name = USER_ROLE_ID), inverseJoinColumns = @JoinColumn(name = UserPermission.USER_PERMISSION_ID))
    private List<UserPermission> permissions;
}
