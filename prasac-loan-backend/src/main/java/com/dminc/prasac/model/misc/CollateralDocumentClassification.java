package com.dminc.prasac.model.misc;

import java.util.Arrays;

public enum CollateralDocumentClassification {
    DOCUMENTS(15L), PICTURES(16L), OTHERS(17L);

    private final Long value;

    CollateralDocumentClassification(Long value) {
        this.value = value;
    }

    public Long getValue() {
        return this.value;
    }

    public static boolean contains(Long classification) {
        return Arrays.stream(CollateralDocumentClassification.values()).anyMatch(c -> c.getValue().equals(classification));
    }

}
