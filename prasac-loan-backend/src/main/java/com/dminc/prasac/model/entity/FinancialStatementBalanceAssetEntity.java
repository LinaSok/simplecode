package com.dminc.prasac.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import com.dminc.prasac.model.misc.AuditEntity;
import com.dminc.prasac.model.misc.LocalConstants;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = FinancialStatementBalanceAssetEntity.REGION)
@Table(name = "ps_financial_statement_balance_assets")
@AllArgsConstructor
@NoArgsConstructor
public class FinancialStatementBalanceAssetEntity extends AuditEntity {
    public static final String REGION = "financialStatementBalanceAsset";

    @Id
    @GenericGenerator(name = REGION, strategy = LocalConstants.ID_GENERATOR)
    @GeneratedValue(generator = REGION)
    private Long id;

    @Column(name = "current_year_account_receivable")
    private Double currentYearAccountReceivable;

    @Column(name = "current_year_cash")
    private Double currentYearCash;

    @Column(name = "current_year_material_inventory")
    private Double currentYearMaterialInventory;

    @Column(name = "current_year_others")
    private Double currentYearOthers;

    @Column(name = "forecast_year_account_receivable")
    private Double forecastYearAccountReceivable;

    @Column(name = "forecast_year_cash")
    private Double forecastYearCash;

    @Column(name = "forecast_year_material_inventory")
    private Double forecastYearMaterialInventory;

    @Column(name = "forecast_year_others")
    private Double forecastYearOthers;

    @OneToOne(mappedBy = REGION)
    private FinancialStatementBalanceEntity financialStatementBalance;
}