package com.dminc.prasac.model.entity;

import com.dminc.prasac.model.misc.AuditEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = MaritalStatusEntity.REGION)
@Table(name = "ps_marital_status")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MaritalStatusEntity extends AuditEntity {
    public static final String REGION = "maritalStatus";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String code;

    private String status;

    @Column(name = "status_in_khmer")
    private String khmerStatus;

}