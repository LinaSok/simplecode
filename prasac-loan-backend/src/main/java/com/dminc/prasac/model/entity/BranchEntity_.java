package com.dminc.prasac.model.entity;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(BranchEntity.class)
public final class BranchEntity_ {

    public static volatile SingularAttribute<BranchEntity, Long> id;

    private BranchEntity_() {
        // empty
    }
}
