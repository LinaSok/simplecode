package com.dminc.prasac.model.dto.misc;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanType implements Serializable {

    private static final long serialVersionUID = -8991085257411164368L;
    private Long id;

    private String type;

}