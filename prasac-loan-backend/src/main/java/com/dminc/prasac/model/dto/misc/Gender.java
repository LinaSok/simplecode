package com.dminc.prasac.model.dto.misc;

public enum Gender {
    MALE, FEMALE
}
