package com.dminc.prasac.model.dto.user;

import com.dminc.prasac.model.dto.misc.Gender;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@SuppressWarnings({"PMD.ImmutableField", "PMD.ExcessiveParameterList"})
public class NewUserRequest extends UserRequest {
    @NotBlank
    private String username;

    @Builder
    public NewUserRequest(String staffId, String firstName, String lastName, LocalDate dateOfBirth, Long branchId, Long positionId, Gender gender, List<Long> roles, String email, String username) {
        super(staffId, firstName, lastName, dateOfBirth, branchId, positionId, gender, roles, email);
        this.username = username;
    }
}
