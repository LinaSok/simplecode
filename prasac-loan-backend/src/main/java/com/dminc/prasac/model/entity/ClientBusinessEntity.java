package com.dminc.prasac.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.entity.selection.VillageEntity;
import com.dminc.prasac.model.misc.AuditEntity;
import com.dminc.prasac.model.misc.LocalConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = ClientBusinessEntity.REGION)
@Table(name = "ps_client_business")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@SuppressWarnings("PMD.TooManyFields")
public class ClientBusinessEntity extends AuditEntity {
    public static final String REGION = "clientBusiness";
    public static final String CLIENT_BUSINESS_ID = "client_business_id";

    @Id
    @GenericGenerator(name = REGION, strategy = LocalConstants.ID_GENERATOR)
    @GeneratedValue(generator = REGION)
    private Long id;

    @Column(name = "address_group_no")
    private Integer addressGroupNo;

    @Column(name = "business_activities_detail")
    @Type(type = "text")
    private String businessActivitiesDetail;

    @Column(name = "house_no")
    private String houseNo;

    private String latitude;

    private String longitude;

    @Column(name = "manager_name")
    private String managerName;

    private String name;

    @Column(name = "number_of_manager")
    private Integer numberOfManager;

    private String street;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "loan_id")
    private LoanEntity loan;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "village_id")
    private VillageEntity village;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "business_experience_id")
    private BusinessExperienceEntity businessExperience;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "business_cash_flow_id")
    private ClientBusinessCashFlowEntity clientBusinessCashFlow;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "business_location_status_id")
    private ClientBusinessLocationStatusEntity clientBusinessLocationStatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "business_period_id")
    private ClientBusinessPeriodEntity clientBusinessPeriod;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "business_progress_id")
    private ClientBusinessProgressEntity clientBusinessProgress;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "license_contract_validity_id")
    private LicenseContractValidityEntity licenseContractValidity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "business_ownership_id")
    private BusinessOwnerEntity businessOwner;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "financial_statement_income_revenue_id")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = FinancialStatementIncomeRevenueEntity.REGION)
    @Cascade({CascadeType.ALL})
    private FinancialStatementIncomeRevenueEntity financialStatementIncomeRevenue;

    @Column(name = "uuid")
    private String uuid;

}