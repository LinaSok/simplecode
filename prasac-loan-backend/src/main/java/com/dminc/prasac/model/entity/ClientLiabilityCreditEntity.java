package com.dminc.prasac.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.entity.selection.CurrencyEntity;
import com.dminc.prasac.model.misc.AuditEntity;
import com.dminc.prasac.model.misc.LocalConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = ClientLiabilityCreditEntity.REGION)
@Table(name = "ps_client_liability_credit")
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("PMD.TooManyFields")
@Builder
public class ClientLiabilityCreditEntity extends AuditEntity {
    public static final String REGION = "clientLiabilityCredit";
    public static final String CLIENT_LIABILITY_CREDIT_ID = "client_liability_credit_id";
    private static final String YES_NO = "yes_no";

    @Id
    @GenericGenerator(name = REGION, strategy = LocalConstants.ID_GENERATOR)
    @GeneratedValue(generator = REGION)
    private Long id;

    @Column(name = "disbursement_date")
    private Date disbursementDate;

    @Column(name = "first_principal_repayment")
    private Double firstPrincipalRepayment;

    @Column(name = "institution_name")
    private String institutionName;

    @Column(name = "interest_rate_monthly")
    private Double interestRateMonthly;

    @Column(name = "loan_amount")
    private Double loanAmount;

    @Column(name = "loan_cycle")
    private Integer loanCycle;

    @Column(name = "loan_limit")
    private Double loanLimit;

    @Column(name = "lock_in_period_monthly")
    private Double lockInPeriodMonthly;

    @Column(name = "maturity_date")
    private Date maturityDate;

    @Column(name = "monthly_interest_repayment")
    private Double monthlyInterestRepayment;

    @Column(name = "mortgaged_collateral_number")
    private String mortgagedCollateralNumber;

    @Column(name = "number_of_late_days")
    private Integer numberOfLateDays;

    @Column(name = "number_of_late_installments")
    private Integer numberOfLateInstallments;

    @Column(name = "pay_off_after_new_loan")
    @Type(type = YES_NO)
    private Boolean payOffAfterNewLoan;

    @Column(name = "penalty_amount_for_paid_off")
    private Double penaltyAmountForPaidOff;

    @Column(name = "tenure_monthly")
    private Double tenureMonthly;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "loan_id")
    private LoanEntity loan;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "repayment_mode_id")
    private RepaymentModeEntity repaymentMode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "currency_id")
    private CurrencyEntity currency;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "loan_purpose_id")
    private LoanPurposeEntity loanPurpose;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "loan_type_id")
    private LoanTypeEntity loanType;

    @Column(name = "length_of_using")
    private Integer lengthOfUsing;

    @Column(name = "uuid")
    private String uuid;

}