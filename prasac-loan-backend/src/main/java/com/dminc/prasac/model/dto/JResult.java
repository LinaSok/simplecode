package com.dminc.prasac.model.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JResult implements Serializable {
    private String message;


}
