package com.dminc.prasac.model.misc;

import java.util.Arrays;

public enum RequestedFrom {
    TABLET, BACK_OFFICE, ANY;

    public boolean in(RequestedFrom... fromUsers) {
        return Arrays.stream(fromUsers).anyMatch(agent -> agent == this);
    }
}
