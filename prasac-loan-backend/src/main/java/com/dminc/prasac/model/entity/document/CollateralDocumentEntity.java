package com.dminc.prasac.model.entity.document;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import com.dminc.prasac.model.entity.DocumentEntity;
import com.dminc.prasac.model.entity.LoanCollateralEntity;
import com.dminc.prasac.model.misc.AuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@Entity
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = CollateralDocumentEntity.REGION)
@Table(name = "ps_collateral_document")

public class CollateralDocumentEntity extends AuditEntity {
    public static final String REGION = "collateralDocument";

    @Id
    @GenericGenerator(name = "collateral_document_id", strategy = "increment")
    @GeneratedValue(generator = "collateral_document_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "collateral_id")
    private LoanCollateralEntity collateral;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "document_id")
    private DocumentEntity document;

}
