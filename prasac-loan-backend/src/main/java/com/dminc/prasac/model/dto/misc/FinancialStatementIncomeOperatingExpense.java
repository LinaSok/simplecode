package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.dminc.prasac.service.validator.constraint.group.Default;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("PMD.TooManyFields")
public class FinancialStatementIncomeOperatingExpense implements Serializable {
    private static final long serialVersionUID = -3092757288908264891L;

    private Long id;

    private Double currentMonthTotal;

    private Double nextMonthTotal;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double currentMonthAdministrativeExpense;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double currentMonthDepreciationExpense;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double currentMonthFamilyExpense;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double currentMonthMaintenanceExpense;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double currentMonthOthersExpense;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double currentMonthRentalExpense;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double currentMonthSalaryExpense;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double currentMonthUtilityExpense;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double forecastMonthAdministrativeExpense;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double forecastMonthDepreciationExpense;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double forecastMonthFamilyExpense;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double forecastMonthMaintenanceExpense;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double forecastMonthOthersExpense;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double forecastMonthRentalExpense;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double forecastMonthSalaryExpense;

    @ApiModelProperty(required = true)
    @NotNull(groups = Default.class)
    private Double forecastMonthUtilityExpense;

}