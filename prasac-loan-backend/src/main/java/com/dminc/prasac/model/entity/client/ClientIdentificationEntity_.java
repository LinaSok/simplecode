package com.dminc.prasac.model.entity.client;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(ClientIdentificationEntity.class)
public final class ClientIdentificationEntity_ {

    public static volatile SingularAttribute<ClientIdentificationEntity, String> idNumber;

    private ClientIdentificationEntity_() {
        // empty
    }
}
