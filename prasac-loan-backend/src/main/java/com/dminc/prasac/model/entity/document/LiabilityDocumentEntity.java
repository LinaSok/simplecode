package com.dminc.prasac.model.entity.document;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import com.dminc.prasac.model.entity.ClientLiabilityCreditEntity;
import com.dminc.prasac.model.entity.DocumentEntity;
import com.dminc.prasac.model.misc.AuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@Entity
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = LiabilityDocumentEntity.REGION)
@Table(name = "ps_client_liability_credit_document")

public class LiabilityDocumentEntity extends AuditEntity {
    public static final String REGION = "clientLiabilityCreditDocument";

    @Id
    @GenericGenerator(name = "liability_document_id", strategy = "increment")
    @GeneratedValue(generator = "liability_document_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "liability_id")
    private ClientLiabilityCreditEntity liability;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "document_id")
    private DocumentEntity document;

}
