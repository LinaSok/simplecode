package com.dminc.prasac.model.entity.client;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(ClientEntity.class)
public final class ClientEntity_ {

    public static volatile ListAttribute<ClientEntity, ClientIdentificationEntity> identifications;
    public static volatile SingularAttribute<ClientEntity, String> cif;
    public static volatile SingularAttribute<ClientEntity, String> fullName;
    public static volatile SingularAttribute<ClientEntity, String> khmerFullName;
    public static volatile SingularAttribute<ClientEntity, Long> id;

    private ClientEntity_() {
        // empty
    }
}
