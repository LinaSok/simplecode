package com.dminc.prasac.model.dto.misc;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CollateralArea implements Serializable {

    private static final long serialVersionUID = 7136067092215849401L;
    private Long id;

    private String area;

    private String khmerArea;

    private String code;
}
