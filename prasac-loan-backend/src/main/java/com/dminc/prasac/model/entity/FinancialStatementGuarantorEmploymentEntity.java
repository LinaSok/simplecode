package com.dminc.prasac.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;

import com.dminc.prasac.model.misc.AuditEntity;
import com.dminc.prasac.model.misc.LocalConstants;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = FinancialStatementGuarantorEmploymentEntity.REGION)
@Table(name = "ps_financial_statement_guarantor_employment")
@AllArgsConstructor
@NoArgsConstructor
public class FinancialStatementGuarantorEmploymentEntity extends AuditEntity {
    public static final String REGION = "financialStatementGuarantorEmployment";

    @Id
    @GenericGenerator(name = REGION, strategy = LocalConstants.ID_GENERATOR)
    @GeneratedValue(generator = REGION)
    private Long id;

    private String activity;

    @Column(name = "annual_income")
    private Double annualIncome;

    @Column(name = "annual_expense")
    private Double annualExpense;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "financial_statement_guarantor_id")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = FinancialStatementGuarantorEntity.REGION)
    @Cascade({CascadeType.ALL})
    private FinancialStatementGuarantorEntity financialStatementGuarantor;
}