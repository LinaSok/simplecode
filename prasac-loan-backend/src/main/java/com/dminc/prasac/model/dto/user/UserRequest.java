package com.dminc.prasac.model.dto.user;

import com.dminc.prasac.model.dto.misc.Gender;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRequest {
    @NotBlank
    private String staffId;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    @NotNull
    private LocalDate dateOfBirth;
    @NotNull
    private Long branch;
    @NotNull
    private Long position;
    @NotNull
    private Gender gender;
    @NotNull
    private List<Long> roles;
    @NotBlank
    @Email
    private String email;
}
