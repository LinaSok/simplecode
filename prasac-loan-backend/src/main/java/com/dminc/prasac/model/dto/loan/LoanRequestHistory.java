package com.dminc.prasac.model.dto.loan;

import com.dminc.prasac.model.dto.misc.Currency;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanRequestHistory implements Serializable {

    private static final long serialVersionUID = -1656135804082675266L;

    private String clientCif;

    private String clientType;

    private String fullName;

    private String khmerFullname;

    private LocalDate dob;

    private String maritalStatus;

    private Integer no;

    private LocalDate requestDate;

    private Double requestAmount;

    private Currency currency;

    private String requestStatus;

    private String rejectReason;
}
