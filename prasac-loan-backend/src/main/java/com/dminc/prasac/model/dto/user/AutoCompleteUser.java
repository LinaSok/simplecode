package com.dminc.prasac.model.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AutoCompleteUser {
    private Long id;
    private String firstName;
    private String lastName;
    private String staffId;
}
