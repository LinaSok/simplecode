package com.dminc.prasac.service.selection;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.common.tools.mapping.Mapper;
import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.LoanPurpose;
import com.dminc.prasac.model.entity.LoanPurposeEntity;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.repository.LoanPurposeRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class LoanPurposeService extends CommonService<LoanPurpose, LoanPurposeEntity> {
    private final LoanPurposeRepository repository;

    public LoanPurposeService(LoanPurposeRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, LoanPurpose.class);
        this.repository = repository;
    }

    @Cacheable(cacheNames = "getLoanPurposeByCode", key = "#code", condition = "#code!=null", unless = LocalConstants.RESULT_NULL)
    public LoanPurpose getLoanPurposeByCode(String code) throws BusinessException {
        if (StringUtils.isNotEmpty(code)) {
            final LoanPurposeEntity purpose = repository.findByCode(code);
            return converter.runServiceTask((Mapper mapper) -> mapper.map(purpose, LoanPurpose.class));
        }
        return null;
    }

    @Cacheable(cacheNames = "getLoanPurposeByCbcCode", key = "#code", condition = "#code!=null", unless = LocalConstants.RESULT_NULL)
    public LoanPurpose getLoanPurposeByCbcCode(String code) {
        if (StringUtils.isNotEmpty(code)) {
            return convert(repository.findTopByCbcCode(code));
        }
        return null;
    }
}



