package com.dminc.prasac.service.credit;

import com.dminc.prasac.model.dto.loan.CreditHistoryMonitoring;

import java.util.List;

public interface CreditHistoryMonitorService {

    List<CreditHistoryMonitoring> getByClient(String cif);
}
