package com.dminc.prasac.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AdminTaskService {

    public static final String LINE_SEPARATOR = System.lineSeparator();
    @Autowired
    private EmailManagerService emailManagerService;
    @Autowired
    private RedisCacheManager cacheManager;
    @Autowired
    private UserService userService;

    public String processTask(final String taskName, final Map<String, String[]> paramMap) {
        log.info(String.format("========== Process Admin Task : %s =========", taskName));

        switch (taskName) {
            case "test":
                log.info("===========task test is exectued=======");
                break;
            case "sendMail":
                sendMail(paramMap.get("email")[0], paramMap.get("title")[0]);
                break;
            case "flushCache":
                return clearAllCaches();
            case "reactivate":
                userService.reactivate(paramMap.get("userName")[0]);
                break;
            case "resetpwd":
                userService.resetPasswordByTheProvidedPwd(paramMap.get("userName")[0], paramMap.get("pwd")[0]);
                break;
            default:
                break;
        }
        return taskName;
    }

    private String clearAllCaches() {
        final StringBuilder cacheNames = new StringBuilder();
        cacheNames.append("Clear caches :").append(LINE_SEPARATOR);

        cacheManager.getCacheNames().stream()
                .forEach(cacheName -> {
                    cacheManager.getCache(cacheName).clear();
                    log.info("clear cache key {}", cacheName);
                    cacheNames.append(cacheName).append(LINE_SEPARATOR);
                });


        return cacheNames.toString();
    }


    private void sendMail(String to, String title) {
        emailManagerService.sendMail(title, "This is testing body", to);
    }

}
