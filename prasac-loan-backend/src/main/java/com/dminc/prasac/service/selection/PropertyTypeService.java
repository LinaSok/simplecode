package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.PropertyType;
import com.dminc.prasac.model.entity.PropertyTypeEntity;
import com.dminc.prasac.repository.PropertyTypeRepository;

@Service
public class PropertyTypeService extends CommonService<PropertyType, PropertyTypeEntity> {

    public PropertyTypeService(PropertyTypeRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, PropertyType.class);
    }
}



