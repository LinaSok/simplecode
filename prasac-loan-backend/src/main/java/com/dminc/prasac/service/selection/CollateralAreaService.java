package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.CollateralArea;
import com.dminc.prasac.model.entity.CollateralAreaEntity;
import com.dminc.prasac.repository.CollateralAreaRepository;

@Service
public class CollateralAreaService extends CommonService<CollateralArea, CollateralAreaEntity> {

    public CollateralAreaService(CollateralAreaRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, CollateralArea.class);
    }
}



