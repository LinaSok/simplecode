package com.dminc.prasac.service;

import com.dminc.common.tools.exceptions.InvalidParametersBusinessException;
import com.dminc.prasac.model.dto.user.*;
import com.dminc.prasac.model.entity.BranchEntity;
import com.dminc.prasac.model.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

@SuppressWarnings("PMD.TooManyMethods")
public interface UserService {
    UserResponse createNewUser(NewUserRequest newUser) throws InvalidParametersBusinessException;

    User updateUser(User user);

    UserProfile getCurrentUserDetail(String name);

    void deActivateUser(long userId);

    UserResponse updateUser(UserRequest request, long userId);

    void notifyUserByEmail(String title, String content, String email);

    void resetPassword(long userId);

    UserProfile getUserProfile(Long userId);

    List<UserProfile> getUserProfile(BranchEntity branch, List<com.dminc.prasac.model.entity.UserRole> role);

    List<UserProfile> getUserProfile(List<com.dminc.prasac.model.entity.UserRole> roles);

    Page<UserProfile> searchProfile(SearchUserProfileRequest request, Pageable pageable);

    Page<AutoCompleteUser> searchProfile(String query, Pageable pageable);

    List<UserRole> getDecisionMakerGroup();

    void resetPasswordByTheProvidedPwd(String userName, String pwd);

    void reactivate(String userName);

    void activateUser(long userId);
}
