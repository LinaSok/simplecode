package com.dminc.prasac.service.loan;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dminc.prasac.model.dto.misc.CollateralMarketValueAnalyse;
import com.dminc.prasac.model.entity.CollateralMarketValueAnalyseEntity;
import com.dminc.prasac.model.entity.LoanCollateralEntity;
import com.dminc.prasac.repository.CollateralMarketValueAnalyseRepository;
import com.dminc.prasac.service.ObjectsConverter;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AppraisalCollateralMarketService {
    private final CollateralMarketValueAnalyseRepository repository;

    private final ObjectsConverter converter;

    public List<CollateralMarketValueAnalyse> saveMarkets(List<CollateralMarketValueAnalyseEntity> entities,
                                                          LoanCollateralEntity collateral) {

        setLoanToEach(collateral, entities);
        final List<CollateralMarketValueAnalyseEntity> marketEntities = saveAll(entities);
        deleteTheUnattached(marketEntities);

        return converter.getObjectsMapper().map(marketEntities, CollateralMarketValueAnalyse.class);
    }

    private List<CollateralMarketValueAnalyseEntity> saveAll(List<CollateralMarketValueAnalyseEntity> entities) {
        return repository.saveAll(entities);
    }

    private void setLoanToEach(LoanCollateralEntity collateral, List<CollateralMarketValueAnalyseEntity> entities) {
        entities.forEach(entity -> entity.setLoanCollateral(collateral));
    }

    private void deleteTheUnattached(List<CollateralMarketValueAnalyseEntity> marketEntities) {
        final List<Long> ids = marketEntities.stream().map(CollateralMarketValueAnalyseEntity::getId).filter(Objects::nonNull)
                .collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(ids)) {
            repository.deleteAllByIdNotIn(ids);
        }
    }

}
