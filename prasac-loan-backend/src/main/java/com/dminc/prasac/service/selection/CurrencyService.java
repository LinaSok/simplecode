package com.dminc.prasac.service.selection;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.Currency;
import com.dminc.prasac.model.entity.selection.CurrencyEntity;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.repository.CurrencyRepository;

@Service
public class CurrencyService extends CommonService<Currency, CurrencyEntity> {
    private final CurrencyRepository repository;

    public CurrencyService(CurrencyRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, Currency.class);
        this.repository = repository;
    }

    @Override
    @Cacheable(value = "CurrencyService.getAll", key = "#root.methodName")
    public List<Currency> findAll() throws BusinessException {
        return super.findAll();
    }

    @Cacheable(value = "getCurrencyByCode", key = "#code", condition = "#code!=null", unless = LocalConstants.RESULT_NULL)
    public Currency getByCode(String code) throws BusinessException {
        if (StringUtils.isEmpty(code)) {
            return null;
        }
        return convert(repository.findTopByCode(code));
    }

}
