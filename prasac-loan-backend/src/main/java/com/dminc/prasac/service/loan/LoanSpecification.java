package com.dminc.prasac.service.loan;

import com.dminc.prasac.model.dto.loan.SearchLoanRequest;
import com.dminc.prasac.model.dto.loan.SortingField;
import com.dminc.prasac.model.entity.BranchEntity_;
import com.dminc.prasac.model.entity.client.ClientEntity;
import com.dminc.prasac.model.entity.client.ClientEntity_;
import com.dminc.prasac.model.entity.client.ClientIdentificationEntity;
import com.dminc.prasac.model.entity.client.ClientIdentificationEntity_;
import com.dminc.prasac.model.entity.loan.ClientLoan;
import com.dminc.prasac.model.entity.loan.ClientLoan_;
import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.entity.loan.LoanEntity_;
import com.dminc.prasac.model.entity.loan.LoanStatusEntity_;
import com.dminc.prasac.model.entity.selection.ClientTypeEntity;
import com.dminc.prasac.model.entity.selection.ClientTypeEntity_;
import com.dminc.prasac.model.misc.AuditEntity_;
import com.dminc.prasac.util.QueryEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.List;

public final class LoanSpecification {

    private LoanSpecification() {
        // private constructor
    }

    public static Specification<LoanEntity> sortByRequestedDateOrFullName(SearchLoanRequest request) {
        return (Specification<LoanEntity>) (root, query, criteriaBuilder) -> {
            if (request.getSortingField() == null) {
                query.orderBy(criteriaBuilder.desc(root.get(AuditEntity_.createdDate)));
            } else {
                if (SortingField.REQUESTED_DATE.equals(request.getSortingField())) {
                    if (Sort.Direction.DESC.equals(request.getDirection())) {
                        query.orderBy(criteriaBuilder.desc(root.get(AuditEntity_.createdDate)));
                    } else {
                        query.orderBy(criteriaBuilder.asc(root.get(AuditEntity_.createdDate)));
                    }
                } else {
                    final Join<LoanEntity, ClientEntity> client = root.join(LoanEntity_.clients);
                    if (Sort.Direction.DESC.equals(request.getDirection())) {
                        query.orderBy(criteriaBuilder.desc(client.get(ClientEntity_.fullName)));
                    } else {
                        query.orderBy(criteriaBuilder.asc(client.get(ClientEntity_.fullName)));
                    }
                }
            }
            return null;
        };
    }

    public static Specification<LoanEntity> findByFreeText(String text, Long clientType) {
        return (Specification<LoanEntity>) (root, query, criteriaBuilder) -> {
            query.distinct(true);
            if (StringUtils.isEmpty(text)) {
                return null;
            }
            final Join<LoanEntity, ClientEntity> client = root.join(LoanEntity_.clients);
            final Join<ClientEntity, ClientIdentificationEntity> identification = client.join(ClientEntity_.identifications);

            final Predicate cif = criteriaBuilder.equal(client.get(ClientEntity_.cif), text);
            final Predicate name = criteriaBuilder.like(criteriaBuilder.lower(client.get(ClientEntity_.fullName)), QueryEscapeUtils.toContainingCondition(StringUtils.lowerCase(text)));
            final Predicate khmerName = criteriaBuilder.like(client.get(ClientEntity_.khmerFullName), QueryEscapeUtils.toContainingCondition(text));
            final Predicate idNumber = criteriaBuilder.equal(identification.get(ClientIdentificationEntity_.idNumber), text);
            final Predicate searchPredicate = criteriaBuilder.or(cif, name, khmerName, idNumber);
            if (clientType != null) {
                final Join<LoanEntity, ClientLoan> clientLoans = root.join(LoanEntity_.clientLoans);
                final Predicate clientTypePredicate = criteriaBuilder.equal(clientLoans.get(ClientLoan_.clientType).get(ClientTypeEntity_.id), clientType);
                return criteriaBuilder.and(searchPredicate, clientTypePredicate);
            }
            return searchPredicate;
        };
    }

    public static Specification<LoanEntity> filterByBranch(Long branchId) {
        return (Specification<LoanEntity>) (root, query, criteriaBuilder) -> {
            if (branchId != null) {
                return criteriaBuilder.equal(root.get(LoanEntity_.branch).get(BranchEntity_.id), branchId);
            }
            return null;
        };
    }

    public static Specification<LoanEntity> filterByStatus(Long statusId) {
        return (Specification<LoanEntity>) (root, query, criteriaBuilder) -> {
            if (statusId != null) {
                return criteriaBuilder.equal(root.get(LoanEntity_.loanStatus).get(LoanStatusEntity_.id), statusId);
            }
            return null;
        };
    }

    public static Specification<LoanEntity> filterByClient(Long clientId) {
        return (Specification<LoanEntity>) (root, query, criteriaBuilder) -> {
            if (clientId != null) {
                final Join<LoanEntity, ClientLoan> clientLoan = root.join(LoanEntity_.clientLoans);
                final Join<LoanEntity, ClientEntity> client = root.join(LoanEntity_.clients);
                final Join<ClientLoan, ClientTypeEntity> clientType = clientLoan.join(ClientLoan_.clientType);
                final Predicate byClientPre = criteriaBuilder.equal(client.get(ClientEntity_.id), clientId);
                final Predicate clientTypePre = criteriaBuilder.equal(clientType.get(ClientTypeEntity_.id), ClientTypeEntity.TYPE_BORROWER_ID);
                return criteriaBuilder.and(byClientPre, clientTypePre);
            }
            return null;
        };
    }

    /**
     * Filter to get pending task for RO
     *
     * @param pendingLoanId
     * @return
     */
    public static Specification<LoanEntity> filterByPendingLoanIds(List<Long> pendingLoanId) {
        return (Specification<LoanEntity>) (root, query, cb) -> {
            if (CollectionUtils.isEmpty(pendingLoanId)) {
                return null;
            }
            final Path<Long> ids = root.get(LoanEntity_.id);
            return ids.in(pendingLoanId);
        };
    }

    /**
     * RO user, when moves to new branch, he/she can view loan which was created after he join only
     *
     * @param createdDate
     * @return
     */
    public static Specification<LoanEntity> filterByCreatedDate(LocalDateTime createdDate) {
        return (Specification<LoanEntity>) (root, query, cb) -> {
            if (createdDate == null) {
                return null;
            }
            return cb.greaterThanOrEqualTo(root.get(AuditEntity_.createdDate), createdDate);
        };
    }
}
