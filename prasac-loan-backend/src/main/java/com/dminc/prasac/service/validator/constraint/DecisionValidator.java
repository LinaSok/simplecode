package com.dminc.prasac.service.validator.constraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.dminc.prasac.model.misc.LoanStatusEnum;

public class DecisionValidator implements ConstraintValidator<Decision, Long> {
    @Override
    public boolean isValid(Long loanStatus, ConstraintValidatorContext context) {
        return loanStatus == null || loanStatus.equals(LoanStatusEnum.PASSED_PRE_SCREENING.getValue()) || loanStatus
                .equals(LoanStatusEnum.FAIL_PRE_SCREENING.getValue()) || loanStatus
                .equals(LoanStatusEnum.NEW_PRE_SCREENING.getValue());

    }
}
