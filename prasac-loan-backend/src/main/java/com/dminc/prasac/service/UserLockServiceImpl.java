package com.dminc.prasac.service;

import com.dminc.prasac.model.entity.UserLock;
import com.dminc.prasac.repository.UserLockRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserLockServiceImpl implements UserLockService {

    private final UserLockRepository userLockRopsitory;

    @Override
    public UserLock findLockedUser(final String userName) {
        return this.userLockRopsitory.findByUsername(userName).orElse(UserLock.builder().build());
    }

    @Override
    public void saveLockedUser(UserLock lockedUser) {
        this.userLockRopsitory.save(lockedUser);
    }

    @Transactional
    @Override
    public void unlockUser(String userName) {
        this.userLockRopsitory.deleteByUsername(userName);
    }

}
