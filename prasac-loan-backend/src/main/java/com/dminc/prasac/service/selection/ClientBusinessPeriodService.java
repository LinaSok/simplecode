package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.ClientBusinessPeriod;
import com.dminc.prasac.model.entity.ClientBusinessPeriodEntity;
import com.dminc.prasac.repository.ClientBusinessPeriodRepository;

@Service
public class ClientBusinessPeriodService extends CommonService<ClientBusinessPeriod, ClientBusinessPeriodEntity> {

    public ClientBusinessPeriodService(ClientBusinessPeriodRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, ClientBusinessPeriod.class);
    }
}



