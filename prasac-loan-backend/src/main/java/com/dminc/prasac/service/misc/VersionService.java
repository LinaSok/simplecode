package com.dminc.prasac.service.misc;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dminc.prasac.model.dto.misc.Version;
import com.dminc.prasac.model.dto.selection.SelectionRequest;
import com.dminc.prasac.model.entity.VersionEntity;
import com.dminc.prasac.repository.VersionRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class VersionService {
    private final VersionRepository repository;

    public Version getInfo(SelectionRequest request) {
        final Optional<VersionEntity> addressVersion = repository
                .findFirstByNameAndUpdatedDateAfter(VersionEntity.ADDRESS_VALUE_OF_NAME_FIELD, request.getLastModified());

        return Version.builder().address(addressVersion.isPresent()).build();
    }
}
