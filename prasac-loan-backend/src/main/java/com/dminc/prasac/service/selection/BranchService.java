package com.dminc.prasac.service.selection;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.user.Branch;
import com.dminc.prasac.model.entity.BranchEntity;
import com.dminc.prasac.repository.BranchRepository;

@Service
public class BranchService extends CommonService<Branch, BranchEntity> {

    public BranchService(BranchRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, Branch.class);
    }

    @Override
    @Cacheable(value = "BranchService.getAll", key = "#root.methodName")
    public List<Branch> findAll() throws BusinessException {
        return super.findAll();
    }
}



