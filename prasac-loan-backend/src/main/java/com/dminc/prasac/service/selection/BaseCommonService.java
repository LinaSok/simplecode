package com.dminc.prasac.service.selection;

import java.time.LocalDateTime;
import java.util.List;

import com.dminc.common.tools.exceptions.BusinessException;

public interface BaseCommonService<D, T> {
    List<D> findAll() throws BusinessException;

    List<D> findAll(LocalDateTime lastModified) throws BusinessException;

    List<D> convert(List<T> types);

    D convert(T type);
}
