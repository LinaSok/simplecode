package com.dminc.prasac.service;

import com.dminc.common.tools.exceptions.InternalBusinessException;
import com.dminc.prasac.model.dto.user.UserProfile;
import com.dminc.prasac.model.entity.User;
import com.dminc.prasac.model.entity.client.ClientEntity;
import com.dminc.prasac.model.entity.loan.ClientLoan;
import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.entity.loan.LoanStatusEntity;
import com.dminc.prasac.model.entity.selection.CommuneEntity;
import com.dminc.prasac.model.entity.selection.CurrencyEntity;
import com.dminc.prasac.model.entity.selection.DistrictEntity;
import com.dminc.prasac.model.entity.selection.ProvinceEntity;
import com.dminc.prasac.model.entity.selection.VillageEntity;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.repository.CurrencyRepository;
import com.dminc.prasac.repository.LoanStatusRepository;
import com.dminc.prasac.repository.loan.LoanRepository;
import com.dminc.prasac.repository.selection.VillageRepository;
import com.dminc.prasac.util.EmailTemplate;
import com.dminc.prasac.util.PrasacHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
@Slf4j
@SuppressWarnings({"PMD.CyclomaticComplexity", "PMD.StdCyclomaticComplexity", "PMD.NPathComplexity"})
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class EmailParserService {

    private static final String EMAIL_TEMPLATE = "emailTemplate.xml";
    private final EmailManagerService emailManagerService;
    private final LoanRepository loanRepository;
    private final VillageRepository villageRepository;
    private EmailTemplate mailTemplate;
    private final CurrencyRepository currencyRepository;
    private final LoanStatusRepository loanStatusRepository;


    public EmailTemplate getMailTemplate() {
        if (mailTemplate != null) {
            return mailTemplate;
        }

        final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try {
            try (InputStream inputStream = classLoader.getResourceAsStream(EMAIL_TEMPLATE)) {
                log.info("Loading html content {} : inputStream not null ? {} ", EMAIL_TEMPLATE, inputStream != null);
                if (null == inputStream) {
                    log.debug("Missing email template.");
                    return null;
                }
                try {
                    //set it mailTemplate
                    mailTemplate = this.parseEmailTemplate(inputStream);
                    return mailTemplate;
                } catch (XMLStreamException e) {
                    log.debug("Cannot parse the email template xml");
                }
            }
        } catch (IOException e) {
            log.error("Missing email template.");
            throw new InternalBusinessException("Email not sent", -103, e);
        }

        return mailTemplate;
    }

    private EmailTemplate parseEmailTemplate(InputStream inputStream) throws XMLStreamException {
        EmailTemplate.EmailTemplateBuilder builder = EmailTemplate.builder();
        final XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        final XMLStreamReader r = inputFactory.createXMLStreamReader(inputStream);

        while (r.hasNext()) {
            int eventType = r.next();
            if (eventType == XMLStreamReader.START_ELEMENT && "EmailTemplate".equals(r.getLocalName())) {
                int event = r.getEventType();
                while (true) {
                    if (event == XMLStreamReader.END_ELEMENT && "EmailTemplate".equals(r.getLocalName())) {
                        break;
                    }
                    switch (event) {
                        case XMLStreamConstants.START_DOCUMENT:
                            break;
                        case XMLStreamConstants.START_ELEMENT:
                            switch (r.getLocalName()) {
                                case "CreatedNewUser":
                                    builder.createdNewUser(extractContent(r));
                                    break;
                                case "LoanProcess":
                                    builder.loanProcess(extractContent(r));
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case XMLStreamConstants.CHARACTERS:
                            break;
                        case XMLStreamConstants.END_ELEMENT:
                            break;
                        case XMLStreamConstants.END_DOCUMENT:
                            break;
                        default:
                            break;
                    }
                    if (!r.hasNext()) {
                        break;
                    }
                    event = r.next();
                }
                break;
            }
        }
        return builder.build();
    }

    private EmailTemplate.Template extractContent(XMLStreamReader r) throws XMLStreamException {
        final String title = r.getAttributeValue(null, "subject");
        final String content = getText(r);
        return EmailTemplate.Template.builder().title(title).content(content).build();
    }

    private String getText(XMLStreamReader r) throws XMLStreamException {
        int event = r.getEventType();
        while (true) {
            switch (event) {
                case XMLStreamConstants.START_DOCUMENT:
                    break;
                case XMLStreamConstants.START_ELEMENT:
                    break;
                case XMLStreamConstants.CHARACTERS:
                    if (r.isWhiteSpace()) {
                        break;
                    }
                    return r.getText();
                case XMLStreamConstants.END_ELEMENT:
                    break;
                case XMLStreamConstants.END_DOCUMENT:
                    break;

                default:
                    break;
            }

            if (!r.hasNext()) {
                break;
            }
            event = r.next();
        }
        return "";
    }

    private String replaceContent(EmailTemplate.Template template, LoanEntity loanEntity, String firstName, String comment, Optional<String> status) {
        final User user = PrasacHelper.getCurrentUser();
        assert user != null;
        final String currentUserProfile = user.getUserProfile().getFirstName();
        final ClientLoan borrowerLoan = loanEntity.getClientLoans().stream().filter(client -> client.getClientType().getId().equals(1L)).findFirst().get();

        final ClientEntity borrower = borrowerLoan.getClient();
        final String borrowerKhName = borrower.getKhmerFullName();
        final String borrowerName = borrower.getFullName();

        final Optional<ClientLoan> coBorrowerLoan = loanEntity.getClientLoans().stream().filter(client -> client.getClientType().getId().equals(2L)).findFirst();
        String coBorrowerKHName = StringUtils.EMPTY;
        String coBorrowerName = StringUtils.EMPTY;
        if (coBorrowerLoan.isPresent()) {
            final ClientEntity coBorrower = coBorrowerLoan.get().getClient();
            coBorrowerKHName = coBorrower.getKhmerFullName();
            coBorrowerName = coBorrower.getFullName();
        }

        final VillageEntity village = villageRepository.getOne(borrower.getCurrentVillage().getId());
        final CommuneEntity communeEntity = village.getCommune();
        final DistrictEntity districtEntity = communeEntity.getDistrict();
        final ProvinceEntity province = districtEntity.getProvince();

        final String currentAddress = String.format("%s, %s, %s, %s, %s, %s", borrower.getCurrentAddressHouseNo(), borrower.getCurrentAddressStreet(),
                village.getName(), communeEntity.getName(), districtEntity.getName(), province.getName());

        final LoanStatusEntity statusEntity = loanStatusRepository.getOne(loanEntity.getLoanStatus().getId());

        final CurrencyEntity currencyEntity = currencyRepository.findById(loanEntity.getCurrency().getId()).orElse(null);

        //Never expect currency to be null
        return template.getContent().replace(LocalConstants.EMAIL_CONTENT_EMAIL_USER_NAME, firstName)
                .replace(LocalConstants.EMAIL_CONTENT_ASSIGNED_FROM_USER, currentUserProfile)
                .replace(LocalConstants.EMAIL_CONTENT_BORROWER_KH_NAME, borrowerKhName)
                .replace(LocalConstants.EMAIL_CONTENT_BORROWER_NAME, borrowerName)
                .replace(LocalConstants.EMAIL_CONTENT_LOAN_ID, loanEntity.getId().toString())
                .replace(LocalConstants.EMAIL_CONTENT_CURRENT_ADDRESS, currentAddress)
                .replace(LocalConstants.EMAIL_CONTENT_COMMENT, Optional.ofNullable(comment).orElse(""))
                .replace(LocalConstants.EMAIL_CONTENT_LOAN_CURRENCY, currencyEntity == null ? "" : currencyEntity.getCurrency())
                .replace(LocalConstants.EMAIL_CONTENT_LOAN_AMOUNT, this.formatLoanAmount(loanEntity.getLoanAmount()))
                .replace(LocalConstants.EMAIL_CONTENT_CO_BORROWER_KH_NAME, StringUtils.isEmpty(coBorrowerKHName) ? "" : " & " + coBorrowerKHName)
                .replace(LocalConstants.EMAIL_CONTENT_CO_BORROWER_NAME, StringUtils.isEmpty(coBorrowerName) ? "" : " & " + coBorrowerName)
                .replace(LocalConstants.EMAIL_CONTENT_LOAN_STATUS, status.orElse(statusEntity.getStatus()));

    }


    private String formatLoanAmount(Double loanAmount) {
        NumberFormat nf = NumberFormat.getInstance(new Locale("en_US"));
        return nf.format(loanAmount);
    }

    private String replaceTitle(LoanEntity loanEntity, final Optional<String> status) {
        final LoanStatusEntity statusEntity = loanStatusRepository.getOne(loanEntity.getLoanStatus().getId());
        final CurrencyEntity currencyEntity = currencyRepository.findById(loanEntity.getCurrency().getId()).orElse(CurrencyEntity.builder().build());

        final ClientLoan borrowerLoan = loanEntity.getClientLoans().stream().filter(client -> client.getClientType().getId().equals(1L)).findFirst().get();
        final ClientEntity borrower = borrowerLoan.getClient();
        final String borrowerName = borrower.getFullName();

        return String.format("%s, %s %s, %s", borrowerName, this.formatLoanAmount(loanEntity.getLoanAmount()),
                currencyEntity.getCurrency(), status.orElseGet(() -> statusEntity.getStatus()));
    }

    private EmailTemplate.Template getLoanProcessTemplate() {
        final EmailTemplate emailTemplate = this.getMailTemplate();
        return emailTemplate.getLoanProcess();
    }

    public void notifyUser(final Long id, final UserProfile profile, final String comment, final Optional<String> status) {
        final LoanEntity loanEntity = loanRepository.getOne(id);
        final EmailTemplate.Template template = this.getLoanProcessTemplate();
        final String content = this.replaceContent(template, loanEntity, profile.getFirstName(), comment, status);
        final String title = this.replaceTitle(loanEntity, status);
        if (profile.getIsEnabled()) {
            emailManagerService.sendMail(title, content, profile.getEmail());
        }
    }

    public void notifyUsers(final Long id, final List<UserProfile> profiles, final String comment, final Optional<String> status) {
        profiles.forEach(profile -> this.notifyUser(id, profile, comment, status));
    }
}
