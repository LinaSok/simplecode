package com.dminc.prasac.service.client;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.common.tools.exceptions.ItemNotFoundBusinessException;
import com.dminc.common.tools.mapping.Mapper;
import com.dminc.prasac.config.Redis;
import com.dminc.prasac.integration.domain.CrmClient;
import com.dminc.prasac.integration.service.CrmClientService;
import com.dminc.prasac.model.dto.loan.ClientRequest;
import com.dminc.prasac.model.dto.misc.Client;
import com.dminc.prasac.model.dto.misc.MaritalStatus;
import com.dminc.prasac.model.dto.misc.Nationality;
import com.dminc.prasac.model.dto.misc.SearchResultClient;
import com.dminc.prasac.model.entity.client.ClientEntity;
import com.dminc.prasac.model.entity.client.ClientIdentificationEntity;
import com.dminc.prasac.model.entity.selection.VillageEntity;
import com.dminc.prasac.model.misc.LoanStatusEnum;
import com.dminc.prasac.model.request.SearchClientRequest;
import com.dminc.prasac.repository.ClientLoanRepository;
import com.dminc.prasac.repository.ClientRepository;
import com.dminc.prasac.service.ClientIdentificationService;
import com.dminc.prasac.service.ObjectsConverter;
import com.dminc.prasac.service.selection.LocationService;
import com.dminc.prasac.service.selection.MaritalStatusService;
import com.dminc.prasac.service.selection.NationalityService;
import com.dminc.prasac.util.QueryEscapeUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
@SuppressWarnings({"PMD.TooManyMethods"})
public class ClientServiceImpl implements ClientService {
    private final ObjectsConverter converter;

    private final ClientRepository repository;
    private final ClientLoanRepository clientLoanRepository;

    private final CrmClientService crmClientService;
    private final NationalityService nationalityService;
    private final LocationService locationService;
    private final ClientIdentificationService identificationService;
    private final MaritalStatusService maritalStatusService;
    private final Redis redis;
    @Value("${prasac.search.cache.time-out:5}")
    private int cacheTimeout;

    @Transactional(readOnly = true)
    @Override
    public Optional<Client> getById(Long id) {
        final Optional<ClientEntity> client = repository.findById(id);
        return Optional.ofNullable(converter.runServiceTask((Mapper mapper) -> mapper.map(client.orElse(null), Client.class)));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Client> getByCif(String cif) {
        final Optional<ClientEntity> client = repository.findFirstByCif(cif);
        return Optional.ofNullable(converter.runServiceTask((Mapper mapper) -> mapper.map(client.orElse(null), Client.class)));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Client> search(SearchClientRequest request, Pageable pageable) throws BusinessException {
        final String cacheKey = getSearchClientKey(request);
        if (!redis.hasKey(cacheKey)) {
            final List<CrmClient> clientInfos = crmClientService.searchClient(request);
            final List<Client> clients = clientInfos.stream().map(CrmClient::toClient).collect(Collectors.toList());

            if (StringUtils.isEmpty(request.getClientCif())) {
                clients.addAll(searchClients(request));
            }
            if (CollectionUtils.isNotEmpty(clients)) {
                redis.put(cacheKey, clients, cacheTimeout, TimeUnit.MINUTES);
            }
        }
        final int skip = pageable.getPageNumber() * pageable.getPageSize();
        final List<Client> totalClients = (List<Client>) redis.get(cacheKey);

        final List<Client> clients = org.apache.commons.collections4.CollectionUtils.emptyIfNull(totalClients)
                .stream()
                .skip(skip)
                .limit(pageable.getPageSize())
                .collect(Collectors.toList());
        return new PageImpl<>(clients, pageable, org.apache.commons.collections4.CollectionUtils.size(totalClients));
    }

    private String getSearchClientKey(SearchClientRequest request) {
        if (StringUtils.isNotEmpty(request.getClientCif())) {
            return String.format("search_client_result_%s", request.getClientCif());
        }
        return String.format("search_client_result_%s_%s_%s_%s",
                request.getName(),
                request.getKhmerName(),
                request.getIdNumber(),
                request.getDob() == null ? "" : request.getDob().toString());
    }

    private List<Client> searchClients(SearchClientRequest request) {
        final String fullName = StringUtils.normalizeSpace(Objects.toString(QueryEscapeUtils.toContainingCondition(request.getName()), ""));
        final String khmerName = StringUtils.normalizeSpace(Objects.toString(QueryEscapeUtils.toContainingCondition(request.getKhmerName()), ""));
        final String idNumber = Objects.toString(request.getIdNumber(), "");
        final List<ClientEntity> clientEntities = repository.searchClients(fullName, khmerName, request.getDob(), idNumber, LoanStatusEnum.NEWLY_ASSIGNED.getValue());

        return clientEntities.stream().map(this::convertClientToSearchResult)
                .collect(Collectors.toList());
    }

    @Override
    @Cacheable(value = "getClientByCifFromCrmCache", key = "#request")
    @Transactional(readOnly = true)
    public SearchResultClient getClientByCifFromCrm(ClientRequest request) throws BusinessException {
        if (StringUtils.isNotEmpty(request.getClientCif())) {
            return crmClientService.getByCif(request.getClientCif()).map(crmClient -> {
                final SearchResultClient client = crmClient.toClient();
                final MaritalStatus maritalStatus = maritalStatusService.getByCode(crmClient.getMaritalStatus());
                client.setMaritalStatus(maritalStatus == null ? null : maritalStatus.getId());

                return client;
            }).orElseThrow(() -> new ItemNotFoundBusinessException("CIF not found"));
        } else {
            final Optional<ClientEntity> optionalClient = repository.findById(request.getClientId());
            return optionalClient.map(this::convertClientToSearchResult).orElseThrow(() -> new ItemNotFoundBusinessException("Client not found"));
        }
    }

    @SuppressWarnings({"PMD.NPathComplexity"})
    private SearchResultClient convertClientToSearchResult(ClientEntity clientEntity) {
        final SearchResultClient client = converter.getObjectsMapper().map(clientEntity, SearchResultClient.class);
        final ClientIdentificationEntity identificationEntity = clientEntity.getIdentifications().get(0);
        client.setIdType(identificationEntity.getClientIdentificationType().getCode());
        client.setIdNumber(identificationEntity.getIdNumber());
        final VillageEntity currentVillage = clientEntity.getCurrentVillage();
        final String address = String.format("%s %s %s, %s %s %s %s",
                clientEntity.getCurrentAddressHouseNo() == null ? "N/A" : clientEntity.getCurrentAddressHouseNo(),
                clientEntity.getCurrentAddressStreet() == null ? "N/A" : clientEntity.getCurrentAddressStreet(),
                clientEntity.getCurrentAddressGroupNo() == null ? "N/A" : clientEntity.getCurrentAddressGroupNo(),
                currentVillage.getName(),
                currentVillage.getCommune().getName(),
                currentVillage.getCommune().getDistrict().getName(),
                currentVillage.getCommune().getDistrict().getProvince().getName());
        client.setAddress(address);
        setClientBranch(clientEntity, client);
        client.setRawMaritalStatus(clientEntity.getMaritalStatus() == null ? "" : clientEntity.getMaritalStatus().getStatus());
        return client;
    }
    private void setClientBranch(ClientEntity clientEntity, SearchResultClient client) {
        if (StringUtils.isBlank(client.getCif())) {
            Optional.ofNullable(client.getId()).ifPresent(clientId -> {
                clientLoanRepository.getFirstByClient_IdOrderByCreatedDateDesc(clientId).ifPresent(clientLoan -> {
                    Optional.ofNullable(clientLoan.getLoan()).ifPresent(loan -> {
                        Optional.ofNullable(loan.getBranch()).ifPresent(branch -> client.setBranch(branch.getCode()));
                    });
                });
            });
        } else {
            client.setBranch(clientEntity.getBranch() == null ? "" : clientEntity.getBranch().getCode());
        }
    }

    @Override
    @Cacheable(value = "getDetailByCifFromCrmCache", key = "#cif")
    @Transactional(readOnly = true)
    public Client getDetailByCifFromCrm(String cif) throws BusinessException {
        return crmClientService.getByCif(cif).map(this::convert)
                .orElseThrow(() -> new ItemNotFoundBusinessException("CIF not found"));
    }

    private Client convert(CrmClient crmClient) throws BusinessException {
        Client client = crmClient.toClient();
        final Optional<ClientEntity> optionalClient = repository.findFirstByCif(crmClient.getCif());
        optionalClient.ifPresent(c -> client.setId(c.getId()));
        final Nationality nationality = nationalityService.findByCode(crmClient.getNationality());
        client.setNationality(nationality == null ? null : nationality.getId());
        client.setIdentifications(
                identificationService.verifyIdentity(crmClient.getIdentityNumber(), crmClient.getIdentityType()));
        final VillageEntity village = locationService
                .findVillageEntityByLocationCodes(crmClient.getVillageCode(), crmClient.getCommuneCode(),
                        crmClient.getDistrictCode(), crmClient.getProvinceCode());
        final MaritalStatus maritalStatus = maritalStatusService.getByCode(crmClient.getMaritalStatus());
        client.setMaritalStatus(maritalStatus == null ? null : maritalStatus.getId());
        if (village != null) {
            client.setCurrentVillage(village.getId());
            client.setCurrentCommune(village.getCommune().getId());
            client.setCurrentDistrict(village.getCommune().getDistrict().getId());
            client.setCurrentProvince(village.getCommune().getDistrict().getProvince().getId());
        }
        return client;
    }
}
