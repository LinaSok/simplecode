package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.CollateralDescription;
import com.dminc.prasac.model.entity.CollateralDescriptionEntity;
import com.dminc.prasac.repository.CollateralDescriptionRepository;

@Service
public class CollateralDescriptionService extends CommonService<CollateralDescription, CollateralDescriptionEntity> {

    public CollateralDescriptionService(CollateralDescriptionRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, CollateralDescription.class);
    }
}



