package com.dminc.prasac.service.selection;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.CollateralTitleType;
import com.dminc.prasac.model.entity.CollateralTitleTypeEntity;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.repository.CollateralTitleTypeRepository;

@Service
public class CollateralTitleTypeService extends CommonService<CollateralTitleType, CollateralTitleTypeEntity> {
    private final CollateralTitleTypeRepository repository;

    public CollateralTitleTypeService(CollateralTitleTypeRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, CollateralTitleType.class);
        this.repository = repository;
    }

    @Cacheable(value = "collateralTitleTypeByCode", key = "#code", condition = "#code!=null", unless = LocalConstants.RESULT_NULL)
    public CollateralTitleType getByCode(String code) throws BusinessException {
        if (StringUtils.isEmpty(code)) {
            return null;
        }
        return convert(repository.findTopByCode(code));
    }
}
