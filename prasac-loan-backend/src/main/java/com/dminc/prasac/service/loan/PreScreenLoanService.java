package com.dminc.prasac.service.loan;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import com.dminc.prasac.model.dto.user.UserProfile;
import com.dminc.prasac.model.entity.BranchEntity;
import com.dminc.prasac.model.entity.UserPermission;
import com.dminc.prasac.model.entity.UserRole;
import com.dminc.prasac.model.entity.client.ClientEntity;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.model.request.ClientPreScreenRequest;
import com.dminc.prasac.repository.*;
import com.dminc.prasac.repository.loan.LoanRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.common.tools.mapping.Mapper;
import com.dminc.prasac.model.dto.loan.Loan;
import com.dminc.prasac.model.entity.LoanRequestTypeEntity;
import com.dminc.prasac.model.entity.User;
import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.entity.loan.LoanStatusEntity;
import com.dminc.prasac.model.misc.LoanStatusEnum;
import com.dminc.prasac.model.misc.RequestedFrom;
import com.dminc.prasac.model.request.PreScreenRequest;
import com.dminc.prasac.service.EmailParserService;
import com.dminc.prasac.service.ObjectsConverter;
import com.dminc.prasac.service.UserService;
import com.dminc.prasac.util.PrasacHelper;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PreScreenLoanService {
    private final UserProfileRepository profileRepository;
    private final LoanRepository loanRepository;
    private final ClientRepository clientRepository;
    private final ClientLoanRepository clientLoanRepository;

    private final ObjectsConverter converter;
    private final ClientLoanCrudService clientLoanCrudService;
    private final UserPermissionRepository userPermissionRepository;
    private final UserRoleRepository userRoleRepository;
    private final UserService userService;
    private final EmailParserService emailService;

    public Loan createPreScreen(PreScreenRequest request, RequestedFrom agent) throws BusinessException {
        deleteNewClients(request, agent);
        LoanEntity entity = converter.runServiceTask((Mapper mapper) -> mapper.map(request, LoanEntity.class));

        populateLoanRequestType(entity);
        populateBranchAndAssignedUser(agent, entity);
        populateLoanStatus(request, agent, entity);
        populateSubLists(entity);

        Loan loan = clientLoanCrudService.savePreScreen(request, entity);
        if (RequestedFrom.BACK_OFFICE.equals(agent)) {
            //Email notify BM group
            Optional<UserPermission> permission = userPermissionRepository.findById(LocalConstants.RECIEVE_PRESCREENING_PERMISSION_ID);
            if (permission.isPresent()) {
                final List<UserRole> userRoles = userRoleRepository.findDistinctByPermissionsIn(Collections.singletonList(permission.get()));
                final BranchEntity branchEntity = BranchEntity.builder().id(request.getBranch()).build();
                final List<UserProfile> profiles = userService.getUserProfile(branchEntity, userRoles);
                emailService.notifyUsers(loan.getId(), profiles, Strings.EMPTY, Optional.empty());
            }
        }
        return loan;
    }

    // Delete new clients with those in loan that has status newly assigned, to avoid duplicate client data which was created by BO without reviewing
    // This only does when Tablet asking for
    private void deleteNewClients(PreScreenRequest request, RequestedFrom agent) {
        Optional.ofNullable(request.getId()).ifPresent(loanId -> {
            loanRepository.getFirstById(loanId).ifPresent(loanEntity -> {
                if (loanEntity.getLoanStatus().getId().equals(LoanStatusEnum.NEWLY_ASSIGNED.getValue()) && RequestedFrom.TABLET.equals(agent)) {
                    List<Long> requestClientIds = request.getClients().stream().map(ClientPreScreenRequest::getId).filter(Objects::nonNull).collect(Collectors.toList());

                    List<Long> clientIds = clientLoanRepository.getAllByLoan(loanEntity).stream().map(clientLoan -> {
                        if (Objects.nonNull(clientLoan.getClient()) && !hasClient(requestClientIds, clientLoan.getClient())) {
                            return clientLoan.getClient().getId();
                        }
                        return null;
                    }).filter(Objects::nonNull).collect(Collectors.toList());

                    clientRepository.deleteAllByIdIn(clientIds);
                }
            });
        });
    }

    private boolean hasClient(List<Long> requestClientIds, ClientEntity clientEntity) {
        return requestClientIds.stream().anyMatch(clientId -> clientId.equals(clientEntity.getId()));
    }

    public void populateSubLists(LoanEntity entity) {
        if (Objects.nonNull(entity.getId())) {
            entity.setBusinesses(emptyIfNull(entity.getBusinesses()));
            entity.setProjects(emptyIfNull(entity.getProjects()));
            entity.setLiabilityCredits(emptyIfNull(entity.getLiabilityCredits()));
            entity.setCollateralList(emptyIfNull(entity.getCollateralList()));
        }
    }

    private static <T> List<T> emptyIfNull(List<T> list) {
        return CollectionUtils.isEmpty(list) ? Arrays.asList() : list;
    }

    /**
     * LoanEntity here not yet saved in Database.
     *
     * @param request
     * @param agent
     * @param entity
     */
    private void populateLoanStatus(PreScreenRequest request, RequestedFrom agent, LoanEntity entity) {
        // We populate loan status only for back office
        // Because from tablet they will have to select pass or fail status
        if (RequestedFrom.BACK_OFFICE.equals(agent) && request.getLoanStatus() == null) {
            entity.setLoanStatus(LoanStatusEntity.builder().id(LoanStatusEnum.NEW_PRE_SCREENING.getValue()).build());
        }
    }

    private void populateLoanRequestType(LoanEntity entity) {
        entity.setLoanRequestType(LoanRequestTypeEntity.builder().id(LoanRequestTypeEntity.NEW_LOAN_ID).build());
    }

    private void populateBranchAndAssignedUser(RequestedFrom agent, LoanEntity entity) {
        if (RequestedFrom.TABLET.equals(agent)) {
            // populate assigned user
            final User user = User.builder().id(PrasacHelper.getCurrentUserId()).build();
            entity.setAssignedUser(user);

            // populate branch
            final com.dminc.prasac.model.entity.UserProfile profile = profileRepository.findByUser(user);
            entity.setBranch(profile.getBranch());
        }
    }

}
