package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.LoanStatus;
import com.dminc.prasac.model.entity.loan.LoanStatusEntity;
import com.dminc.prasac.repository.LoanStatusRepository;

@Service
public class LoanStatusService extends CommonService<LoanStatus, LoanStatusEntity> {
    public LoanStatusService(LoanStatusRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, LoanStatus.class);
    }
}
