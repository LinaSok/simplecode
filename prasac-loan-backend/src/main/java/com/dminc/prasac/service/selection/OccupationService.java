package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.ClientOccupation;
import com.dminc.prasac.model.entity.ClientOccupationEntity;
import com.dminc.prasac.repository.ClientOccupationRepository;

@Service
public class OccupationService extends CommonService<ClientOccupation, ClientOccupationEntity> {

    public OccupationService(ClientOccupationRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, ClientOccupation.class);
    }
}
