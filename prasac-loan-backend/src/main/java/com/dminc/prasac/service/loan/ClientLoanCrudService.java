package com.dminc.prasac.service.loan;

import com.dminc.prasac.model.dto.loan.ClientRequest;
import com.dminc.prasac.model.dto.loan.Loan;
import com.dminc.prasac.model.dto.misc.Client;
import com.dminc.prasac.model.dto.misc.SearchResultClient;
import com.dminc.prasac.model.entity.client.ClientEntity;
import com.dminc.prasac.model.entity.loan.ClientLoan;
import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.entity.selection.ClientTypeEntity;
import com.dminc.prasac.model.request.LoanAppraisalRequest;
import com.dminc.prasac.model.request.PreScreenRequest;
import com.dminc.prasac.repository.ClientLoanRepository;
import com.dminc.prasac.repository.ClientRepository;
import com.dminc.prasac.repository.loan.LoanRepository;
import com.dminc.prasac.service.ObjectsConverter;
import com.dminc.prasac.service.client.ClientService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@SuppressWarnings("PMD.TooManyMethods")
public class ClientLoanCrudService {
    private final ClientLoanRepository clientLoanRepository;
    private final ClientRepository clientRepository;
    private final LoanRepository loanRepository;

    private final ObjectsConverter converter;
    private final ClientService clientService;

    public Loan savePreScreen(PreScreenRequest request, LoanEntity loanEntity) {
        return save(loanEntity, toClientRequests(request.getClients()), true);
    }

    @Transactional
    public Loan saveAppraisal(LoanAppraisalRequest request, LoanEntity loanEntity) {
        return save(loanEntity, toClientRequests(request.getClients()), false);
    }

    private <T> List<Client> toClientRequests(List<T> requests) {
        return converter.getObjectsMapper().map(requests, Client.class);
    }

    public Loan save(LoanEntity loanEntityRequest, List<Client> clientRequests, boolean isPreScreen) {
        loanEntityRequest.getClients().forEach(clientEntity -> {
            populateShortName(clientEntity);
            saveOrMergeClients(clientEntity, isPreScreen);
        });

        int resubmit = loanEntityRequest.getResubmitCounter() == null ? 0 : loanEntityRequest.getResubmitCounter();
        if (resubmit == 0 && !isPreScreen) {
            loanEntityRequest.setRequestDate(LocalDateTime.now());
        }
        final LoanEntity loanEntityInDB = loanRepository.save(loanEntityRequest);
        linkClientsToLoan(clientRequests, loanEntityInDB);
        unlinkClientsFromLoan(loanEntityInDB);

        return converter.getObjectsMapper().map(loanEntityInDB, Loan.class);
    }

    private void linkClientsToLoan(List<Client> clientRequests, LoanEntity loanEntityInDB) {
        loanEntityInDB.getClients().forEach(clientEntity -> createClientLoan(clientRequests, loanEntityInDB, clientEntity));
    }

    private void createClientLoan(List<Client> clientRequests, LoanEntity loanEntityInDB, ClientEntity clientEntity) {
        final Optional<ClientLoan> byClientAndLoan = clientLoanRepository.getByClientAndLoan(clientEntity, loanEntityInDB);
        if (byClientAndLoan.isPresent()) {
            findClientRequestByIdentityNumber(clientRequests, clientEntity).ifPresent(clientByIdentityNumber -> {
                final ClientTypeEntity clientTypeEntity = ClientTypeEntity.builder().id(clientByIdentityNumber.getClientType()).build();
                final ClientLoan clientLoan = byClientAndLoan.get();
                clientLoan.setClientType(clientTypeEntity);
                clientLoanRepository.save(clientLoan);
            });
        } else {
            findClientRequestByIdentityNumber(clientRequests, clientEntity).ifPresent(clientByIdentityNumber ->
                    clientLoanRepository.save(populateClientLoan(loanEntityInDB, clientEntity, clientByIdentityNumber)));
        }
    }

    private ClientLoan populateClientLoan(LoanEntity loanEntityInDB, ClientEntity clientEntity, Client clientByIdentityNumber) {
        final ClientTypeEntity clientTypeEntity = ClientTypeEntity.builder().id(clientByIdentityNumber.getClientType()).build();
        return ClientLoan.builder().clientType(clientTypeEntity).client(clientEntity).loan(loanEntityInDB).build();
    }

    private Optional<Client> findClientRequestByIdentityNumber(List<Client> clientRequests, ClientEntity clientEntity) {
        return clientRequests.stream().filter(clientRequest -> clientRequest.getIdentifications().stream().anyMatch(identity -> {
            final String firstIdentityNumber = clientEntity.getIdentifications().get(0).getIdNumber();
            return identity.getIdNumber().equals(firstIdentityNumber);
        })).findFirst();
    }

    private void saveOrMergeClients(ClientEntity entity, boolean isPreScreen) {
        final Consumer<ClientEntity> clientEntityConsumer = fromDB -> populateAndKeepClientDataInDB(entity, isPreScreen, fromDB);
        if (Objects.isNull(entity.getId())) {
            if (StringUtils.isNotBlank(entity.getCif())) {
                clientRepository.findFirstByCif(entity.getCif()).ifPresent(clientEntityConsumer);
            }
        } else {
            clientRepository.findById(entity.getId()).ifPresent(clientEntityConsumer);
        }
        clientRepository.save(entity);

    }

    private void populateAndKeepClientDataInDB(ClientEntity entity, boolean isPreScreen, ClientEntity fromDB) {
        entity.setId(fromDB.getId());
        entity.setLoans(fromDB.getLoans());

        // If Pre Screen we need to keep some client info, otherwise it will override to null
        if (isPreScreen) {
            entity.setIncomeAmount(fromDB.getIncomeAmount());
            entity.setNoOfActiveChildren(fromDB.getNoOfActiveChildren());
            entity.setNoOfChildren(fromDB.getNoOfChildren());
            entity.setNoOfChildrenAgeGreaterThan18(fromDB.getNoOfChildrenAgeGreaterThan18());
            entity.setPeriodInCurrentAddress(fromDB.getPeriodInCurrentAddress());
            entity.setWorkingPeriod(fromDB.getWorkingPeriod());
            entity.setWorkplaceGroupNo(fromDB.getWorkplaceGroupNo());
            entity.setWorkplaceHouseNo(fromDB.getWorkplaceHouseNo());
            entity.setWorkplaceName(fromDB.getWorkplaceName());
            entity.setWorkplaceStreet(fromDB.getWorkplaceStreet());
            entity.setClientOccupation(fromDB.getClientOccupation());
            entity.setClientRelationship(fromDB.getClientRelationship());
            entity.setClientResidentStatus(fromDB.getClientResidentStatus());
            entity.setClientWorkingTime(fromDB.getClientWorkingTime());
            entity.setMaritalStatus(fromDB.getMaritalStatus());
            entity.setBirthVillage(fromDB.getBirthVillage());
            entity.setWorkplaceVillage(fromDB.getWorkplaceVillage());
        }
    }

    private void populateShortName(ClientEntity entity) {
        if (StringUtils.isNotEmpty(entity.getCif()) && StringUtils.isEmpty(entity.getShortName())) {
            final ClientRequest request = ClientRequest.builder()
                    .clientCif(entity.getCif())
                    .build();
            final SearchResultClient client = clientService.getClientByCifFromCrm(request);
            entity.setShortName(client.getShortName());
        }
    }

    private void unlinkClientsFromLoan(LoanEntity loanEntity) {
        final List<Long> clientIds = loanEntity.getClients().stream().map(clientEntity -> clientEntity.getId())
                .filter(Objects::nonNull).collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(clientIds)) {
            clientLoanRepository.deleteAllByLoanAndClient_IdNotIn(loanEntity, clientIds);
            setClientLoanReferences(loanEntity, clientIds);
        }
    }

    private void setClientLoanReferences(LoanEntity loanEntity, List<Long> clientIds) {
        loanEntity.setClientLoans(clientLoanRepository.getAllByLoanAndClient_IdIn(loanEntity, clientIds));
        loanEntity.getClients().stream().forEach(clientEntity -> {
            clientEntity.setClientLoans(clientLoanRepository.getAllByClient(clientEntity));
        });
    }

}
