package com.dminc.prasac.service.selection;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.DocumentType;
import com.dminc.prasac.model.entity.DocumentTypeEntity;
import com.dminc.prasac.repository.DocumentTypeRepository;

@Service
public class DocumentTypeService extends CommonService<DocumentType, DocumentTypeEntity> {

    public DocumentTypeService(DocumentTypeRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, DocumentType.class);
    }

    @Override
    @Cacheable(value = "DocumentTypeService.getAll", key = "#root.methodName")
    public List<DocumentType> findAll() throws BusinessException {
        return super.findAll();
    }
}
