package com.dminc.prasac.service.loan;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.common.tools.exceptions.InvalidParametersBusinessException;
import com.dminc.common.tools.exceptions.ItemNotFoundBusinessException;
import com.dminc.common.tools.mapping.Mapper;
import com.dminc.prasac.model.dto.CommentResponse;
import com.dminc.prasac.model.dto.loan.Loan;
import com.dminc.prasac.model.dto.loan.SearchLoanRequest;
import com.dminc.prasac.model.dto.loan.SearchLoanResponse;
import com.dminc.prasac.model.dto.misc.AssignLoanRequest;
import com.dminc.prasac.model.dto.misc.LoanStatus;
import com.dminc.prasac.model.dto.user.UserProfile;
import com.dminc.prasac.model.entity.ApprovalState;
import com.dminc.prasac.model.entity.BranchEntity;
import com.dminc.prasac.model.entity.LoanApprovalState;
import com.dminc.prasac.model.entity.User;
import com.dminc.prasac.model.entity.UserPermission;
import com.dminc.prasac.model.entity.UserProfileLogEntity;
import com.dminc.prasac.model.entity.UserRole;
import com.dminc.prasac.model.entity.client.ClientEntity;
import com.dminc.prasac.model.entity.loan.CommentEntity;
import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.entity.loan.LoanStatusEntity;
import com.dminc.prasac.model.entity.selection.ClientTypeEntity;
import com.dminc.prasac.model.misc.LoanStatusEnum;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.model.misc.RequestedFrom;
import com.dminc.prasac.model.request.LoanAppraisalRequest;
import com.dminc.prasac.model.request.PreScreenRequest;
import com.dminc.prasac.repository.CommentRepository;
import com.dminc.prasac.repository.LoanApprovalStateRepository;
import com.dminc.prasac.repository.LoanStatusRepository;
import com.dminc.prasac.repository.UserPermissionRepository;
import com.dminc.prasac.repository.UserProfileLogRepository;
import com.dminc.prasac.repository.UserRoleRepository;
import com.dminc.prasac.repository.loan.LoanRepository;
import com.dminc.prasac.service.BusinessError;
import com.dminc.prasac.service.EmailParserService;
import com.dminc.prasac.service.ObjectsConverter;
import com.dminc.prasac.service.UserService;
import com.dminc.prasac.service.client.LoanValidator;
import com.dminc.prasac.service.selection.BranchService;
import com.dminc.prasac.service.selection.CurrencyService;
import com.dminc.prasac.util.LoanStatusHelper;
import com.dminc.prasac.util.PrasacHelper;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
@SuppressFBWarnings("BC_UNCONFIRMED_CAST_OF_RETURN_VALUE")
@SuppressWarnings({"PMD.TooManyMethods", "PMD.GodClass", "PMD.TooManyFields", "PMD.CyclomaticComplexity", "PMD.AvoidDeeplyNestedIfStmts"})
public class LoanServiceImpl implements LoanService {

    private final LoanRepository repository;
    private final ObjectsConverter converter;
    private final LoanValidator loanValidator;
    private final CurrencyService currencyService;
    private final BranchService branchService;
    private final UserService userService;
    private final CommentRepository commentRepository;
    private final EmailParserService emailService;
    private final AppraisalLoanService appraisalLoanService;
    private final PreScreenLoanService preScreenLoanService;
    private final LoanApprovalService loanApprovalService;
    private final UserPermissionRepository userPermissionRepository;
    private final UserRoleRepository userRoleRepository;
    private final LoanStatusRepository loanStatusRepository;
    private final UserProfileLogRepository userProfileLogRepository;
    private final LoanApprovalStateRepository loanApprovalStateRepository;

    @Transactional
    @Override
    public Loan createPreScreen(PreScreenRequest request, RequestedFrom agent) throws BusinessException {
        loanValidator.validatePreScreen(request, agent);

        try {
            omitClientIdentityIDs(request);
            return preScreenLoanService.createPreScreen(request, agent);
        } catch (EntityNotFoundException | JpaObjectRetrievalFailureException e) {
            BusinessError.OBJECT_NOT_FOUND.throwExceptionHttpStatus404();
        }
        return null;
    }

    private void omitClientIdentityIDs(PreScreenRequest loan) {
        CollectionUtils.emptyIfNull(loan.getClients()).forEach(clientEntity -> {
            CollectionUtils.emptyIfNull(clientEntity.getIdentifications()).forEach(cid -> cid.setId(null));
        });
    }

    private void omitClientIdentityIDs(LoanAppraisalRequest loan) {
        CollectionUtils.emptyIfNull(loan.getClients()).forEach(clientEntity -> {
            CollectionUtils.emptyIfNull(clientEntity.getIdentifications()).forEach(cid -> cid.setId(null));
        });
    }

    @Transactional
    @Override
    public Loan submitLoanAppraisal(LoanAppraisalRequest request) {
        log.info("validate loan appraisal data");
        loanValidator.validateAppraisal(request);
        try {
            omitClientIdentityIDs(request);
            return appraisalLoanService.submitLoanAppraisal(request);
        } catch (EntityNotFoundException | JpaObjectRetrievalFailureException e) {
            BusinessError.OBJECT_NOT_FOUND.throwExceptionHttpStatus404();
        }
        return null;
    }

    @Transactional
    @Override
    public void assignBack(Long id, String comment) {
        LoanEntity loanEntity = repository.getOne(id);
        LoanStatusEntity loanStatusEntity = loanStatusRepository.getOne(LoanStatusEnum.TO_BE_CORRECTED.getValue());
        loanEntity.setLoanStatus(loanStatusEntity);
        repository.save(loanEntity);

        //save comment
        final CommentEntity commentEntity = CommentEntity.builder().comment(comment).loan(loanEntity).userRole(loanEntity.getUserRole()).build();
        commentRepository.save(commentEntity);

        //Notify by email
        final User coUSer = loanEntity.getAssignedUser();
        final UserProfile profile = userService.getUserProfile(coUSer.getId());

        emailService.notifyUser(id, profile, comment, Optional.empty());
    }

    @Override
    @Transactional
    @SuppressWarnings("PMD.PreserveStackTrace")
    public Loan assign(Long id, AssignLoanRequest request) throws BusinessException {
        if (request.getUserId() == null && request.getBranchId() == null) {
            throw new InvalidParametersBusinessException("At least branch, user or group of dission makers must be present in request");
        }
        try {
            log.info("Get loan by id {}", id);
            final LoanEntity loan = repository.getOne(id);
            if (request.getBranchId() != null) {
                loanValidator.validateAssignedBranch(request.getBranchId());
                final BranchEntity branch = BranchEntity.builder().id(request.getBranchId()).build();
                loan.setBranch(branch);
                final LoanStatusEntity loanStatusEntity = loanStatusRepository.findById(LoanStatusEnum.NEW_PRE_SCREENING.getValue()).get();
                loan.setLoanStatus(loanStatusEntity);

                //Email notify BM group
                Optional<UserPermission> permission = userPermissionRepository.findById(LocalConstants.RECIEVE_PRESCREENING_PERMISSION_ID);
                if (permission.isPresent()) {
                    final List<UserRole> userRoles = userRoleRepository.findDistinctByPermissionsIn(Collections.singletonList(permission.get()));
                    final List<UserProfile> profiles = userService.getUserProfile(branch, userRoles);
                    emailService.notifyUsers(id, profiles, Strings.EMPTY, Optional.empty());
                }
            }
            if (request.getUserId() != null) {
                loanValidator.validateAssignedUser(request.getUserId());
                final User user = User.builder().id(request.getUserId()).build();
                loan.setAssignedUser(user);
                final LoanStatusEntity loanStatusEntity = loanStatusRepository.findById(LoanStatusEnum.NEWLY_ASSIGNED.getValue()).get();
                loan.setLoanStatus(loanStatusEntity);
                //Email notify user
                final UserProfile profile = userService.getUserProfile(request.getUserId());
                emailService.notifyUser(id, profile, Strings.EMPTY, Optional.empty());

            }
            log.info("Assign loan to branch {} user {}", request.getBranchId(), request.getUserId());
            final LoanEntity loanObject = repository.save(loan);
            return converter.runServiceTask((Mapper mapper) -> mapper.map(loanObject, Loan.class));
        } catch (EntityNotFoundException e) {
            BusinessError.OBJECT_NOT_FOUND.throwExceptionHttpStatus404(String.format("Loan not found id %s", id));
        }
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public Loan getById(Long id) throws BusinessException {
        log.info("Get loan by Id = {}", id);
        final LoanEntity loanEntity = repository.getFirstById(id).orElseThrow(() -> new ItemNotFoundBusinessException("Loan not found"));
        Loan loan = converter.getObjectsMapper().map(loanEntity, Loan.class);

        log.info("Find out what permission current user has");
        final int committeeLevel = loanEntity.getCommitteeLevel() == null ? 0 : loanEntity.getCommitteeLevel();
        final List<Integer> userCommitteeLevel = PrasacHelper.getUserLevels();

        //Special case, when Review Level 3 submit, they need to select either CC2 or CC3 but the loan will pending at LOA_REVIEW
        if (committeeLevel == LocalConstants.LEVEL_LAO_REVIEW) {
            UserPermission permission = userPermissionRepository.findById(LocalConstants.REVIEW_LEVEL_4).get();
            List<UserRole> userRoles = userRoleRepository.findDistinctByPermissionsIn(Collections.singletonList(permission));
            AppraisalLoanService.setLoanStatusText(userRoles, loan);
        }

        if (!userCommitteeLevel.contains(committeeLevel)) {
            loan.setLoanStatus(0L);
        }

        if (committeeLevel == LocalConstants.LEVEL_CC2) {
            log.info("User committee level is same as loan committee level.");
            log.info("Check if he already took action");
            final User user = PrasacHelper.getCurrentUser();
            final LoanApprovalState loanApprovalState = loanApprovalStateRepository.findByLoanAndCreatedBy(loanEntity, user.getUsername());
            boolean isMatch = user.getRoles().stream().anyMatch(userRole -> userRole.getId().equals(loanEntity.getUserRole().getId()));
            log.info("User already made decision. Set state 0 to disable BO button");
            if (loanApprovalState != null || !isMatch) {
                loan.setLoanStatus(0L);
            }
        }
        log.info("======== Get loan by id done ========");
        return loan;
    }

    private List<Long> getPendingTasks(final List<String> pendingIds) {
        if (CollectionUtils.isNotEmpty(pendingIds)) {
            //Merge pending list id together for query
            log.info("Merging pending {} list id together for query", pendingIds);
            final List<String> filterEmpty = pendingIds.stream().filter(StringUtils::isNotEmpty).collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(filterEmpty)) {
                final String tasks = String.join(",", filterEmpty);
                return Arrays.stream(tasks.split(",")).map(Long::valueOf).collect(Collectors.toList());
            }
        }
        return Collections.emptyList();
    }

    @Override
    @Transactional(readOnly = true)
    public Page<SearchLoanResponse> search(SearchLoanRequest request, Pageable pageable) {
        final User user = PrasacHelper.getCurrentUser();
        final com.dminc.prasac.model.entity.UserProfile profile = user.getUserProfile();
        List<Long> pendingTasks = null;
        UserProfileLogEntity lastBranch = null;
        request.setBorrowerType(ClientTypeEntity.TYPE_BORROWER_ID);

        if (!PrasacHelper.hasViewAllBranchPermission()) {
            /**
             * RO user has difference permission. RO user is the user who switch branch alot.
             * RO user is also the CC1 so, he/she will might have pending task from previous branch.
             * Once he/she switch branch, he/she will have to carry over his tasks from previous branch so he/she can finish it.
             *
             * We have function to store the pending tasks when RO user profile updated.
             * In this function, we filter out any branch in the log which has the same branch as the current branch.
             * Then pick the last one from previous branch so we can get the date which he joined current branch to filter
             * because RO can see the loan in the branch starting from the date he joined only.
             */
            if (PrasacHelper.hasROPermission()) {
                log.info("Current user has RO permission. Retrieving his pending tasks");
                final List<UserProfileLogEntity> logEntities = userProfileLogRepository.findByUserProfileIdAndIsRO(profile.getId(), true);
                log.info("Filter out the current branch from the log entity");
                final List<UserProfileLogEntity> filteredLogEntities = logEntities.stream().filter(log -> !log.getBranchId().equals(profile.getBranch().getId())).collect(Collectors.toList());
                log.info("Remaining branch {} which is not same as current branch", CollectionUtils.emptyIfNull(filteredLogEntities).size());
                //Got list of string off loan id with comma separated
                final List<String> pendingIds = CollectionUtils.emptyIfNull(filteredLogEntities).stream().map(UserProfileLogEntity::getPendingLoanId).collect(Collectors.toList());
                pendingTasks = this.getPendingTasks(pendingIds);
                filteredLogEntities.sort(Comparator.comparing(UserProfileLogEntity::getCreatedDate).reversed());
                if (CollectionUtils.isNotEmpty(filteredLogEntities)) {
                    lastBranch = filteredLogEntities.get(filteredLogEntities.size() - 1);
                }
            }
            if (PrasacHelper.hasViewInBranchPermission() || PrasacHelper.hasROPermission()) {
                if (request.getBranch() == null) {
                    request.setBranch(profile.getBranch().getId());
                }
            } else if (PrasacHelper.hasBRReviewPermission()) {
                request.setBranch(profile.getBranch().getId());
                request.setStatus(LoanStatusEnum.PENDING.getValue());
            } else if (PrasacHelper.hasBMPermission()) {
                request.setBranch(profile.getBranch().getId());
            } else {
                BusinessError.UNAUTHORIZED_REQUEST.throwExceptionHttpStatus403();
            }
        }

        final Specification<LoanEntity> specification = Specification
                .where(LoanSpecification.findByFreeText(request.getSearchText(), request.getBorrowerType()))
                .and(LoanSpecification.filterByBranch(request.getBranch()))
                .and(LoanSpecification.filterByStatus(request.getStatus()))
                .and(LoanSpecification.sortByRequestedDateOrFullName(request))
                .and(LoanSpecification.filterByCreatedDate(lastBranch == null ? null : lastBranch.getCreatedDate()))
                .or(LoanSpecification.filterByPendingLoanIds(pendingTasks));

        final Page<LoanEntity> loanEntities = repository.findAll(specification, pageable);

        final List<SearchLoanResponse> loans = loanEntities.getContent().stream().map(this::convertToSearchLoanResponse)
                .filter(Objects::nonNull).collect(Collectors.toList());
        return new PageImpl<>(loans, pageable, loanEntities.getTotalElements());
    }

    /**
     * Get the list of the new assigned pre-screening
     *
     * @param lastModified
     * @param pageable
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public Page<Loan> getAssignedLoan(final LocalDateTime lastModified, Pageable pageable) {
        final Optional<User> user = Optional.ofNullable(PrasacHelper.getCurrentUser());

        if (!user.isPresent()) {
            return new PageImpl(Collections.emptyList());
        }
        final LoanStatusEntity loanStatusEntity = LoanStatusEntity.builder().id(LoanStatusEnum.NEWLY_ASSIGNED.getValue()).build();
        final Page<LoanEntity> loanEntities = lastModified == null
                ? repository.findByAssignedUserAndBranchAndLoanStatusIn(user.get(), user.get().getUserProfile().getBranch(), Arrays.asList(loanStatusEntity), pageable)
                : repository.findByAssignedUserAndBranchAndLoanStatusInAndUpdatedDateAfter(user.get(), user.get().getUserProfile().getBranch(), Arrays.asList(loanStatusEntity), lastModified, pageable);

        return loanEntities.map(loanEntity -> {
            Loan loan = converter.getObjectsMapper().map(loanEntity, Loan.class);
            loan.getClients().stream().forEach(client -> client.setId(null));

            return loan;
        });
    }

    /**
     * After the pre-screening form passed-pre-screening, Tablet user will call this function to update their loans status
     *
     * @param lastModified
     * @param pageable
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public Page<Loan> getMyList(final LocalDateTime lastModified, Pageable pageable) {
        final Optional<User> user = Optional.ofNullable(PrasacHelper.getCurrentUser());
        final LoanStatusEntity newlyAssigned = loanStatusRepository.getOne(LoanStatusEnum.NEWLY_ASSIGNED.getValue());
        final LoanStatusEntity failPreScreening = loanStatusRepository.getOne(LoanStatusEnum.FAIL_PRE_SCREENING.getValue());
        if (!user.isPresent()) {
            return new PageImpl(Collections.emptyList());
        }

        final Page<LoanEntity> loanEntities = lastModified == null
                ? repository.findByAssignedUserAndBranchAndLoanStatusNotIn(user.get(), user.get().getUserProfile().getBranch(), Arrays.asList(newlyAssigned, failPreScreening), pageable)
                : repository.findByAssignedUserAndBranchAndUpdatedDateAfterAndLoanStatusNotIn(user.get(), user.get().getUserProfile().getBranch(), lastModified, Arrays.asList(newlyAssigned, failPreScreening), pageable);

        return loanEntities.map(loanEntity -> converter.getObjectsMapper().map(loanEntity, Loan.class));
    }


    private SearchLoanResponse convertToSearchLoanResponse(LoanEntity entity) {
        // don't need to check null as there is at least borrower in loan
        return entity.getClients().stream().filter(client -> filterClientByBorrowerType(entity)).findFirst()
                .map(client -> getBuildSearchLoanResponse(entity, client)).orElse(null);
    }

    @Override
    public boolean filterClientByBorrowerType(LoanEntity entity) {
        return CollectionUtils.emptyIfNull(entity.getClientLoans()).stream()
                .anyMatch(clientLoan -> clientLoan.getClientType().getId().equals(ClientTypeEntity.TYPE_BORROWER_ID));
    }

    private SearchLoanResponse getBuildSearchLoanResponse(LoanEntity entity, ClientEntity client) {
        UserRole assignedGroup = entity.getUserRole();
        final int committeeLevel = entity.getCommitteeLevel() == null ? 0 : entity.getCommitteeLevel();
        if (committeeLevel == LocalConstants.LEVEL_LAO_REVIEW) {
            final UserPermission permission = userPermissionRepository.findById(LocalConstants.REVIEW_LEVEL_4).orElse(null);
            final List<UserRole> userRoles = userRoleRepository.findDistinctByPermissionsIn(Collections.singletonList(permission));
            final Optional<UserRole> userRole = CollectionUtils.emptyIfNull(userRoles).stream().findFirst();
            if (userRole.isPresent()) {
                assignedGroup = userRole.get();
            } else {
                //Just debugging purposes. Can be removed later once the app stable
                log.debug("Cannot find the any group for LOA_REVIEW");
            }
        }


        return SearchLoanResponse.builder().clientCif(client.getCif()).clientFullname(client.getFullName())
                .clientKhmerFullname(client.getKhmerFullName()).loanId(entity.getId()).loanAmount(entity.getLoanAmount())
                .createdDate(entity.getCreatedDate().toLocalDate()).currency(currencyService.convert(entity.getCurrency()))
                .loanStatus(convertLoanStatus(entity.getLoanStatus())).branch(branchService.convert(entity.getBranch()))
                .loanStatusText(LoanStatusHelper.getStatusText(entity.getLoanStatus(), assignedGroup))
                .updatedDate(entity.getUpdatedDate().toLocalDate())
                .requestDate(entity.getRequestDate() == null ? null : entity.getRequestDate().toLocalDate()).build();
    }

    private LoanStatus convertLoanStatus(LoanStatusEntity entity) {
        return converter.runServiceTask((Mapper mapper) -> mapper.map(entity, LoanStatus.class));
    }

    @Transactional
    @Override
    public List<CommentResponse> getComments(Long id, final LocalDateTime lastModified) {
        final LoanEntity loan = repository.getOne(id);
        List<CommentEntity> commentEntities;
        if (lastModified == null) {
            commentEntities = commentRepository.getAllByLoanOrderByCreatedDateDesc(loan);
        } else {
            commentEntities = commentRepository.getAllByLoanAndCreatedDateAfterOrderByCreatedDateDesc(loan, lastModified);
        }
        List<CommentResponse> responses = converter.getObjectsMapper().map(commentEntities, CommentResponse.class);
        return responses.stream().map(response -> {
            final String userGroup = response.getUserRole().getName();
            response.setGroupName(userGroup);
            return response;
        }).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public Loan committeeSubmitLoan(Long id, String comment, Optional<Long> groupId) {
        final LoanEntity loanEntity = repository.getOne(id);
        final Integer committeeLevel = loanEntity.getCommitteeLevel();
        if (Objects.nonNull(committeeLevel) && committeeLevel == LocalConstants.LEVEL_CC2) {
            BusinessError.LOAN_REACH_MAX_LEVEL.throwExceptionHttpStatus400();
        }

        if (groupId.isPresent() && committeeLevel < LocalConstants.COMMITTEE_LEVEL_3) {
            BusinessError.COMMITTEE_LEVEL_NOT_ACCEPTABLE.throwExceptionHttpStatus400();
        }

        Loan loan = appraisalLoanService.submitNextLevel(id, comment, groupId);
        final int currentUserCommitteeLevel = PrasacHelper.getUserCommitteeLevel();
        if (currentUserCommitteeLevel < loan.getCommitteeLevel()) {
            loan.setLoanStatus(0L);
        }
        return loan;
    }

    @Transactional
    @Override
    public Loan rejectLoan(Long id, String comment, boolean isRejectedByClient) {
        final LoanEntity loanEntity = repository.getOne(id);
        if (isRejectedByClient) {
            loanEntity.setLoanStatus(LoanStatusEntity.builder().id(LoanStatusEnum.REJECTED_BY_CLIENT.getValue()).build());
            return converter.getObjectsMapper().map(repository.saveAndFlush(loanEntity), Loan.class);
        }
        this.validCommitteeLevel(loanEntity);
        loanApprovalService.saveApprovalState(loanEntity, comment, ApprovalState.REJECTED);

        final LoanEntity updatedLoan = repository.getOne(id);
        return updateLoanStatus(comment, updatedLoan);
    }

    @Transactional
    @Override
    public Loan completeReject(final Long id) {
        final LoanEntity loanEntity = repository.getOne(id);
        loanEntity.setLoanStatus(LoanStatusEntity.builder().id(LoanStatusEnum.COMPLETE_REJECTED.getValue()).build());
        return converter.getObjectsMapper().map(repository.saveAndFlush(loanEntity), Loan.class);
    }

    /**
     * Depends on committee level.
     * If rejected/approved by cc1, email send to only TL & CO. Suspended will notify to only CO
     *
     * @param entity
     */
    private void sendNotificationEmail(LoanEntity entity, final String comment, Optional<String> status, final int committeeLevel) {
        final User user = entity.getAssignedUser();
        final UserProfile userProfile = converter.getObjectsMapper().map(user.getUserProfile(), UserProfile.class);
        final Long statusId = entity.getLoanStatus().getId();
        switch (committeeLevel) {
            case LocalConstants.NON_COMMITTEE_LEVEL:
                if (LoanStatusEnum.getInstance(statusId).equals(LoanStatusEnum.SUSPENDED)) {
                    log.info("Sending email to CO only");
                    emailService.notifyUser(entity.getId(), userProfile, comment, status);
                }
                break;
            case LocalConstants.LEVEL_CC1:
                if (!(LoanStatusEnum.getInstance(statusId).equals(LoanStatusEnum.SUSPENDED)
                        || LoanStatusEnum.getInstance(statusId).equals(LoanStatusEnum.IN_PROGRESS))) {
                    // send email to TL & CO
                    log.info("Sending email to TL and CO");
                    final Optional<UserPermission> permission = userPermissionRepository.findById(LocalConstants.PRINT_TEMPLATE);
                    final List<UserProfile> profiles = getProfileForEmail(entity, userProfile, permission);
                    emailService.notifyUsers(entity.getId(), profiles, comment, status);
                }
                break;
            case LocalConstants.LEVEL_CC2:
                if (!(LoanStatusEnum.getInstance(statusId).equals(LoanStatusEnum.SUSPENDED)
                        || LoanStatusEnum.getInstance(statusId).equals(LoanStatusEnum.IN_PROGRESS))) {
                    //Send to BM, TL & CO
                    log.info("Sending email to BM, TL, and CO");
                    final List<UserPermission> permissions = userPermissionRepository.findDistinctByIdIn(Arrays.asList(LocalConstants.PRINT_TEMPLATE, LocalConstants.RECIEVE_PRESCREENING_PERMISSION_ID));
                    final List<UserRole> roles = userRoleRepository.findDistinctByPermissionsIn(permissions);
                    List<UserProfile> profiles = addCOProfileForEmail(entity, userProfile, roles);
                    emailService.notifyUsers(entity.getId(), profiles, comment, status);
                }
                break;
            case LocalConstants.COMMITTEE_LEVEL_3:
                if (LoanStatusEnum.getInstance(statusId).equals(LoanStatusEnum.SUSPENDED)) {
                    log.debug("LOAN suspended by CC2 or CC3");
                    log.info("Sending email to CO and BM");
                    final Optional<UserPermission> permission = userPermissionRepository.findById(LocalConstants.RECIEVE_PRESCREENING_PERMISSION_ID);
                    final List<UserProfile> profiles = getProfileForEmail(entity, userProfile, permission);
                    emailService.notifyUsers(entity.getId(), profiles, comment, status);

                } else {
                    log.debug("Committee level 3 is auto submit from CC1 when cannot make decision");
                    log.info("Send email CC1_HO_REVIEW");
                    final UserRole role = entity.getUserRole();
                    final List<UserProfile> profiles = userService.getUserProfile(Collections.singletonList(role));
                    emailService.notifyUsers(entity.getId(), profiles, comment, status);
                }
                break;
            default:
                log.debug("Committee level {} not belong to CC1 or CC2", committeeLevel);
                break;
        }
    }

    private List<UserProfile> addCOProfileForEmail(LoanEntity entity, UserProfile userProfile, List<UserRole> roles) {
        if (roles == null) {
            return Collections.singletonList(userProfile);
        }
        List<UserProfile> profiles = userService.getUserProfile(entity.getBranch(), roles);
        if (profiles == null) {
            profiles = new ArrayList<>();
        }
        profiles.add(userProfile);
        return profiles;
    }

    private List<UserProfile> getProfileForEmail(LoanEntity entity, UserProfile userProfile, Optional<UserPermission> permission) {
        if (permission.isPresent()) {
            final List<UserRole> userRoles = userRoleRepository.findDistinctByPermissionsIn(Collections.singletonList(permission.get()));
            return addCOProfileForEmail(entity, userProfile, userRoles);
        }
        return this.addCOProfileForEmail(entity, userProfile, null);
    }

    @Transactional
    @Override
    public Loan approvedLoan(Long id, String comment) {
        final LoanEntity loanEntity = repository.getOne(id);
        this.validCommitteeLevel(loanEntity);
        loanApprovalService.saveApprovalState(loanEntity, comment, ApprovalState.APPROVED);

        final LoanEntity updatedLoan = repository.getOne(id);
        return updateLoanStatus(comment, updatedLoan);
    }

    private Loan updateLoanStatus(String comment, LoanEntity loanEntity) {
        final LoanStatusEntity status = loanEntity.getLoanStatus();
        final int committeeLevel = loanEntity.getCommitteeLevel();

        final LoanEntity loan = loanApprovalService.updateLoanStatus(loanEntity);
        //Get committee level from the updated loan cause the approval can change the committee level
        final int updatedCommitteeLevel = loan.getCommitteeLevel();
        final Loan response = converter.getObjectsMapper().map(loan, Loan.class);

        if (!loan.getLoanStatus().equals(status) || committeeLevel != updatedCommitteeLevel) {
            //Reset approval data if loan suspended so CC can approve again
            final Long statusId = loan.getLoanStatus().getId();
            if (LoanStatusEnum.getInstance(statusId).equals(LoanStatusEnum.SUSPENDED)) {
                //delete all records from committee approval for the loan
                loanApprovalStateRepository.deleteByLoan_Id(loan.getId());
            }
            this.sendNotificationEmail(loan, comment, Optional.ofNullable(response.getLoanStatusText()), updatedCommitteeLevel);
        }
        //After update loan status in db, set response to 0 to make decision button disabled
        response.setLoanStatus(0L);
        return response;
    }

    @Transactional
    @Override
    public Loan suspendedLoan(Long id, String comment) {
        log.info("===== Suspending Loan =====");
        final LoanEntity loanEntity = repository.getOne(id);
        this.validCommitteeLevel(loanEntity);
        loanApprovalService.saveApprovalState(loanEntity, comment, ApprovalState.SUSPENDED);
        final LoanEntity updatedLoan = repository.getOne(id);
        return this.updateLoanStatus(comment, updatedLoan);
    }

    private void validCommitteeLevel(final LoanEntity loanEntity) {
        log.debug("Check current user role");
        final User user = PrasacHelper.getCurrentUser();
        final List<UserRole> role = user.getRoles();
        if (loanEntity.getCommitteeLevel() == null || loanEntity.getCommitteeLevel() < LocalConstants.LEVEL_CC1) {
            BusinessError.PENDING_ERROR.throwExceptionHttpStatus400();
        }
        log.debug("Check current user permission");
        List<UserPermission> currentUserPermissions = new ArrayList<>();
        for (UserRole userRole : role) {
            currentUserPermissions.addAll(userRole.getPermissions());
        }

        boolean isCC1 = currentUserPermissions.stream().anyMatch(permission -> permission.getId().equals(LocalConstants.REVIEW_LEVEL_2));
        boolean isDecisionMaker = currentUserPermissions.stream().anyMatch(permission -> permission.getId().equals(LocalConstants.DECISION_MAKER));

        if (!(isCC1 || isDecisionMaker) || (isCC1 && loanEntity.getCommitteeLevel() > LocalConstants.LEVEL_CC1)) {
            BusinessError.COMMITTEE_LEVEL_NOT_ACCEPTABLE.throwExceptionHttpStatus400();
        }

        if (isDecisionMaker && !isCC1 && loanEntity.getCommitteeLevel() < LocalConstants.LEVEL_CC2) {
            BusinessError.COMMITTEE_LEVEL_NOT_ACCEPTABLE.throwExceptionHttpStatus400();
        }
    }
}
