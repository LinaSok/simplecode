package com.dminc.prasac.service.loan;

import com.dminc.common.tools.exceptions.InvalidParametersBusinessException;
import com.dminc.common.tools.mapping.Mapper;
import com.dminc.prasac.model.dto.loan.Loan;
import com.dminc.prasac.model.dto.misc.CollateralDescription;
import com.dminc.prasac.model.dto.misc.LoanCollateral;
import com.dminc.prasac.model.dto.user.UserProfile;
import com.dminc.prasac.model.entity.User;
import com.dminc.prasac.model.entity.UserPermission;
import com.dminc.prasac.model.entity.UserRole;
import com.dminc.prasac.model.entity.loan.CommentEntity;
import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.entity.loan.LoanStatusEntity;
import com.dminc.prasac.model.misc.LoanStatusEnum;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.model.request.LoanAppraisalRequest;
import com.dminc.prasac.repository.CommentRepository;
import com.dminc.prasac.repository.UserPermissionRepository;
import com.dminc.prasac.repository.UserRoleRepository;
import com.dminc.prasac.repository.loan.LoanRepository;
import com.dminc.prasac.service.EmailParserService;
import com.dminc.prasac.service.ObjectsConverter;
import com.dminc.prasac.service.UserService;
import com.dminc.prasac.util.LoanStatusHelper;
import com.dminc.prasac.util.PrasacHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@SuppressWarnings({"PMD.StdCyclomaticComplexity", "PMD.CyclomaticComplexity", "PMD.NPathComplexity"})
public class AppraisalLoanService {

    private static final int REVIEW_LEVEL_1 = 1;
    private static final int REVIEW_LEVEL_2 = 2;
    public static final int REVIEW_LEVEL_3 = 3;
    private static final int REVIEW_LEVEL_4 = 4;
    private static final int REVIEW_LEVEL_5 = 5;
    private final ObjectsConverter converter;
    private final LoanRepository repository;
    private final ClientLoanCrudService clientLoanCrudService;
    private final CommentRepository commentRepository;
    private final UserRoleRepository userRoleRepository;
    private final UserPermissionRepository userPermissionRepository;
    private final UserService userService;
    private final EmailParserService emailService;

    public Loan submitLoanAppraisal(LoanAppraisalRequest request) {
        log.info("submitLoanAppraisal");

        final LoanEntity loanInDB = repository.getOne(request.getId());
        List<LoanCollateral> listCollateral = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(request.getCollateralList())) {
            listCollateral.addAll(request.getCollateralList());
        }
        if (CollectionUtils.isNotEmpty(request.getOtherAssetsList())) {
            // merge list other-asset to collateral list with description id =2
            request.getOtherAssetsList().forEach(c -> c.setCollateralDescription(CollateralDescription.OTHER_ASSETS_DESC));
            listCollateral.addAll(request.getOtherAssetsList());
        }
        //set back to request
        request.setCollateralList(listCollateral);

        LoanEntity loanEntity = converter.runServiceTask((Mapper mapper) -> mapper.map(request, LoanEntity.class));
        log.info("populateResubmitCounter");
        populateResubmitCounter(loanInDB, loanEntity);
        log.info("populateLoanStatus");
        populateLoanStatus(loanEntity, loanInDB);
        log.info("keepDataInDB");
        keepDataInDB(loanInDB, loanEntity);

        log.info("saveAppraisal - loanEntity");
        final Loan loan = clientLoanCrudService.saveAppraisal(request, loanEntity);

        //Notify BR_REVIEW send mail
        log.info("Notify BR_REVIEW send mail");
        final Optional<UserPermission> permission = userPermissionRepository.findById(LocalConstants.REVIEW_LEVEL_1);
        if (permission.isPresent()) {
            final List<UserRole> userRoles = userRoleRepository.findDistinctByPermissionsIn(Collections.singletonList(permission.get()));
            final List<UserProfile> profiles = userService.getUserProfile(loanEntity.getBranch(), userRoles);
            final String loanStatusText = LoanStatusHelper.getStatusText(loanEntity.getLoanStatus(), userRoles.stream().findFirst().orElse(null));
            emailService.notifyUsers(loanEntity.getId(), profiles, Strings.EMPTY, Optional.ofNullable(loanStatusText));
        }

        log.info("===saveAppraisal completed===");
        return loan;
    }

    // Populate fields (no inputs from request) from DB, otherwise it could override to null
    private void keepDataInDB(LoanEntity fromDB, LoanEntity loanEntity) {
        loanEntity.setAssignedUser(fromDB.getAssignedUser());
        loanEntity.setBranch(fromDB.getBranch());
        loanEntity.setRequestDate(fromDB.getRequestDate());
        loanEntity.setCreatedDate(fromDB.getCreatedDate());
    }

    private UserRole getReviewLevelOneUser() {
        final UserPermission permission = userPermissionRepository.findById(LocalConstants.REVIEW_LEVEL_1).get();
        List<UserRole> userRoles = userRoleRepository.findDistinctByPermissionsIn(Collections.singletonList(permission));
        Optional<UserRole> userRole = userRoles.stream().findFirst();
        if (userRole.isPresent()) {
            return userRole.get();
        }
        log.info("======== No user group for review level 1. ========");
        return null;
    }

    private void populateResubmitCounter(LoanEntity loanInDB, LoanEntity loanEntity) {
        Integer counter = loanInDB.getResubmitCounter();
        if (loanInDB.getLoanStatus().getId().equals(LoanStatusEnum.TO_BE_CORRECTED.getValue())
                || loanInDB.getLoanStatus().getId().equals(LoanStatusEnum.SUSPENDED.getValue())) {
            final Integer resubmitCounter = loanInDB.getResubmitCounter();
            counter = Objects.isNull(resubmitCounter) ? 1 : resubmitCounter + 1;
        }
        loanEntity.setResubmitCounter(counter);
    }


    /**
     * Set the committee level base on the data in database.
     * Committee level can be null, 0 or > 0
     * Committee level == 0, when the Review Level 3 suspended the loan
     * Committee level > 0, when Review Level 4 suspended the loan
     * <p>
     * Status will always be pending. The status text to be displayed depends on the User Role in Loan entity
     *
     * @param loanEntity
     * @param fromDB
     */
    private void populateLoanStatus(LoanEntity loanEntity, LoanEntity fromDB) {
        loanEntity.setLoanStatus(LoanStatusEntity.builder().id(LoanStatusEnum.PENDING.getValue()).build());
        int level = fromDB.getCommitteeLevel() == null ? 0 : fromDB.getCommitteeLevel();
        if (level == 0) {
            loanEntity.setCommitteeLevel(REVIEW_LEVEL_1);
            loanEntity.setUserRole(this.getReviewLevelOneUser());
        } else {
            loanEntity.setCommitteeLevel(fromDB.getCommitteeLevel());
            if (level == LocalConstants.COMMITTEE_LEVEL_3) {
                //get CC1_HO_REVIEW
                final UserPermission permission = userPermissionRepository.findById(LocalConstants.REVIEW_LEVEL_3).orElse(null);
                final List<UserRole> userRoles = userRoleRepository.findDistinctByPermissionsIn(Collections.singletonList(permission));
                log.debug("User group for review level 3: {}", org.apache.commons.collections4.CollectionUtils.emptyIfNull(userRoles).size());
                loanEntity.setUserRole(userRoles.stream().findFirst().get());
            } else {
                loanEntity.setUserRole(fromDB.getUserRole());
            }
        }
    }

    /**
     * Submit the loan appraisal for next level check.
     * If next level <= 3, the BO doesn't need to select the group which mean it will auto assign in this function
     * But if the level already reach 4, means REVIEW_LEVEL 3 already selected which group to assign the loan to,
     * so we don't need to find the group
     */
    @Transactional
    public Loan submitNextLevel(final Long loanId, final String comment, final Optional<Long> groupId) {
        final User user = PrasacHelper.getCurrentUser();
        log.info("Submitting loan to next level from: {}", user.getUsername());
        LoanEntity loan = repository.getOne(loanId);
        final int currentCommitteeLevel = loan.getCommitteeLevel();
        final int nextLevel = currentCommitteeLevel + 1;
        log.info("Getting committee level");
        final UserRole committeeRole = loan.getUserRole();
        log.info("Set committee next level to {}", nextLevel);
        loan.setCommitteeLevel(nextLevel);
        UserPermission permission;
        List<UserRole> userRoles = null;
        UserRole assignedUserGroup = null;
        switch (nextLevel) {
            case REVIEW_LEVEL_2:
                permission = userPermissionRepository.findById(LocalConstants.REVIEW_LEVEL_2).get();
                userRoles = userRoleRepository.findDistinctByPermissionsIn(Collections.singletonList(permission));
                loan.setUserRole(userRoles.stream().findFirst().orElse(null));
                break;
            case REVIEW_LEVEL_3:
                permission = userPermissionRepository.findById(LocalConstants.REVIEW_LEVEL_3).get();
                userRoles = userRoleRepository.findDistinctByPermissionsIn(Collections.singletonList(permission));
                loan.setUserRole(userRoles.stream().findFirst().orElse(null));
                break;
            case REVIEW_LEVEL_4:
                if (!groupId.isPresent()) {
                    throw new InvalidParametersBusinessException("Review level 3 must select either assign to cc2 or cc3");
                }
                assignedUserGroup = userRoleRepository.findById(groupId.get()).orElse(null);
                loan.setUserRole(assignedUserGroup);
                break;
            default:
                log.debug("Next level is {}", nextLevel);
        }
        //update status
        loan.setLoanStatus(LoanStatusEntity.builder().id(LoanStatusEnum.PENDING.getValue()).build());

        final LoanEntity updatedLoan = repository.saveAndFlush(loan);

        //save comments
        final CommentEntity loanComment = CommentEntity.builder().comment(comment).loan(updatedLoan).userRole(committeeRole)
                .build();
        commentRepository.saveAndFlush(loanComment);

        Loan loanResponse = converter.getObjectsMapper().map(updatedLoan, Loan.class);

        //Special case, when Review Level 3 submit, they need to select either CC2 or CC3 but the loan will pending at LOA_REVIEW
        if (nextLevel == 4) {
            permission = userPermissionRepository.findById(LocalConstants.REVIEW_LEVEL_4).get();
            userRoles = userRoleRepository.findDistinctByPermissionsIn(Collections.singletonList(permission));
            setLoanStatusText(userRoles, loanResponse);
        }

        //LOA_REVIEW submit to CC2
        if (nextLevel == 5) {
            log.debug("Send notification to CC2/CC3 group");
            final List<UserProfile> profiles = userService.getUserProfile(Collections.singletonList(loan.getUserRole()));
            emailService.notifyUsers(updatedLoan.getId(), profiles, comment, Optional.of(loanResponse.getLoanStatusText()));
        }

        if (assignedUserGroup == null) {
            //Email Committee notification
            if (CollectionUtils.isNotEmpty(userRoles)) {
                //CC1 to CC1_HO_REVIEW wont care about branch
                if (nextLevel == LocalConstants.COMMITTEE_LEVEL_3) {
                    final List<UserProfile> profiles = userService.getUserProfile(userRoles);
                    emailService.notifyUsers(updatedLoan.getId(), profiles, comment, Optional.of(loanResponse.getLoanStatusText()));
                } else {
                    final List<UserProfile> profiles = userService.getUserProfile(updatedLoan.getBranch(), userRoles);
                    emailService.notifyUsers(updatedLoan.getId(), profiles, comment, Optional.of(loanResponse.getLoanStatusText()));
                }
            }
        } else {
            //send email to LOA_HO
            final List<UserProfile> loaHoProfile = userService.getUserProfile(userRoles);
            //CC2/CC3 profiles
            final List<UserProfile> profiles = userService.getUserProfile(Collections.singletonList(assignedUserGroup));
            profiles.addAll(loaHoProfile);
            emailService.notifyUsers(updatedLoan.getId(), profiles, comment, Optional.of(loanResponse.getLoanStatusText()));
        }

        log.info("Find out what permission current user has");
        final int userCommitteeLevel = PrasacHelper.getUserCommitteeLevel();
        log.info("User Committee level {}", userCommitteeLevel);

        //Just to make BO disable submit button.
        if (nextLevel > userCommitteeLevel || nextLevel == LocalConstants.LEVEL_CC2) {
            loanResponse.setLoanStatus(0L);
        }

        return loanResponse;
    }

    public static void setLoanStatusText(List<UserRole> userRoles, Loan loanResponse) {
        String roleName = "";
        if (CollectionUtils.isNotEmpty(userRoles)) {
            final Optional<UserRole> userRole = userRoles.stream().findFirst();
            if (userRole.isPresent()) {
                roleName = userRole.get().getName();
            }
        }
        loanResponse.setLoanStatusText(String.format("PENDING AT %s", roleName));
    }
}
