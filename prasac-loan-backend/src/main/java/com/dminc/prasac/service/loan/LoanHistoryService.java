package com.dminc.prasac.service.loan;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.common.tools.mapping.Mapper;
import com.dminc.prasac.integration.domain.PrasacLoanHistory;
import com.dminc.prasac.integration.domain.PrasacLoanHistoryInfo;
import com.dminc.prasac.integration.domain.PrasacLoanRequestHistory;
import com.dminc.prasac.integration.service.PrasacLoanService;
import com.dminc.prasac.model.dto.loan.Loan;
import com.dminc.prasac.model.dto.loan.LoanHistory;
import com.dminc.prasac.model.dto.loan.ClientRequest;
import com.dminc.prasac.model.dto.loan.LoanRequestHistory;
import com.dminc.prasac.model.dto.misc.Client;
import com.dminc.prasac.model.dto.misc.ClientLiabilityCredit;
import com.dminc.prasac.model.dto.misc.Currency;
import com.dminc.prasac.model.dto.misc.LoanPurpose;
import com.dminc.prasac.model.dto.misc.LoanType;
import com.dminc.prasac.model.dto.misc.RepaymentMode;
import com.dminc.prasac.model.entity.client.ClientEntity;
import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.repository.loan.LoanRepository;
import com.dminc.prasac.service.ObjectsConverter;
import com.dminc.prasac.service.client.ClientService;
import com.dminc.prasac.service.selection.ClientTypeService;
import com.dminc.prasac.service.selection.CollateralTitleTypeService;
import com.dminc.prasac.service.selection.CurrencyService;
import com.dminc.prasac.service.selection.LoanPurposeService;
import com.dminc.prasac.service.selection.LoanTypeService;
import com.dminc.prasac.service.selection.RepaymentModeService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LoanHistoryService {
    private final ObjectsConverter converter;

    private final LoanPurposeService loanPurposeService;
    private final PrasacLoanService prasacLoanService;
    private final RepaymentModeService repaymentModeService;
    private final ClientTypeService clientTypeService;
    private final LoanRepository loanRepository;
    private final CollateralTitleTypeService collateralTitleTypeService;
    private final CurrencyService currencyService;
    private final LoanTypeService loanTypeService;
    private final LoanService loanService;
    private final ClientService clientService;

    @Cacheable(value = "getHistoriesByClient", key = "#clientCif")
    public List<LoanHistory> getHistoriesByClient(String clientCif) throws BusinessException {
        final List<PrasacLoanHistory> loan = prasacLoanService.getLoanHistoriesByClientCif(clientCif);
        return convertLoanHistories(loan);
    }

    @Cacheable(value = "getLoanRequestHistoryByClient", key = "#clientCif")
    public List<LoanRequestHistory> getLoanRequestHistoryByClientCif(String clientCif) {
        final List<PrasacLoanRequestHistory> loanRequest = prasacLoanService.getLoanRequestHistoryByClientCif(clientCif);
        return loanRequest.stream().map(this::convertLoanRequestHistory).collect(Collectors.toList());
    }

    @Cacheable(value = "getLoanRequestHistoryByClient", key = "#clientId")
    @Transactional(readOnly = true)
    public List<LoanRequestHistory> getLoanRequestHistoryByClientId(Long clientId) {
        final Specification<LoanEntity> filter = LoanSpecification.filterByClient(clientId);
        final List<LoanEntity> loans = loanRepository.findAll(filter);
        return loans.stream().map(this::convert).collect(Collectors.toList());
    }

    @Cacheable(value = "getLoanRequestHistoryByClientIdAndCif", key = "#request")
    @Transactional(readOnly = true)
    public List<LoanRequestHistory> getLoanRequestHistoryByClientIdAndCif(ClientRequest request) {

        if (StringUtils.isNotEmpty(request.getClientCif())) {
            final List<LoanRequestHistory> historiesByClientCif = getLoanRequestHistoryByClientCif(request.getClientCif());
            List<LoanRequestHistory> histories = new ArrayList<>(historiesByClientCif);
            final Optional<Client> optionalClient = clientService.getByCif(request.getClientCif());
            optionalClient.ifPresent(client -> {
                final List<LoanRequestHistory> loanRequestHistoryByClientId = getLoanRequestHistoryByClientId(client.getId());
                histories.addAll(loanRequestHistoryByClientId);
            });
            return histories;
        } else {
            return getLoanRequestHistoryByClientId(request.getClientId());
        }
    }

    private LoanRequestHistory convert(LoanEntity loanEntity) {
        // no need to check isPresent as loan has at least borrower
        final ClientEntity client = loanEntity.getClients().stream()
                .filter(clientEntity -> loanService.filterClientByBorrowerType(loanEntity)).findFirst().get();
        return LoanRequestHistory.builder().currency(currencyService.convert(loanEntity.getCurrency()))
                .requestAmount(loanEntity.getLoanAmount()).rejectReason(loanEntity.getPreScreeningFailReason())
                .requestStatus(loanEntity.getLoanStatus().getStatus()).requestDate(loanEntity.getCreatedDate().toLocalDate())
                .clientCif(client.getCif()).dob(client.getDob()).fullName(client.getFullName())
                .khmerFullname(client.getKhmerFullName()).build();

    }

    private LoanRequestHistory convertLoanRequestHistory(PrasacLoanRequestHistory loanHistory) {
        final LoanRequestHistory loan = converter
                .runServiceTask((Mapper mapper) -> mapper.map(loanHistory, LoanRequestHistory.class));
        loan.setCurrency(currencyService.getByCode(loanHistory.getCurrency()));
        return loan;
    }

    private List<LoanHistory> convertLoanHistories(List<PrasacLoanHistory> prasacLoans) throws BusinessException {
        List<LoanHistory> loanHistories = new ArrayList<>();
        for (PrasacLoanHistory loanHistory : prasacLoans) {
            loanHistories.add(convertLoanHistory(loanHistory));
        }
        return loanHistories;
    }

    private LoanHistory convertLoanHistory(PrasacLoanHistory prasacLoanHistory) throws BusinessException {
        final LoanHistory loanHistory = converter
                .runServiceTask((Mapper mapper) -> mapper.map(prasacLoanHistory, LoanHistory.class));
        loanHistory.setLoanPurpose(loanPurposeService.getLoanPurposeByCode(prasacLoanHistory.getLoanPurposeCode()));
        loanHistory.setRepaymentMode(repaymentModeService.getByCode(prasacLoanHistory.getRepaymentModeCode()));
        loanHistory.setCurrency(currencyService.getByCode(prasacLoanHistory.getCurrencyType()));
        loanHistory.setClientType(clientTypeService.getByCode(prasacLoanHistory.getClientType()));
        loanHistory.setCollateralType(collateralTitleTypeService.getByCode(prasacLoanHistory.getCollateralType()));
        loanHistory.setRawCollateralType(prasacLoanHistory.getCollateralType());
        loanHistory.setRawLoanPurpose(prasacLoanHistory.getLoanPurpose());
        return loanHistory;
    }

    public List<ClientLiabilityCredit> getClientLiabilityCredits(long id) {
        final Loan loan = loanService.getById(id);
        final List<String> clientCifs = loan.getClients().stream()
                .map(Client::getCif)
                .filter(StringUtils::isNotEmpty)
                .collect(Collectors.toList());
        final List<PrasacLoanHistoryInfo> loanHistoriesInfoByClients = prasacLoanService
                .getLoanHistoriesInfoByClients(clientCifs);
        return loanHistoriesInfoByClients.stream().map(this::convert).collect(Collectors.toList());
    }

    @SuppressWarnings({"PMD.NPathComplexity"})
    private ClientLiabilityCredit convert(PrasacLoanHistoryInfo info) {
        String interestRateMonthly = info.getInterestRateMonthly();
        if (StringUtils.isNotEmpty(interestRateMonthly)) {
            interestRateMonthly = interestRateMonthly.replaceAll("%", "");
            info.setInterestRateMonthly(interestRateMonthly);
        }
        final ClientLiabilityCredit liab = converter.getObjectsMapper().map(info, ClientLiabilityCredit.class);
        final Currency currency = currencyService.getByCode(info.getCurrencyCode());
        liab.setCurrency(currency == null ? null : currency.getId());
        final LoanPurpose loanPurpose = loanPurposeService.getLoanPurposeByCode(info.getLoanPurposeCode());
        liab.setLoanPurpose(loanPurpose == null ? null : loanPurpose.getId());
        final RepaymentMode repaymentMode = repaymentModeService.getByCode(info.getRepaymentCode());
        liab.setRepaymentMode(repaymentMode == null ? null : repaymentMode.getId());
        final LoanType loanType = loanTypeService.getByCode(info.getLoanTypeCode());
        liab.setLoanType(loanType == null ? null : loanType.getId());
        return liab;
    }
}
