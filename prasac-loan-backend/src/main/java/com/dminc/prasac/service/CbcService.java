package com.dminc.prasac.service;

import com.dminc.common.tools.exceptions.InvalidParametersBusinessException;
import com.dminc.common.tools.exceptions.ItemNotFoundBusinessException;
import com.dminc.prasac.integration.domain.AccountDetail;
import com.dminc.prasac.integration.domain.CbcClient;
import com.dminc.prasac.integration.domain.CbcReport;
import com.dminc.prasac.integration.domain.CbcResponse;
import com.dminc.prasac.integration.domain.CbcScore;
import com.dminc.prasac.integration.domain.Guarantor;
import com.dminc.prasac.integration.domain.PreviousEnquiry;
import com.dminc.prasac.integration.domain.cbc.response.AccountDetailWrapper;
import com.dminc.prasac.integration.domain.cbc.response.Consumer;
import com.dminc.prasac.integration.domain.cbc.response.ConsumerInfo;
import com.dminc.prasac.integration.domain.cbc.response.GuarantorDetailWrapper;
import com.dminc.prasac.integration.domain.cbc.response.Identification;
import com.dminc.prasac.integration.domain.cbc.response.Item;
import com.dminc.prasac.integration.domain.cbc.response.PlaceOfBirth;
import com.dminc.prasac.integration.domain.cbc.response.PreviousEnquiryWrapper;
import com.dminc.prasac.integration.domain.cbc.response.Report;
import com.dminc.prasac.integration.domain.cbc.response.Response;
import com.dminc.prasac.integration.domain.cbc.response.Score;
import com.dminc.prasac.integration.domain.request.Address;
import com.dminc.prasac.integration.domain.request.ClientIdentification;
import com.dminc.prasac.integration.service.CbcIntegrationService;
import com.dminc.prasac.model.dto.misc.ClientIdentificationType;
import com.dminc.prasac.model.dto.misc.LoanPurpose;
import com.dminc.prasac.model.entity.CbcReportEntity;
import com.dminc.prasac.model.entity.CbcStatus;
import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.entity.selection.CommuneEntity;
import com.dminc.prasac.repository.CbcReportRepository;
import com.dminc.prasac.repository.loan.LoanRepository;
import com.dminc.prasac.service.selection.ClientIdentificationTypeService;
import com.dminc.prasac.service.selection.LoanPurposeService;
import com.dminc.prasac.service.selection.LocationService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.bind.JAXB;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@SuppressWarnings({"PMD.TooManyMethods"})
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CbcService {

    public static final Charset CHARSET_UTF_8 = Charset.forName("UTF-8");
    private final LoanRepository loanRepository;
    private final CbcIntegrationService service;
    private final ObjectsConverter converter;
    private final LocationService locationService;
    private final ClientIdentificationTypeService clientIdentificationTypeService;
    private final LoanPurposeService loanPurposeService;
    private final CbcReportRepository repository;

    @Transactional(noRollbackFor = InvalidParametersBusinessException.class)
    public CbcReport cbcReport(long loanId) throws IOException {
        final Optional<LoanEntity> firstById = loanRepository.getFirstById(loanId);
        final LoanEntity loanEntity = firstById.orElseThrow(() -> new ItemNotFoundBusinessException("Loan not found"));
        final Optional<CbcReportEntity> optionalReport = repository.findTopByLoan(loanEntity);
        final Response response;
        final String reportXml;
        // Check if report already check to cbc and response success
        if (optionalReport.isPresent()
                && CbcStatus.OK.equals(optionalReport.get().getStatus())) {
            reportXml = optionalReport.get().getReport();
            response = unmarshal(reportXml);
        } else {
            final CbcResponse report = service.getReport(loanEntity);
            reportXml = report.getResponseXML();
            response = unmarshal(reportXml);
            try {
                checkError(response);
                saveReport(optionalReport, loanEntity, report, CbcStatus.OK);
            } catch (InvalidParametersBusinessException e) {
                // save response error for further checking
                saveReport(optionalReport, loanEntity, report, CbcStatus.ERROR);
                BusinessError.CBC_REPORT_ERROR.throwExceptionHttpStatus400(e.getMessage());
            }
        }
        return convertCbcReport(response);
    }

    private Response unmarshal(String xml) {
        return JAXB.unmarshal(new ByteArrayInputStream(xml.getBytes(CHARSET_UTF_8)), Response.class);
    }

    private void saveReport(Optional<CbcReportEntity> optionalEntity, LoanEntity loanEntity, CbcResponse response, CbcStatus status) {
        if (optionalEntity.isPresent()) {
            final CbcReportEntity entity = optionalEntity.get();
            entity.setReport(response.getResponseXML());
            entity.setRequest(response.getRequestXML());
            entity.setStatus(status);
            repository.save(entity);
        } else {
            saveNewReport(response, loanEntity, status);
        }
    }

    private void saveNewReport(CbcResponse cbcResponse, LoanEntity loanEntity, CbcStatus status) {
        final CbcReportEntity entity = CbcReportEntity.builder()
                .report(cbcResponse.getResponseXML())
                .request(cbcResponse.getRequestXML())
                .loan(loanEntity)
                .status(status)
                .build();
        repository.save(entity);
    }

    private void checkError(Response response) throws InvalidParametersBusinessException {
        final Item item = response.getBody().getItem();
        if (item.getNumberOfError() > 0) {
            final String errorMessage = item.getErrors().stream().map(e -> String.format("Client index {%s}, Error Message {%s}, Error Data {%s}",
                    e.getConsumerSeq(), e.getMessage(), e.getData()))
                    .collect(Collectors.joining("\n"));
            throw new InvalidParametersBusinessException(errorMessage);
        }
    }

    private CbcReport convertCbcReport(Response response) {
        if (response == null) {
            return null;
        }
        final Report report = response.getBody().getItem().getReport();
        final CbcReport cbcReport = converter.getObjectsMapper().map(report, CbcReport.class);
        final LoanPurpose purpose = loanPurposeService.getLoanPurposeByCbcCode(report.getPurposeCode());
        cbcReport.setLoanPurpose(purpose == null ? null : purpose.getCbcDesc());
        cbcReport.setClients(convertClient(report.getConsumers()));
        return cbcReport;
    }

    private List<CbcClient> convertClient(List<Consumer> consumers) {
        return consumers.stream().map(consumer -> {
            final ConsumerInfo conInfo = consumer.getInfo();
            return CbcClient.builder()
                    .clientType(consumer.getClientType())
                    .dob(conInfo.getDob())
                    .nationalityCode(conInfo.getNationalityCode())
                    .gender(conInfo.getGender())
                    .maritalStatusCode(conInfo.getMaritalStatusCode())
                    .firstName(conInfo.getName().getFirstName())
                    .lastName(conInfo.getName().getLastName())
                    .khmerFirstName(conInfo.getName().getKhmerFirstName())
                    .khmerLastName(conInfo.getName().getKhmerLastName())
                    .accountDetails(convertAccountDetails(consumer.getDetailWrapper()))
                    .clientIdentification(convertIdentification(consumer.getIdentification()))
                    .placeOfBirth(convertPlaceOfBirth(conInfo.getPlaceOfBirth()))
                    .previousEnquiries(convertPreviousEnquiry(consumer.getEnquiryWrapper()))
                    .guarantors(convertGuarantor(consumer.getGuarantorDetailWrapper()))
                    .score(convertScore(consumer.getScore()))
                    .summary(consumer.getSummary())
                    .build();
        }).collect(Collectors.toList());
    }

    private CbcScore convertScore(Score score) {
        return converter.getObjectsMapper().map(score, CbcScore.class);
    }

    private List<Guarantor> convertGuarantor(GuarantorDetailWrapper wrapper) {
        if (wrapper == null) {
            return Collections.emptyList();
        }
        return converter.getObjectsMapper().map(wrapper.getGuarantorDetails(), Guarantor.class);
    }

    private List<PreviousEnquiry> convertPreviousEnquiry(PreviousEnquiryWrapper wrapper) {
        if (wrapper == null) {
            return Collections.emptyList();
        }
        return wrapper.getEnquiries().stream().map(en -> {
            final PreviousEnquiry enquiry = converter.getObjectsMapper().map(en, PreviousEnquiry.class);
            final LoanPurpose purpose = loanPurposeService.getLoanPurposeByCbcCode(en.getPurposeCode());
            enquiry.setLoanPurpose(purpose == null ? null : purpose.getCbcDesc());
            enquiry.setFirstName(en.getName().getFirstName());
            enquiry.setLastName(en.getName().getLastName());
            enquiry.setKhmerFirstName(en.getName().getKhmerFirstName());
            enquiry.setKhmerLastName(en.getName().getKhmerLastName());
            return enquiry;
        }).collect(Collectors.toList());
    }

    private List<AccountDetail> convertAccountDetails(AccountDetailWrapper wrapper) {
        if (wrapper == null) {
            return Collections.emptyList();
        }
        return converter.getObjectsMapper().map(wrapper.getAccountDetails(), AccountDetail.class);
    }

    private ClientIdentification convertIdentification(Identification identification) {
        final ClientIdentificationType type = clientIdentificationTypeService.findByCbcCode(identification.getIdType());
        return ClientIdentification.builder()
                .idNumber(identification.getIdNumber())
                .expiryDate(identification.getExpiredDate())
                .clientIdentificationCbc(identification.getIdType())
                .clientIdentificationType(type == null ? null : type.getType())
                .build();
    }

    private Address convertPlaceOfBirth(PlaceOfBirth placeOfBirth) {
        final CommuneEntity communeEntity = locationService.findCommuneEntityByCodes(placeOfBirth.getCommuneCode(), placeOfBirth.getDistrictCode(), placeOfBirth.getProvinceCode());
        return Address.builder()
                .commune(communeEntity.getName())
                .district(communeEntity.getDistrict().getName())
                .province(communeEntity.getDistrict().getProvince().getName())
                .country(communeEntity.getDistrict().getProvince().getCountry().getName())
                .build();
    }
}
