package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.BusinessActivity;
import com.dminc.prasac.model.entity.BusinessActivityEntity;
import com.dminc.prasac.repository.BusinessActivityRepository;

@Service
public class BusinessActivityService extends CommonService<BusinessActivity, BusinessActivityEntity> {

    public BusinessActivityService(BusinessActivityRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, BusinessActivity.class);
    }
}



