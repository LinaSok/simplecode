package com.dminc.prasac.service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.common.tools.mapping.Mapper;
import com.dminc.prasac.model.dto.misc.ClientIdentification;
import com.dminc.prasac.model.dto.misc.ClientIdentificationType;
import com.dminc.prasac.model.entity.client.ClientIdentificationEntity;
import com.dminc.prasac.repository.ClientIdentificationRepository;
import com.dminc.prasac.service.selection.ClientIdentificationTypeService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class ClientIdentificationService {

    private final ClientIdentificationRepository repository;

    private final ClientIdentificationTypeService clientIdentificationTypeService;

    private final ObjectsConverter objectsConverter;

    public List<ClientIdentification> verifyIdentity(String number, String typeCode) throws BusinessException {
        final List<ClientIdentificationEntity> clientIdentifications = repository.findByIdNumber(number);
        if (CollectionUtils.isNotEmpty(clientIdentifications)) {
            return objectsConverter
                    .runServiceTask((Mapper mapper) -> mapper.map(clientIdentifications, ClientIdentification.class));
        }
        // in case identity doesn't exist in our database
        final ClientIdentificationType type = clientIdentificationTypeService.getOneByCode(typeCode);
        ClientIdentification clientIdentification = ClientIdentification.builder().idNumber(number)
                .clientIdentificationType(type.getId()).build();
        return Arrays.asList(clientIdentification);
    }

    public Optional<ClientIdentification> getIdentity(String idNumber, Long identificationType) {
        final Optional<ClientIdentificationEntity> identity = repository
                .findFirstByIdNumberAndClientIdentificationType_Id(idNumber, identificationType);

        return Optional.ofNullable(objectsConverter
                .runServiceTask((Mapper mapper) -> mapper.map(identity.orElse(null), ClientIdentification.class)));
    }

}
