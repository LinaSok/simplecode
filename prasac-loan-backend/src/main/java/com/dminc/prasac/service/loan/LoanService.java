package com.dminc.prasac.service.loan;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.prasac.model.dto.CommentResponse;
import com.dminc.prasac.model.dto.loan.Loan;
import com.dminc.prasac.model.dto.loan.SearchLoanRequest;
import com.dminc.prasac.model.dto.loan.SearchLoanResponse;
import com.dminc.prasac.model.dto.misc.AssignLoanRequest;
import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.misc.RequestedFrom;
import com.dminc.prasac.model.request.LoanAppraisalRequest;
import com.dminc.prasac.model.request.PreScreenRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@SuppressWarnings({"PMD.TooManyMethods"})
public interface LoanService {
    Loan createPreScreen(PreScreenRequest request, RequestedFrom agent) throws BusinessException;

    Loan assign(Long id, AssignLoanRequest request) throws BusinessException;

    Loan getById(Long id) throws BusinessException;

    Page<Loan> getAssignedLoan(LocalDateTime lastModified, Pageable pageable);

    Page<SearchLoanResponse> search(SearchLoanRequest request, Pageable pageable);

    Loan submitLoanAppraisal(LoanAppraisalRequest request);

    void assignBack(Long id, String comment);

    Page<Loan> getMyList(LocalDateTime lastModified, Pageable pageable);

    boolean filterClientByBorrowerType(LoanEntity entity);

    List<CommentResponse> getComments(Long id, LocalDateTime lastModified);

    Loan committeeSubmitLoan(Long id, String comment, Optional<Long> groupId);

    Loan rejectLoan(Long id, String comment, boolean isRejectedByClient);

    Loan approvedLoan(Long id, String comment);

    Loan suspendedLoan(Long id, String comment);

    Loan completeReject(Long id);

}
