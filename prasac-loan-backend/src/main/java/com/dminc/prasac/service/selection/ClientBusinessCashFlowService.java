package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.ClientBusinessCashFlow;
import com.dminc.prasac.model.entity.ClientBusinessCashFlowEntity;
import com.dminc.prasac.repository.ClientBusinessCashFlowRepository;

@Service
public class ClientBusinessCashFlowService extends CommonService<ClientBusinessCashFlow, ClientBusinessCashFlowEntity> {

    public ClientBusinessCashFlowService(ClientBusinessCashFlowRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, ClientBusinessCashFlow.class);
    }
}



