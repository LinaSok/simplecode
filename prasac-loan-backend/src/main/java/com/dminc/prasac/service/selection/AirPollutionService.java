package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.AirPollution;
import com.dminc.prasac.model.entity.AirPollutionEntity;
import com.dminc.prasac.repository.AirPollutionRepository;

@Service
public class AirPollutionService extends CommonService<AirPollution, AirPollutionEntity> {

    public AirPollutionService(AirPollutionRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, AirPollution.class);
    }
}



