package com.dminc.prasac.service;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.common.tools.exceptions.InvalidParametersBusinessException;
import com.dminc.common.tools.mapping.Mapper;
import com.dminc.prasac.model.dto.user.AutoCompleteUser;
import com.dminc.prasac.model.dto.user.NewUserRequest;
import com.dminc.prasac.model.dto.user.SearchUserProfileRequest;
import com.dminc.prasac.model.dto.user.UserRequest;
import com.dminc.prasac.model.dto.user.UserResponse;
import com.dminc.prasac.model.entity.ApprovalState;
import com.dminc.prasac.model.entity.BranchEntity;
import com.dminc.prasac.model.entity.LoanApprovalState;
import com.dminc.prasac.model.entity.User;
import com.dminc.prasac.model.entity.UserPermission;
import com.dminc.prasac.model.entity.UserProfile;
import com.dminc.prasac.model.entity.UserProfileLogEntity;
import com.dminc.prasac.model.entity.UserRole;
import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.entity.loan.LoanStatusEntity;
import com.dminc.prasac.model.misc.LoanStatusEnum;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.repository.LoanApprovalStateRepository;
import com.dminc.prasac.repository.UserPermissionRepository;
import com.dminc.prasac.repository.UserProfileLogRepository;
import com.dminc.prasac.repository.UserProfileRepository;
import com.dminc.prasac.repository.UserRepository;
import com.dminc.prasac.repository.UserRoleRepository;
import com.dminc.prasac.repository.loan.LoanRepository;
import com.dminc.prasac.service.validator.UserValidator;
import com.dminc.prasac.util.EmailTemplate;
import com.dminc.prasac.util.PrasacHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@SuppressWarnings({"PMD.TooManyMethods"})
@Slf4j
public class UserServiceImpl implements UserService {

    private static final String GENERATE_PAZZWORD = "${generatedPassword}";
    private final ObjectsConverter converter;
    private final UserValidator userValidator;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserRoleRepository userRoleRepository;
    private final UserProfileRepository userProfileRepository;
    private final EmailManagerService emailManagerService;
    private final EmailParserService emailTemplateService;
    private final ObjectsConverter objectsConverter;
    private final TokenStore tokenStore;
    private final UserPermissionRepository userPermissionRepository;
    private final UserProfileLogRepository userProfileLogRepository;
    private final LoanRepository loanRepository;
    private final LoanApprovalStateRepository loanApprovalStateRepository;


    @Value("${security.oauth2.client.clientId}")
    private String clientId;

    @Transactional
    public UserResponse createNewUser(NewUserRequest newUser) throws BusinessException {

        if (userValidator.isUserExist(newUser.getUsername())) {
            throw new InvalidParametersBusinessException(String.format("the username already exists %s", newUser.getUsername()), BusinessError.USER_NAME_ALREADY_EXIST.getCode());
        }

        userValidator.validateStaffId(newUser.getStaffId());
        userValidator.validateEmail(newUser.getEmail());
        final List<UserRole> roles = getUserRoles(newUser.getRoles());

        final User user = User.builder()
                .username(newUser.getUsername())
                .roles(roles)
                .enabled(true)
                .build();

        //generate password
        final String password = this.generatePassword();
        user.setPassword(passwordEncoder.encode(password));
        userRepository.save(user);

        final UserProfile userProfile = objectsConverter.getObjectsMapper().map(newUser, UserProfile.class);
        userProfile.setUser(user);
        userProfileRepository.save(userProfile);
        this.sendNewUserEmail(userProfile.getEmail(), password);
        return converter.runServiceTask((Mapper mapper) -> mapper.map(newUser, UserResponse.class));
    }

    private void sendNewUserEmail(final String email, final String password) {
        EmailTemplate template = emailTemplateService.getMailTemplate();
        String emailTemplate = template.getCreatedNewUser().getContent().replace(GENERATE_PAZZWORD, password);
        final String title = template.getCreatedNewUser().getTitle();
        this.notifyUserByEmail(title, emailTemplate, email);
    }

    @Override
    public void notifyUserByEmail(final String title, final String content, final String email) {
        this.emailManagerService.sendMail(title, content, email);
    }


    @Override
    @org.springframework.transaction.annotation.Transactional
    public void resetPassword(long userId) {
        final User user = getUserById(userId);
        final String password = generatePassword();
        user.setPassword(passwordEncoder.encode(password));
        userRepository.save(user);
        final UserProfile userProfile = userProfileRepository.findByUser(user);
        final String emailMessage = String.format("Your password has been reset: %s", password);
        emailManagerService.sendMail("Reset Password", emailMessage, userProfile.getEmail());
    }

    @Transactional
    @Override
    public com.dminc.prasac.model.dto.user.UserProfile getUserProfile(Long id) {
        final Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
            return objectsConverter.getObjectsMapper().map(user.get().getUserProfile(), com.dminc.prasac.model.dto.user.UserProfile.class);
        }
        BusinessError.OBJECT_NOT_FOUND.throwExceptionHttpStatus404("User not found");
        return null;
    }

    @Transactional
    @Override
    public List<com.dminc.prasac.model.dto.user.UserProfile> getUserProfile(BranchEntity branch, List<UserRole> roles) {
        final List<UserProfile> userProfiles = this.userProfileRepository.findByUser_RolesInAndBranch(roles, branch);
        return converter.getObjectsMapper().map(userProfiles, com.dminc.prasac.model.dto.user.UserProfile.class);
    }

    @Override
    public List<com.dminc.prasac.model.dto.user.UserProfile> getUserProfile(List<UserRole> roles) {
        final List<UserProfile> userProfiles = this.userProfileRepository.findByUser_RolesIn(roles);
        return converter.getObjectsMapper().map(userProfiles, com.dminc.prasac.model.dto.user.UserProfile.class);
    }

    private User getUserById(long userId) {
        final Optional<User> optionalUser = userRepository.findById(userId);
        if (optionalUser.isPresent()) {
            return optionalUser.get();
        }
        BusinessError.OBJECT_NOT_FOUND.throwExceptionHttpStatus404();
        return null;
    }

    @org.springframework.transaction.annotation.Transactional
    @Modifying
    @Override
    public UserResponse updateUser(UserRequest request, long userId) {
        log.debug("======= Updating user =======");
        userValidator.validateStaffIdOnUpdate(request.getStaffId(), userId);

        final User user = getUserById(userId);
        log.debug("Get current user role id list");
        final List<Long> currentRoleIds = CollectionUtils.emptyIfNull(user.getRoles()).stream().map(UserRole::getId).collect(Collectors.toList());
        final List<UserRole> userRoles = getUserRoles(request.getRoles());

        log.debug("Check if current user role the same as new role request or not");
        final Collection<Long> roles = CollectionUtils.disjunction(currentRoleIds, request.getRoles());

        user.setRoles(userRoles);
        userRepository.save(user);

        final UserProfile userProfile = userProfileRepository.findByUser(user);

        //add records in profile log
        this.saveProfileLog(userProfile, user);

        final UserProfile profile = objectsConverter.getObjectsMapper().map(request, UserProfile.class);
        profile.setId(userProfile.getId());
        profile.setUser(user);
        userProfileRepository.save(profile);

        if (!roles.isEmpty()) {
            log.debug("Role request not the same as current user role.");
            log.debug("Clear out user access token if there is");
            this.clearUserToken(user.getUsername());
        }
        return objectsConverter.getObjectsMapper().map(user, UserResponse.class);
    }

    /**
     * Saving user profile before update. The purpose is mainly for the RO user which keep changing branches
     * but the loan which in his previous branch and not taking any action yet, he needs to check those loan as well.
     * <p>
     * In case user is RO, we will save his pending loan's id as string with comma separated. If not the list of pending loan will be null or empty.
     *
     * @param previous
     */
    private void saveProfileLog(final UserProfile previous, final User user) {
        //get loan which status pending and committee level 2 (REVIEW_LEVEL_2)
        final LoanStatusEntity pending = LoanStatusEntity.builder().id(LoanStatusEnum.PENDING.getValue()).build();
        final LoanStatusEntity inProgress = LoanStatusEntity.builder().id(LoanStatusEnum.IN_PROGRESS.getValue()).build();
        //See if the user already moved from other branch and his current role is RO
        List loanIds = Collections.emptyList();
        boolean isRO = false;
        if (PrasacHelper.hasROPermission(user)) {
            final List<UserProfileLogEntity> roProfileLog = userProfileLogRepository.findByUserProfileIdAndIsRO(previous.getId(), true);
            List<Long> previousBranchIds = roProfileLog.stream().map(UserProfileLogEntity::getBranchId).collect(Collectors.toList());
            //load from previous branch
            CollectionUtils.emptyIfNull(previousBranchIds).add(previous.getBranch().getId());
            final List<LoanEntity> loanEntities = loanRepository.findByCommitteeLevelAndLoanStatusInAndBranch_IdIn(LocalConstants.COMMITTEE_LEVEL_2, Arrays.asList(pending, inProgress), previousBranchIds);

            //Check if user already take action on some pending loans. If found, then filter out from the loanEntities list
            final List<LoanApprovalState> loanStateByUser = loanApprovalStateRepository.findByCreatedByAndLoanInAndStateNot(previous.getUser().getUsername(), loanEntities, ApprovalState.SUSPENDED);

            if (CollectionUtils.isNotEmpty(loanStateByUser)) {
                final List<LoanEntity> loan = loanStateByUser.stream().map(LoanApprovalState::getLoan).collect(Collectors.toList());
                loanEntities.removeAll(loan);
            }

            loanIds = loanEntities.stream().map(loan -> String.valueOf(loan.getId())).collect(Collectors.toList());
            isRO = true;
        }

        final UserProfileLogEntity entity = UserProfileLogEntity.builder().branchId(previous.getBranch().getId())
                .dateOfBirth(previous.getDateOfBirth())
                .email(previous.getEmail())
                .firstName(previous.getFirstName())
                .lastName(previous.getLastName())
                .gender(previous.getGender())
                .positionId(previous.getPosition().getId())
                .staffId(previous.getStaffId())
                .pendingLoanId(String.join(",", loanIds))
                .userProfileId(previous.getId())
                .isRO(isRO).build();
        this.userProfileLogRepository.save(entity);
    }

    private List<UserRole> getUserRoles(List<Long> rolesId) {
        if (CollectionUtils.isEmpty(rolesId)) {
            BusinessError.OBJECT_BAD_REQUEST.throwExceptionHttpStatus400("The user must have group");
        }
        final List<UserRole> roles = userRoleRepository.findAllById(rolesId);

        if (CollectionUtils.isEmpty(roles)) {
            BusinessError.OBJECT_BAD_REQUEST.throwExceptionHttpStatus400("User role not found in database");
        }
        return roles;
    }

    @org.springframework.transaction.annotation.Transactional
    @Override
    public User updateUser(User user) {
        return this.userRepository.save(user);
    }

    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    @Override
    public com.dminc.prasac.model.dto.user.UserProfile getCurrentUserDetail(String username) {
        final User user = PrasacHelper.getCurrentUser();
        final UserProfile userProfile = userProfileRepository.findByUser(user);
        return objectsConverter.getObjectsMapper().map(userProfile, com.dminc.prasac.model.dto.user.UserProfile.class);
    }

    private void clearUserToken(final String username) {
        final Collection<OAuth2AccessToken> tokens = tokenStore.findTokensByClientIdAndUserName(clientId, username);
        if (CollectionUtils.isNotEmpty(tokens)) {
            log.debug("Clearing out token for user {}", username);
            for (OAuth2AccessToken token : tokens) {
                tokenStore.removeAccessToken(token);
            }
        }
    }

    @Override
    @org.springframework.transaction.annotation.Transactional
    public void deActivateUser(long userId) {
        final User user = getUserById(userId);
        user.setEnabled(false);
        this.clearUserToken(user.getUsername());
        userRepository.saveAndFlush(user);
    }

    /**
     * Auto generate password.
     * 8 Characters length.
     * Mininum 1 uppercase, 1 lowercase, 1 number, 1 special character.
     *
     * @return String
     */
    private String generatePassword() {
        final String upperCaseLetters = RandomStringUtils.random(1, 65, 90, true, false);
        final String lowerCaseLetters = RandomStringUtils.random(1, 97, 122, true, false);
        final String numbers = RandomStringUtils.randomNumeric(1);
        final String specialChar = RandomStringUtils.random(1, 33, 47, false, false);
        final String totalChars = RandomStringUtils.randomAlphanumeric(4);

        final String combinedChars = upperCaseLetters.concat(lowerCaseLetters)
                .concat(numbers)
                .concat(specialChar)
                .concat(totalChars);

        final List<Character> pwdChars = combinedChars.chars()
                .mapToObj(c -> (char) c)
                .collect(Collectors.toList());

        Collections.shuffle(pwdChars);
        return pwdChars.stream()
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                .toString();
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public Page<com.dminc.prasac.model.dto.user.UserProfile> searchProfile(SearchUserProfileRequest request, Pageable pageable) {
        String fullName = StringUtils.isNotEmpty(request.getStaffIdOrfullName()) ? request.getStaffIdOrfullName().replaceAll("\\s+", "") : request.getStaffIdOrfullName();
        Specification specification = Specification
                .where(UserProfileSpecification.filterByFullNameOrStaffId(fullName, request.getStaffIdOrfullName()))
                .and(UserProfileSpecification.filterByBranch(request.getBranchId()))
                .and(UserProfileSpecification.filterByRole(request.getRoleId()))
                .and(UserProfileSpecification.filterByIsActive(request.getIsEnabled()))
                .and(UserProfileSpecification.sortByFields(request));
        final Page<UserProfile> userProfileEntities = userProfileRepository.findAll(specification, pageable);
        return userProfileEntities.map(userProfile -> converter.getObjectsMapper().map(userProfile, com.dminc.prasac.model.dto.user.UserProfile.class));
    }

    /**
     * This used auto completion for BM to search for CO to be assigned pre-screening.
     * User should be able to lookup for the CO within their own branch only.
     *
     * @param query
     * @param pageable
     * @return
     */
    @Transactional
    @Override
    public Page<AutoCompleteUser> searchProfile(final String query, Pageable pageable) {
        final Long userId = PrasacHelper.getCurrentUserId();
        final com.dminc.prasac.model.dto.user.UserProfile currentUserProfile = this.getUserProfile(userId);
        final Long branchId = currentUserProfile.getBranch().getId();

        String fullName = StringUtils.isNotEmpty(query) ? query.replaceAll("\\s+", "") : query;
        Specification specification = Specification
                .where(UserProfileSpecification.filterStartWithFullNameOrStaffId(fullName, query))
                .and(UserProfileSpecification.filterByBranch(branchId));

        Page<UserProfile> userProfileEntities = userProfileRepository.findAll(specification, pageable);

        final List<AutoCompleteUser> autoCompleteUsers = userProfileEntities.get().map(this::convertToAutoComplet).collect(Collectors.toList());
        return new PageImpl<>(autoCompleteUsers, pageable, userProfileEntities.getTotalElements());
    }

    /**
     * Function to convert from profile to AutoCompletUser Object.
     * Autocomplet object used for assigning loan to CO user, so the id returned is not the profile_id but it will be user_id
     *
     * @param profile
     * @return
     */
    private AutoCompleteUser convertToAutoComplet(UserProfile profile) {
        return AutoCompleteUser.builder().id(profile.getUser().getId()).firstName(profile.getFirstName()).lastName(profile.getLastName()).staffId(profile.getStaffId()).build();
    }

    @Transactional
    @Override
    public List<com.dminc.prasac.model.dto.user.UserRole> getDecisionMakerGroup() {
        //TODOs : use constant instead for 16L?
        UserPermission permission = userPermissionRepository.findById(16L).get();
        List<UserRole> userRoles = userRoleRepository.findDistinctByPermissionsIn(Collections.singletonList(permission));

        return converter.getObjectsMapper().map(userRoles, com.dminc.prasac.model.dto.user.UserRole.class);
    }

    @Transactional
    @Override
    public void resetPasswordByTheProvidedPwd(String userName, String pwd) {
        UserValidator.validatePassword(pwd);
        final User user = userRepository.findOneByUsername(userName);
        if (user == null) {
            BusinessError.OBJECT_NOT_FOUND.throwExceptionHttpStatus404();
        }
        user.setPassword(passwordEncoder.encode(pwd));
        user.setLocked(false);
        user.setEnabled(true);
        userRepository.save(user);
    }

    @Transactional
    @Override
    public void reactivate(String userName) {
        updateUserState(userRepository.findOneByUsername(userName));
    }

    @Transactional
    @Override
    public void activateUser(long userId) {
        updateUserState(getUserById(userId));

    }

    private void updateUserState(User user) {
        if (user == null) {
            BusinessError.OBJECT_NOT_FOUND.throwExceptionHttpStatus404();
        }
        user.setEnabled(true);
        user.setLocked(false);
        userRepository.save(user);
    }
}

