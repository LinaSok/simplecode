package com.dminc.prasac.service;

import com.dminc.prasac.model.dto.user.UserPermissionGroup;
import com.dminc.prasac.repository.UserPermissionGroupRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserPermissionGroupService {
    private final UserPermissionGroupRepository userPermissionGroupRepository;
    private final ObjectsConverter converter;

    public List<UserPermissionGroup> findAll() {
        final List<com.dminc.prasac.model.entity.UserPermissionGroup> userPermissionGroups = userPermissionGroupRepository.findAll();
        return converter.getObjectsMapper().map(userPermissionGroups, UserPermissionGroup.class);
    }
}
