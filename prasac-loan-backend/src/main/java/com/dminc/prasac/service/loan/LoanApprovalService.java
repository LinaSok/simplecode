package com.dminc.prasac.service.loan;

import com.dminc.common.tools.exceptions.InvalidParametersBusinessException;
import com.dminc.prasac.model.entity.ApprovalState;
import com.dminc.prasac.model.entity.LoanApprovalState;
import com.dminc.prasac.model.entity.User;
import com.dminc.prasac.model.entity.UserPermission;
import com.dminc.prasac.model.entity.UserRole;
import com.dminc.prasac.model.entity.loan.CommentEntity;
import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.entity.loan.LoanStatusEntity;
import com.dminc.prasac.model.misc.LoanStatusEnum;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.repository.CommentRepository;
import com.dminc.prasac.repository.LoanApprovalStateRepository;
import com.dminc.prasac.repository.UserPermissionRepository;
import com.dminc.prasac.repository.UserRepository;
import com.dminc.prasac.repository.UserRoleRepository;
import com.dminc.prasac.repository.loan.LoanRepository;
import com.dminc.prasac.service.BusinessError;
import com.dminc.prasac.util.PrasacHelper;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Slf4j
@SuppressWarnings({"PMD.UseCollectionIsEmpty"})
public class LoanApprovalService {

    LoanApprovalStateRepository repository;
    CommentRepository commentRepository;
    LoanRepository loanRepository;
    UserPermissionRepository userPermissionRepository;
    UserRoleRepository userRoleRepository;
    UserRepository userRepository;

    @Transactional
    public void saveApprovalState(final LoanEntity loan, final String comment, final ApprovalState state) {
        final User user = PrasacHelper.getCurrentUser();
        LoanApprovalState loanApprovalState = repository.findByLoanAndCreatedBy(loan, user.getUsername());
        if (loanApprovalState == null) {
            loanApprovalState = LoanApprovalState.builder().loan(loan).state(state).build();
        } else {
            if (!loanApprovalState.getState().equals(ApprovalState.SUSPENDED)) {
                throw new InvalidParametersBusinessException("User already made decision", BusinessError.ALL_READ_MADE_DECISION.getCode());
            }
            loanApprovalState.setState(state);
        }

        // Save approval sate
        repository.save(loanApprovalState);
        //Save comment
        final CommentEntity commentEntity = CommentEntity.builder().loan(loan).userRole(loan.getUserRole()).comment(comment).build();
        commentRepository.save(commentEntity);
    }

    @org.springframework.transaction.annotation.Transactional
    public LoanEntity updateLoanStatus(final LoanEntity loan) {
        this.applyRules(loan);
        return loanRepository.saveAndFlush(loan);
    }

    private void applyRules(LoanEntity loan) {
        List<LoanApprovalState> states = this.repository.findByLoan(loan);

        if (LocalConstants.LEVEL_CC2 == loan.getCommitteeLevel()) {
            final UserRole userRole = loan.getUserRole();
            final List<User> levelTwoUsers = userRepository.findDistinctByRolesInAndEnabledIs(Collections.singletonList(userRole), true);
            final List<String> levelTwoUsernames = levelTwoUsers.stream().map(User::getUsername).collect(Collectors.toList());
            states = states.stream().filter(state -> levelTwoUsernames.contains(state.getCreatedBy())).collect(Collectors.toList());
        }

        List<LoanApprovalState> suspended = states.stream().filter(state -> state.getState().equals(ApprovalState.SUSPENDED)).collect(Collectors.toList());
        List<LoanApprovalState> rejected = states.stream().filter(state -> state.getState().equals(ApprovalState.REJECTED)).collect(Collectors.toList());
        List<LoanApprovalState> approved = states.stream().filter(state -> state.getState().equals(ApprovalState.APPROVED)).collect(Collectors.toList());

        if (LocalConstants.LEVEL_CC1 == loan.getCommitteeLevel()) {
            this.applyRuleLevel1(loan, states, suspended, rejected, approved);
        }

        if (LocalConstants.LEVEL_CC2 == loan.getCommitteeLevel()) {
            this.applyRuleLevel2(loan, states, suspended, rejected, approved);
        }
    }

    private void applyRuleLevel1(LoanEntity loan, final List<LoanApprovalState> states, final List<LoanApprovalState> suspended, final List<LoanApprovalState> rejected, final List<LoanApprovalState> approved) {
        log.info("Applying loan status rule for CC1 approval");
        final List<UserPermission> ccOnePermission = userPermissionRepository.findDistinctByIdIn(Arrays.asList(LocalConstants.REVIEW_LEVEL_2, LocalConstants.DECISION_MAKER));
        final UserPermission reviewLevelTwo = userPermissionRepository.getOne(LocalConstants.REVIEW_LEVEL_2);
        final UserPermission decisionMaking = userPermissionRepository.getOne(LocalConstants.DECISION_MAKER);
        //We identify user CC1 only if user with role review_level 2 and decision making
        log.info("Get Review Level 2 or Decision Maker user roles");
        final List<UserRole> ccOneOrDecisionMakerUserRoles = userRoleRepository.findDistinctByPermissionsIn(ccOnePermission);
        log.info("{} role founds", CollectionUtils.emptyIfNull(ccOneOrDecisionMakerUserRoles).size());
        log.info("Filter out the role without REVIEW LEVEL 2 permission and decision making");
        final List<UserRole> ccOne = ccOneOrDecisionMakerUserRoles.stream().filter(role -> (role.getPermissions().contains(decisionMaking) && role.getPermissions().contains(reviewLevelTwo))).collect(Collectors.toList());
        log.info("{} CC1 roles remaining", CollectionUtils.emptyIfNull(ccOne).size());

        //Some decision makers has other id than LEVEL 4
        final List<User> levelOneUsers = userRepository.findDistinctByRolesInAndEnabledIs(ccOne, true);

        log.info("Filter only user same branch with loan");

        final List<User> userInBranch = levelOneUsers.stream().filter(user -> user.getUserProfile() != null)
                .filter(user -> loan.getBranch().getId().equals(user.getUserProfile().getBranch().getId())).collect(Collectors.toList());
        //
        log.info("Found user in branch: {} users", userInBranch.size());
        //This just for debug purpose
        List<Long> userIds = userInBranch.stream().map(User::getId).collect(Collectors.toList());
        log.info("Remaining user id: {}", userIds.toString());

        if (states.isEmpty()) {
            loan.setLoanStatus(LoanStatusEntity.builder().id(LoanStatusEnum.PENDING.getValue()).build());
        } else if (states.size() < CollectionUtils.emptyIfNull(userInBranch).size()) {
            loan.setLoanStatus(LoanStatusEntity.builder().id(LoanStatusEnum.IN_PROGRESS.getValue()).build());
        } else if (suspended.size() >= 1) {
            //Move back to the state which will be pending at BR_REVIEW when CO submit again
            loan.setCommitteeLevel(0);
            loan.setLoanStatus(LoanStatusEntity.builder().id(LoanStatusEnum.SUSPENDED.getValue()).build());
        } else if (rejected.size() >= 2) {
            loan.setLoanStatus(LoanStatusEntity.builder().id(LoanStatusEnum.REJECTED.getValue()).build());
        } else if (approved.size() >= 2) {
            loan.setLoanStatus(LoanStatusEntity.builder().id(LoanStatusEnum.APPROVED.getValue()).build());
        } else if (states.size() == userInBranch.size()) {
            log.debug("====== Auto submit to next level HO =======");
            loan.setUserRole(getHoReviewUserRole());
            loan.setCommitteeLevel(LocalConstants.COMMITTEE_LEVEL_3);
            loan.setLoanStatus(LoanStatusEntity.builder().id(LoanStatusEnum.IN_PROGRESS.getValue()).build());
        } else {
            log.debug("====== Condition not matched HO =======");
            loan.setLoanStatus(LoanStatusEntity.builder().id(LoanStatusEnum.IN_PROGRESS.getValue()).build());
        }
        log.debug("Suspended: {}, rejected: {}, approved: {}", suspended.size(), rejected.size(), approved.size());
        log.info("===== Review Level 1 applied =====");
    }

    private void applyRuleLevel2(LoanEntity loan, final List<LoanApprovalState> states, final List<LoanApprovalState> suspended, final List<LoanApprovalState> rejected, final List<LoanApprovalState> approved) {
        log.info("Applying loan status rule for CC2 and CC3");
        //get group out of loan because level 2, user will need to select group (cc2 or cc3) to assign to
        final UserRole userRole = loan.getUserRole();
        final List<User> levelTwoUsers = userRepository.findDistinctByRolesInAndEnabledIs(Collections.singletonList(userRole), true);

        if (states.isEmpty()) {
            loan.setLoanStatus(LoanStatusEntity.builder().id(LoanStatusEnum.PENDING.getValue()).build());
        } else if (states.size() < CollectionUtils.emptyIfNull(levelTwoUsers).size()) {
            loan.setLoanStatus(LoanStatusEntity.builder().id(LoanStatusEnum.IN_PROGRESS.getValue()).build());
        } else if (suspended.size() >= 1) {
            //Move back to HO_REVIEW
            log.debug("Loan suspended by CC2/CC3. Set committee level to HO_REVIEW and update group");
            loan.setCommitteeLevel(LocalConstants.COMMITTEE_LEVEL_3);
            loan.setLoanStatus(LoanStatusEntity.builder().id(LoanStatusEnum.SUSPENDED.getValue()).build());
        } else if (rejected.size() >= 2) {
            loan.setLoanStatus(LoanStatusEntity.builder().id(LoanStatusEnum.REJECTED.getValue()).build());
        } else if (approved.size() >= 3) {
            loan.setLoanStatus(LoanStatusEntity.builder().id(LoanStatusEnum.APPROVED.getValue()).build());
        } else {
            log.debug("====== All Condition not matched =======");
            loan.setLoanStatus(LoanStatusEntity.builder().id(LoanStatusEnum.IN_PROGRESS.getValue()).build());
        }
        log.debug("Suspended: {}, rejected: {}, approved: {}", suspended.size(), rejected.size(), approved.size());
        log.info("===== Review Level 2 applied =====");
    }

    /**
     * Get the user role (group) for HO_REVIEW.
     * Note expect the user role is null.
     *
     * @return
     */
    private UserRole getHoReviewUserRole() {
        final UserPermission permission = userPermissionRepository.findById(LocalConstants.REVIEW_LEVEL_3).orElse(null);
        final List<UserRole> userRoles = userRoleRepository.findDistinctByPermissionsIn(Collections.singletonList(permission));
        log.debug("User group for review level 3: {}", CollectionUtils.emptyIfNull(userRoles).size());
        return userRoles.stream().findFirst().orElse(null);
    }
}
