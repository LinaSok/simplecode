package com.dminc.prasac.service;

import com.dminc.prasac.model.dto.user.SearchUserProfileRequest;
import com.dminc.prasac.model.entity.BranchEntity_;
import com.dminc.prasac.model.entity.User;
import com.dminc.prasac.model.entity.UserProfile;
import com.dminc.prasac.model.entity.UserProfile_;
import com.dminc.prasac.model.entity.UserRole;
import com.dminc.prasac.model.entity.UserRole_;
import com.dminc.prasac.model.entity.User_;
import com.dminc.prasac.model.misc.AuditEntity_;
import com.dminc.prasac.util.QueryEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;
import java.util.Locale;

public final class UserProfileSpecification {

    private UserProfileSpecification() {
        // private constructor
    }

    public static Specification<UserProfile> filterByFullNameOrStaffId(String fullName, String staffId) {
        return (Specification<UserProfile>) (root, query, criteriaBuilder) -> {
            if (StringUtils.isNotEmpty(staffId)) {
                Expression<String> fullNameExp = getFullNameExpression(root, criteriaBuilder, UserProfile_.lastName,
                        UserProfile_.firstName);
                final Predicate fullNamePre = criteriaBuilder.like(criteriaBuilder.upper(fullNameExp),
                        QueryEscapeUtils.toContainingCondition(fullName.toUpperCase(Locale.ENGLISH)));
                final Predicate staffIdPre = criteriaBuilder
                        .like(criteriaBuilder.upper(root.get(UserProfile_.staffId)), QueryEscapeUtils.toContainingCondition(staffId.toUpperCase(Locale.ENGLISH)));
                return criteriaBuilder.or(fullNamePre, staffIdPre);
            }
            return null;
        };
    }

    public static Specification<UserProfile> filterStartWithFullNameOrStaffId(String fullName, String staffId) {
        return (Specification<UserProfile>) (root, query, criteriaBuilder) -> {
            if (staffId != null) {
                Expression<String> firstLastNameExp = getFullNameExpression(root, criteriaBuilder, UserProfile_.firstName,
                        UserProfile_.lastName);
                Expression<String> lastFirstNameExp = getFullNameExpression(root, criteriaBuilder, UserProfile_.lastName,
                        UserProfile_.firstName);
                final Predicate firstLastNamePre = criteriaBuilder.like(criteriaBuilder.upper(firstLastNameExp),
                        QueryEscapeUtils.toEndingWithCondition(fullName.toUpperCase(Locale.ENGLISH)));
                final Predicate lastFirstNamePre = criteriaBuilder.like(criteriaBuilder.upper(lastFirstNameExp),
                        QueryEscapeUtils.toEndingWithCondition(fullName.toUpperCase(Locale.ENGLISH)));
                final Predicate staffIdPre = criteriaBuilder
                        .like(criteriaBuilder.upper(root.get(UserProfile_.staffId)), QueryEscapeUtils.toContainingCondition(staffId.toUpperCase(Locale.ENGLISH)));
                return criteriaBuilder.or(firstLastNamePre, lastFirstNamePre, staffIdPre);
            }
            return null;
        };
    }

    public static Specification<UserProfile> filterByBranch(Long branchId) {
        return (Specification<UserProfile>) (root, query, criteriaBuilder) -> {
            if (branchId != null) {
                return criteriaBuilder.equal(root.get(UserProfile_.branch).get(BranchEntity_.id), branchId);
            }
            return null;
        };
    }

    public static Specification<UserProfile> filterByRole(Long roleId) {
        return (Specification<UserProfile>) (root, query, criteriaBuilder) -> {
            if (roleId != null) {
                final Join<UserProfile, User> user = root.join(UserProfile_.user);
                final Join<User, UserRole> userRoles = user.join(User_.roles);
                final CriteriaBuilder.In<Long> inUserRoleIds = criteriaBuilder.in(userRoles.get(UserRole_.id));

                return inUserRoleIds.value(roleId);
            }
            return null;
        };
    }

    public static Specification<UserProfile> filterByIsActive(Boolean isActive) {
        return (Specification<UserProfile>) (root, query, criteriaBuilder) -> {
            if (isActive != null) {
                final Join<UserProfile, User> user = root.join(UserProfile_.user);
                return criteriaBuilder.equal(user.get(User_.enabled), isActive);
            }
            return null;
        };
    }

    public static Specification<UserProfile> sortByFields(SearchUserProfileRequest request) {
        return (Specification<UserProfile>) (root, query, criteriaBuilder) -> {

            final Expression<?> orderByExpression = getOrderFieldName(root, criteriaBuilder, request);

            if (Sort.Direction.DESC.equals(request.getDirection())) {
                query.orderBy(criteriaBuilder.desc(orderByExpression));
            } else {
                query.orderBy(criteriaBuilder.asc(orderByExpression));
            }

            return null;
        };
    }

    /**
     * get order field name
     */
    private static Expression<?> getOrderFieldName(Root<UserProfile> root, CriteriaBuilder criteriaBuilder,
                                                   SearchUserProfileRequest request) {

        final Expression<?> expression = root.get(AuditEntity_.createdDate);
        if (request.getSortingField() != null) {
            switch (request.getSortingField()) {
                case REQUESTED_DATE:
                    return expression;
                case FULL_NAME:
                    return getFullNameExpression(root, criteriaBuilder, UserProfile_.lastName, UserProfile_.firstName);

                default:
                    return expression;
            }
        }
        return expression;
    }

    private static Expression<String> getFullNameExpression(Root<UserProfile> root, CriteriaBuilder criteriaBuilder,
                                                            SingularAttribute<UserProfile, String> lastName, SingularAttribute<UserProfile, String> firstName) {
        return criteriaBuilder
                .function("CONCAT_WS", String.class, criteriaBuilder.literal(""), root.get(lastName), root.get(firstName));
    }

}