package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.LaborForce;
import com.dminc.prasac.model.entity.LaborForceEntity;
import com.dminc.prasac.repository.LaborForceRepository;

@Service
public class LaborForceService extends CommonService<LaborForce, LaborForceEntity> {

    public LaborForceService(LaborForceRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, LaborForce.class);
    }
}



