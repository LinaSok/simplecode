package com.dminc.prasac.service.selection;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.RepaymentMode;
import com.dminc.prasac.model.entity.RepaymentModeEntity;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.repository.RepaymentModeRepository;

@Service
public class RepaymentModeService extends CommonService<RepaymentMode, RepaymentModeEntity> {
    private final RepaymentModeRepository repository;

    public RepaymentModeService(RepaymentModeRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, RepaymentMode.class);
        this.repository = repository;
    }

    @Cacheable(value = "getRepaymentModeByCode", key = "#code", condition = "#code!=null", unless = LocalConstants.RESULT_NULL)
    public RepaymentMode getByCode(String code) throws BusinessException {
        if (StringUtils.isEmpty(code)) {
            return null;
        }
        return convert(repository.findTopByCode(code));
    }
}
