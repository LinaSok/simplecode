package com.dminc.prasac.service.selection;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.ClientType;
import com.dminc.prasac.model.entity.selection.ClientTypeEntity;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.repository.selection.ClientTypeRepository;

@Service
public class ClientTypeService extends CommonService<ClientType, ClientTypeEntity> {
    private final ClientTypeRepository repository;

    public ClientTypeService(ClientTypeRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, ClientType.class);
        this.repository = repository;
    }

    @Cacheable(value = "clientIdentificationByCode", key = "#code", condition = "#code!=null", unless = LocalConstants.RESULT_NULL)
    public ClientType getByCode(String code) throws BusinessException {
        if (StringUtils.isEmpty(code)) {
            return null;
        }
        return convert(repository.findTopByCode(code));
    }
}
