package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.ClientWorkingTime;
import com.dminc.prasac.model.entity.ClientWorkingTimeEntity;
import com.dminc.prasac.repository.ClientWorkingTimeRepository;

@Service
public class ClientWorkingTimeService extends CommonService<ClientWorkingTime, ClientWorkingTimeEntity> {
    public ClientWorkingTimeService(ClientWorkingTimeRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, ClientWorkingTime.class);
    }
}
