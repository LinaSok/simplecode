package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.ClientResidentStatus;
import com.dminc.prasac.model.entity.ClientResidentStatusEntity;
import com.dminc.prasac.repository.ClientResidentStatusRepository;

@Service
public class ClientResidentStatusService extends CommonService<ClientResidentStatus, ClientResidentStatusEntity> {
    public ClientResidentStatusService(ClientResidentStatusRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, ClientResidentStatus.class);
    }
}
