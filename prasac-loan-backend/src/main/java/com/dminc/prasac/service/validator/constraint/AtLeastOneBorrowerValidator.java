package com.dminc.prasac.service.validator.constraint;

import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dminc.prasac.model.entity.selection.ClientTypeEntity;
import com.dminc.prasac.model.request.ClientPreScreenRequest;
import com.dminc.prasac.model.request.ClientRequest;
import com.dminc.prasac.service.ObjectsConverter;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AtLeastOneBorrowerValidator implements ConstraintValidator<AtLeastOneBorrower, List<ClientPreScreenRequest>> {
    private final ObjectsConverter converter;

    @Override
    public boolean isValid(List<ClientPreScreenRequest> clients, ConstraintValidatorContext context) {
        return validateBorrower(converter.getObjectsMapper().map(clients, ClientRequest.class));
    }

    public static boolean validateBorrower(List<ClientRequest> clients) {
        if (CollectionUtils.isEmpty(clients)) {
            return true;
        }
        final Predicate<ClientRequest> predicateAtLeastOneBorrower = client -> Objects.nonNull(client.getClientType()) && client
                .getClientType().equals(ClientTypeEntity.TYPE_BORROWER_ID);

        return clients.stream().filter(predicateAtLeastOneBorrower).findFirst().isPresent();
    }
}
