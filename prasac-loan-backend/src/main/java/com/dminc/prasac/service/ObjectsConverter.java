package com.dminc.prasac.service;

import com.dminc.prasac.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.DefaultOrikaObjectsConverter;
import com.dminc.prasac.converter.ClientEntityToClientResponse;
import com.dminc.prasac.converter.DocumentEntityToDocument;
import com.dminc.prasac.converter.LoanEntityToLoan;
import com.dminc.prasac.converter.LongToAirPollutionEntityConverter;
import com.dminc.prasac.converter.LongToBranchEntityConverter;
import com.dminc.prasac.converter.LongToBusinessActivityEntityConverter;
import com.dminc.prasac.converter.LongToBusinessExperienceEntityConverter;
import com.dminc.prasac.converter.LongToBusinessOwnerEntityConverter;
import com.dminc.prasac.converter.LongToClientBusinessCashFlowEntityConverter;
import com.dminc.prasac.converter.LongToClientBusinessEntityConverter;
import com.dminc.prasac.converter.LongToClientBusinessLocationStatusEntityConverter;
import com.dminc.prasac.converter.LongToClientBusinessPeriodEntityConverter;
import com.dminc.prasac.converter.LongToClientBusinessProgressEntityConverter;
import com.dminc.prasac.converter.LongToClientEntityConverter;
import com.dminc.prasac.converter.LongToClientIdentificationTypeEntityConverter;
import com.dminc.prasac.converter.LongToClientLoanConverter;
import com.dminc.prasac.converter.LongToClientOccupationEntity;
import com.dminc.prasac.converter.LongToClientRelationshipEntity;
import com.dminc.prasac.converter.LongToClientResidentStatusEntity;
import com.dminc.prasac.converter.LongToClientTypeEntityConverter;
import com.dminc.prasac.converter.LongToClientWorkingTimeEntity;
import com.dminc.prasac.converter.LongToCollateralAreaEntityConverter;
import com.dminc.prasac.converter.LongToCollateralDescriptionEntityConverter;
import com.dminc.prasac.converter.LongToCollateralRegistrationEntityConverter;
import com.dminc.prasac.converter.LongToCollateralTitleTypeEntityConverter;
import com.dminc.prasac.converter.LongToCommuneEntityConverter;
import com.dminc.prasac.converter.LongToCurrencyEntityConverter;
import com.dminc.prasac.converter.LongToDistrictEntityConverter;
import com.dminc.prasac.converter.LongToDocumentTypeEntityConverter;
import com.dminc.prasac.converter.LongToEnergyEntityConverter;
import com.dminc.prasac.converter.LongToFinancialStatementBalanceEntityConverter;
import com.dminc.prasac.converter.LongToFinancialStatementGuarantorEntityConverter;
import com.dminc.prasac.converter.LongToFinancialStatementIncomeEntityConverter;
import com.dminc.prasac.converter.LongToHealthSafetyEntityConverter;
import com.dminc.prasac.converter.LongToLaborForceEntityConverter;
import com.dminc.prasac.converter.LongToLicenseContractValidityEntityConverter;
import com.dminc.prasac.converter.LongToLoanCollateralEntityConverter;
import com.dminc.prasac.converter.LongToLoanEntityConverter;
import com.dminc.prasac.converter.LongToLoanPurposeEntityConverter;
import com.dminc.prasac.converter.LongToLoanRequestTypeEntityConverter;
import com.dminc.prasac.converter.LongToLoanStatusEntityConverter;
import com.dminc.prasac.converter.LongToLoanTypeEntityConverter;
import com.dminc.prasac.converter.LongToMaritalStatusEntity;
import com.dminc.prasac.converter.LongToMarketValueAnalyseDescriptionEntityConverter;
import com.dminc.prasac.converter.LongToNationalityEntityConverter;
import com.dminc.prasac.converter.LongToNaturalResourceEntityConverter;
import com.dminc.prasac.converter.LongToPositionEntityConverter;
import com.dminc.prasac.converter.LongToProductWasteEntityConverter;
import com.dminc.prasac.converter.LongToPropertyTypeEntityConverter;
import com.dminc.prasac.converter.LongToProvinceEntityConverter;
import com.dminc.prasac.converter.LongToRepaymentModeEntityConverter;
import com.dminc.prasac.converter.LongToUserEntityConverter;
import com.dminc.prasac.converter.LongToUserRoleConverter;
import com.dminc.prasac.converter.LongToVillageEntityConverter;
import com.dminc.prasac.converter.LongToWaterPollutionEntityConverter;
import com.dminc.prasac.converter.StringToGender;
import com.dminc.prasac.converter.UserProfileToProfileResponse;
import com.dminc.prasac.repository.document.BusinessDocumentRepository;
import com.dminc.prasac.repository.document.ClientDocumentRepository;
import com.dminc.prasac.repository.document.CollateralDocumentRepository;
import com.dminc.prasac.repository.document.LiabilityDocumentRepository;
import com.dminc.prasac.repository.document.LoanDocumentRepository;
import com.dminc.prasac.repository.selection.VillageRepository;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFactory;

/**
 * Created by carlos on 14/11/17.
 */
@SuppressWarnings("PMD.UseUtilityClass")
@Service
@Slf4j
public class ObjectsConverter extends DefaultOrikaObjectsConverter {
    @Autowired
    public ObjectsConverter(MapperFactory factory, ClientDocumentRepository clientDocRepo, LoanDocumentRepository loanDocRepo,
                            CollateralDocumentRepository collateralDocRepo, BusinessDocumentRepository businessDocRepo,
                            LiabilityDocumentRepository liabilityDocRepo, VillageRepository villageRepository, UserRepository userRepository) {
        super(factory);
        setCustomConverters(factory, clientDocRepo, loanDocRepo, collateralDocRepo, businessDocRepo, liabilityDocRepo,
                villageRepository, userRepository);
    }

    public static void setCustomConverters(MapperFactory factory, ClientDocumentRepository clientDocRepo,
                                           LoanDocumentRepository loanDocRepo, CollateralDocumentRepository collateralDocRepo,
                                           BusinessDocumentRepository businessDocRepo, LiabilityDocumentRepository liabilityDocRepo,
                                           VillageRepository villageRepository, UserRepository userRepository) {
        factory.getConverterFactory().registerConverter(new LongToClientTypeEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToClientIdentificationTypeEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToNationalityEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToVillageEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToCurrencyEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToLoanStatusEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToBranchEntityConverter());
        factory.getConverterFactory().registerConverter(new UserProfileToProfileResponse());
        factory.getConverterFactory().registerConverter(new LongToCommuneEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToDistrictEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToProvinceEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToPositionEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToUserRoleConverter());
        factory.getConverterFactory().registerConverter(new LongToClientEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToLoanRequestTypeEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToRepaymentModeEntityConverter());
        factory.getConverterFactory().registerConverter(new ClientEntityToClientResponse(villageRepository));
        factory.getConverterFactory().registerConverter(new StringToGender());
        factory.getConverterFactory().registerConverter(new LongToUserEntityConverter());
        factory.getConverterFactory().registerConverter(
                new LoanEntityToLoan(clientDocRepo, loanDocRepo, collateralDocRepo, businessDocRepo, liabilityDocRepo,
                        villageRepository, userRepository));
        factory.getConverterFactory().registerConverter(new LongToClientOccupationEntity());
        factory.getConverterFactory().registerConverter(new LongToClientRelationshipEntity());
        factory.getConverterFactory().registerConverter(new LongToClientResidentStatusEntity());
        factory.getConverterFactory().registerConverter(new LongToClientWorkingTimeEntity());
        factory.getConverterFactory().registerConverter(new LongToMaritalStatusEntity());
        factory.getConverterFactory().registerConverter(new LongToLoanTypeEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToLoanPurposeEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToLoanEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToBusinessExperienceEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToClientBusinessCashFlowEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToClientBusinessLocationStatusEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToClientBusinessPeriodEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToClientBusinessProgressEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToLicenseContractValidityEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToBusinessOwnerEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToCollateralAreaEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToCollateralDescriptionEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToCollateralRegistrationEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToCollateralTitleTypeEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToPropertyTypeEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToMarketValueAnalyseDescriptionEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToLoanCollateralEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToEnergyEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToBusinessActivityEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToHealthSafetyEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToLaborForceEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToNaturalResourceEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToProductWasteEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToWaterPollutionEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToAirPollutionEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToFinancialStatementIncomeEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToClientLoanConverter());
        factory.getConverterFactory().registerConverter(new LongToClientBusinessEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToFinancialStatementBalanceEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToFinancialStatementGuarantorEntityConverter());
        factory.getConverterFactory().registerConverter(new LongToDocumentTypeEntityConverter());
        factory.getConverterFactory().registerConverter(new DocumentEntityToDocument());

    }
}
