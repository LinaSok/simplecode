package com.dminc.prasac.service.loan;

import com.dminc.common.tools.exceptions.InternalBusinessException;
import com.dminc.common.tools.exceptions.ItemNotFoundBusinessException;
import com.dminc.prasac.integration.domain.SavingAccount;
import com.dminc.prasac.integration.domain.request.CollateralPoolRequest;
import com.dminc.prasac.integration.domain.request.FCLoanRequest;
import com.dminc.prasac.integration.service.CollateralPoolService;
import com.dminc.prasac.integration.service.CollateralService;
import com.dminc.prasac.integration.service.CrmClientService;
import com.dminc.prasac.integration.service.PrasacLoanService;
import com.dminc.prasac.integration.service.SavingAccountService;
import com.dminc.prasac.model.dto.misc.CollateralDescription;
import com.dminc.prasac.model.dto.user.UserProfile;
import com.dminc.prasac.model.entity.BranchEntity;
import com.dminc.prasac.model.entity.LoanCollateralEntity;
import com.dminc.prasac.model.entity.client.ClientEntity;
import com.dminc.prasac.model.entity.loan.ClientLoan;
import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.misc.FlexCubeIntegrationStatus;
import com.dminc.prasac.model.misc.LoanStatusEnum;
import com.dminc.prasac.repository.ClientRepository;
import com.dminc.prasac.repository.loan.LoanRepository;
import com.dminc.prasac.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.dminc.prasac.model.entity.selection.ClientTypeEntity.TYPE_BORROWER_ID;
import static com.dminc.prasac.model.entity.selection.ClientTypeEntity.TYPE_CO_BORROWER_ID;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class LoanFlexCubeIntegrationImpl {
    public static final Predicate<LoanCollateralEntity> FILTER_COLLATERAL_PREDICATE = collateral -> CollateralDescription.COLLATERAL_DESC.equals(collateral.getCollateralDescription().getId());
    private static final Predicate<ClientLoan> FILTER_CO_BORROWER_PREDICATE = clientLoan -> TYPE_CO_BORROWER_ID.equals(clientLoan.getClientType().getId());
    private static final Predicate<ClientLoan> FIND_BORROWER = clientLoan -> TYPE_BORROWER_ID.equals(clientLoan.getClientType().getId());
    private final LoanRepository repository;
    private final SavingAccountService savingAccountService;
    private final ClientRepository clientRepository;
    private final CollateralService collateralService;
    private final CollateralPoolService collateralPoolService;
    private final PrasacLoanService prasacLoanService;
    private final CrmClientService crmClientService;
    private final UserService userService;

    @Transactional(noRollbackFor = InternalBusinessException.class)
    //@Async
    public void integrateFlexCube(long id) {
        final Optional<LoanEntity> optionalEntity = repository.findById(id);
        final LoanEntity entity = optionalEntity.orElseThrow(() -> new ItemNotFoundBusinessException("Loan not found"));

        if (!LoanStatusEnum.APPROVED.equals(LoanStatusEnum.getInstance(entity.getLoanStatus().getId()))) {
            throw new InternalBusinessException("Loan can not be submitted to Flexcube");
        }

        if (FlexCubeIntegrationStatus.SUCCESS.equals(entity.getIntegrationStatus())) {
            throw new InternalBusinessException("Loan was already submitted to Flexcube");
        }
        try {
            final UserProfile userProfile = userService.getUserProfile(entity.getAssignedUser().getId());
            createClients(entity.getClients(), entity.getBranch(), userProfile.getStaffId());
            final ClientEntity borrower = entity.getClientLoans().stream().filter(FIND_BORROWER)
                    .findFirst()
                    .get()
                    .getClient();
            final List<ClientEntity> joiners = entity.getClientLoans().stream()
                    .filter(FILTER_CO_BORROWER_PREDICATE)
                    .map(ClientLoan::getClient)
                    .collect(Collectors.toList());
            createSavingAccount(entity, borrower, joiners);
            createCollateral(entity, borrower);
            createCollateralPool(entity, borrower);
            createLoan(entity);
        } catch (InternalBusinessException | IOException e) {
            entity.setIntegrationStatus(FlexCubeIntegrationStatus.FAIL);
            repository.saveAndFlush(entity);
            throw new InternalBusinessException(e);
        }
    }

    private void createSavingAccount(LoanEntity entity, ClientEntity client, List<ClientEntity> joiners) throws IOException {
        final SavingAccount savingAccount = savingAccountService.create(client, joiners, entity.getCurrency());
        client.setSavingAccountNumber(savingAccount.getAccountNumber());
        clientRepository.saveAndFlush(client);
    }

    private void createLoan(LoanEntity entity) throws IOException {
        final FCLoanRequest loan = prasacLoanService.createLoan(entity);
        entity.setLoanNumber(loan.getLoanNumber());
        entity.setIntegrationStatus(FlexCubeIntegrationStatus.SUCCESS);
        repository.saveAndFlush(entity);
    }


    private void createClients(List<ClientEntity> clients, BranchEntity branch, String staffId) throws IOException {
        for (ClientEntity client : clients) {
            createClient(client, branch, staffId);
        }
    }

    private void createClient(ClientEntity client, BranchEntity branch, String staffId) throws IOException {
        // Flexcube is required branch in order to create.
        client.setBranch(branch);
        crmClientService.create(client, staffId);
        log.info(client.toString());
        clientRepository.saveAndFlush(client);
    }

    private void createCollateral(LoanEntity loanEntity, ClientEntity borrower) {
        loanEntity.getCollateralList().stream()
                .filter(FILTER_COLLATERAL_PREDICATE)
                .forEach(col ->
                        collateralService.create(col, borrower, loanEntity.getCurrency().getCode(), loanEntity.getBranch().getCode()));
    }

    private void createCollateralPool(LoanEntity loanEntity, ClientEntity borrower) throws IOException {
        final List<String> collateralCodes = loanEntity.getCollateralList().stream()
                .filter(FILTER_COLLATERAL_PREDICATE)
                .map(LoanCollateralEntity::getCollateralTitleDeedNo)
                .collect(Collectors.toList());
        final CollateralPoolRequest pool = collateralPoolService.create(collateralCodes, loanEntity.getBranch().getCode(), borrower.getCif(), loanEntity.getCurrency().getCode());
        borrower.setCollateralPoolCode(pool.getCollateralPoolCode());
        clientRepository.saveAndFlush(borrower);
    }
}
