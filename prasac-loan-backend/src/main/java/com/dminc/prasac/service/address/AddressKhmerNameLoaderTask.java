package com.dminc.prasac.service.address;

import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.core.io.ClassPathResource;

import com.dminc.common.tools.exceptions.InternalBusinessException;
import com.dminc.prasac.model.dto.address.StaticData;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.exception.CustomChangeException;
import liquibase.exception.LiquibaseException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;
import liquibase.statement.SqlStatement;
import liquibase.statement.core.RawSqlStatement;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuppressWarnings("PMD.JUnit4TestShouldUseBeforeAnnotation")
public class AddressKhmerNameLoaderTask implements CustomTaskChange {

    private final ObjectMapper mapper = new ObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true);
    private static final String PROVINCE_CODE_FIELD = "PCODE";

    private static Long parseLong(String longText) throws InternalBusinessException {
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        Long result;
        try {
            result = numberFormat.parse(longText).longValue();
        } catch (ParseException ex) {
            throw new InternalBusinessException(ex);
        }

        return result;
    }

    @Override
    @SneakyThrows
    public void execute(Database database) throws CustomChangeException {
        try {
            final InputStream file = new ClassPathResource("static/address_in_khmer.min.json").getInputStream();
            final StaticData staticData = mapper.readValue(file, StaticData.class);
            updateProvince(database, staticData);
            updateDistricts(database, staticData);
            updateCommunes(database, staticData);
            updateVillages(database, staticData);
        } catch (IOException | InternalBusinessException e) {
            log.error(e.getMessage(), e);
        }
    }

    private void updateProvince(Database database, StaticData staticData) throws InternalBusinessException, LiquibaseException {
        final List<Province> provinces = getProvinces(staticData);
        final List<RawSqlStatement> statements = new ArrayList<>();
        for (int i = 0; i < provinces.size(); i++) {
            final Province province = provinces.get(i);
            final StringBuilder sql = new StringBuilder("update ps_province");
            sql.append(String.format(" set khmer_name='%s'", province.getKhmerName()))
                    .append(String.format(" where code=%s", province.getCode()));
            RawSqlStatement rawSqlStatement = new RawSqlStatement(sql.toString());
            statements.add(rawSqlStatement);
        }
        database.execute(statements.toArray(new RawSqlStatement[statements.size()]), null);
    }


    private void updateDistricts(Database database, StaticData staticData) throws LiquibaseException, InternalBusinessException {
        final List<SqlStatement> statements = new ArrayList<>();
        final District[] districts = getDistricts(staticData).toArray(new District[0]);
        for (int i = 0; i < districts.length; i++) {
            final District district = districts[i];
            final StringBuilder sql = new StringBuilder("update ps_district d inner join ps_province p on d.province_id=p.id");
            sql.append(String.format(" set d.khmer_name='%s'", district.getKhmerName()))
                    .append(String.format(" where d.code=%s and p.code=%s", district.getCode(), district.getProvinceCode()));
            RawSqlStatement rawSqlStatement = new RawSqlStatement(sql.toString());
            statements.add(rawSqlStatement);
        }
        database.execute(statements.toArray(new RawSqlStatement[statements.size()]), null);
    }

    private void updateCommunes(Database database, StaticData staticData) throws LiquibaseException, InternalBusinessException {
        final List<SqlStatement> statements = new ArrayList<>();
        final Commune[] communes = getCommunes(staticData).toArray(new Commune[0]);
        for (int i = 0; i < communes.length; i++) {
            final Commune commune = communes[i];
            final StringBuilder sql = new StringBuilder("update ps_commune c inner join ps_district d on c.district_id=d.id inner join ps_province p on d.province_id=p.id");
            sql.append(String.format(" set c.khmer_name='%s'", commune.getKhmerName()))
                    .append(String.format(" where d.code=%s and p.code=%s and c.code=%s",
                            commune.getDistrictCode(),
                            commune.getProvinceCode(),
                            commune.getCode()));
            RawSqlStatement rawSqlStatement = new RawSqlStatement(sql.toString());
            statements.add(rawSqlStatement);
        }
        database.execute(statements.toArray(new RawSqlStatement[statements.size()]), null);
    }

    private void updateVillages(Database database, StaticData staticData) throws LiquibaseException, InternalBusinessException {
        final List<SqlStatement> statements = new ArrayList<>();
        final Village[] villages = getVillages(staticData).toArray(new Village[0]);
        for (int i = 0; i < villages.length; i++) {
            final Village village = villages[i];
            final StringBuilder sql = new StringBuilder("update ps_village v inner join ps_commune c on v.commune_id=c.id  inner join ps_district d on c.district_id=d.id inner join ps_province p on d.province_id=p.id");
            sql.append(String.format(" set v.khmer_name='%s'", village.getKhmerName()))
                    .append(String.format(" where d.code=%s and p.code=%s and c.code=%s and v.code=%s",
                            village.getDistrictCode(),
                            village.getProvinceCode(),
                            village.getCommuneCode(),
                            village.getCode()));
            RawSqlStatement rawSqlStatement = new RawSqlStatement(sql.toString());
            statements.add(rawSqlStatement);
        }
        database.execute(statements.toArray(new RawSqlStatement[statements.size()]), null);
    }

    private List<Province> getProvinces(StaticData staticData) throws InternalBusinessException {
        Map<Long, Province> provinceMap = new HashMap<>();
        for (Map<String, String> v : staticData.getAddresses()) {
            final Long provinceCode = parseLong(v.get(PROVINCE_CODE_FIELD));
            final String provinceName = v.get("PROVINCE_KH");
            final Province provinceBuilder = Province.builder().code(provinceCode).khmerName(provinceName).build();
            provinceMap.putIfAbsent(provinceBuilder.getCode(), provinceBuilder);
        }
        return new ArrayList<>(provinceMap.values());
    }

    private Set<District> getDistricts(StaticData staticData) throws InternalBusinessException {
        Set<District> districts = new HashSet<>();
        for (Map<String, String> m : staticData.getAddresses()) {
            final Long code = parseLong(m.get("DCODE"));
            final District district = District.builder()
                    .code(code)
                    .khmerName(m.get("DISTRICT_KH"))
                    .provinceCode(parseLong(m.get(PROVINCE_CODE_FIELD)))
                    .build();
            districts.add(district);

        }
        return districts;
    }

    private Set<Commune> getCommunes(StaticData staticData) throws InternalBusinessException {
        Set<Commune> communes = new HashSet<>();
        for (Map<String, String> m : staticData.getAddresses()) {
            final long code = parseLong(m.get("CCODE"));
            final Commune commune = Commune.builder()
                    .code(code)
                    .khmerName(m.get("COMMUNE_KH"))
                    .districtCode(parseLong(m.get("DCODE")))
                    .provinceCode(parseLong(m.get(PROVINCE_CODE_FIELD)))
                    .build();
            communes.add(commune);
        }
        return communes;
    }

    private Set<Village> getVillages(StaticData staticData) throws InternalBusinessException {
        Set<Village> villages = new HashSet<>();
        for (Map<String, String> m : staticData.getAddresses()) {
            final long code = parseLong(m.get("VCODE"));
            final Village village = Village.builder()
                    .code(code)
                    .khmerName(m.get("VILLAGE_KH"))
                    .communeCode(parseLong(m.get("CCODE")))
                    .location(parseLong(m.get("LOCATION")))
                    .districtCode(parseLong(m.get("DCODE")))
                    .provinceCode(parseLong(m.get(PROVINCE_CODE_FIELD)))
                    .build();
            villages.add(village);
        }
        return villages;
    }

    @Override
    public String getConfirmationMessage() {
        return null;
    }

    @Override
    public void setUp() throws SetupException {
        //Nothing to implement
    }

    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {
        //Nothing to implement
    }

    @Override
    public ValidationErrors validate(Database database) {
        return null;
    }

    @Builder
    @Getter
    @Setter
    @EqualsAndHashCode
    private static class Province {
        private long id;
        private long code;
        private String name;
        private String khmerName;
        private long countryId;
    }

    @Builder
    @Getter
    @Setter
    @EqualsAndHashCode
    private static class District {
        private long id;
        private long code;
        private String name;
        private String khmerName;
        private long provinceCode;
    }

    @Builder
    @Getter
    @Setter
    @EqualsAndHashCode
    private static class Commune {
        private long id;
        private long code;
        private String name;
        private String khmerName;
        private long districtCode;
        private long provinceCode;
    }

    @Builder
    @Getter
    @Setter
    @EqualsAndHashCode
    private static class Village {
        private long id;
        private long code;
        private String name;
        private String khmerName;
        private long location;
        private long communeCode;
        private long provinceCode;
        private long districtCode;
    }
}
