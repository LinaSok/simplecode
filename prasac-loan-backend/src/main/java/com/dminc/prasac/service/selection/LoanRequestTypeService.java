package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.LoanRequestType;
import com.dminc.prasac.model.entity.LoanRequestTypeEntity;
import com.dminc.prasac.repository.LoanRequestTypeRepository;

@Service
public class LoanRequestTypeService extends CommonService<LoanRequestType, LoanRequestTypeEntity> {

    public LoanRequestTypeService(LoanRequestTypeRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, LoanRequestType.class);
    }
}



