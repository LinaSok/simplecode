package com.dminc.prasac.service.validator.constraint;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dminc.prasac.model.request.ClientRequest;
import com.dminc.prasac.model.request.LoanAppraisalClientRequest;
import com.dminc.prasac.service.ObjectsConverter;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ValidCIFsAppraisalValidator implements ConstraintValidator<ValidCIFs, List<LoanAppraisalClientRequest>> {
    private final ObjectsConverter converter;
    private final ValidCIFsValidator validCIFsValidator;

    @Override
    public boolean isValid(List<LoanAppraisalClientRequest> clients, ConstraintValidatorContext context) {
        return validCIFsValidator.validateCIFs(converter.getObjectsMapper().map(clients, ClientRequest.class));
    }

}

