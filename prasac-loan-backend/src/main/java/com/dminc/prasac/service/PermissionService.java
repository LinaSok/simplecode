package com.dminc.prasac.service;

import com.dminc.prasac.model.entity.UserPermission;
import com.dminc.prasac.repository.PermissionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PermissionService {

    private final PermissionRepository permissionRepository;

    public Page<UserPermission> getPermission(Pageable pageable) {
        return permissionRepository.findAll(pageable);
    }
}
