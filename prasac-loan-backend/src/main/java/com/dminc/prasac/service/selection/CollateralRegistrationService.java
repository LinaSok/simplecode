package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.CollateralRegistration;
import com.dminc.prasac.model.entity.CollateralRegistrationEntity;
import com.dminc.prasac.repository.CollateralRegistrationRepository;

@Service
public class CollateralRegistrationService extends CommonService<CollateralRegistration, CollateralRegistrationEntity> {

    public CollateralRegistrationService(CollateralRegistrationRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, CollateralRegistration.class);
    }
}



