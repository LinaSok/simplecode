package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.BusinessOwner;
import com.dminc.prasac.model.entity.BusinessOwnerEntity;
import com.dminc.prasac.repository.BusinessOwnerRepository;

@Service
public class BusinessOwnerService extends CommonService<BusinessOwner, BusinessOwnerEntity> {

    public BusinessOwnerService(BusinessOwnerRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, BusinessOwner.class);
    }
}



