package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.WaterPollution;
import com.dminc.prasac.model.entity.WaterPollutionEntity;
import com.dminc.prasac.repository.WaterPollutionRepository;

@Service
public class WaterPollutionService extends CommonService<WaterPollution, WaterPollutionEntity> {

    public WaterPollutionService(WaterPollutionRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, WaterPollution.class);
    }
}



