package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.Energy;
import com.dminc.prasac.model.entity.EnergyEntity;
import com.dminc.prasac.repository.EnergyRepository;

@Service
public class EnergyService extends CommonService<Energy, EnergyEntity> {

    public EnergyService(EnergyRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, Energy.class);
    }
}



