package com.dminc.prasac.service.selection;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.MaritalStatus;
import com.dminc.prasac.model.entity.MaritalStatusEntity;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.repository.MaritalStatusRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class MaritalStatusService extends CommonService<MaritalStatus, MaritalStatusEntity> {
    private final MaritalStatusRepository repository;

    public MaritalStatusService(MaritalStatusRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {

        super(repository, converter, MaritalStatus.class);
        this.repository = repository;
    }

    @Cacheable(value = "getMaritalStatusByCode", key = "#code", condition = "#code!=null", unless = LocalConstants.RESULT_NULL)
    public MaritalStatus getByCode(String code) {
        if (StringUtils.isEmpty(code)) {
            return null;
        }
        return convert(repository.findByCode(code));
    }
}
