package com.dminc.prasac.service.address;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.springframework.util.ResourceUtils;

import com.dminc.common.tools.exceptions.InternalBusinessException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JSONFileService<T> {
    public static final long INIT_VERSION = 1L;

    private final ObjectMapper mapper;
    private final Class<T> clazz;
    private final String fileName;

    public JSONFileService(ObjectMapper mapper, Class clazz, String fileName) {
        this.mapper = mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        this.clazz = clazz;
        this.fileName = fileName;
    }

    protected T getFileData() throws InternalBusinessException {
        try {
            return this.mapper.readValue(getFile(), clazz);
        } catch (IOException e) {
            throw new InternalBusinessException(e);
        }
    }

    private File getFile() throws InternalBusinessException {
        try {
            return ResourceUtils.getFile("classpath:static/" + fileName + ".json");
        } catch (FileNotFoundException e) {
            throw new InternalBusinessException(e);
        }
    }

}

