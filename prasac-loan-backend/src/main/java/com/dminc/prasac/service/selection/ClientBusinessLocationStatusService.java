package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.ClientBusinessLocationStatus;
import com.dminc.prasac.model.entity.ClientBusinessLocationStatusEntity;
import com.dminc.prasac.repository.ClientBusinessLocationStatusRepository;

@Service
public class ClientBusinessLocationStatusService extends CommonService<ClientBusinessLocationStatus, ClientBusinessLocationStatusEntity> {

    public ClientBusinessLocationStatusService(ClientBusinessLocationStatusRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, ClientBusinessLocationStatus.class);
    }
}



