package com.dminc.prasac.service.selection;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.LoanType;
import com.dminc.prasac.model.entity.LoanTypeEntity;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.repository.LoanTypeRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class LoanTypeService extends CommonService<LoanType, LoanTypeEntity> {
    private final LoanTypeRepository repository;

    public LoanTypeService(LoanTypeRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, LoanType.class);
        this.repository = repository;
    }

    @Cacheable(value = "getLoanTypeByCode", key = "#code", condition = "#code!=null", unless = LocalConstants.RESULT_NULL)
    public LoanType getByCode(String code) {
        if (StringUtils.isNotEmpty(code)) {
            return convert(repository.findTopByCode(code));
        }
        return null;
    }
}



