package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.MarketValueAnalyseDescription;
import com.dminc.prasac.model.entity.MarketValueAnalyseDescriptionEntity;
import com.dminc.prasac.repository.MarketValueAnalyseDescriptionRepository;

@Service
public class MarketValueAnalyseDescriptionService extends CommonService<MarketValueAnalyseDescription, MarketValueAnalyseDescriptionEntity> {

    public MarketValueAnalyseDescriptionService(MarketValueAnalyseDescriptionRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, MarketValueAnalyseDescription.class);
    }
}



