package com.dminc.prasac.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.email.SpringJavaSenderImpl;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class EmailManagerService {

    @Autowired
    private SpringJavaSenderImpl javaSender;

    @Value("${mail.from.name}")
    private String fromName;

    @Value("${mail.from.address}")
    private String fromAddress;

    @Async
    public void sendMail(final String title, final String textMsg, final String to) {
        sendTemplateMail(textMsg, title, fromName, to);
    }


    @Async
    protected void sendTemplateMail(final String htmlString, final String title, final String fromName, final String... to) {

        trackLog(fromAddress, title, htmlString, to);
        javaSender.sendMail(fromName, fromAddress, title, htmlString, to);
        log.debug("Send email complete...");

    }

    private void trackLog(String fromAddress, String title, String bodyText, String... to) {
        log.debug("Send email from {}, to {}", fromAddress, to);
        log.debug("title :  {}", title);
        log.debug("bodyText :  {}", bodyText);
    }

}
