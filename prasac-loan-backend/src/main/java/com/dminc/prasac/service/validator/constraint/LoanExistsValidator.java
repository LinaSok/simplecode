package com.dminc.prasac.service.validator.constraint;

import java.util.Objects;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.exceptions.ItemNotFoundBusinessException;
import com.dminc.prasac.service.loan.LoanService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LoanExistsValidator implements ConstraintValidator<LoanExists, Long> {
    private final LoanService loanService;

    @Override
    public boolean isValid(Long loanId, ConstraintValidatorContext context) {
        try {
            return Objects.nonNull(loanService.getById(loanId));
        } catch (ItemNotFoundBusinessException e) {
            return false;
        }
    }

}

