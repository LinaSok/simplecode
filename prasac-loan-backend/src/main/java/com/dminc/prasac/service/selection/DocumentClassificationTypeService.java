package com.dminc.prasac.service.selection;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.DocumentClassificationType;
import com.dminc.prasac.model.entity.DocumentClassificationTypeEntity;
import com.dminc.prasac.repository.DocumentClassificationTypeRepository;

@Service
public class DocumentClassificationTypeService
        extends CommonService<DocumentClassificationType, DocumentClassificationTypeEntity> {

    public DocumentClassificationTypeService(DocumentClassificationTypeRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, DocumentClassificationType.class);
    }

    @Override
    @Cacheable(value = "DocumentClassificationType.getAll", key = "#root.methodName")
    public List<DocumentClassificationType> findAll() throws BusinessException {
        return super.findAll();
    }
}
