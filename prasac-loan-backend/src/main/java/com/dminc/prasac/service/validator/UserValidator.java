package com.dminc.prasac.service.validator;

import com.dminc.common.tools.exceptions.InvalidParametersBusinessException;
import com.dminc.prasac.model.entity.User;
import com.dminc.prasac.model.entity.UserProfile;
import com.dminc.prasac.model.entity.UserRole;
import com.dminc.prasac.repository.UserProfileRepository;
import com.dminc.prasac.repository.UserRepository;
import com.dminc.prasac.repository.UserRoleRepository;
import com.dminc.prasac.service.BusinessError;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserValidator {

    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;
    private final UserProfileRepository userProfileRepository;

    public boolean isUserExist(String username) {
        final Optional<User> existing = Optional.ofNullable(userRepository.findOneByUsername(username));
        return existing.isPresent();
    }

    public boolean isRoleExist(String roleName) {
        final Optional<UserRole> existing = userRoleRepository.findOneByName(roleName);
        return existing.isPresent();
    }

    public void validateStaffId(String staffId) {
        final UserProfile userProfile = userProfileRepository.findByStaffId(staffId);
        if (userProfile != null) {
            BusinessError.STAFF_ID_ALREADY_USED.throwExceptionHttpStatus400();
        }
    }

    public void validateStaffIdOnUpdate(String staffId, long userId) {
        final UserProfile userProfile = userProfileRepository.findByStaffId(staffId);
        if (userProfile != null && userId != userProfile.getUser().getId()) {
            BusinessError.STAFF_ID_ALREADY_USED.throwExceptionHttpStatus400();
        }
    }

    public void validateEmail(String email) {
        final UserProfile userProfile = userProfileRepository.findByEmail(email);
        if (!Objects.isNull(userProfile)) {
            throw new InvalidParametersBusinessException(String.format("User with email %s already exist", email), BusinessError.USER_WITH_EMAIL_ALREADY_EXIST.getCode());
        }
    }

    public static void validatePassword(String password) {

        if (StringUtils.isEmpty(password)) {
            BusinessError.INVALID_PASSWORD_RULE.throwExceptionHttpStatus400();
        }

        final boolean hasUppercase = !password.equals(StringUtils.lowerCase(password, Locale.ENGLISH));
        // password must contain an uppercase letter
        if (!hasUppercase) {
            log.warn("password should have at least one upper case");
            BusinessError.INVALID_PASSWORD_RULE.throwExceptionHttpStatus400();
        }
        //check length at least 8 char
        if (password.length() < 8) {
            log.warn("password length is too short at least 8 char.");
            BusinessError.INVALID_PASSWORD_RULE.throwExceptionHttpStatus400();
        }
        //check max length 15
        if (password.length() > 15) {
            log.warn("password length is too long.");
            BusinessError.INVALID_PASSWORD_RULE.throwExceptionHttpStatus400();
        }
        //check has number
        if (!Pattern.compile("[0-9]").matcher(password).find()) {
            log.warn("password have at least one digit number");
            BusinessError.INVALID_PASSWORD_RULE.throwExceptionHttpStatus400();
        }
    }
}
