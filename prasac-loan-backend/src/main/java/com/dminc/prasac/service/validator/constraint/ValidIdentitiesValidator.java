package com.dminc.prasac.service.validator.constraint;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dminc.prasac.integration.domain.CrmClient;
import com.dminc.prasac.integration.service.CrmClientService;
import com.dminc.prasac.model.dto.misc.Client;
import com.dminc.prasac.model.dto.misc.ClientIdentification;
import com.dminc.prasac.model.request.ClientPreScreenRequest;
import com.dminc.prasac.model.request.ClientRequest;
import com.dminc.prasac.service.ClientIdentificationService;
import com.dminc.prasac.service.ObjectsConverter;
import com.dminc.prasac.service.client.ClientService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ValidIdentitiesValidator implements ConstraintValidator<ValidIdentities, List<ClientPreScreenRequest>> {
    private final CrmClientService crmClientService;
    private final ClientService clientService;
    private final ClientIdentificationService idService;
    private final ObjectsConverter converter;

    @Override
    public boolean isValid(List<ClientPreScreenRequest> clients, ConstraintValidatorContext context) {
        return validateIdentities(converter.getObjectsMapper().map(clients, ClientRequest.class));
    }

    public boolean validateIdentities(List<ClientRequest> clients) {
        return clients.stream().allMatch(clientRequest -> {
            Predicate<ClientIdentification> notExistPredicate;

            final String cifRequest = clientRequest.getCif();

            if (StringUtils.isBlank(cifRequest)) {
                notExistPredicate = getIdentityPredicate(clientRequest);
            } else {
                notExistPredicate = getIdentityPredicateRegardingCif(clientRequest, cifRequest);
            }

            return clientRequest.getIdentifications().stream().allMatch(notExistPredicate);
        });
    }

    private Predicate<ClientIdentification> getIdentityPredicateRegardingCif(ClientRequest clientRequest, String cifRequest) {
        return identity -> {
            final Optional<CrmClient> crmClient = getCrmClient(identity);

            if (crmClient.isPresent()) {
                return crmClient.get().getCif().equals(cifRequest);
            } else {
                return notExistInMysql(clientRequest, identity);
            }
        };
    }

    private Predicate<ClientIdentification> getIdentityPredicate(ClientRequest clientRequest) {
        return identity -> {
            final Optional<ClientIdentification> identification = getIdentity(identity);
            final boolean isFirstCaseValid = !getCrmClient(identity).isPresent() && !identification.isPresent();

            // On create
            if (clientRequest.getId() == null) {
                return isFirstCaseValid;
            }
            // On update
            else {
                final boolean isSecondCaseValid =
                        identification.isPresent() && identification.get().getClient().equals(clientRequest.getId());
                return isFirstCaseValid || isSecondCaseValid;
            }
        };
    }

    private boolean notExistInMysql(ClientRequest clientRequest, ClientIdentification identity) {
        final Optional<ClientIdentification> identification = getIdentity(identity);
        final Optional<Client> client = identification.map(i -> clientService.getById(i.getClient())).orElse(Optional.empty());

        return !identification.isPresent() || matchCif(clientRequest, client);
    }

    public static boolean matchCif(ClientRequest clientRequest, Optional<Client> client) {
        if (Objects.isNull(clientRequest.getId())) {
            return client.isPresent() && StringUtils.isNotBlank(clientRequest.getCif()) && clientRequest.getCif()
                    .equals(client.get().getCif());
        } else {
            return true;
        }
    }

    private Optional<ClientIdentification> getIdentity(ClientIdentification identity) {
        return idService.getIdentity(identity.getIdNumber(), identity.getClientIdentificationType());
    }

    private Optional<CrmClient> getCrmClient(ClientIdentification identity) {
        return crmClientService.searchByIdentity(identity.getIdNumber(), identity.getClientIdentificationType());
    }
}
