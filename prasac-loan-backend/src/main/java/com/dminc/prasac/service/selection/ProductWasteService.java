package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.ProductWaste;
import com.dminc.prasac.model.entity.ProductWasteEntity;
import com.dminc.prasac.repository.ProductWasteRepository;

@Service
public class ProductWasteService extends CommonService<ProductWaste, ProductWasteEntity> {

    public ProductWasteService(ProductWasteRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, ProductWaste.class);
    }
}



