package com.dminc.prasac.service.validator.constraint;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dminc.prasac.integration.service.CrmClientService;
import com.dminc.prasac.model.dto.misc.Client;
import com.dminc.prasac.model.request.ClientPreScreenRequest;
import com.dminc.prasac.model.request.ClientRequest;
import com.dminc.prasac.service.ObjectsConverter;
import com.dminc.prasac.service.client.ClientService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ValidCIFsValidator implements ConstraintValidator<ValidCIFs, List<ClientPreScreenRequest>> {
    private final CrmClientService crmClientService;
    private final ClientService clientService;
    private final ObjectsConverter converter;

    @Override
    public boolean isValid(List<ClientPreScreenRequest> clients, ConstraintValidatorContext context) {
        return validateCIFs(converter.getObjectsMapper().map(clients, ClientRequest.class));
    }

    public boolean validateCIFs(List<ClientRequest> clients) {
        final Predicate<ClientRequest> hasCIFPredicate = client -> StringUtils.isNotBlank(client.getCif());
        final List<ClientRequest> clientWithCIFList = clients.stream().filter(hasCIFPredicate).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(clientWithCIFList)) {
            return true;
        }

        final Predicate<ClientRequest> validCIFPredicate = clientRequest -> {
            final boolean cifExist = crmClientService.exist(clientRequest.getCif());

            // On create
            if (clientRequest.getId() == null) {
                return cifExist;
            }
            // On Update
            else {
                final Optional<Client> client = clientService.getById(clientRequest.getId());
                return cifExist && client.isPresent() && ValidIdentitiesValidator.matchCif(clientRequest, client);
            }
        };

        return clientWithCIFList.stream().allMatch(validCIFPredicate);
    }

}

