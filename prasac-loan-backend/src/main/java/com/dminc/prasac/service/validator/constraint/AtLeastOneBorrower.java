package com.dminc.prasac.service.validator.constraint;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({FIELD, PARAMETER})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {AtLeastOneBorrowerValidator.class, AtLeastOneBorrowerAppraisalValidator.class})
public @interface AtLeastOneBorrower {
    String message() default "should contain at least one borrower";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    @Target({TYPE, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Documented
    @interface List {
        AtLeastOneBorrower[] value();
    }
}
