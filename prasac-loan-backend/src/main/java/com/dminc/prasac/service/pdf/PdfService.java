package com.dminc.prasac.service.pdf;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.dminc.prasac.model.dto.pdf.PDFs;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PdfService {

    private final ObjectMapper mapper = new ObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true);
    @Value("${server.baseUrl}")
    private String serverUrl;
    private PDFs listOfPdfs;

    //@Cacheable(value = "getAllPDFs", key = "#root.methodName")
    public PDFs getAllPDFs() {

        if (listOfPdfs == null) {
            log.debug("listOfPdf is null");
            try {
                final InputStream file = new ClassPathResource("static/pdfs/pdfs_descriptor.json").getInputStream();
                listOfPdfs = mapper.readValue(file, PDFs.class);
                listOfPdfs.getPdfs().forEach(pdf -> pdf.setUrl(String.format("%s%s", serverUrl, pdf.getUrl())));
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
        return listOfPdfs;
    }
}
