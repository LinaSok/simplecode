package com.dminc.prasac.service.selection;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.prasac.model.dto.address.Province;
import com.dminc.prasac.model.entity.selection.CommuneEntity;
import com.dminc.prasac.model.entity.selection.CountryEntity;
import com.dminc.prasac.model.entity.selection.ProvinceEntity;
import com.dminc.prasac.model.entity.selection.VillageEntity;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.repository.selection.CommuneRepository;
import com.dminc.prasac.repository.selection.CountryRepository;
import com.dminc.prasac.repository.selection.ProvinceRepository;
import com.dminc.prasac.repository.selection.VillageRepository;
import com.dminc.prasac.service.ObjectsConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LocationService {
    private final CountryRepository countryRepository;
    private final ProvinceRepository provinceRepository;
    private final VillageRepository villageRepository;
    private final CommuneRepository communeRepository;
    private final ObjectsConverter objectsConverter;

    @Transactional(readOnly = true)
    public Page<Province> getCambodiaProvinces(Pageable pageable) {

        return getProvinces(pageable, CountryEntity.CAMBODIA_ID);
    }

    @Transactional(readOnly = true)
    public Page<Province> getProvinces(Pageable pageable, Long countryId) {
        final CountryEntity country = countryRepository.findById(countryId).get();
        final Page<ProvinceEntity> provinces = provinceRepository.findByCountry(country, pageable);
        return provinces.map(province -> objectsConverter.getObjectsMapper().map(province, Province.class));
    }

    @Cacheable(value = "findVillageEntityByLocationCodes", key = "{#vcode, #ccode, #dcode, #pcode}", condition = "#vcode!=null && #ccode!=null && #dcode!=null && #pcode!=null", unless = LocalConstants.RESULT_NULL)
    public VillageEntity findVillageEntityByLocationCodes(String vcode, String ccode, String dcode, String pcode)
            throws BusinessException {
        return villageRepository.findVillageByLocationCodes(vcode, ccode, dcode, pcode);
    }

    @Cacheable(value = "findCommuneEntityByCodes", key = "{#ccode, #dcode, #pcode}", condition = "#ccode!=null && #dcode!=null && #pcode!=null", unless = LocalConstants.RESULT_NULL)
    public CommuneEntity findCommuneEntityByCodes(String ccode, String dcode, String pcode) {
        return communeRepository.findByCodes(Long.valueOf(ccode), Long.valueOf(dcode), Long.valueOf(pcode));
    }
}