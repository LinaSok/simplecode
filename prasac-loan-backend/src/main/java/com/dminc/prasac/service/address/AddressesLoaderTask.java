package com.dminc.prasac.service.address;

import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

import org.springframework.core.io.ClassPathResource;

import com.dminc.common.tools.exceptions.InternalBusinessException;
import com.dminc.prasac.model.dto.address.StaticData;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.exception.CustomChangeException;
import liquibase.exception.LiquibaseException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;
import liquibase.statement.SqlStatement;
import liquibase.statement.core.InsertOrUpdateStatement;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuppressWarnings({"PMD.AccessorMethodGeneration", "PMD.JUnit4TestShouldUseBeforeAnnotation", "CPD-START"})
public class AddressesLoaderTask implements CustomTaskChange {
    private final ObjectMapper mapper = new ObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true);
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_CODE = "code";
    private static final String COLUMN_PROVINCE_CODE = "PCODE";
    private static final String CCODE = "CCODE";
    private static final String DCODE = "DCODE";

    private static Long parseLong(String longText) throws InternalBusinessException {
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        Long result;
        try {
            result = numberFormat.parse(longText).longValue();
        } catch (ParseException ex) {
            throw new InternalBusinessException(ex);
        }

        return result;
    }

    @Override
    public void execute(Database database) throws CustomChangeException {
        try {
            final InputStream file = new ClassPathResource("static/address.min.json").getInputStream();
            final StaticData staticData = mapper.readValue(file, StaticData.class);
            List<SqlStatement> statements = new ArrayList<>();
            final List<Province> provinces = getProvinces(staticData);
            for (int i = 0; i < provinces.size(); i++) {
                InsertOrUpdateStatement statement = new InsertOrUpdateStatement("", "", "ps_province", "id");
                Province province = provinces.get(i);
                long id = i + 1L;
                statement.addColumnValue(COLUMN_ID, id);
                statement.addColumnValue(COLUMN_NAME, province.getName());
                statement.addColumnValue(COLUMN_CODE, province.getCode());
                statement.addColumnValue("country_id", 1);
                province.setId(id);
                statements.add(statement);
            }
            database.execute(statements.toArray(new InsertOrUpdateStatement[statements.size()]), null);
            insertDistricts(database, staticData, provinces);
        } catch (IOException | LiquibaseException | InternalBusinessException e) {
            log.error(e.getMessage(), e);
        }
    }

    private void insertDistricts(Database database, StaticData staticData, List<Province> provinces)
            throws LiquibaseException {
        final List<SqlStatement> statements = new ArrayList<>();
        final District[] districts = getDistricts(staticData).toArray(new District[0]);
        for (int i = 0; i < districts.length; i++) {
            InsertOrUpdateStatement statement = new InsertOrUpdateStatement("", "", "ps_district", "id");
            District district = districts[i];
            long id = i + 1L;
            statement.addColumnValue(COLUMN_ID, id);
            statement.addColumnValue(COLUMN_NAME, district.getName());
            statement.addColumnValue(COLUMN_CODE, district.getCode());
            final Predicate<Province> filterProvince = p -> district.provinceCode == p.code;
            Optional<Province> province = provinces.stream().filter(filterProvince).findFirst();
            long provinceId = -1L;
            if (province.isPresent()) {
                provinceId = province.get().id;
            }
            statement.addColumnValue("province_id", provinceId);
            district.setId(id);
            statements.add(statement);
        }
        database.execute(statements.toArray(new InsertOrUpdateStatement[statements.size()]), null);
        insertCommunes(database, staticData, Arrays.asList(districts));
    }

    private void insertCommunes(Database database, StaticData staticData, List<District> districts)
            throws LiquibaseException {
        final List<SqlStatement> statements = new ArrayList<>();
        final Commune[] communes = getCommunes(staticData).toArray(new Commune[0]);
        for (int i = 0; i < communes.length; i++) {
            InsertOrUpdateStatement statement = new InsertOrUpdateStatement("", "", "ps_commune", "id");
            Commune commune = communes[i];
            long id = i + 1L;
            statement.addColumnValue(COLUMN_ID, id);
            statement.addColumnValue(COLUMN_NAME, commune.getName());
            statement.addColumnValue(COLUMN_CODE, commune.getCode());
            final Predicate<District> filterDistrict = d -> commune.getDistrictCode() == d.code
                    && commune.getProvinceCode() == d.provinceCode;
            long districtId = -1L;
            final Optional<District> district = districts.stream().filter(filterDistrict).findFirst();
            if (district.isPresent()) {
                districtId = district.get().id;
            }
            statement.addColumnValue("district_id", districtId);
            commune.setId(id);
            statements.add(statement);
        }
        database.execute(statements.toArray(new InsertOrUpdateStatement[statements.size()]), null);
        insertVillages(database, staticData, Arrays.asList(communes));
    }

    private void insertVillages(Database database, StaticData staticData, List<Commune> communes)
            throws LiquibaseException {
        final List<SqlStatement> statements = new ArrayList<>();
        final Village[] villages = getVillages(staticData).toArray(new Village[0]);
        for (int i = 0; i < villages.length; i++) {
            InsertOrUpdateStatement statement = new InsertOrUpdateStatement("", "", "ps_village", "id");
            Village village = villages[i];
            long id = i + 1L;
            statement.addColumnValue(COLUMN_ID, id);
            statement.addColumnValue(COLUMN_NAME, village.getName());
            statement.addColumnValue(COLUMN_CODE, village.getCode());
            statement.addColumnValue("location", village.getLocation());
            final Predicate<Commune> filterCommune = c -> village.communeCode == c.code
                    && village.getDistrictCode() == c.districtCode && village.provinceCode == c.provinceCode;
            long communeId = -1L;
            final Optional<Commune> commune = communes.stream().filter(filterCommune).findFirst();
            if (commune.isPresent()) {
                communeId = commune.get().id;
            }
            statement.addColumnValue("commune_id", communeId);
            village.setId(id);
            statements.add(statement);
        }
        database.execute(statements.toArray(new InsertOrUpdateStatement[statements.size()]), null);
    }

    private List<Province> getProvinces(StaticData staticData) throws InternalBusinessException {
        Map<Long, Province> provinceMap = new HashMap<>();
        for (Map<String, String> v : staticData.getAddresses()) {
            final Long provinceCode = parseLong(v.get(COLUMN_PROVINCE_CODE));
            final String provinceName = v.get("PROVINCE");
            final Province provinceBuilder = Province.builder().code(provinceCode).name(provinceName).build();
            provinceMap.putIfAbsent(provinceBuilder.getCode(), provinceBuilder);
        }
        return new ArrayList<>(provinceMap.values());
    }

    private Set<District> getDistricts(StaticData staticData) throws InternalBusinessException {
        Set<District> districts = new HashSet<>();
        for (Map<String, String> m : staticData.getAddresses()) {
            final District district = District.builder().code(parseLong(m.get(DCODE))).name(m.get("DISTRICT"))
                    .provinceCode(parseLong(m.get(COLUMN_PROVINCE_CODE))).build();
            districts.add(district);

        }
        return districts;
    }

    private Set<Commune> getCommunes(StaticData staticData) throws InternalBusinessException {
        Set<Commune> communes = new HashSet<>();
        for (Map<String, String> m : staticData.getAddresses()) {
            final Commune commune = Commune.builder().code(parseLong(m.get(CCODE))).name(m.get("COMMUNE"))
                    .districtCode(parseLong(m.get(DCODE))).provinceCode(parseLong(m.get(COLUMN_PROVINCE_CODE))).build();
            communes.add(commune);
        }
        return communes;
    }

    private Set<Village> getVillages(StaticData staticData) throws InternalBusinessException {
        Set<Village> villages = new HashSet<>();
        for (Map<String, String> m : staticData.getAddresses()) {
            final Village village = Village.builder().code(parseLong(m.get("VCODE"))).name(m.get("VILLAGE"))
                    .communeCode(parseLong(m.get(CCODE))).location(parseLong(m.get("LOCATION")))
                    .districtCode(parseLong(m.get(DCODE))).provinceCode(parseLong(m.get(COLUMN_PROVINCE_CODE))).build();
            villages.add(village);
        }
        return villages;
    }

    @Override
    public String getConfirmationMessage() {
        return null;
    }

    @Override
    public void setUp() throws SetupException {
        //No implement
    }

    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {
        //no implement
    }

    @Override
    public ValidationErrors validate(Database database) {
        return null;
    }

    @Builder
    @Getter
    @Setter
    @EqualsAndHashCode
    private static class Province {
        private long id;
        private long code;
        private String name;
        private String khmerName;
        private long countryId;
    }

    @Builder
    @Getter
    @Setter
    @EqualsAndHashCode
    private static class District {
        private long id;
        private long code;
        private String name;
        private String khmerName;
        private long provinceCode;
    }

    @Builder
    @Getter
    @Setter
    @EqualsAndHashCode
    private static class Commune {
        private long id;
        private long code;
        private String name;
        private String khmerName;
        private long districtCode;
        private long provinceCode;
    }

    @Builder
    @Getter
    @Setter
    @EqualsAndHashCode
    private static class Village {
        private long id;
        private long code;
        private String name;
        private String khmerName;
        private long location;
        private long communeCode;
        private long provinceCode;
        private long districtCode;
    }
}
