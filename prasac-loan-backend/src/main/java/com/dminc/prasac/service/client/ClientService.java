package com.dminc.prasac.service.client;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.prasac.model.dto.loan.ClientRequest;
import com.dminc.prasac.model.dto.misc.Client;
import com.dminc.prasac.model.dto.misc.SearchResultClient;
import com.dminc.prasac.model.request.SearchClientRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

@SuppressWarnings({"PMD.TooManyMethods"})
public interface ClientService {

    Page<Client> search(SearchClientRequest request, Pageable pageable) throws BusinessException;

    Client getDetailByCifFromCrm(String cif) throws BusinessException;

    SearchResultClient getClientByCifFromCrm(ClientRequest request);

    Optional<Client> getById(Long id) throws BusinessException;

    Optional<Client> getByCif(String cif);
}
