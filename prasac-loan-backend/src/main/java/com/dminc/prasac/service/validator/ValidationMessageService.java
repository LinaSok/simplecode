package com.dminc.prasac.service.validator;

import java.lang.annotation.Annotation;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.exceptions.InvalidParametersBusinessException;
import com.dminc.prasac.service.BusinessError;
import com.dminc.prasac.service.validator.constraint.AtLeastOneBorrower;
import com.dminc.prasac.service.validator.constraint.Decision;
import com.dminc.prasac.service.validator.constraint.FailReason;
import com.dminc.prasac.service.validator.constraint.LoanExists;
import com.dminc.prasac.service.validator.constraint.ValidCIFs;
import com.dminc.prasac.service.validator.constraint.ValidIdentities;

@Service
@SuppressWarnings("PMD.CyclomaticComplexity")
public class ValidationMessageService<T> {

    public void throwExceptionIfAnyErrors(Set<ConstraintViolation> violations) {

        if (CollectionUtils.isNotEmpty(violations)) {
            final ConstraintViolation validation = violations.iterator().next();
            final Class<? extends Annotation> constraint = validation.getConstraintDescriptor().getAnnotation().annotationType();

            throw new InvalidParametersBusinessException(getMsg(validation), getErrorCode(constraint));
        }

    }

    public int getErrorCode(Class<? extends Annotation> constraint) {
        int errorCode;

        if (ValidCIFs.class.isAssignableFrom(constraint)) {
            errorCode = BusinessError.INVALID_CIF.getCode();
        } else if (AtLeastOneBorrower.class.isAssignableFrom(constraint)) {
            errorCode = BusinessError.AT_LEAST_ONE_BORROWER.getCode();
        } else if (Decision.class.isAssignableFrom(constraint)) {
            errorCode = BusinessError.PRE_SCREEN_DECISION.getCode();
        } else if (FailReason.class.isAssignableFrom(constraint)) {
            errorCode = BusinessError.PRE_SCREEN_FAIL_REASON.getCode();
        } else if (ValidIdentities.class.isAssignableFrom(constraint)) {
            errorCode = BusinessError.DUPLICATE_IDENTIFICATION.getCode();
        } else if (NotNull.class.isAssignableFrom(constraint) || NotBlank.class.isAssignableFrom(constraint) || Size.class
                .isAssignableFrom(constraint)) {
            errorCode = BusinessError.REQUIRED.getCode();
        } else if (LoanExists.class.isAssignableFrom(constraint)) {
            errorCode = BusinessError.LOAN_NOT_FOUND.getCode();
        } else {
            errorCode = BusinessError.OBJECT_BAD_REQUEST.getCode();
        }

        return errorCode;
    }

    private String getMsg(ConstraintViolation violation) {
        return String.format("[%s] %s", violation.getPropertyPath(), violation.getMessage());
    }
}
