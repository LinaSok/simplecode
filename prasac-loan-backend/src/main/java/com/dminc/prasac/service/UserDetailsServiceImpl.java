package com.dminc.prasac.service;

import com.dminc.prasac.model.entity.User;
import com.dminc.prasac.model.entity.UserLock;
import com.dminc.prasac.repository.UserLockRepository;
import com.dminc.prasac.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

import static com.dminc.prasac.security.CustomDaoAuthenticationProvider.LOCKED_PERIOD;

@Service("userDetailsService")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserDetailsServiceImpl implements UserDetailsService {

    public static final String ACCOUNT_IS_LOCKED = "account_is_locked";
    private final UserRepository userRepository;
    private final UserLockRepository userLockRopsitory;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findOneByUsername(username);
        if (user == null) {
            BusinessError.BAD_CREDENTIAL.throwExceptionHttpStatus401();
        }

        try {
            new AccountStatusUserDetailsChecker().check(user);
        } catch (LockedException e) {
            //try to unlock user
            this.unlockUser(user);
        } catch (DisabledException e) {
            BusinessError.ACCOUNT_DISABLED.throwExceptionHttpStatus401();
        }
        //User role permission are lazy load. Need to invoke here to avoid the duplicate list of role in response.
        //Maybe we can find better solution later
        user.getAuthorities();
        return user;
    }

    private void unlockUser(User user) {
        //If user is locked, he/she should have records in UserLock table
        final Optional<UserLock> locked = userLockRopsitory.findByUsername(user.getUsername());
        if (locked.isPresent()) {
            //Lock period is not null only when attempt_count == 3
            LocalDateTime lockedPeriod = locked.get().getStartLockedPeriod() == null ? locked.get().getCreatedDate() : locked.get().getStartLockedPeriod();
            if (LocalDateTime.now().compareTo(lockedPeriod.plusMinutes(LOCKED_PERIOD)) > 0) {
                //delete locked record
                userLockRopsitory.deleteById(locked.get().getId());
                //unlock
                user.setLocked(false);
                userRepository.save(user);
            } else {
                throw new LockedException(ACCOUNT_IS_LOCKED);
            }
        } else {
            //unlock user. seems somewhere the lock record was removed but not enabled user
            user.setLocked(false);
            userRepository.save(user);
        }
    }
}



