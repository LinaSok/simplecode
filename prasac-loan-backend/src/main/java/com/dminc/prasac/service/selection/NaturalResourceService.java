package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.NaturalResource;
import com.dminc.prasac.model.entity.NaturalResourceEntity;
import com.dminc.prasac.repository.NaturalResourceRepository;

@Service
public class NaturalResourceService extends CommonService<NaturalResource, NaturalResourceEntity> {

    public NaturalResourceService(NaturalResourceRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, NaturalResource.class);
    }
}



