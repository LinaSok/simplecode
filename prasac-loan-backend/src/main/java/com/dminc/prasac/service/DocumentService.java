package com.dminc.prasac.service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.common.tools.exceptions.InternalBusinessException;
import com.dminc.common.tools.exceptions.ItemNotFoundBusinessException;
import com.dminc.prasac.controller.rest.DocumentController;
import com.dminc.prasac.model.dto.misc.Document;
import com.dminc.prasac.model.entity.ClientBusinessEntity;
import com.dminc.prasac.model.entity.ClientLiabilityCreditEntity;
import com.dminc.prasac.model.entity.DocumentEntity;
import com.dminc.prasac.model.entity.DocumentTypeEntity;
import com.dminc.prasac.model.entity.LoanCollateralEntity;
import com.dminc.prasac.model.entity.client.ClientEntity;
import com.dminc.prasac.model.entity.document.BusinessDocumentEntity;
import com.dminc.prasac.model.entity.document.ClientDocumentEntity;
import com.dminc.prasac.model.entity.document.CollateralDocumentEntity;
import com.dminc.prasac.model.entity.document.LiabilityDocumentEntity;
import com.dminc.prasac.model.entity.document.LoanDocumentEntity;
import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.request.ImageUploadRequest;
import com.dminc.prasac.repository.DocumentRepository;
import com.dminc.prasac.repository.document.BusinessDocumentRepository;
import com.dminc.prasac.repository.document.ClientDocumentRepository;
import com.dminc.prasac.repository.document.CollateralDocumentRepository;
import com.dminc.prasac.repository.document.LiabilityDocumentRepository;
import com.dminc.prasac.repository.document.LoanDocumentRepository;
import com.dminc.prasac.service.validator.DocumentValidator;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
@SuppressWarnings("PMD.TooManyMethods")
public class DocumentService {
    private final DocumentRepository repository;
    private final ClientDocumentRepository clientDocRepo;
    private final LoanDocumentRepository loanDocRepo;
    private final CollateralDocumentRepository collateralDocRepo;
    private final BusinessDocumentRepository businessDocRepo;
    private final LiabilityDocumentRepository liabilityDocRepo;

    private final DocumentValidator documentValidator;

    @Transactional
    public List<Document> upload(ImageUploadRequest request) throws BusinessException {
        log.info("Validation document request");
        documentValidator.validate(request);

        final LoanEntity loanEntity = LoanEntity.builder().id(request.getLoan()).build();
        final List<DocumentEntity> documentEntities = request.getDocumentRequests().stream().map(document -> {
            final MultipartFile file = document.getImage();
            try {
                return DocumentEntity.builder() //
                        .loan(loanEntity) //
                        .description(cleanFileName(file.getOriginalFilename())) //
                        .image(file.getBytes()) //
                        .imageType(file.getContentType()) //
                        .documentType(DocumentTypeEntity.builder().id(document.getDocumentType()).build()) //
                        .build();
            } catch (IOException e) {
                throw new InternalBusinessException("Something went wrong with file upload, {}", e);
            }
        }).collect(Collectors.toList());

        final Iterable<DocumentEntity> entities = repository.saveAll(documentEntities);

        saveDocumentsType(request, loanEntity, entities);

        return convert(entities);
    }

    private void saveDocumentsType(ImageUploadRequest request, LoanEntity loanEntity, Iterable<DocumentEntity> documentEntities) {
        final Stream<DocumentEntity> entities = StreamSupport.stream(documentEntities.spliterator(), false);
        switch (request.getDocumentRequestType()) {
            case LOAN:
                saveLoanDocs(loanEntity, entities);
                break;

            case BUSINESS:
                saveBusinessDocs(request, entities);
                break;

            case CLIENT:
                saveClientDocs(request, entities);
                break;

            case LIABILITY:
                saveLiabilityDocs(request, entities);
                break;

            case COLLATERAL:
            case OTHER_ASSET:
                saveCollateralDocs(request, entities);
                break;
            default:
                break;
        }
    }


    private void saveCollateralDocs(ImageUploadRequest request, Stream<DocumentEntity> entities) {
        final LoanCollateralEntity loanCollateralEntity = LoanCollateralEntity.builder().id(request.getCollateral()).build();
        final List<CollateralDocumentEntity> collateralDocumentEntities = entities
                .map(documentEntity -> CollateralDocumentEntity.builder().document(documentEntity)
                        .collateral(loanCollateralEntity).build()).collect(Collectors.toList());
        collateralDocRepo.saveAll(collateralDocumentEntities);
    }

    private void saveLiabilityDocs(ImageUploadRequest request, Stream<DocumentEntity> entities) {
        final ClientLiabilityCreditEntity liabilityCreditEntity = ClientLiabilityCreditEntity.builder().id(request.getLiability())
                .build();
        final List<LiabilityDocumentEntity> liabilityDocumentEntities = entities
                .map(documentEntity -> LiabilityDocumentEntity.builder().document(documentEntity).liability(liabilityCreditEntity)
                        .build()).collect(Collectors.toList());
        liabilityDocRepo.saveAll(liabilityDocumentEntities);
    }

    private void saveClientDocs(ImageUploadRequest request, Stream<DocumentEntity> entities) {
        final ClientEntity clientEntity = ClientEntity.builder().id(request.getClient()).build();
        final List<ClientDocumentEntity> clientDocumentEntities = entities
                .map(documentEntity -> ClientDocumentEntity.builder().document(documentEntity).client(clientEntity).build())
                .collect(Collectors.toList());
        clientDocRepo.saveAll(clientDocumentEntities);
    }

    private void saveBusinessDocs(ImageUploadRequest request, Stream<DocumentEntity> entities) {
        final ClientBusinessEntity businessEntity = ClientBusinessEntity.builder().id(request.getBusiness()).build();
        final List<BusinessDocumentEntity> businessDocumentEntities = entities
                .map(documentEntity -> BusinessDocumentEntity.builder().document(documentEntity).business(businessEntity).build())
                .collect(Collectors.toList());
        businessDocRepo.saveAll(businessDocumentEntities);
    }

    private void saveLoanDocs(LoanEntity loanEntity, Stream<DocumentEntity> entities) {
        final List<LoanDocumentEntity> loanDocumentEntities = entities
                .map(documentEntity -> LoanDocumentEntity.builder().document(documentEntity).loan(loanEntity).build())
                .collect(Collectors.toList());
        loanDocRepo.saveAll(loanDocumentEntities);
    }

    private String cleanFileName(String fileName) {
        final String currentTimeMillis = String.valueOf(System.currentTimeMillis());

        return cleanSpace(StringUtils.isBlank(fileName) ? currentTimeMillis : String.join(currentTimeMillis, fileName));
    }

    private String cleanSpace(String string) {
        return string.replaceAll("[^a-zA-Z0-9-_\\.]", "_");
    }

    private Document convert(final DocumentEntity document) {
        String imageUrl = getImageUrl(document.getId());
        return Document.builder().id(document.getId()).documentType(document.getDocumentType().getId()).image(imageUrl).build();
    }

    public static String getImageUrl(Long documentId) {
        return String.format("%s/%s", DocumentController.API_DOCUMENTS_ENDPOINT, Long.toString(documentId));
    }

    private List<Document> convert(final Iterable<DocumentEntity> documentEntities) {
        return StreamSupport.stream(documentEntities.spliterator(), false).map(document -> convert(document))
                .collect(Collectors.toList());
    }

    public byte[] getImageByDocumentId(Long documentId) {
        return repository.findById(documentId).map(DocumentEntity::getImage)
                .orElseThrow(() -> new ItemNotFoundBusinessException("Image found by document id, " + documentId));
    }

    public void delete(Long id) {
        repository.findById(id).ifPresent(document -> {
            loanDocRepo.findTopByDocument_Id(id).ifPresent(entity -> loanDocRepo.delete(entity));
            clientDocRepo.findTopByDocument_Id(id).ifPresent(entity -> clientDocRepo.delete(entity));
            liabilityDocRepo.findTopByDocument_Id(id).ifPresent(entity -> liabilityDocRepo.delete(entity));
            businessDocRepo.findTopByDocument_Id(id).ifPresent(entity -> businessDocRepo.delete(entity));
            collateralDocRepo.findTopByDocument_Id(id).ifPresent(entity -> collateralDocRepo.delete(entity));

            repository.delete(document);
        });
    }

    public List<Document> getLoanDocs(Long loanId) {
        final List<Long> documentIds = loanDocRepo.findByLoan_Id(loanId).stream().map(entity -> entity.getDocument().getId())
                .collect(Collectors.toList());
        return convert(repository.findAllById(documentIds));
    }

    public List<Document> getClientDocs(Long clientId, Long loanId) {
        final List<Long> documentIds = clientDocRepo.findByClient_Id(clientId).stream()
                .map(entity -> entity.getDocument().getId()).collect(Collectors.toList());

        return convert(repository.findAllByIdInAndLoan_Id(documentIds, loanId));
    }

    public List<Document> getBusinessDocs(Long businessId, Long loanId) {
        final List<Long> documentIds = businessDocRepo.findByBusiness_Id(businessId).stream()
                .map(entity -> entity.getDocument().getId()).collect(Collectors.toList());

        return convert(repository.findAllByIdInAndLoan_Id(documentIds, loanId));
    }

    public List<Document> getLiabilityDocs(Long liabilityId, Long loanId) {
        final List<Long> documentIds = liabilityDocRepo.findByLiability_Id(liabilityId).stream()
                .map(entity -> entity.getDocument().getId()).collect(Collectors.toList());

        return convert(repository.findAllByIdInAndLoan_Id(documentIds, loanId));
    }

    public List<Document> getCollateralDocs(Long collateralId, Long loanId) {
        final List<Long> documentIds = collateralDocRepo.findByCollateral_Id(collateralId).stream()
                .map(entity -> entity.getDocument().getId()).collect(Collectors.toList());

        return convert(repository.findAllByIdInAndLoan_Id(documentIds, loanId));
    }
}

