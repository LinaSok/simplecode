package com.dminc.prasac.service.credit;

import com.dminc.common.tools.mapping.Mapper;
import com.dminc.prasac.integration.domain.PrasacCreditHistoryMonitoring;
import com.dminc.prasac.integration.service.PrasacCreditHistoryMonitoringService;
import com.dminc.prasac.model.dto.loan.CreditHistoryMonitoring;
import com.dminc.prasac.model.dto.misc.Currency;
import com.dminc.prasac.service.ObjectsConverter;
import com.dminc.prasac.service.selection.CurrencyService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CreditHistoryMonitorServiceImpl implements CreditHistoryMonitorService {

    private final PrasacCreditHistoryMonitoringService service;
    private final ObjectsConverter objectsConverter;
    private final CurrencyService currencyService;

    @Override
    @Cacheable(value = "getCreditHistoriesMonitoringByClient", key = "#clientCif")
    public List<CreditHistoryMonitoring> getByClient(String clientCif) {
        final List<PrasacCreditHistoryMonitoring> monitorings = service.getCreditHistoryMonitoringsByClient(clientCif);
        return monitorings.stream().map(this::convert).collect(Collectors.toList());
    }

    private CreditHistoryMonitoring convert(PrasacCreditHistoryMonitoring credit) {
        final CreditHistoryMonitoring creditHistoryMonitoring = objectsConverter.runServiceTask((Mapper mapper) -> mapper.map(credit, CreditHistoryMonitoring.class));
        final Currency currency = currencyService.getByCode(credit.getCurrency());
        creditHistoryMonitoring.setCurrency(currency);
        return creditHistoryMonitoring;
    }
}
