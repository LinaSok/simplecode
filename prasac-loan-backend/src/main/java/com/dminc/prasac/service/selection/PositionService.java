package com.dminc.prasac.service.selection;

import com.dminc.common.tools.mapping.Mapper;
import com.dminc.prasac.model.dto.user.Position;
import com.dminc.prasac.model.entity.PositionEntity;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.repository.PositionRepository;
import com.dminc.prasac.service.ObjectsConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PositionService {

    private final PositionRepository repository;
    private final ObjectsConverter converter;

    @Cacheable(value = "getAllPositionsCache", unless = LocalConstants.RESULT_SIZE_0)
    public List<Position> getAll() {
        final List<PositionEntity> positions = repository.findAll();
        return converter.runServiceTask((Mapper mapper) -> mapper.map(positions, Position.class));
    }
}
