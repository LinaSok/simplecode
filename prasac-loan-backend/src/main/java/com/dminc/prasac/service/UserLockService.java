package com.dminc.prasac.service;

import com.dminc.prasac.model.entity.UserLock;

public interface UserLockService {
    UserLock findLockedUser(String userName);

    void saveLockedUser(UserLock lockedUser);

    void unlockUser(final String userName);

}
