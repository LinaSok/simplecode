package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.ClientRelationship;
import com.dminc.prasac.model.entity.ClientRelationshipEntity;
import com.dminc.prasac.repository.ClientRelationshipRepository;

@Service
public class ClientRelationshipService extends CommonService<ClientRelationship, ClientRelationshipEntity> {
    public ClientRelationshipService(ClientRelationshipRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, ClientRelationship.class);
    }
}
