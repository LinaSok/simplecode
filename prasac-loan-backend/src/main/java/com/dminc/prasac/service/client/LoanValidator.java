package com.dminc.prasac.service.client;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.exceptions.InvalidParametersBusinessException;
import com.dminc.common.tools.exceptions.ItemNotFoundBusinessException;
import com.dminc.prasac.model.entity.UserPermission;
import com.dminc.prasac.model.entity.UserRole;
import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.model.misc.RequestedFrom;
import com.dminc.prasac.model.request.LoanAppraisalRequest;
import com.dminc.prasac.model.request.PreScreenRequest;
import com.dminc.prasac.repository.BranchRepository;
import com.dminc.prasac.repository.UserRepository;
import com.dminc.prasac.repository.UserRoleRepository;
import com.dminc.prasac.service.BusinessError;
import com.dminc.prasac.service.loan.AppraisalLoanService;
import com.dminc.prasac.service.validator.ValidationMessageService;
import com.dminc.prasac.service.validator.constraint.group.COConstraint;
import com.dminc.prasac.service.validator.constraint.group.Default;
import com.dminc.prasac.service.validator.constraint.group.Update;
import com.dminc.prasac.util.PrasacHelper;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LoanValidator {

    private final BranchRepository branchRepository;
    private final UserRepository userRepository;
    private final Validator validator;
    private final ValidationMessageService messageService;
    private final UserRoleRepository userRoleRepository;

    //Validate constraints (javax.validation.constraints.*) which are annotated in PreScreenRequest
    public void validatePreScreen(PreScreenRequest request, RequestedFrom agent) throws InvalidParametersBusinessException {
        Set<ConstraintViolation<PreScreenRequest>> violations = agent.in(RequestedFrom.TABLET, RequestedFrom.ANY) ? validateAllConstraints(request) : validateDefaultConstraints(request);
        messageService.throwExceptionIfAnyErrors(violations);
    }

    //Validate only default constraints and depends on action
    private Set<ConstraintViolation<PreScreenRequest>> validateDefaultConstraints(PreScreenRequest request) {
        final Class<?>[] createGroup = PrasacHelper.asValidationGroups(Default.class);

        return validator.validate(request, createGroup);
    }

    //Validate all constraints (Default + Credit Officer) and depends on action
    private Set<ConstraintViolation<PreScreenRequest>> validateAllConstraints(PreScreenRequest request) {
        final Class<?>[] createGroup = PrasacHelper.asValidationGroups(Default.class, COConstraint.class);

        return validator.validate(request, createGroup);
    }

    public void validateAssignedBranch(Long branchId) throws ItemNotFoundBusinessException {
        final boolean exist = branchRepository.existsById(branchId);
        if (!exist) {
            throw new ItemNotFoundBusinessException(String.format("Branch not found %s", branchId));
        }
    }

    public void validateAssignedUser(Long userId) throws ItemNotFoundBusinessException {
        final boolean exist = userRepository.existsById(userId);
        if (!exist) {
            throw new ItemNotFoundBusinessException(String.format("User not found %s", userId));
        }
    }

    public void validateAssignedGroup(LoanEntity loan, Long userRoleId) {
        final Optional<UserRole> userRole = userRoleRepository.findById(userRoleId);
        if (!userRole.isPresent()) {
            throw new ItemNotFoundBusinessException(String.format("User role not found %s", userRoleId));
        }
        /*
          verify loan committee level. Loan can be assigned to decision maker group only committee level = 3.
          Also, the user group to be assigned must be decision makers only
         */
        final UserRole group = userRole.get();
        final List<UserPermission> permissions = group.getPermissions();
        final Optional<UserPermission> decisionMaker = permissions.stream().filter(permission -> permission.getName().equals(LocalConstants.PERMISSION_DECISION_MAKING)).findFirst();
        if (!decisionMaker.isPresent()) {
            throw new InvalidParametersBusinessException("Loan can be assigned to decision maker group only");
        }
        /*
           Verify loan committee level. Only committee level reach 3, otherwise can't be assigned to decision maker yet
         */
        if (loan.getCommitteeLevel() != AppraisalLoanService.REVIEW_LEVEL_3) {
            throw new InvalidParametersBusinessException("Only loan with reviewer level 3 can be to the group");
        }

    }

    public void validateAppraisal(LoanAppraisalRequest request) {
        Set<ConstraintViolation<LoanAppraisalRequest>> violations = validator
                .validate(request, PrasacHelper.asValidationGroups(Default.class, Update.class));
        messageService.throwExceptionIfAnyErrors(violations);

        //check list collateral and other assets at least one has value
        if (CollectionUtils.isEmpty(request.getOtherAssetsList()) && CollectionUtils.isEmpty(request.getCollateralList())) {
            BusinessError.OBJECT_BAD_REQUEST.throwExceptionHttpStatus400("The collateral or other assets can not empty");
        }
    }
}
