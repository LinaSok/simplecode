package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.BusinessExperience;
import com.dminc.prasac.model.entity.BusinessExperienceEntity;
import com.dminc.prasac.repository.BusinessExperienceRepository;

@Service
public class BusinessExperienceService extends CommonService<BusinessExperience, BusinessExperienceEntity> {

    public BusinessExperienceService(BusinessExperienceRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, BusinessExperience.class);
    }
}



