package com.dminc.prasac.service;

import com.dminc.common.tools.exceptions.AuthenticationBusinessException;
import com.dminc.common.tools.exceptions.AuthorizationRequiredBusinessException;
import com.dminc.common.tools.exceptions.ConflictBusinessException;
import com.dminc.common.tools.exceptions.InternalBusinessException;
import com.dminc.common.tools.exceptions.InvalidParametersBusinessException;
import com.dminc.common.tools.exceptions.ItemNotFoundBusinessException;

public enum BusinessError {

    BAD_CREDENTIAL(1, "Login failed! Please make sure that you input the correct username and password."), //
    ACCOUNT_LOCKED(2, "The account is locked."), //
    INVALID_TOKEN(3, "The token is invalid or expired"), //
    ACCOUNT_DISABLED(4, "ACCOUNT_IS_DISABLED"), //
    INVALID_PASSWORD_RULE(1001, "The password you have entered does not conform to the password policy."), //
    SAME_CURRENT_NEW_PASSWORD(1002, "Current password and new password can not be the same."), //
    OBJECT_ALREADY_EXISTED(409, "The item already existed in the system"), //
    OBJECT_NOT_FOUND(404, "The item is not existed in the system"), //
    STAFF_ID_ALREADY_USED(422, "Staff Id already belong to other user"), //
    OBJECT_BAD_REQUEST(400, "Your request is invalid"), //
    UNAUTHORIZED_REQUEST(100, "Your request not allowed"), //
    ALL_READ_MADE_DECISION(101, "You already made decision"), //
    USER_NAME_ALREADY_EXIST(102, "User already exist"), //
    USER_WITH_EMAIL_ALREADY_EXIST(103, "Email already exist in profile"), //

    REQUIRED(99, "must not be null"), //
    INVALID_CIF(107, "contain invalid CIF number(s)"), //
    AT_LEAST_ONE_BORROWER(104, "should contain at least one borrower"), //
    PRE_SCREEN_DECISION(105, "decision must be 'passed' or 'failed' or 'new pre-screening'"), //
    PRE_SCREEN_FAIL_REASON(106, "please provide fail reason"), //
    DUPLICATE_IDENTIFICATION(108, "duplicate identification(s) with other client"), //
    INVALID_LOAN(109, "loan not editable"),
    LOAN_NOT_FOUND(110, "loan not found"),
    PENDING_ERROR(111, "Loan pending on previous reviewer"),
    COMMITTEE_LEVEL_NOT_ACCEPTABLE(112, "Your level not match with committee level in loan"),
    USER_GROUP_HAS_LOAN(113, "User group has pending loans"),
    CBC_REPORT_ERROR(600, "CBC report error"),
    CAN_NOT_CONNECT_TO_ORACLE(115, "Can not connect to Core Banking Database"),
    LOAN_REACH_MAX_LEVEL(114, "Loan already reach CC2 or CC3");


    private int code;
    private String message;

    BusinessError(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void throwException() {
        throwException(getMessage());
    }

    public void throwExceptionHttpStatus404() {
        throw new ItemNotFoundBusinessException(message, code);
    }

    public void throwExceptionHttpStatus404(String message) {
        throw new ItemNotFoundBusinessException(message, code);
    }

    public void throwExceptionHttpStatus401() {
        throw new AuthenticationBusinessException(message, code);
    }

    public void throwExceptionHttpStatus400() {
        throw new InvalidParametersBusinessException(message, code);
    }

    public void throwExceptionHttpStatus500() {
        throw new InternalBusinessException(message, code);
    }

    public void throwExceptionHttpStatus400(String errorMsg) {
        throw new InvalidParametersBusinessException(errorMsg, code);
    }

    public void throwException(String errorMsg) {
        throw new InternalBusinessException(errorMsg, code);
    }

    public void throwExceptionHttpStatus409() {
        throw new ConflictBusinessException();
    }

    public void throwExceptionHttpStatus403() {
        throw new AuthorizationRequiredBusinessException(message, code);
    }
}
