package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.LicenseContractValidity;
import com.dminc.prasac.model.entity.LicenseContractValidityEntity;
import com.dminc.prasac.repository.LicenseContractValidityRepository;

@Service
public class LicenseContractValidityService extends CommonService<LicenseContractValidity, LicenseContractValidityEntity> {

    public LicenseContractValidityService(LicenseContractValidityRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, LicenseContractValidity.class);
    }
}



