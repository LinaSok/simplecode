package com.dminc.prasac.service;

import com.dminc.common.tools.exceptions.ConflictBusinessException;
import com.dminc.prasac.model.entity.UserPermission;
import com.dminc.prasac.model.entity.UserRole;
import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.model.request.NewRoleRequest;
import com.dminc.prasac.repository.CommentRepository;
import com.dminc.prasac.repository.UserPermissionRepository;
import com.dminc.prasac.repository.UserRoleRepository;
import com.dminc.prasac.repository.loan.LoanRepository;
import com.dminc.prasac.service.validator.UserValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserRoleService {
    private final UserRoleRepository userRoleRepository;
    private final UserValidator userValidator;
    private final ObjectsConverter converter;
    private final UserPermissionRepository userPermissionRepository;
    private final LoanRepository loanRepository;
    private final CommentRepository commentRepository;

    @Transactional
    public UserRole createUserRole(NewRoleRequest newRoleRequest) throws ConflictBusinessException {
        return this.createOrUpdate(newRoleRequest, null);
    }

    @Transactional
    public UserRole updateRole(NewRoleRequest newRoleRequest, Long id) {
        return this.createOrUpdate(newRoleRequest, id);
    }

    /**
     * Create new or update the role id.
     *
     * @param newRoleRequest {@link NewRoleRequest}
     * @param id             - role id to be updated if it provided
     * @return UserRole
     */
    private UserRole createOrUpdate(final NewRoleRequest newRoleRequest, final Long id) {
        UserRole userRole = UserRole.builder().build();
        if (id == null) {
            this.validateNewRole(newRoleRequest.getName());
        } else {
            userRole = userRoleRepository.findById(id).get();
        }

        // Creating new role, the name must not be blank (already validate above)
        // It might be blank when user wants to update only permission
        if (StringUtils.isNotBlank(newRoleRequest.getName())) {
            userRole.setName(newRoleRequest.getName());
        }

        if (CollectionUtils.isNotEmpty(newRoleRequest.getPermissions())) {
            final List<UserPermission> permissions = newRoleRequest.getPermissions().stream()
                    .map(permissionId -> UserPermission.builder().id(permissionId).build()).collect(Collectors.toList());
            userRole.setPermissions(permissions);
        }

        return this.userRoleRepository.save(userRole);

    }

    private void validateNewRole(final String role) {
        if (StringUtils.isBlank(role)) {
            BusinessError.OBJECT_BAD_REQUEST.throwExceptionHttpStatus400("Role name can't be empty");
        }

        if (userValidator.isRoleExist(role)) {
            BusinessError.OBJECT_ALREADY_EXISTED.throwExceptionHttpStatus409();
        }
    }

    @Transactional
    public Page<com.dminc.prasac.model.dto.user.UserRole> getUserRole(Pageable pageable) {
        final Page<UserRole> roles = userRoleRepository.findAll(pageable);
        return roles.map(userRole -> converter.getObjectsMapper().map(userRole, com.dminc.prasac.model.dto.user.UserRole.class));
    }

    @Transactional
    public void deleteRole(Long id) {
        //Check if role link to loan
        final List<LoanEntity> loans = loanRepository.findByUserRole_Id(id);
        if (CollectionUtils.isNotEmpty(loans)) {
            BusinessError.USER_GROUP_HAS_LOAN.throwExceptionHttpStatus400();
        }
        //DELETE FROM JOIN TABLE FIRST
        commentRepository.deleteByUserRole_Id(id);
        userRoleRepository.deleteUserRoleUser(id);
        userRoleRepository.deleteUserRoleUserPermission(id);
        userRoleRepository.deleteById(id);
    }

    /**
     * Function to get only CC2 and CC3 group.
     * Some groups has the same decision maker and include decision maker, so we will filter out those review level1, 2, 3, and 4.
     * <p>
     * This function supposed to be called only when Review Level 4 tried to submit to next level cos they need to select which
     * Decision maker they want to assign the loan to.
     *
     * @return
     */
    @Transactional
    public List<com.dminc.prasac.model.dto.user.UserRole> getDecisionMakers() {
        log.debug("Getting the decision makers group");
        final UserPermission decisionMaker = userPermissionRepository.findById(LocalConstants.DECISION_MAKER).orElse(null);
        final List<UserPermission> lowerLevels = userPermissionRepository.findDistinctByIdIn(Arrays.asList(LocalConstants.REVIEW_LEVEL_1, LocalConstants.REVIEW_LEVEL_2, LocalConstants.REVIEW_LEVEL_3, LocalConstants.REVIEW_LEVEL_4));
        log.debug("Get user roles which have review level1, 2, 3, 4");
        final List<UserRole> lowerLevelRoles = userRoleRepository.findDistinctByPermissionsIn(lowerLevels);
        final List<Long> notInIds = lowerLevelRoles.stream().map(UserRole::getId).collect(Collectors.toList());
        log.debug("Get only role which has decision maker and not review level 1, 2, 3, and 4");
        final List<UserRole> userRoles = userRoleRepository.findDistinctByPermissionsInAndIdNotIn(Collections.singletonList(decisionMaker), notInIds);
        return converter.getObjectsMapper().map(userRoles, com.dminc.prasac.model.dto.user.UserRole.class);
    }
}
