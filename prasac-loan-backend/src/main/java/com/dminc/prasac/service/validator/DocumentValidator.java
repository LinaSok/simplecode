package com.dminc.prasac.service.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dminc.prasac.model.misc.BusinessDocumentClassification;
import com.dminc.prasac.model.misc.ClientDocumentClassification;
import com.dminc.prasac.model.misc.CollateralDocumentClassification;
import com.dminc.prasac.model.misc.LiabilityDocumentClassification;
import com.dminc.prasac.model.misc.LoanDocumentClassification;
import com.dminc.prasac.model.request.ImageUploadRequest;
import com.dminc.prasac.service.BusinessError;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DocumentValidator {

    public static final String DOCUMENT_TYPE_IS_INVALID = "Document type is invalid";

    public void validate(ImageUploadRequest request) {
        if (request.getDocumentRequestType() != null) {
            switch (request.getDocumentRequestType()) {
                case LOAN:
                    validateLoanDocClassification(request);
                    break;
                case BUSINESS:
                    validateBusinessDocClassification(request);
                    break;
                case CLIENT:
                    validateClientDocClassification(request);
                    break;
                case LIABILITY:
                    validateLiabilityDocClassification(request);
                    break;
                case COLLATERAL:
                    validateCollateralDocClassification(request);
                    break;
                case OTHER_ASSET:
                    validateOtherAssetsDocClassification(request);
                    break;
                default:
                    break;
            }
        }
    }

    private void validateOtherAssetsDocClassification(ImageUploadRequest request) {
        final boolean isValid = request.getDocumentRequests().stream()
                .allMatch(documentRequest -> CollateralDocumentClassification.contains(documentRequest.getDocumentType()));

        if (!isValid) {
            BusinessError.OBJECT_BAD_REQUEST.throwExceptionHttpStatus400(DOCUMENT_TYPE_IS_INVALID);
        }
    }

    private void validateCollateralDocClassification(ImageUploadRequest request) {
        final boolean isValid = request.getDocumentRequests().stream()
                .allMatch(documentRequest -> CollateralDocumentClassification.contains(documentRequest.getDocumentType()));

        if (!isValid) {
            BusinessError.OBJECT_BAD_REQUEST.throwExceptionHttpStatus400(DOCUMENT_TYPE_IS_INVALID);
        }
    }

    private void validateLiabilityDocClassification(ImageUploadRequest request) {
        final boolean isValid = request.getDocumentRequests().stream()
                .allMatch(documentRequest -> LiabilityDocumentClassification.contains(documentRequest.getDocumentType()));

        if (!isValid) {
            BusinessError.OBJECT_BAD_REQUEST.throwExceptionHttpStatus400(DOCUMENT_TYPE_IS_INVALID);
        }
    }

    private void validateBusinessDocClassification(ImageUploadRequest request) {
        final boolean isValid = request.getDocumentRequests().stream()
                .allMatch(documentRequest -> BusinessDocumentClassification.contains(documentRequest.getDocumentType()));

        if (!isValid) {
            BusinessError.OBJECT_BAD_REQUEST.throwExceptionHttpStatus400(DOCUMENT_TYPE_IS_INVALID);
        }
    }

    private void validateClientDocClassification(ImageUploadRequest request) {
        final boolean isValid = request.getDocumentRequests().stream()
                .allMatch(documentRequest -> ClientDocumentClassification.contains(documentRequest.getDocumentType()));

        if (!isValid) {
            BusinessError.OBJECT_BAD_REQUEST.throwExceptionHttpStatus400(DOCUMENT_TYPE_IS_INVALID);
        }
    }

    private void validateLoanDocClassification(ImageUploadRequest request) {
        final boolean isValid = request.getDocumentRequests().stream()
                .allMatch(documentRequest -> LoanDocumentClassification.contains(documentRequest.getDocumentType()));

        if (!isValid) {
            BusinessError.OBJECT_BAD_REQUEST.throwExceptionHttpStatus400(DOCUMENT_TYPE_IS_INVALID);
        }
    }
}
