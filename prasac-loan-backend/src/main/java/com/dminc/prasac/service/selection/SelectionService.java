package com.dminc.prasac.service.selection;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dminc.prasac.model.dto.selection.Selection;
import com.dminc.prasac.model.dto.selection.SelectionRequest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@SuppressWarnings("PMD.TooManyFields")
public class SelectionService {
    private final ClientRelationshipService relationshipService;
    private final ClientResidentStatusService residentStatusService;
    private final ClientTypeService clientTypeService;
    private final ClientWorkingTimeService workingTimeService;
    private final MaritalStatusService maritalStatusService;
    private final NationalityService nationalityService;
    private final OccupationService occupationService;
    private final ClientIdentificationTypeService identificationTypeService;
    private final LoanStatusService loanStatusService;
    private final CollateralTitleTypeService collateralTitleTypeService;
    private final CurrencyService currencyService;
    private final DocumentClassificationTypeService documentClassificationTypeService;
    private final DocumentTypeService documentTypeService;
    private final RepaymentModeService repaymentModeService;

    private final BusinessActivityService businessActivityService;
    private final BusinessExperienceService businessExperienceService;
    private final ClientBusinessCashFlowService clientBusinessCashFlowService;
    private final BranchService branchService;
    private final ClientBusinessLocationStatusService clientBusinessLocationStatusService;
    private final ClientBusinessPeriodService clientBusinessPeriodService;
    private final ClientBusinessProgressService clientBusinessProgressService;
    private final CollateralAreaService collateralAreaService;
    private final CollateralDescriptionService collateralDescriptionService;
    private final CollateralRegistrationService collateralRegistrationService;
    private final EnergyService energyService;
    private final HealthSafetyService healthSafetyService;
    private final LaborForceService laborForceService;
    private final LicenseContractValidityService licenseContractValidityService;
    private final LoanPurposeService loanPurposeService;
    private final LoanRequestTypeService loanRequestTypeService;
    private final MarketValueAnalyseDescriptionService marketValueAnalyseDescriptionService;
    private final NaturalResourceService naturalResourceService;
    private final ProductWasteService productWasteService;
    private final PropertyTypeService propertyTypeService;
    private final WaterPollutionService waterPollutionService;
    private final AirPollutionService airPollutionService;
    private final BusinessOwnerService businessOwnerService;
    private final LoanTypeService loanTypeService;

    @Cacheable(value = "predefinedValuesCache", key = "#request.hashCode()")
    @Transactional(readOnly = true)
    public Selection getSelection(SelectionRequest request) {
        log.info("=======predefinedValues=============");
        final LocalDateTime lastModified = request.getLastModified();
        return Selection.builder() //
                .clientIdentificationTypes(identificationTypeService.findAll(lastModified)) //
                .clientOccupations(occupationService.findAll(lastModified)) //
                .clientRelationships(relationshipService.findAll(lastModified)) //
                .clientResidentStatuses(residentStatusService.findAll(lastModified)) //
                .clientTypes(clientTypeService.findAll(lastModified)) //
                .clientWorkingTimes(workingTimeService.findAll(lastModified)) //
                .collateralTitleTypes(collateralTitleTypeService.findAll(lastModified)) //
                .currencies(currencyService.findAll(lastModified)) //
                .documentClassificationTypes(documentClassificationTypeService.findAll(lastModified)) //
                .documentTypes(documentTypeService.findAll(lastModified)) //
                .loanStatuses(loanStatusService.findAll(lastModified)) //
                .maritalStatuses(maritalStatusService.findAll(lastModified)) //
                .nationalities(nationalityService.findAll(lastModified)) //
                .repaymentModes(repaymentModeService.findAll(lastModified)) //

                .businessActivities(businessActivityService.findAll(lastModified)) //
                .businessExperiences(businessExperienceService.findAll(lastModified)) //
                .clientBusinessCashFlows(clientBusinessCashFlowService.findAll(lastModified)) //
                .branches(branchService.findAll(lastModified)) //
                .clientBusinessLocationStatuses(clientBusinessLocationStatusService.findAll(lastModified)) //
                .clientBusinessPeriods(clientBusinessPeriodService.findAll(lastModified)) //
                .clientBusinessProgresses(clientBusinessProgressService.findAll(lastModified)) //
                .collateralAreas(collateralAreaService.findAll(lastModified)) //
                .collateralDescriptions(collateralDescriptionService.findAll(lastModified)) //
                .collateralRegistrations(collateralRegistrationService.findAll(lastModified)) //
                .energies(energyService.findAll(lastModified)) //
                .healthSafeties(healthSafetyService.findAll(lastModified)) //
                .laborForces(laborForceService.findAll(lastModified)) //
                .licenseContractValidities(licenseContractValidityService.findAll(lastModified)) //
                .loanPurposes(loanPurposeService.findAll(lastModified)) //
                .loanRequestTypes(loanRequestTypeService.findAll(lastModified)) //
                .marketValueAnalyseDescriptions(marketValueAnalyseDescriptionService.findAll(lastModified)) //
                .naturalResources(naturalResourceService.findAll(lastModified)) //
                .productWastes(productWasteService.findAll(lastModified)) //
                .propertyTypes(propertyTypeService.findAll(lastModified)) //
                .waterPollutions(waterPollutionService.findAll(lastModified)) //
                .airPollutions(airPollutionService.findAll(lastModified)) //
                .businessOwners(businessOwnerService.findAll(lastModified)) //
                .loanTypes(loanTypeService.findAll(lastModified))//
                .build();
    }

}
