package com.dminc.prasac.service.selection;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.common.tools.exceptions.ItemNotFoundBusinessException;
import com.dminc.prasac.model.dto.misc.ClientIdentificationType;
import com.dminc.prasac.model.entity.ClientIdentificationTypeEntity;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.repository.ClientIdentificationTypeRepository;
import com.dminc.prasac.service.ObjectsConverter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClientIdentificationTypeService extends CommonService<ClientIdentificationType, ClientIdentificationTypeEntity> {
    private final ClientIdentificationTypeRepository repository;

    public ClientIdentificationTypeService(ClientIdentificationTypeRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, ClientIdentificationType.class);
        this.repository = repository;
    }

    @Cacheable(value = "clientIdentificationTypeByCode", key = "#code", condition = "#code!=null", unless = LocalConstants.RESULT_NULL)
    public ClientIdentificationType getOneByCode(String code) throws BusinessException {
        if (StringUtils.isEmpty(code)) {
            return null;
        }
        return convert(repository.findTopByCode(code));
    }

    @Cacheable(value = "clientIdentificationTypeById", key = "#id", condition = "#id!=null", unless = LocalConstants.RESULT_NULL)
    public ClientIdentificationType getById(Long id) throws BusinessException {
        if (id == null) {
            return null;
        }
        final Optional<ClientIdentificationTypeEntity> type = repository.findById(id);

        return type.map(t -> convert(t)).orElseThrow(() -> new ItemNotFoundBusinessException("Client identification type not found."));
    }

    @Cacheable(cacheNames = "findByCbcCode", key = "#code", condition = "#code!=null", unless = LocalConstants.RESULT_NULL)
    public ClientIdentificationType findByCbcCode(String code) {
        if (StringUtils.isNotEmpty(code)) {
            return converter.getObjectsMapper().map(repository.findByCbcCode(code), ClientIdentificationType.class);
        }
        return null;
    }
}
