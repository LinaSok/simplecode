package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.HealthSafety;
import com.dminc.prasac.model.entity.HealthSafetyEntity;
import com.dminc.prasac.repository.HealthSafetyRepository;

@Service
public class HealthSafetyService extends CommonService<HealthSafety, HealthSafetyEntity> {

    public HealthSafetyService(HealthSafetyRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, HealthSafety.class);
    }
}



