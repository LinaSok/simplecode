package com.dminc.prasac.service.selection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.dto.misc.ClientBusinessProgress;
import com.dminc.prasac.model.entity.ClientBusinessProgressEntity;
import com.dminc.prasac.repository.ClientBusinessProgressRepository;

@Service
public class ClientBusinessProgressService extends CommonService<ClientBusinessProgress, ClientBusinessProgressEntity> {

    public ClientBusinessProgressService(ClientBusinessProgressRepository repository, @Qualifier("objectsConverter") ObjectsConverter converter) {
        super(repository, converter, ClientBusinessProgress.class);
    }
}



