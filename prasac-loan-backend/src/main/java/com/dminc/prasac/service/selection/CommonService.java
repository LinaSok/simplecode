package com.dminc.prasac.service.selection;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.common.tools.mapping.Mapper;
import com.dminc.common.tools.mapping.ObjectsConverter;
import com.dminc.prasac.model.misc.AuditEntity;
import com.dminc.prasac.repository.AuditRepository;

public class CommonService<D, T extends AuditEntity> implements BaseCommonService<D, T> {
    protected final ObjectsConverter converter;
    protected final AuditRepository<T> repository;
    protected final Class<D> clazz;

    public CommonService(AuditRepository<T> repository, ObjectsConverter converter, Class clazz) {
        this.repository = repository;
        this.converter = converter;
        this.clazz = clazz;
    }

    @Transactional
    @Override
    public List<D> findAll() throws BusinessException {
        return convert(repository.findAll());
    }

    @Transactional
    @Override
    public List<D> findAll(LocalDateTime lastModified) throws BusinessException {
        List<T> types = lastModified == null ? repository.findAll() : repository.findAllByUpdatedDateAfter(lastModified);
        return convert(types);
    }

    @Override
    public List<D> convert(List<T> types) {
        return converter.runServiceTask((Mapper mapper) -> mapper.map(types, clazz));
    }

    @Override
    public D convert(T type) {
        return converter.runServiceTask((Mapper mapper) -> mapper.map(type, clazz));
    }
}
