package com.dminc.prasac.service.validator.constraint;

import java.lang.reflect.InvocationTargetException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;

import com.dminc.prasac.model.misc.LoanStatusEnum;
import com.dminc.prasac.model.request.PreScreenRequest;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FailReasonValidator implements ConstraintValidator<FailReason, PreScreenRequest> {
    private static final String FIELD_PRE_SCREENING_FAIL_REASON = "preScreeningFailReason";
    private static final String FIELD_LOAN_STATUS = "loanStatus";

    @Override
    public boolean isValid(PreScreenRequest request, ConstraintValidatorContext context) {
        if (request == null || getFieldValue(request, FIELD_LOAN_STATUS) == null) {
            return true;
        }

        Long loanStatus = Long.valueOf(getFieldValue(request, FIELD_LOAN_STATUS));
        String reason = getFieldValue(request, FIELD_PRE_SCREENING_FAIL_REASON);

        if (LoanStatusEnum.FAIL_PRE_SCREENING.getValue().equals(loanStatus) && StringUtils.isBlank(reason)) {
            populateContext(context);
            return false;
        }

        return true;
    }

    private String getFieldValue(PreScreenRequest request, String fieldName) {
        try {
            return BeanUtils.getProperty(request, fieldName);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            log.error(e.getLocalizedMessage(), e);
            return null;
        }
    }

    private void populateContext(ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
                .addPropertyNode(FIELD_PRE_SCREENING_FAIL_REASON).addConstraintViolation();
    }
}
