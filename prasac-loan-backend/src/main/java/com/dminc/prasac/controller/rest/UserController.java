package com.dminc.prasac.controller.rest;

import java.security.Principal;

import com.dminc.prasac.service.validator.UserValidator;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dminc.common.tools.exceptions.AuthorizationRequiredBusinessException;
import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.common.tools.exceptions.InvalidParametersBusinessException;
import com.dminc.prasac.model.dto.user.UserProfile;
import com.dminc.prasac.model.entity.User;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.service.BusinessError;
import com.dminc.prasac.service.UserDetailsServiceImpl;
import com.dminc.prasac.service.UserService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import springfox.documentation.annotations.ApiIgnore;

@Slf4j
@RestController
@RequestMapping("api/oauth")
@Api(tags = "User management", description = "Manage user information")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Secured(value = {LocalConstants.ROLE_USER})
public class UserController {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final UserDetailsServiceImpl userDetailsService;


    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Error getting user profile process"),
            @ApiResponse(code = 200, message = "User retrieved successfully"),
            @ApiResponse(code = 403, message = "Access denied"),
            @ApiResponse(code = 400, message = "Current password not matched")})
    @GetMapping(value = "/me")

    public UserProfile getMe(@ApiIgnore Principal principal) throws BusinessException {
        UserProfile userProfile = null;
        if (principal != null && principal.getName() != null) {
            userProfile = userService.getCurrentUserDetail(principal.getName());
        }
        if (userProfile == null) {
            BusinessError.OBJECT_NOT_FOUND.throwExceptionHttpStatus404();
        }
        log.debug("{}", userProfile.toString());
        return userProfile;
    }


    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Error during updating process"),
            @ApiResponse(code = 200, message = "User updated successfully"),
            @ApiResponse(code = 403, message = "Access denied"),
            @ApiResponse(code = 400, message = "Current password not matched")})
    @ApiOperation(value = "Change current user password")
    @PostMapping(value = "changepassword")
    public void changePassword(@ApiIgnore Principal principal, final String currentPassword, final String newPassword) throws BusinessException {
        if (principal == null) {
            throw new AuthorizationRequiredBusinessException("unauthorized", 403);
        }
        //check the validation
        UserValidator.validatePassword(newPassword);
        final String username = principal.getName();
        final User user = (User) userDetailsService.loadUserByUsername(username);

        if (!passwordEncoder.matches(currentPassword, user.getPassword())) {
            throw new InvalidParametersBusinessException("The Current password is not matched", 1000);
        }

        if (currentPassword.equals(newPassword)) {
            BusinessError.SAME_CURRENT_NEW_PASSWORD.throwExceptionHttpStatus400();
        }

        final String password = passwordEncoder.encode(newPassword);
        user.setPassword(password);
        this.userService.updateUser(user);
    }


}