package com.dminc.prasac.controller.admin;

import com.dminc.common.tools.Constants;
import com.dminc.prasac.model.dto.user.UserPermissionGroup;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.service.UserPermissionGroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Sanya Eng
 * The controller to retrieve the permission list for user role creation.
 * Due to the business, the permission will be fixed and no added or delete.
 */
@RestController
@Api(tags = "User management", description = "Manage user information")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping("api/permission")
public class PermissionController {

    private final UserPermissionGroupService userPermissionGroupService;

    @PreAuthorize(LocalConstants.CAN_VIEW_USERS)
    @GetMapping(value = "v1/group")
    @ApiOperation(value = "Get all permission with grouping", response = UserPermissionGroup.class, produces = Constants.JSON_MIME_UTF8)
    public List<UserPermissionGroup> getPermissionGroup() {
        return userPermissionGroupService.findAll();
    }

}
