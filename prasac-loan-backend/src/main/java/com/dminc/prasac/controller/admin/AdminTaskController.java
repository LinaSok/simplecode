package com.dminc.prasac.controller.admin;

import javax.servlet.http.HttpServletRequest;

import com.dminc.prasac.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dminc.common.tools.exceptions.AuthorizationRequiredBusinessException;
import com.dminc.prasac.model.dto.JResult;
import com.dminc.prasac.service.AdminTaskService;

@RestController
@RequestMapping("/api/_admin")
public class AdminTaskController {

    @Value("${admin.pass.key:myKey}")
    private String apiKey;
    @Autowired
    private AdminTaskService service;
    @Autowired
    private UserService userService;

    /**
     * <pre>
     *   It is mainly used for patching any data for specific user or creating something on the fly without stop server
     *   or it can be using for testing data purpose such as push service, sending email.
     *   To access it, you must login as ROLE_ADMIN first.
     * </pre>
     *
     * @param request
     * @param taskName
     * @param key
     */
    @RequestMapping(value = "task/{taskName}", method = {RequestMethod.GET})
    public ResponseEntity<JResult> processAdminTask(final HttpServletRequest request, @PathVariable final String taskName,
                                                    final @RequestParam String key) {

        if (!apiKey.equals(key)) {
            throw new AuthorizationRequiredBusinessException("403 forbidden access");
        }
        return new ResponseEntity<>(new JResult(service.processTask(taskName, request.getParameterMap())), HttpStatus.OK);
    }
}
