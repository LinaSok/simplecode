package com.dminc.prasac.controller.rest.selection;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.prasac.model.dto.misc.RepaymentMode;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.service.selection.RepaymentModeService;

import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(LocalConstants.API_V1 + RepaymentController.REPAYMENT_MODE)
@Api(tags = "Repayment mode management")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RepaymentController {
    public static final String REPAYMENT_MODE = "/repayment-modes";

    private final RepaymentModeService service;

    @GetMapping
    public List<RepaymentMode> getAll() throws BusinessException {
        return service.findAll();
    }
}
