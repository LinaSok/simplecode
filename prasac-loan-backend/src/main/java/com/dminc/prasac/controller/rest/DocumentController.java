package com.dminc.prasac.controller.rest;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dminc.common.tools.Constants;
import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.prasac.model.dto.misc.Document;
import com.dminc.prasac.model.dto.misc.DocumentClassificationType;
import com.dminc.prasac.model.dto.misc.DocumentType;
import com.dminc.prasac.model.misc.DocumentRequestType;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.model.request.COImageUploadRequest;
import com.dminc.prasac.model.request.DocumentRequest;
import com.dminc.prasac.model.request.ImageUploadRequest;
import com.dminc.prasac.service.DocumentService;
import com.dminc.prasac.service.selection.DocumentClassificationTypeService;
import com.dminc.prasac.service.selection.DocumentTypeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(DocumentController.API_DOCUMENTS_ENDPOINT)
@Api(tags = "Document management", description = "Manage information about documents.")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
@SuppressWarnings("PMD.TooManyMethods")
public class DocumentController extends AbstractCachedRestApi {
    public static final String API_DOCUMENTS = "/documents";
    public static final String API_DOCUMENTS_ENDPOINT = LocalConstants.API_V1 + API_DOCUMENTS;
    public static final String API_DOCUMENT_TYPES = "/types";
    public static final String API_CLASSIFICATION_TYPES = "/classification/types";

    private static final String PATH_LOAN = "/loan";
    private static final String PATH_CLIENT_CLIENT_ID = "/client/{clientId}";
    private static final String PATH_LIABILITY_LIABILITY_ID = "/liability/{liabilityId}";
    private static final String PATH_LOAN_LOAN_ID = PATH_LOAN + "/{loanId}";
    private static final String PATH_BUSINESS_BUSINESS_ID = "/business/{businessId}";
    private static final String PATH_COLLATERAL_COLLATERAL_ID = "/collateral/{collateralId}";
    private static final String CO_PATH = "/co";

    private final DocumentService service;
    private final DocumentTypeService documentTypeService;
    private final DocumentClassificationTypeService classificationTypeService;

    @GetMapping(API_DOCUMENT_TYPES)
    @ApiOperation(value = "get all document types", response = DocumentType.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    public List<DocumentType> getDocumentTypes(HttpServletResponse response) throws BusinessException {
        addCacheHeaders(LocalConstants.ONE_DAY_IN_SECONDS, false, response);
        return documentTypeService.findAll();
    }

    @PreAuthorize(LocalConstants.ALL_REVIEW_LEVEL_TABLET_USER_DECISION_MAKING)
    @GetMapping(API_CLASSIFICATION_TYPES)
    @ApiOperation(value = "get all document classification types", response = DocumentClassificationType.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    public List<DocumentClassificationType> getDocumentClassificationTypes(HttpServletResponse response)
            throws BusinessException {
        addCacheHeaders(LocalConstants.ONE_DAY_IN_SECONDS, false, response);
        return classificationTypeService.findAll();
    }

    @PreAuthorize(LocalConstants.ALL_REVIEW_LEVEL_TABLET_USER_DECISION_MAKING)
    @ApiOperation(value = "get loan documents", response = Document.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    @GetMapping(value = PATH_LOAN_LOAN_ID)
    public List<Document> getLoanDocs(HttpServletRequest request, @PathVariable Long loanId) throws BusinessException {
        return service.getLoanDocs(loanId);
    }

    @PreAuthorize(LocalConstants.ALL_REVIEW_LEVEL_TABLET_USER_DECISION_MAKING)
    @ApiOperation(value = "get client documents of a loan", response = Document.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    @GetMapping(value = PATH_CLIENT_CLIENT_ID)
    public List<Document> getClientDocs(HttpServletRequest request, @PathVariable Long clientId,
                                        @RequestParam(required = true) Long loanId) throws BusinessException {
        return service.getClientDocs(clientId, loanId);
    }

    @PreAuthorize(LocalConstants.ALL_REVIEW_LEVEL_TABLET_USER_DECISION_MAKING)
    @ApiOperation(value = "get business documents of a loan", response = Document.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    @GetMapping(value = PATH_BUSINESS_BUSINESS_ID)
    public List<Document> getBusinessDocs(HttpServletRequest request, @PathVariable Long businessId,
                                          @RequestParam(required = true) Long loanId) throws BusinessException {
        return service.getBusinessDocs(businessId, loanId);
    }

    @PreAuthorize(LocalConstants.ALL_REVIEW_LEVEL_TABLET_USER_DECISION_MAKING)
    @ApiOperation(value = "get liability documents of a loan", response = Document.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    @GetMapping(value = PATH_LIABILITY_LIABILITY_ID)
    public List<Document> getLiabilityDocs(HttpServletRequest request, @PathVariable Long liabilityId,
                                           @RequestParam(required = true) Long loanId) throws BusinessException {
        return service.getLiabilityDocs(liabilityId, loanId);
    }

    @PreAuthorize(LocalConstants.ALL_REVIEW_LEVEL_TABLET_USER_DECISION_MAKING)
    @ApiOperation(value = "get collateral documents of a loan", response = Document.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    @GetMapping(value = PATH_COLLATERAL_COLLATERAL_ID)
    public List<Document> getCollateralDocs(@PathVariable Long collateralId, @RequestParam Long loanId) throws BusinessException {
        return service.getCollateralDocs(collateralId, loanId);
    }

    @PreAuthorize(LocalConstants.ALL_REVIEW_LEVEL_TABLET_USER_DECISION_MAKING)
    @ApiOperation(value = "Upload loan documents", response = Document.class, consumes = LocalConstants.MULTIPART_FORM_DATA, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    @PostMapping(value = PATH_LOAN, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public List<Document> uploadLoanDocs(HttpServletRequest request, @Valid @ModelAttribute ImageUploadRequest uploadRequest)
            throws BusinessException {
        uploadRequest.setDocumentRequestType(DocumentRequestType.LOAN);

        return service.upload(uploadRequest);
    }

    @PreAuthorize(LocalConstants.ALL_REVIEW_LEVEL_TABLET_USER_DECISION_MAKING)
    @ApiOperation(value = "Upload loan documents for CO", response = Document.class, consumes = LocalConstants.MULTIPART_FORM_DATA, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    @PostMapping(value = "loan/co", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public List<Document> uploadCODocs(@Valid @ModelAttribute COImageUploadRequest request) throws BusinessException {
        ImageUploadRequest uploadRequest = populateImageUploadRequest(request, DocumentRequestType.LOAN);

        return service.upload(uploadRequest);
    }

    private ImageUploadRequest populateImageUploadRequest(COImageUploadRequest request, DocumentRequestType requestType) {
        final List<DocumentRequest> documentRequests = Arrays.stream(request.getImages())
                .map(image -> DocumentRequest.builder().image(image).documentType(request.getDocumentType()).build())
                .collect(Collectors.toList());
        return ImageUploadRequest.builder().documentRequestType(requestType).loan(request.getLoan())
                .documentRequests(documentRequests).build();
    }

    @PreAuthorize(LocalConstants.ALL_REVIEW_LEVEL_TABLET_USER_DECISION_MAKING)
    @ApiOperation(value = "Upload client documents of a loan", response = Document.class, consumes = LocalConstants.MULTIPART_FORM_DATA, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    @PostMapping(value = PATH_CLIENT_CLIENT_ID, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public List<Document> uploadClientDocs(HttpServletRequest request, @Valid @ModelAttribute ImageUploadRequest uploadRequest,
                                           @PathVariable Long clientId) throws BusinessException {
        uploadRequest.setDocumentRequestType(DocumentRequestType.CLIENT);
        uploadRequest.setClient(clientId);

        return service.upload(uploadRequest);
    }

    @PreAuthorize(LocalConstants.ALL_REVIEW_LEVEL_TABLET_USER_DECISION_MAKING)
    @ApiOperation(value = "Upload client documents of a loan for CO", response = Document.class, consumes = LocalConstants.MULTIPART_FORM_DATA, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    @PostMapping(value = PATH_CLIENT_CLIENT_ID + CO_PATH, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public List<Document> uploadCOClientDocs(COImageUploadRequest request, @PathVariable Long clientId) throws BusinessException {
        ImageUploadRequest uploadRequest = populateImageUploadRequest(request, DocumentRequestType.CLIENT);
        uploadRequest.setClient(clientId);

        return service.upload(uploadRequest);
    }

    @PreAuthorize(LocalConstants.ALL_REVIEW_LEVEL_TABLET_USER_DECISION_MAKING)
    @ApiOperation(value = "Upload liability documents of a loan", response = Document.class, consumes = LocalConstants.MULTIPART_FORM_DATA, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    @PostMapping(value = PATH_LIABILITY_LIABILITY_ID, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public List<Document> uploadLiabilityDocs(HttpServletRequest request, @Valid @ModelAttribute ImageUploadRequest uploadRequest,
                                              @PathVariable Long liabilityId) throws BusinessException {
        uploadRequest.setDocumentRequestType(DocumentRequestType.LIABILITY);
        uploadRequest.setLiability(liabilityId);

        return service.upload(uploadRequest);
    }

    @PreAuthorize(LocalConstants.ALL_REVIEW_LEVEL_TABLET_USER_DECISION_MAKING)
    @ApiOperation(value = "Upload liability documents of a loan for CO", response = Document.class, consumes = LocalConstants.MULTIPART_FORM_DATA, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    @PostMapping(value = PATH_LIABILITY_LIABILITY_ID + CO_PATH, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public List<Document> uploadCOLiabilityDocs(@ModelAttribute COImageUploadRequest request,
                                                @PathVariable Long liabilityId) throws BusinessException {

        ImageUploadRequest uploadRequest = populateImageUploadRequest(request, DocumentRequestType.LIABILITY);
        uploadRequest.setLiability(liabilityId);

        return service.upload(uploadRequest);
    }

    @PreAuthorize(LocalConstants.ALL_REVIEW_LEVEL_TABLET_USER_DECISION_MAKING)
    @ApiOperation(value = "Upload business documents of a loan", response = Document.class, consumes = LocalConstants.MULTIPART_FORM_DATA, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    @PostMapping(value = PATH_BUSINESS_BUSINESS_ID, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public List<Document> uploadBusinessDocs(HttpServletRequest request, @Valid @ModelAttribute ImageUploadRequest uploadRequest,
                                             @PathVariable Long businessId) throws BusinessException {
        uploadRequest.setDocumentRequestType(DocumentRequestType.BUSINESS);
        uploadRequest.setBusiness(businessId);

        return service.upload(uploadRequest);
    }

    @PreAuthorize(LocalConstants.ALL_REVIEW_LEVEL_TABLET_USER_DECISION_MAKING)
    @ApiOperation(value = "Upload business documents of a loan for CO", response = Document.class, consumes = LocalConstants.MULTIPART_FORM_DATA, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    @PostMapping(value = PATH_BUSINESS_BUSINESS_ID + CO_PATH, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public List<Document> uploadBusinessDocs(@ModelAttribute COImageUploadRequest request, @PathVariable Long businessId)
            throws BusinessException {
        ImageUploadRequest uploadRequest = populateImageUploadRequest(request, DocumentRequestType.BUSINESS);
        uploadRequest.setBusiness(businessId);

        return service.upload(uploadRequest);
    }

    @PreAuthorize(LocalConstants.ALL_REVIEW_LEVEL_TABLET_USER_DECISION_MAKING)
    @ApiOperation(value = "Upload collateral documents of a loan", response = Document.class, consumes = LocalConstants.MULTIPART_FORM_DATA, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    @PostMapping(value = PATH_COLLATERAL_COLLATERAL_ID, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public List<Document> uploadCollateralDocs(HttpServletRequest request,
                                               @Valid @ModelAttribute ImageUploadRequest uploadRequest, @PathVariable Long collateralId) throws BusinessException {
        log.info("Upload collateral documents of a loan");
        uploadRequest.setDocumentRequestType(DocumentRequestType.COLLATERAL);
        uploadRequest.setCollateral(collateralId);

        return service.upload(uploadRequest);
    }

    @PreAuthorize(LocalConstants.ALL_REVIEW_LEVEL_TABLET_USER_DECISION_MAKING)
    @ApiOperation(value = "Upload collateral documents of a loan for CO", response = Document.class, consumes = LocalConstants.MULTIPART_FORM_DATA, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    @PostMapping(value = PATH_COLLATERAL_COLLATERAL_ID + CO_PATH, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public List<Document> uploadCOCollateralDocs(@ModelAttribute COImageUploadRequest request,
                                                 @PathVariable Long collateralId) throws BusinessException {

        log.info("Upload collateral documents of a loan for CO");
        final ImageUploadRequest uploadRequest = populateImageUploadRequest(request, DocumentRequestType.COLLATERAL);
        uploadRequest.setCollateral(collateralId);

        return service.upload(uploadRequest);
    }

    @PreAuthorize(LocalConstants.ALL_REVIEW_LEVEL_TABLET_USER_DECISION_MAKING)
    @ApiOperation(value = "Upload other asset documents of a loan for CO", response = Document.class, consumes = LocalConstants.MULTIPART_FORM_DATA, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    @PostMapping(value = "/asset/{assetId}" + CO_PATH, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public List<Document> uploadCOAssetDocs(@ModelAttribute COImageUploadRequest request,
                                            @PathVariable Long assetId) throws BusinessException {

        log.info("Upload collateral documents of a loan for CO");
        final ImageUploadRequest uploadRequest = populateImageUploadRequest(request, DocumentRequestType.COLLATERAL);
        uploadRequest.setCollateral(assetId);

        return service.upload(uploadRequest);
    }

    @PreAuthorize(LocalConstants.ALL_REVIEW_LEVEL_TABLET_USER_DECISION_MAKING)
    @ApiOperation(value = "Get image by document id", produces = MediaType.IMAGE_JPEG_VALUE)
    @GetMapping(value = "/{documentId}", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getImageByDocumentId(@PathVariable Long documentId) {
        return service.getImageByDocumentId(documentId);
    }

    @PreAuthorize(LocalConstants.ALL_REVIEW_LEVEL_TABLET_USER_DECISION_MAKING)
    @ApiOperation(value = "Delete document by id", produces = Constants.JSON_MIME_UTF8)
    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }

}
