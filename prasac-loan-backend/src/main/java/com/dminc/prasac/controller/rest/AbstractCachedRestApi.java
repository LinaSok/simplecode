package com.dminc.prasac.controller.rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.dminc.common.tools.Constants;

/**
 * Created by carlos on 24/05/18.
 */
public class AbstractCachedRestApi {
    protected void addCacheHeaders(int seconds, boolean variesPerAuth, HttpServletResponse response) {
        if (seconds < 0) {
            throw new IllegalArgumentException("Invalid cache time: " + seconds);
        }
        response.setHeader("Cache-Control", "max-age=" + seconds);
        final List<String> varies = new ArrayList<>();
        varies.add(Constants.HEADER_X_API_KEY_NAME);
        if (variesPerAuth) {
            varies.add(Constants.AUTHORIZATION);
        }
        response.setHeader("Vary", String.join(",", varies));
    }
}
