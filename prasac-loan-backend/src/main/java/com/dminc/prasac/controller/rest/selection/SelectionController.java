package com.dminc.prasac.controller.rest.selection;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dminc.common.tools.Constants;
import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.prasac.controller.rest.AbstractCachedRestApi;
import com.dminc.prasac.model.dto.selection.Selection;
import com.dminc.prasac.model.dto.selection.SelectionRequest;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.service.selection.SelectionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(LocalConstants.API_V1 + SelectionController.API_SELECTIONS)
@Api(tags = "Predefined values management", description = "Manage information about predefined values.")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SelectionController extends AbstractCachedRestApi {
    public static final String API_SELECTIONS = "/selections";

    private final SelectionService service;

    @GetMapping
    @ApiOperation(value = "get some of predefined values after the lastModified.", response = Selection.class, produces = Constants.JSON_MIME_UTF8)
    public Selection getStatuses(HttpServletResponse response, @ModelAttribute SelectionRequest request)
            throws BusinessException {
        addCacheHeaders(LocalConstants.ONE_DAY_IN_SECONDS, false, response);
        return service.getSelection(request);
    }

}
