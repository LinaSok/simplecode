package com.dminc.prasac.controller.rest;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dminc.common.tools.Constants;
import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.prasac.model.dto.misc.Version;
import com.dminc.prasac.model.dto.selection.SelectionRequest;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.service.misc.VersionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(LocalConstants.API_V1 + VersionController.API_VERSIONS)
@Api(tags = "Entity version management", description = "Manage information about version of entities.")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class VersionController extends AbstractCachedRestApi {

    public static final String API_VERSIONS = "/versions";

    private final VersionService service;

    @GetMapping
    @ApiOperation(value = "get version of entities", response = Version.class, produces = Constants.JSON_MIME_UTF8)
    @ApiImplicitParam(name = "lastModified", dataType = "string", required = true, value = Constants.DATE_MINUTE_FORMAT)
    public Version getInfo(HttpServletResponse response, @Valid @ModelAttribute SelectionRequest request) throws BusinessException {
        addCacheHeaders(60, false, response);
        return service.getInfo(request);
    }

}
