package com.dminc.prasac.controller.rest.selection;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.prasac.model.dto.misc.CollateralTitleType;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.service.selection.CollateralTitleTypeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping(LocalConstants.API_V1 + CollateralController.API_COLLATERAL)
@Api(tags = "Collateral management")
public class CollateralController {
    public static final String API_COLLATERAL = "/collateral";
    public static final String API_COLLATERAL_TYPE = "/types";
    private final CollateralTitleTypeService collateralTitleTypeService;

    @GetMapping(API_COLLATERAL_TYPE)
    @ApiOperation(value = "Get all Collateral Title type")
    public List<CollateralTitleType> getCollateralTitleType() throws BusinessException {
        return collateralTitleTypeService.findAll();
    }
}
