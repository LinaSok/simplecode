package com.dminc.prasac.controller.rest.selection;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dminc.common.tools.Constants;
import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.prasac.controller.rest.AbstractCachedRestApi;
import com.dminc.prasac.model.dto.misc.Currency;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.service.selection.CurrencyService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(LocalConstants.API_V1 + CurrencyController.API_CURRENCIES)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Api(tags = "Currency management", description = "Manage information about currencies")
public class CurrencyController extends AbstractCachedRestApi {
    public static final String API_CURRENCIES = "/currencies";

    private final CurrencyService service;

    @GetMapping
    @ApiOperation(value = "get all currencies", response = Currency.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    public List<Currency> getCurrencies(HttpServletResponse response) throws BusinessException {
        addCacheHeaders(LocalConstants.ONE_DAY_IN_SECONDS, false, response);
        return service.findAll();
    }

}
