package com.dminc.prasac.controller.rest;

import com.dminc.common.tools.Constants;
import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.prasac.integration.domain.PrasacLoanRepaymentSchedule;
import com.dminc.prasac.integration.domain.PrasacLoanStatement;
import com.dminc.prasac.integration.service.PrasacLoanService;
import com.dminc.prasac.model.dto.CommentResponse;
import com.dminc.prasac.model.dto.loan.ApprovalRequest;
import com.dminc.prasac.model.dto.loan.Loan;
import com.dminc.prasac.model.dto.loan.RejectLoanRequest;
import com.dminc.prasac.model.dto.loan.SearchLoanRequest;
import com.dminc.prasac.model.dto.loan.SearchLoanResponse;
import com.dminc.prasac.model.dto.misc.AssignLoanRequest;
import com.dminc.prasac.model.dto.misc.ClientLiabilityCredit;
import com.dminc.prasac.model.dto.misc.LoanStatus;
import com.dminc.prasac.model.dto.selection.SelectionRequest;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.model.misc.RequestedFrom;
import com.dminc.prasac.model.request.AssignBackRequest;
import com.dminc.prasac.model.request.LoanAppraisalRequest;
import com.dminc.prasac.model.request.PreScreenRequest;
import com.dminc.prasac.model.request.SubmitNextLevelRequest;
import com.dminc.prasac.service.loan.LoanFlexCubeIntegrationImpl;
import com.dminc.prasac.service.loan.LoanHistoryService;
import com.dminc.prasac.service.loan.LoanService;
import com.dminc.prasac.service.selection.LoanStatusService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(LocalConstants.API_V1 + LoanController.API_LOANS)
@Api(tags = "Loan management", description = "Manage information about loans.")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@SuppressWarnings({"PMD.TooManyMethods"})
public class LoanController extends AbstractCachedRestApi {
    public static final String API_LOAN_STATUSES = "/statuses";
    public static final String API_LOANS = "/loans";
    private static final String API_ASSIGN_LOAN = "/assign/{id}";
    private static final String SEARCH = "/search";
    private static final String MY_LIST = "/mylist";
    private static final String ASSIGNED_LOAN = "/assignedloan";
    private static final String ASSIGN_BACK = "/{id}/assignback";
    private static final String LOAN_COMMENTS = "/{id}/comments";
    private static final String LIABILITY_CREDIT_HISTORY = "/{id}/liability-credit-history";
    private static final String ID = "/{id}";
    private static final String SUBMIT_NEXT_LEVEL = "/{id}/nextlevel";
    private static final String APPRAISAL_VALIDATION_MSG = "&minus; errorCode: 109, loan not editable or not found<br>";
    private static final String LOAN_VALIDATION_MSG = "&minus; errorCode: 99, some field(s) not presented<br>" + //
            "&minus; errorCode: 107, invalid CIF number(s)<br>" + //
            "&minus; errorCode: 104, should contain at least one borrower<br>" + //
            "&minus; errorCode: 108, duplicate identification(s) with other client<br>";
    private static final String PRE_SCREEN_VALIDATION_MSG =
            "&minus; errorCode: 105, decision must be 'passed' or 'failed' or 'new pre-screening'<br>" + //
                    "&minus; errorCode: 106, please provide fail reason<br>";
    private static final String PRE_SCREEN_VALIDATION_MESSAGES = LOAN_VALIDATION_MSG + PRE_SCREEN_VALIDATION_MSG;

    private final LoanService service;
    private final LoanStatusService statusService;
    private final LoanHistoryService loanHistoryService;
    private final PrasacLoanService prasacLoanService;
    private final LoanFlexCubeIntegrationImpl integration;

    @PreAuthorize(LocalConstants.CAN_CREATE_PRE_SCREENING)
    @PostMapping
    @ApiOperation(value = "Create new loan at pre-screening by CO", response = Loan.class, produces = Constants.JSON_MIME_UTF8)
    @ApiResponses(value = {@ApiResponse(code = 400, message = LOAN_VALIDATION_MSG + PRE_SCREEN_VALIDATION_MSG)})
    public Loan createByMobile(@RequestBody PreScreenRequest request) throws BusinessException {
        return service.createPreScreen(request, RequestedFrom.TABLET);
    }

    @PreAuthorize(LocalConstants.CAN_CREATE_PRE_SCREENING)
    @PostMapping("/admin")
    @ApiOperation(value = "Create new loan at pre-screening by BO", response = Loan.class, produces = Constants.JSON_MIME_UTF8)
    @ApiResponses(value = {@ApiResponse(code = 400, message = PRE_SCREEN_VALIDATION_MESSAGES)})
    public Loan createByBackOffice(@RequestBody PreScreenRequest request) throws BusinessException {
        return service.createPreScreen(request, RequestedFrom.BACK_OFFICE);
    }

    @PreAuthorize(LocalConstants.ALL_REVIEW_LEVEL_DECISION_MAKING)
    @PostMapping(SUBMIT_NEXT_LEVEL)
    @ApiOperation(value = "Submit to next level for BE_REVIEW, REVIEW_LEVEL2, REVIEW_LEVEL3, or REVIEW_LEVEL4", response = Loan.class, produces = Constants.JSON_MIME_UTF8)
    public Loan submitNexLevel(@PathVariable Long id, @RequestBody SubmitNextLevelRequest request) {
        return service.committeeSubmitLoan(id, request.getComment(), Optional.ofNullable(request.getUserRoleId()));
    }

    @PreAuthorize(LocalConstants.CAN_SUBMIT_LOAN_APPRAISAL)
    @PostMapping("/appraisal")
    @ApiOperation(value = "Submit loan appraisal", response = Loan.class, produces = Constants.JSON_MIME_UTF8)
    @ApiResponses(value = {@ApiResponse(code = 400, message = LOAN_VALIDATION_MSG + APPRAISAL_VALIDATION_MSG)})
    public Loan submitLoanAppraisal(@RequestBody LoanAppraisalRequest request) throws BusinessException {
        return service.submitLoanAppraisal(request);
    }

    @GetMapping(API_LOAN_STATUSES)
    @ApiOperation(value = "get all statuses", response = LoanStatus.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    public List<LoanStatus> getStatuses(HttpServletResponse response) throws BusinessException {
        addCacheHeaders(LocalConstants.ONE_DAY_IN_SECONDS, false, response);
        return statusService.findAll();
    }

    @PreAuthorize(LocalConstants.CAN_ASSIGN_LOAN)
    @PutMapping(API_ASSIGN_LOAN)
    @ApiOperation(value = "Assign loan to Branch or CO", response = Loan.class, produces = Constants.JSON_MIME_UTF8)
    public Loan assign(@PathVariable("id") Long id, @RequestBody AssignLoanRequest request) throws BusinessException {
        return service.assign(id, request);
    }

    @PreAuthorize(LocalConstants.CAN_VIEW_LOAN)
    @GetMapping(SEARCH)
    @ApiOperation(value = "search loan", response = SearchLoanResponse.class, produces = Constants.JSON_MIME_UTF8)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "offset", dataType = LocalConstants.INTEGER, paramType = LocalConstants.QUERY, value = "The page number. Default = 0 (0..N)"),
            @ApiImplicitParam(name = "limit", dataType = LocalConstants.INTEGER, paramType = LocalConstants.QUERY, value = "Number of records per page. Default 10")})
    public Page<SearchLoanResponse> search(SearchLoanRequest request, @ApiIgnore Pageable pageable) {
        return service.search(request, pageable);
    }

    @PreAuthorize(LocalConstants.TABLET_USER)
    @GetMapping(MY_LIST)
    @ApiOperation(value = "Get current user loans which are not newly assign", response = Loan.class, produces = Constants.JSON_MIME_UTF8)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "offset", dataType = LocalConstants.INTEGER, paramType = LocalConstants.QUERY, value = "The page number. Default = 0 (0..N)"),
            @ApiImplicitParam(name = "limit", dataType = LocalConstants.INTEGER, paramType = LocalConstants.QUERY, value = "Number of records per page. Default 10")})
    public Page<Loan> myList(@ModelAttribute SelectionRequest request, @ApiIgnore Pageable pageable) {
        return service.getMyList(request.getLastModified(), pageable);
    }

    @PreAuthorize(LocalConstants.TABLET_USER)
    @GetMapping(ASSIGNED_LOAN)
    @ApiOperation(value = "Get current user newly assigned loan", response = Loan.class, produces = Constants.JSON_MIME_UTF8)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "offset", dataType = LocalConstants.INTEGER, paramType = LocalConstants.QUERY, value = "The page number. Default = 0 (0..N)"),
            @ApiImplicitParam(name = "limit", dataType = LocalConstants.INTEGER, paramType = LocalConstants.QUERY, value = "Number of records per page. Default 10")})
    public Page<Loan> assignedLoan(@ModelAttribute SelectionRequest request, @ApiIgnore Pageable pageable) {
        return service.getAssignedLoan(request.getLastModified(), pageable);
    }

    @PreAuthorize(LocalConstants.CAN_ASSIGN_BACK)
    @ApiOperation(value = "Assign back the loan for correction")
    @PostMapping(ASSIGN_BACK)
    public void assignBack(@PathVariable Long id, @RequestBody AssignBackRequest request) {
        this.service.assignBack(id, request.getComment());
    }

    @PreAuthorize(LocalConstants.CAN_VIEW_COMMENTS)
    @ApiOperation(value = "Get list of comments for a loan")
    @GetMapping(LOAN_COMMENTS)
    public List<CommentResponse> getComments(@PathVariable Long id, @ModelAttribute SelectionRequest request) {
        return service.getComments(id, request.getLastModified());
    }

    @PreAuthorize(LocalConstants.TABLET_USER)
    @ApiOperation(value = "Get liability credit history by clients")
    @GetMapping(LIABILITY_CREDIT_HISTORY)
    public List<ClientLiabilityCredit> getClientLiabilityCredit(@PathVariable("id") long id) {
        return loanHistoryService.getClientLiabilityCredits(id);
    }

    @PreAuthorize(LocalConstants.CAN_VIEW_LOAN)
    @ApiOperation(value = "Get loan by ID")
    @GetMapping(ID)
    public Loan getLoanById(@PathVariable Long id) {
        return this.service.getById(id);
    }

    @PreAuthorize(LocalConstants.CAN_REJECT_LOAN)
    @PostMapping("/{id}/reject")
    @ApiOperation("Reject loan")
    public Loan rejectLoan(@PathVariable Long id, @RequestBody RejectLoanRequest request) {
        return service.rejectLoan(id, request.getComment(), request.isByClient());
    }

    @PreAuthorize(LocalConstants.CAN_MAKE_DECISION)
    @PostMapping("/{id}/approve")
    @ApiOperation("Approve Loan")
    public Loan approveLoan(@PathVariable Long id, @RequestBody ApprovalRequest request) {
        return service.approvedLoan(id, request.getComment());
    }

    @PreAuthorize(LocalConstants.CAN_MAKE_DECISION)
    @PostMapping("/{id}/suspend")
    @ApiOperation("Suspend loan")
    public Loan suspendLoan(@PathVariable Long id, @RequestBody ApprovalRequest request) {
        return service.suspendedLoan(id, request.getComment());
    }

    @PreAuthorize(LocalConstants.TABLET_USER)
    @PostMapping("/{id}/completereject")
    @ApiOperation("Complete reject which will be called by the TABLET USER")
    public Loan completeReject(@PathVariable Long id) {
        return service.completeReject(id);
    }

    @PreAuthorize(LocalConstants.CAN_VIEW_LOAN)
    @ApiOperation(value = "Get loan statement by clientCif")
    @GetMapping("/statement")
    @ResponseBody
    public ModelAndView getLoanStatementById(@RequestParam String clientCif) {
        final List<PrasacLoanStatement> loanStatements = this.prasacLoanService.getLoanStatementByClientCif(clientCif);
        if (CollectionUtils.isEmpty(loanStatements)) {
            return new ModelAndView("noresult-found");
        }
        final ModelAndView modelAndView = new ModelAndView("Loan_Statement_Report_Template");
        modelAndView.addObject("loanStatements", loanStatements);
        return modelAndView;
    }

    @PreAuthorize(LocalConstants.CAN_VIEW_LOAN)
    @ApiOperation(value = "Get loan repayment schedule by clientCif")
    @GetMapping("/repayment-schedule")
    public ModelAndView getLoanRepaymentScheduleById(@RequestParam String clientCif) {
        final List<PrasacLoanRepaymentSchedule> loanRepaymentsSchedule = this.prasacLoanService.getLoanRepaymentsSchedule(clientCif);
        if (CollectionUtils.isEmpty(loanRepaymentsSchedule)) {
            return new ModelAndView("noresult-found");
        }
        final ModelAndView modelAndView = new ModelAndView("Loan_loanRepaymentsSchedule_Report_Template");
        modelAndView.addObject("loanRepaymentsSchedule", loanRepaymentsSchedule);
        return modelAndView;
    }

    @GetMapping("/{id}/integrate-flexcube")
    public void integrateFlexCube(@PathVariable("id") long id) throws IOException {
        integration.integrateFlexCube(id);
    }
}
