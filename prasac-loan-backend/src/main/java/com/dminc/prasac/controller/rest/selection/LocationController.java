package com.dminc.prasac.controller.rest.selection;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dminc.common.tools.Constants;
import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.prasac.controller.rest.AbstractCachedRestApi;
import com.dminc.prasac.model.dto.address.Province;
import com.dminc.prasac.model.entity.selection.ProvinceEntity;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.service.selection.LocationService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import springfox.documentation.annotations.ApiIgnore;

@Slf4j
@RestController
@RequestMapping(LocalConstants.API_V1 + LocationController.API_LOCATIONS)
@Api(tags = "Location management", description = "Manage information about locations.")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LocationController extends AbstractCachedRestApi {

    public static final String API_LOCATIONS = "/locations";
    public static final String API_CAMBODIA = "/cambodia";
    private static final String SORT_DESC = "Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Ex: name,desc (Will return the sort by name desc)";
    private static final String PROVINCE_COLUMN_NAME = ProvinceEntity.COLUMN_NAME;

    private final LocationService service;

    @GetMapping(API_CAMBODIA)
    @ApiOperation(value = "Get all provinces in Cambodia", response = Province.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "offset", dataType = LocalConstants.INTEGER, paramType = LocalConstants.QUERY, value = "The page number. Default = 0 (0..N)"),
            @ApiImplicitParam(name = "limit", dataType = LocalConstants.INTEGER, paramType = LocalConstants.QUERY, value = "Number of records per page. Default 10"),
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = LocalConstants.QUERY, value = SORT_DESC)})
    public Page<Province> getCambodiaProvinces(
            @ApiIgnore @PageableDefault(sort = {PROVINCE_COLUMN_NAME}, direction = Sort.Direction.ASC) final Pageable pageable,
            HttpServletResponse response) throws BusinessException {
        addCacheHeaders(LocalConstants.ONE_DAY_IN_SECONDS, false, response);
        return service.getCambodiaProvinces(pageable);
    }

    @ApiOperation(value = "Get all provinces of a country", response = Province.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "offset", dataType = LocalConstants.INTEGER, paramType = LocalConstants.QUERY, value = "The page number. Default = 0 (0..N)"),
            @ApiImplicitParam(name = "limit", dataType = LocalConstants.INTEGER, paramType = LocalConstants.QUERY, value = "Number of records per page. Default 10"),
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = LocalConstants.QUERY, value = SORT_DESC)})
    @GetMapping("/country/{countryId}/provinces")
    public Page<Province> getProvinces(
            @ApiIgnore @PageableDefault(sort = {PROVINCE_COLUMN_NAME}, direction = Sort.Direction.ASC) final Pageable pageable,
            @PathVariable Long countryId, HttpServletResponse response) {
        addCacheHeaders(LocalConstants.ONE_DAY_IN_SECONDS, false, response);
        return this.service.getProvinces(pageable, countryId);
    }
}
