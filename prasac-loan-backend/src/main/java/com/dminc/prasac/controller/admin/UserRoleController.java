package com.dminc.prasac.controller.admin;

import com.dminc.common.tools.Constants;
import com.dminc.prasac.model.entity.UserRole;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.model.request.NewRoleRequest;
import com.dminc.prasac.service.ObjectsConverter;
import com.dminc.prasac.service.UserRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@RequestMapping("api/admin")
@Api(tags = "User management", description = "Manage user information")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserRoleController {

    private final UserRoleService userRoleService;
    private final ObjectsConverter converter;

    @PreAuthorize("hasAuthority('" + LocalConstants.PERMISSION_CREATE_ROLE + "')")
    @PostMapping("v1/role")
    @ApiOperation(value = "Add role", response = com.dminc.prasac.model.dto.user.UserRole.class, produces = Constants.JSON_MIME_UTF8)
    public com.dminc.prasac.model.dto.user.UserRole createUserRole(@RequestBody NewRoleRequest userRole) {
        final UserRole role = userRoleService.createUserRole(userRole);
        return converter.getObjectsMapper().map(role, com.dminc.prasac.model.dto.user.UserRole.class);
    }

    @PreAuthorize("hasAuthority('" + LocalConstants.PERMISSION_EDIT_ROLE + "')")
    @PostMapping("v1/role/{id}")
    @ApiOperation(value = "Edit role", response = com.dminc.prasac.model.dto.user.UserRole.class, produces = Constants.JSON_MIME_UTF8)
    public com.dminc.prasac.model.dto.user.UserRole updateUserRole(@PathVariable Long id, @RequestBody NewRoleRequest userRole) {
        final UserRole role = userRoleService.updateRole(userRole, id);
        return converter.getObjectsMapper().map(role, com.dminc.prasac.model.dto.user.UserRole.class);
    }

    @PreAuthorize("hasAuthority('" + LocalConstants.PERMISSION_DELETE_ROLE + "')")
    @ApiOperation(value = "Delete user role (group)")
    @DeleteMapping("v1/role/{id}")
    public void deleteRole(@PathVariable Long id) {
        userRoleService.deleteRole(id);
    }

    @PreAuthorize(LocalConstants.CAN_VIEW_USER_GROUP)
    @GetMapping("v1/role")
    @ApiOperation(value = "Get page of user role (group)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "offset", dataType = "integer", paramType = "query", value = "The page number. Default = 0 (0..N)"),
            @ApiImplicitParam(name = "limit", dataType = "integer", paramType = "query", value = "Number of records per page. Default 10"),
            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query",
                    value = "Sorting criteria in the format: property(,asc|desc). "
                            + "Default sort order is ascending. "
                            + "Multiple sort criteria are supported. Ex: name,desc (Will return the sort by name desc)")})
    public Page<com.dminc.prasac.model.dto.user.UserRole> getRoleList(@ApiIgnore @PageableDefault(sort = {"name"}, direction = Sort.Direction.ASC) final Pageable pageable) {
        return userRoleService.getUserRole(pageable);
    }

    @GetMapping("v1/role/decisionmaker")
    @ApiOperation(value = "Get list of Review level 4")
    public List<com.dminc.prasac.model.dto.user.UserRole> getDecisionMaker() {
        return userRoleService.getDecisionMakers();
    }

}
