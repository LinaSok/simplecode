package com.dminc.prasac.controller.page;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.prasac.integration.domain.CbcReport;
import com.dminc.prasac.model.dto.loan.CreditHistoryMonitoring;
import com.dminc.prasac.model.dto.loan.LoanHistory;
import com.dminc.prasac.model.dto.loan.ClientRequest;
import com.dminc.prasac.model.dto.loan.LoanRequestHistory;
import com.dminc.prasac.model.dto.misc.SearchResultClient;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.service.CbcService;
import com.dminc.prasac.service.client.ClientService;
import com.dminc.prasac.service.credit.CreditHistoryMonitorService;
import com.dminc.prasac.service.loan.LoanHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.List;

import static com.dminc.prasac.controller.page.ClientPageController.API_CLIENT;

@Controller
@RequestMapping(API_CLIENT)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
@Api(tags = "Client History", value = "Client's history report")
public class ClientPageController {
    public static final String API_CLIENT = "/clients";
    public static final String API_LOAN_HISTORY_CLIENT_CIF = "/{clientCif}/credit-history.html";
    public static final String API_LOAN_REQUEST_HISTORY = "/loan-request-history.html";
    public static final String API_CREDIT_HISTORY_MONITORING_BY_CLIENT = "/{clientCif}/credit-history-monitoring.html";
    public static final String API_CBC_REPORT = "{loanId}/CBC-report.html";

    private final LoanHistoryService loanHistoryService;
    private final CreditHistoryMonitorService creditHistoryMonitorService;
    private final ClientService clientService;
    private final CbcService cbcService;


    @PreAuthorize(LocalConstants.CAN_VIEW_LOAN_REQUEST_HISTORY)
    @GetMapping(API_LOAN_HISTORY_CLIENT_CIF)
    @ApiOperation(value = "Get credit history by Client cif")
    public ModelAndView getLoanHistory(@PathVariable("clientCif") String clientCif) throws BusinessException {
        final ModelAndView modelAndView = new ModelAndView("creditHistory");
        final List<LoanHistory> loans = loanHistoryService.getHistoriesByClient(clientCif);
        modelAndView.addObject("loans", loans);
        return modelAndView;
    }

    @PreAuthorize(LocalConstants.CAN_VIEW_LOAN_REQUEST_HISTORY)
    @GetMapping(API_CREDIT_HISTORY_MONITORING_BY_CLIENT)
    @ApiOperation(value = "Get credit history monitoring by client")
    public ModelAndView getCreditHistoryMonitoringByClient(@PathVariable("clientCif") String clientCif) {
        final ModelAndView modelAndView = new ModelAndView("creditHistoryMonitoring");
        final List<CreditHistoryMonitoring> creditHistories = creditHistoryMonitorService.getByClient(clientCif);
        modelAndView.addObject("credits", creditHistories);
        return modelAndView;
    }

    @PreAuthorize(LocalConstants.CAN_VIEW_LOAN_REQUEST_HISTORY)
    @GetMapping(API_LOAN_REQUEST_HISTORY)
    @ApiOperation(value = "Get loan request history")

    public ModelAndView getLoanRequestHistory(ClientRequest request) {
        final ModelAndView modelAndView = new ModelAndView("loanRequestHistory");
        final SearchResultClient client = clientService.getClientByCifFromCrm(request);

        modelAndView.addObject("client", client);
        List<LoanRequestHistory> loans = loanHistoryService.getLoanRequestHistoryByClientIdAndCif(request);
        modelAndView.addObject("loans", loans);
        return modelAndView;
    }

    @PreAuthorize(LocalConstants.CAN_VIEW_LOAN_REQUEST_HISTORY)
    @GetMapping(API_CBC_REPORT)
    @ApiOperation(value = "Get loan CBC report")
    public ModelAndView getCBCLoanReport(@PathVariable("loanId") long loanId) throws IOException {
        final CbcReport cbcReport = cbcService.cbcReport(loanId);
        if (cbcReport == null) {
            return new ModelAndView("noresult-found");
        }

        final ModelAndView modelAndView = new ModelAndView("CBC_Report_Template");
        modelAndView.addObject("cbcReport", cbcReport);
        return modelAndView;
    }
}
