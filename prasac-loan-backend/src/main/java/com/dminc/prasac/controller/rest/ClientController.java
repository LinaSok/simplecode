package com.dminc.prasac.controller.rest;

import com.dminc.common.tools.Constants;
import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.common.tools.exceptions.ItemNotFoundBusinessException;
import com.dminc.prasac.model.dto.loan.CreditHistoryMonitoring;
import com.dminc.prasac.model.dto.loan.LoanHistory;
import com.dminc.prasac.model.dto.loan.ClientRequest;
import com.dminc.prasac.model.dto.loan.LoanRequestHistory;
import com.dminc.prasac.model.dto.misc.Client;
import com.dminc.prasac.model.dto.misc.ClientIdentificationType;
import com.dminc.prasac.model.dto.misc.ClientOccupation;
import com.dminc.prasac.model.dto.misc.ClientRelationship;
import com.dminc.prasac.model.dto.misc.ClientResidentStatus;
import com.dminc.prasac.model.dto.misc.ClientType;
import com.dminc.prasac.model.dto.misc.ClientWorkingTime;
import com.dminc.prasac.model.dto.misc.MaritalStatus;
import com.dminc.prasac.model.dto.misc.Nationality;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.model.request.SearchClientRequest;
import com.dminc.prasac.service.client.ClientService;
import com.dminc.prasac.service.credit.CreditHistoryMonitorService;
import com.dminc.prasac.service.loan.LoanHistoryService;
import com.dminc.prasac.service.selection.ClientIdentificationTypeService;
import com.dminc.prasac.service.selection.ClientRelationshipService;
import com.dminc.prasac.service.selection.ClientResidentStatusService;
import com.dminc.prasac.service.selection.ClientTypeService;
import com.dminc.prasac.service.selection.ClientWorkingTimeService;
import com.dminc.prasac.service.selection.MaritalStatusService;
import com.dminc.prasac.service.selection.NationalityService;
import com.dminc.prasac.service.selection.OccupationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping(LocalConstants.API_V1 + ClientController.API_CLIENTS)
@Api(tags = "Client management", description = "Manage information about clients.")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ClientController extends AbstractCachedRestApi {

    public static final String API_CLIENTS = "/clients";
    public static final String API_RELATIONSHIPS = "/relationships";
    public static final String API_RESIDENT_STATUSES = "/resident/statuses";
    public static final String API_CLIENT_TYPES = "/types";
    public static final String API_WORKING_TIMES = "/working/times";
    public static final String API_MARITAL_STATUSES = "/marital/statuses";
    public static final String API_NATIONALITIES = "/nationalities";
    public static final String API_OCCUPATIONS = "/occupations";
    public static final String API_IDENTIFICATION_TYPES = "/identification/types";
    public static final String API_SEARCH = "/search";
    public static final String API_CREDIT_HISTORY_MONITORING_BY_CLIENT = "/{clientCif}/credit-history-monitoring";
    public static final String API_LOAN_HISTORY_CLIENT_CIF = "/{clientCif}/loan-history";
    public static final String API_LOAN_REQUEST_HISTORY = "{clientCif}/loan-request-history";
    public static final String CLIENT_CIF = "clientCif";

    private final ClientRelationshipService relationshipService;
    private final ClientResidentStatusService residentStatusService;
    private final ClientTypeService clientTypeService;
    private final ClientWorkingTimeService workingTimeService;
    private final MaritalStatusService maritalStatusService;
    private final NationalityService nationalityService;
    private final OccupationService occupationService;
    private final ClientIdentificationTypeService identificationTypeService;
    private final ClientService clientService;
    private final CreditHistoryMonitorService creditHistoryMonitorService;
    private final LoanHistoryService loanHistoryService;

    @GetMapping(API_RELATIONSHIPS)
    @Secured({LocalConstants.ROLE_USER, LocalConstants.ROLE_MOBILE_CLIENT})
    @ApiOperation(value = "get all relationships", response = ClientRelationship.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    public List<ClientRelationship> getRelationships(HttpServletResponse response) throws BusinessException {
        addCacheHeaders(LocalConstants.ONE_DAY_IN_SECONDS, false, response);
        return relationshipService.findAll();
    }

    @GetMapping(API_RESIDENT_STATUSES)
    @ApiOperation(value = "get all resident statuses", response = ClientResidentStatus.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    public List<ClientResidentStatus> getResidentStatuses(HttpServletResponse response) throws BusinessException {
        addCacheHeaders(LocalConstants.ONE_DAY_IN_SECONDS, false, response);
        return residentStatusService.findAll();
    }

    @GetMapping(API_CLIENT_TYPES)
    @ApiOperation(value = "get client types", response = ClientType.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    public List<ClientType> getClientTypes(HttpServletResponse response) throws BusinessException {
        addCacheHeaders(LocalConstants.ONE_DAY_IN_SECONDS, false, response);
        return clientTypeService.findAll();
    }

    @GetMapping(API_WORKING_TIMES)
    @ApiOperation(value = "get all working times", response = ClientWorkingTime.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    public List<ClientWorkingTime> getWorkingTimes(HttpServletResponse response) throws BusinessException {
        addCacheHeaders(LocalConstants.ONE_DAY_IN_SECONDS, false, response);
        return workingTimeService.findAll();
    }

    @GetMapping(API_MARITAL_STATUSES)
    @ApiOperation(value = "get all marital statuses", response = MaritalStatus.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    public List<MaritalStatus> getMaritalStatuses(HttpServletResponse response) throws BusinessException {
        addCacheHeaders(LocalConstants.ONE_DAY_IN_SECONDS, false, response);
        return maritalStatusService.findAll();
    }

    @GetMapping(API_NATIONALITIES)
    @ApiOperation(value = "get all nationalities", response = Nationality.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    public List<Nationality> getNationalities(HttpServletResponse response) throws BusinessException {
        addCacheHeaders(LocalConstants.ONE_DAY_IN_SECONDS, false, response);
        return nationalityService.findAll();
    }

    @GetMapping(API_OCCUPATIONS)
    @ApiOperation(value = "get all client occupations", response = ClientOccupation.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    public List<ClientOccupation> getOccupations(HttpServletResponse response) throws BusinessException {
        addCacheHeaders(LocalConstants.ONE_DAY_IN_SECONDS, false, response);
        return occupationService.findAll();
    }

    @GetMapping(API_IDENTIFICATION_TYPES)
    @ApiOperation(value = "get all identification types", response = ClientIdentificationType.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    public List<ClientIdentificationType> getIdentificationTypes(HttpServletResponse response) throws BusinessException {
        addCacheHeaders(LocalConstants.ONE_DAY_IN_SECONDS, false, response);
        return identificationTypeService.findAll();
    }

    @PreAuthorize(LocalConstants.TABLET_USER)
    @GetMapping(API_SEARCH)
    @ApiOperation(value = "search client by cif, name, idNumber and dob", response = Client.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dob", dataType = "date", paramType = "query", value = "Format as yyyy-mm-dd, example: 1974-07-06"),
            @ApiImplicitParam(name = "offset", dataType = "integer", paramType = "query", value = "The page number. Default = 0 (0..N)"),
            @ApiImplicitParam(name = "limit", dataType = "integer", paramType = "query", value = "Number of records per page. Default 10")})
    public Page<Client> searchClients(SearchClientRequest request, @ApiIgnore final Pageable pageable) throws BusinessException {
        return clientService.search(request, pageable);
    }


    @PreAuthorize(LocalConstants.TABLET_USER)
    @GetMapping()
    @ApiOperation(value = "get client detail by cif", response = Client.class, produces = Constants.JSON_MIME_UTF8)
    public Client getDetailByCif(ClientRequest request) throws BusinessException {
        if (StringUtils.isNotEmpty(request.getClientCif())) {
            return clientService.getDetailByCifFromCrm(request.getClientCif());
        } else {
            return clientService.getById(request.getClientId()).orElseThrow(() -> new ItemNotFoundBusinessException("Client not found", 404));
        }
    }

    @PreAuthorize(LocalConstants.TABLET_USER)
    @GetMapping(API_LOAN_HISTORY_CLIENT_CIF)
    @ApiOperation(value = "Get Loan history by Client cif", response = LoanHistory.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    public List<LoanHistory> getLoanHistory(@PathVariable(CLIENT_CIF) String clientCif) throws BusinessException {
        return loanHistoryService.getHistoriesByClient(clientCif);
    }

    @PreAuthorize(LocalConstants.TABLET_USER)
    @GetMapping(API_CREDIT_HISTORY_MONITORING_BY_CLIENT)
    @ApiOperation(value = "Get credit history monitoring by client", response = CreditHistoryMonitoring.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    public List<CreditHistoryMonitoring> getCreditHistoryMonitoringByClient(@PathVariable(CLIENT_CIF) String clientCif) {
        return creditHistoryMonitorService.getByClient(clientCif);
    }

    @PreAuthorize(LocalConstants.TABLET_USER)
    @GetMapping(API_LOAN_REQUEST_HISTORY)
    @ApiOperation(value = "Get loan request history", response = LoanRequestHistory.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    public List<LoanRequestHistory> getLoanRequestHistoryByClientCif(@PathVariable(CLIENT_CIF) String clientCif) {
        return loanHistoryService.getLoanRequestHistoryByClientCif(clientCif);
    }
}
