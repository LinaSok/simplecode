package com.dminc.prasac.controller.rest;

import com.dminc.common.tools.Constants;
import com.dminc.prasac.model.dto.user.Branch;
import com.dminc.prasac.model.dto.user.Position;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.service.selection.BranchService;
import com.dminc.prasac.service.selection.PositionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(PrasacController.API_V1_PRASAC)
@Api(tags = "Prasac Organization", description = "Controller to get object related to Prasac. Ex: Branch, Position")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PrasacController {

    public static final String API_V1_PRASAC = "api/v1/prasac";
    public static final String API_POSITIONS = "positions";
    public static final String API_BRANCHES = "branches";
    private final BranchService branchService;
    private final PositionService positionService;

    @ApiOperation(value = "Get all branches", response = Branch.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    @GetMapping(API_BRANCHES)
    public List<Branch> getAllBranches() {
        return branchService.findAll();
    }

    @GetMapping(API_POSITIONS)
    @ApiOperation(value = "Get all positions", response = Position.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    public List<Position> getAllPosition() {
        return positionService.getAll();
    }
}
