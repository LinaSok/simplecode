package com.dminc.prasac.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dminc.prasac.model.dto.pdf.PDFs;
import com.dminc.prasac.service.pdf.PdfService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("api/v1")
@Api(tags = "PDF file management")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PdfController {

    private final PdfService pdfService;

    @GetMapping("pdfs")
    @ApiOperation(value = "Get PDF files descriptors")
    public PDFs getPdfDocumentDescriptors() {
        return pdfService.getAllPDFs();
    }
}
