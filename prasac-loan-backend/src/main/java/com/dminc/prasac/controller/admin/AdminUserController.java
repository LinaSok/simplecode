package com.dminc.prasac.controller.admin;

import com.dminc.common.tools.Constants;
import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.prasac.model.dto.user.*;
import com.dminc.prasac.model.entity.User;
import com.dminc.prasac.model.misc.LocalConstants;
import com.dminc.prasac.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/admin")
@Api(tags = "User management", description = "Manage user information")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@SuppressWarnings("PMD.AvoidDuplicateLiterals")
public class AdminUserController {

    private final UserService userService;

    @PreAuthorize("hasAuthority('" + LocalConstants.PERMISSION_CREATE_USER + "')")
    @PostMapping("v1/user")
    @ApiOperation(value = "Add new user", response = UserResponse.class, produces = Constants.JSON_MIME_UTF8)
    public UserResponse create(@RequestBody @Valid NewUserRequest userRequest) throws BusinessException {
        return userService.createNewUser(userRequest);
    }

    @PreAuthorize("hasAuthority('" + LocalConstants.PERMISSION_EDIT_USER + "')")
    @PutMapping("v1/user/{userId}")
    @ApiOperation(value = "Update user", response = UserResponse.class, produces = Constants.JSON_MIME_UTF8)
    public UserResponse update(@PathVariable("userId") long userId, @RequestBody @Valid UserRequest userRequest) throws BusinessException {
        return userService.updateUser(userRequest, userId);
    }

    @PreAuthorize("hasAuthority('" + LocalConstants.PERMISSION_DEACTIVATE_USER + "')")
    @PutMapping("v1/user/{userId}/deactivate")
    @ApiOperation(value = "DeActivate user by id")
    public void deActivateUser(@PathVariable("userId") long userId) {
        userService.deActivateUser(userId);
    }

    @PreAuthorize("hasAuthority('" + LocalConstants.PERMISSION_DEACTIVATE_USER + "')")
    @PutMapping("v1/user/{userId}/activate")
    @ApiOperation(value = "Activate user by id")
    public void activateUser(@PathVariable("userId") long userId) {
        userService.activateUser(userId);
    }

    @PreAuthorize("hasAuthority('" + LocalConstants.PERMISSION_EDIT_USER + "')")
    @PutMapping("v1/user/{userId}/reset-password")
    @ApiOperation(value = "Reset password")
    public void resetPassword(@PathVariable("userId") long userId) {
        userService.resetPassword(userId);
    }

    @PreAuthorize(LocalConstants.CAN_VIEW_USERS)
    @GetMapping("v1/user/{id}")
    @ApiOperation(value = "Get user profile by user id")
    public UserProfile getUserProfile(@PathVariable Long id) {
        return userService.getUserProfile(id);
    }

    @PreAuthorize(LocalConstants.CAN_VIEW_USERS)
    @GetMapping("v1/user")
    @ApiOperation(value = "search user profiles", response = User.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "offset", dataType = LocalConstants.INTEGER, paramType = LocalConstants.QUERY, value = "The page number. Default = 0 (0..N)"),
            @ApiImplicitParam(name = "limit", dataType = LocalConstants.INTEGER, paramType = LocalConstants.QUERY, value = "Number of records per page. Default 10")})
    public Page<UserProfile> searchUserProfile(SearchUserProfileRequest request, @ApiIgnore Pageable pageable) {
        return userService.searchProfile(request, pageable);
    }

    @PreAuthorize(LocalConstants.CAN_RECEIVE_PRE_SCREENING)
    @GetMapping("v1/userautocomplete")
    @ApiOperation(value = "search user profiles for assigning pre-screening", response = AutoCompleteUser.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "offset", dataType = LocalConstants.INTEGER, paramType = LocalConstants.QUERY, value = "The page number. Default = 0 (0..N)"),
            @ApiImplicitParam(name = "limit", dataType = LocalConstants.INTEGER, paramType = LocalConstants.QUERY, value = "Number of records per page. Default 10")})
    public Page<AutoCompleteUser> searchUserProfileAutoComplete(@RequestParam String query, @ApiIgnore Pageable pageable) {
        return userService.searchProfile(query, pageable);
    }

    @PreAuthorize(LocalConstants.CAN_VIEW_DECISION_MAKER_GROUP)
    @GetMapping("v1/decisionmakers")
    @ApiOperation(value = "API for LOA_REVIEW to get Decision Maker Group", response = AutoCompleteUser.class, produces = Constants.JSON_MIME_UTF8, responseContainer = LocalConstants.LIST)
    public List<UserRole> getDecisionMakerGroup() {
        return userService.getDecisionMakerGroup();
    }
}
