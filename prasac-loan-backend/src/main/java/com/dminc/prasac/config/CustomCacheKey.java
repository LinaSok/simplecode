package com.dminc.prasac.config;

import java.io.Serializable;

import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class CustomCacheKey implements Serializable {

    public static final CustomCacheKey EMPTY = new CustomCacheKey();

    private final Object[] params;

    public CustomCacheKey(Object... elements) {
        Assert.notNull(elements, "null value");
        this.params = new Object[elements.length];
        System.arraycopy(elements, 0, this.params, 0, elements.length);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " [" + StringUtils.arrayToCommaDelimitedString(this.params) + "]";
    }
}