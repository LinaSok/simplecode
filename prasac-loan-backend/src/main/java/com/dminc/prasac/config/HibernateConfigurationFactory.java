package com.dminc.prasac.config;

import com.dminc.prasac.model.misc.LocalConstants;
import lombok.Builder;
import lombok.Getter;
import org.springframework.core.env.Environment;

import java.util.Properties;

@Builder
@Getter
public class HibernateConfigurationFactory {

    @Builder.Default
    private static final String INSTANCE_NAME = "hazelcast:hibernateInstanceName";

    public Properties getProperties(Environment env) {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", "validate");
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MariaDBDialect");
        properties.setProperty("hibernate.connection.autocommit", LocalConstants.FALSE);
        properties.setProperty("hibernate.jdbc.batch_size", "200");
        properties.setProperty("hibernate.globally_quoted_identifiers", LocalConstants.TRUE);
        properties.setProperty("hibernate.current_session_context_class", "thread");
        properties.setProperty("hibernate.show_sql", env.getProperty("hibernate.show_sql", LocalConstants.FALSE));
        properties.setProperty("hibernate.generate_statistics", env.getProperty("hibernate.generate_statistics", LocalConstants.FALSE));
        return properties;
    }
}
