package com.dminc.prasac.config;

import com.dminc.common.tools.RestJsonExceptionResolver;
import com.dminc.common.tools.email.SpringJavaSenderImpl;
import com.dminc.common.tools.filter.BasicAuthenticationFilter;
import com.dminc.common.tools.filter.CORSConfig;
import com.dminc.common.tools.filter.LogRequestIDFilter;
import com.dminc.common.tools.filter.TracerRequestFilter;
import com.dminc.prasac.security.RestTemplateHeaderModifierInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.HibernateValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.beanvalidation.SpringConstraintValidatorFactory;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.Filter;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Collections;
import java.util.List;

@Configuration
@Slf4j
public class AppConfig implements WebMvcConfigurer {

    public static final String API_PATH = "/api/*";
    @Autowired
    private Environment env;

    @Value("${crossdomain.urls}")
    private String crossDomainUrls;

    @Value("${prasac.client.service.x-authorization}")
    private String xauthorization;

    @Value("${prasac.client.service.connection-timeout}")
    private int connectionTimeout;

    @Value("${prasac.client.service.read-timeout}")
    private int readTimeout;

    @Override
    public void configureHandlerExceptionResolvers(final List<HandlerExceptionResolver> exceptionResolvers) {
        exceptionResolvers.add(restJsonExceptionResolver());
    }

    private String getProperty(final String key) {
        return env.getProperty(key);
    }

    @Bean
    public RestJsonExceptionResolver restJsonExceptionResolver() {
        final RestJsonExceptionResolver bean = new RestJsonExceptionResolver();
        RestJsonExceptionResolver.registerExceptionWithHTTPCode(org.springframework.beans.TypeMismatchException.class, 400);
        RestJsonExceptionResolver.registerExceptionWithHTTPCode(MissingServletRequestParameterException.class, 400);
        RestJsonExceptionResolver.registerExceptionWithHTTPCode(MethodArgumentNotValidException.class, 400);
        RestJsonExceptionResolver.registerExceptionWithHTTPCode(ServletRequestBindingException.class, 400);
        RestJsonExceptionResolver.registerExceptionWithHTTPCode(AccessDeniedException.class, 403);

        bean.setOrder(1);

        bean.setDiagnosticsDisabled(Boolean.parseBoolean(getProperty("json.diagnosticsDisabled")));
        // set general error message
        RestJsonExceptionResolver.setCustomMessage(getProperty("json.errormsg"));

        return bean;
    }

    @Bean()
    public FilterRegistrationBean logRequestIdFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new LogRequestIDFilter());
        registration.addUrlPatterns(API_PATH);
        registration.setOrder(1);
        return registration;
    }

    @Bean
    public FilterRegistrationBean requestDumperFilter() {
        final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setName("tracingLogfilter");
        final Filter filter = new TracerRequestFilter();
        registrationBean.setFilter(filter);
        registrationBean.addUrlPatterns(API_PATH);
        registrationBean.setOrder(2);
        return registrationBean;
    }

    @Bean
    public RestTemplate restTemplate() {
        final RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(connectionTimeout);
        factory.setReadTimeout(readTimeout);
        restTemplate.setRequestFactory(factory);
        restTemplate.setInterceptors(Collections.singletonList(new RestTemplateHeaderModifierInterceptor(xauthorization)));
        return restTemplate;
    }

    @Bean()
    public FilterRegistrationBean basicAuthenticationFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        final Filter filter = new BasicAuthenticationFilter();
        registration.addInitParameter("username", env.getProperty("spring.security.username"));
        registration.addInitParameter("secret", env.getProperty("spring.security.password"));
        registration.setFilter(filter);
        registration.addUrlPatterns("/swagger-ui.html");
        registration.addUrlPatterns("/actuator/*");
        registration.setOrder(4);

        return registration;
    }

    @Bean
    public FilterRegistrationBean corsFilterRegistrationBean() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setName("corsfilter");
        registrationBean.addUrlPatterns(API_PATH);
        CORSConfig corsFilter = new CORSConfig(crossDomainUrls);
        registrationBean.setFilter(corsFilter);
        return registrationBean;
    }

    @Bean
    public SpringJavaSenderImpl springEmailManager() {
        return new SpringJavaSenderImpl(getProperty("mail.host"), env.getProperty("mail.port", Integer.class, 25),
                getProperty("mail.username"), getProperty("mail.password"), getProperty("mail.protocol"),
                env.getProperty("mail.smtp.auth.enable", Boolean.class, false),
                env.getProperty("mail.smtp.starttls.enable", Boolean.class, false),
                env.getProperty("mail.debug.enable", Boolean.class, false));
    }

    @Bean
    public Validator validator(final AutowireCapableBeanFactory autowireCapableBeanFactory) {
        ValidatorFactory validatorFactory = Validation.byProvider(HibernateValidator.class).configure()
                .constraintValidatorFactory(new SpringConstraintValidatorFactory(autowireCapableBeanFactory))
                .buildValidatorFactory();

        return validatorFactory.getValidator();
    }
}
