package com.dminc.prasac.config;

import java.util.concurrent.Executor;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import lombok.extern.slf4j.Slf4j;

@Configuration
@EnableAsync(proxyTargetClass = true)
@EnableScheduling
@Slf4j
public class AsyncConfig implements AsyncConfigurer {

    @Bean
    public ThreadPoolTaskScheduler taskScheduler() {

        log.info("Setting up thread pool task scheduler with 10 threads.");
        final ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(10);
        scheduler.setThreadNamePrefix("task-");
        scheduler.setAwaitTerminationSeconds(600); // 10 minutes
        scheduler.setWaitForTasksToCompleteOnShutdown(true);
        scheduler.setErrorHandler(t -> log.error("Unknown error occurred while executing task.", t));
        scheduler.setRejectedExecutionHandler((r, e) -> log.error(String.format("Execution of task %s was rejected for unknown reasons.", r)));
        return scheduler;
    }

    @Override
    public Executor getAsyncExecutor() {
        final Executor executor = this.taskScheduler();
        log.info("Configuring asynchronous method executor {}.", executor);
        return executor;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return new SimpleAsyncUncaughtExceptionHandler();
    }
}
