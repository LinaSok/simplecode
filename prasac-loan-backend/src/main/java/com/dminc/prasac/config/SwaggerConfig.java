package com.dminc.prasac.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.OAuth;
import springfox.documentation.service.ResourceOwnerPasswordCredentialsGrant;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Value("${security.oauth2.client.clientId}")
    private String clientId;

    @Value("${security.oauth2.client.clientSecret}")
    private String clientSecret;
    private List<AuthorizationScope> authorizationScopeList;

    @PostConstruct
    public void init() {
        authorizationScopeList = new ArrayList<>();
        authorizationScopeList.add(new AuthorizationScope("read", "read all"));
        authorizationScopeList.add(new AuthorizationScope("trust", "trust all"));
        authorizationScopeList.add(new AuthorizationScope("write", "access all"));
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .build()
                .apiInfo(apiInfo())
                .securitySchemes(Collections.singletonList(securitySchema()))
                .securityContexts(Collections.singletonList(securityContext()))
                .consumes(getConsumes());
    }

    private Set<String> getConsumes() {
        Set<String> consumes = new HashSet<>();
        consumes.add("application/json");
        consumes.add("application/x-www-form-urlencoded");
        return consumes;
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Prasac Loan Api Info",
                "For login API <b>/api/oauth/token</b>"
                        + "\n Please send "
                        + "\n\t- Basic Authentication ( clientId = "
                        + clientId
                        + ", clientSecret = "
                        + clientSecret
                        + " )"
                        + "\n\t- Parameters(in <b>form data</b> format)"
                        + "\n* grant_type=password "
                        + "\n* username"
                        + "\n* password\n\n"
                        + "For refresh toke API <b>/api/oauth/token</b>"
                        + "\n Please send "
                        + "\n\t- Basic Authentication ( clientId = "
                        + clientId
                        + ", clientSecret = "
                        + clientSecret
                        + " )"
                        + "\n\t- Parameters(in <b>form data</b> format)"
                        + "\n* grant_type=refresh_token "
                        + "\n* refresh_token",
                "",
                "",
                null,
                "", "", Collections.emptyList());
    }

    private OAuth securitySchema() {


        return new OAuth("oauth2schema", authorizationScopeList,
                Collections.singletonList(new ResourceOwnerPasswordCredentialsGrant("/api/oauth/token")));

    }

    private SecurityContext securityContext() {
        return SecurityContext.builder().securityReferences(defaultAuth()).forPaths(PathSelectors.any())
                .build();
    }

    private List<SecurityReference> defaultAuth() {
        return Collections.singletonList(new SecurityReference("oauth2schema", authorizationScopeList.toArray(new AuthorizationScope[authorizationScopeList.size()])));

    }

    @Bean
    public SecurityConfiguration securityInfo() {
        return SecurityConfigurationBuilder.builder()
                .clientId(clientId)
                .clientSecret(clientSecret)
                .scopeSeparator(" ")
                .build();
    }
}
