package com.dminc.prasac.config;

import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class LiquiBaseConfig {

    @Autowired
    private DataSource datasource;

    @Value("${liquibase.changeLog}")
    private String changeLogPath;

    @Bean(name = "liquibase")
    public SpringLiquibase getLiquiBaseBean() {
        SpringLiquibase bean = new SpringLiquibase();
        bean.setDataSource(datasource);
        bean.setChangeLog(changeLogPath);
        return bean;
    }
}
