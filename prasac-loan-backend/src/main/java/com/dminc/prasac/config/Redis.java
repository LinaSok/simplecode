package com.dminc.prasac.config;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class Redis {
    private final RedisTemplate<String, Object> redisTemplate;

    public void put(String key, Object object) {
        redisTemplate.opsForValue().set(key, object);
    }

    public void put(String key, Object object, long timeout, TimeUnit timeUnit) {
        redisTemplate.opsForValue().set(key, object, timeout, timeUnit);
    }

    public Object get(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    public Object get(String key, long start, long end) {
        return redisTemplate.opsForValue().get(key, start, end);
    }

    @SuppressFBWarnings("NP_NULL_ON_SOME_PATH_FROM_RETURN_VALUE")
    public boolean hasKey(String key) {
        return redisTemplate.hasKey(key);
    }
}
