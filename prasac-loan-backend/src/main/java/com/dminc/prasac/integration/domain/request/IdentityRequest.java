package com.dminc.prasac.integration.domain.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IdentityRequest {
    private String identityNumber;
    private String identityType;
}
