package com.dminc.prasac.integration.service;

import com.dminc.prasac.integration.domain.SavingAccount;
import com.dminc.prasac.model.entity.client.ClientEntity;
import com.dminc.prasac.model.entity.selection.CurrencyEntity;

import java.io.IOException;
import java.util.List;

public interface SavingAccountService {

    SavingAccount create(ClientEntity client, List<ClientEntity> joiners, CurrencyEntity currency) throws IOException;
}
