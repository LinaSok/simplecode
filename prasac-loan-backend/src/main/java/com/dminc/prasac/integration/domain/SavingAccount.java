package com.dminc.prasac.integration.domain;

import com.dminc.prasac.integration.domain.request.Address;
import com.dminc.prasac.integration.domain.request.ClientRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class SavingAccount implements Serializable {
    private static final long serialVersionUID = -8924025428986929777L;

    private String customerNo;
    private String currencyCode;
    private String accountClassCode;
    private String accountDesc; // Borrower full name
    private String branchCode;
    private Address address;
    private String accountNumber;
    private List<ClientRequest> clients; // Co-borrower
}
