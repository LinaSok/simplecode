package com.dminc.prasac.integration.domain.cbc.response;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;

@XmlRootElement(name = "PROVIDED")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class ConsumerInfo {

    @XmlElement(name = "PCNAM", type = ConsumerName.class)
    private ConsumerName name;

    @XmlElement(name = "PCDOB")
    @XmlJavaTypeAdapter(DateAdapter.class)
    private LocalDate dob;

    @XmlElement(name = "PCPLB", type = PlaceOfBirth.class)
    private PlaceOfBirth placeOfBirth;

    @XmlElement(name = "PCGND")
    private String gender;

    @XmlElement(name = "PCMAR")
    private String maritalStatusCode;

    @XmlElement(name = "PCNAT")
    private String nationalityCode;
}
