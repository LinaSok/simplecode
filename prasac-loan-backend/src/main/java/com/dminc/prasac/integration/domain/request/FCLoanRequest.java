package com.dminc.prasac.integration.domain.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings({"PMD.TooManyFields"})
public class FCLoanRequest {

    private String loanNumber;
    private String branchCode;
    private String currencyCode;
    private Double loanAmount;
    private Integer tenure;
    private List<ClientRequest> clients;
    private Double interestRate;
    private String staffId;
    private String loanPurposeCode;
    private String repaymentModeCode;
    private Integer lockInPeriod;
    private String cbcScoring;
    private Double netIncome;
    private String collateralPoolCode;
    private LocalDate loanRequestDate;
    private Double loanFee;
}
