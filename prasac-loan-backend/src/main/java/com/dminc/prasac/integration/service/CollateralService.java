package com.dminc.prasac.integration.service;

import com.dminc.prasac.model.entity.LoanCollateralEntity;
import com.dminc.prasac.model.entity.client.ClientEntity;

public interface CollateralService {

    void create(LoanCollateralEntity entity, ClientEntity borrower, String currencyCode, String branchCode);
}
