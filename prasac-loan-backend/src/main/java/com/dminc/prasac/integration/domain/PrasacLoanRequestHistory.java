package com.dminc.prasac.integration.domain;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PrasacLoanRequestHistory implements Serializable {

    private String identityNumber;

    private String clientCif;

    private String clientType;

    private String fullName;

    private String khmerFullname;

    private LocalDate dob;

    private String identityType;

    private String address;

    private String maritalStatus;

    private Integer no;

    private LocalDate requestDate;

    private Double requestAmount;

    private String currency;

    private String requestStatus;

    private String rejectReason;

}
