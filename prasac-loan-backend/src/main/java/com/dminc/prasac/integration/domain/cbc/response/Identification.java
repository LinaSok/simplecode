package com.dminc.prasac.integration.domain.cbc.response;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;

@XmlRootElement(name = "CID")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Identification {

    @XmlElement(name = "CID1")
    private String idType;

    @XmlElement(name = "CID2")
    private String idNumber;

    @XmlElement(name = "CID3")
    @XmlJavaTypeAdapter(DateAdapter.class)
    private LocalDate expiredDate;
}
