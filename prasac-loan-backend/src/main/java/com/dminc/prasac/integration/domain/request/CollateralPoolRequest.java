package com.dminc.prasac.integration.domain.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CollateralPoolRequest implements Serializable {
    private static final long serialVersionUID = -1046886877797179782L;

    private String collateralPoolCode;

    /**
     * 432
     */
    private String branchCode;
    /**
     * USD
     */
    private String currencyCode;
    /**
     * 1731
     */
    private List<String> collateralCodes;

    private String clientCif;

}
