package com.dminc.prasac.integration.domain;


import java.io.Serializable;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings({"PMD.TooManyFields"})
public class PrasacLoanHistory implements Serializable {

    private String accountNumber;
    private String clientCif;
    private String requestStatus;
    private String clientType;
    private LocalDate disbursementDate;
    private Double loanFee;
    private String currencyType;
    private Double interestRate;
    private Integer loanTerm;
    private Integer usedTerm;
    private String loanStatus;
    private Integer lockInPeriod;
    private String repaymentMode;
    private Integer lateInstallment;
    private String loanClassification;
    private Double paidOffAmount;
    private String loanPurpose;
    private Integer totalCollateral;
    private String collateralType;
    private String prasacScoring;
    private String cbcScoring;
    private String otherApplicants;
    private Integer loanCycle;
    private String loanPurposeCode;
    private String repaymentModeCode;
    private Integer aging;
    private String multipleLoanCode;
    private String multipleLoan;
    private String otherApplicants1;
    private String no;
}
