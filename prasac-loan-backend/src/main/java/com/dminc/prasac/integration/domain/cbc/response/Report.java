package com.dminc.prasac.integration.domain.cbc.response;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.util.List;

@XmlRootElement(name = "RSP_REPORT")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Report {

    @XmlElement(name = "ENQUIRY_TYPE")
    private String enquiryType;

    @XmlElement(name = "REPORT_DATE")
    @XmlJavaTypeAdapter(DateAdapter.class)
    private LocalDate reportDate;

    @XmlElement(name = "ENQUIRY_NO")
    private String enquiryNo;

    @XmlElement(name = "PRODUCT_TYPE")
    private String purposeCode;

    @XmlElement(name = "NO_OF_APPLICANTS")
    private Integer numberOfApplicants;

    @XmlElement(name = "ACCOUNT_TYPE")
    private String accountType;

    @XmlElement(name = "ENQUIRY_REFERENCE")
    private String enquiryRef;

    @XmlElement(name = "AMOUNT")
    private Double amount;

    @XmlElement(name = "CURRENCY")
    private String currencyCode;

    @XmlElement(name = "CONSUMER", type = Consumer.class)
    private List<Consumer> consumers;
}
