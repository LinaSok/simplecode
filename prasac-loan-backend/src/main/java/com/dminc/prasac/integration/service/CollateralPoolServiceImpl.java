package com.dminc.prasac.integration.service;

import com.dminc.common.tools.exceptions.InternalBusinessException;
import com.dminc.prasac.integration.domain.request.CollateralPoolRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class CollateralPoolServiceImpl implements CollateralPoolService {
    private static final String URL = "/api/v1/collateralpool";
    private final RestTemplate template;
    private final ObjectMapper mapper;

    @Value("${prasac.client.service.url}")
    private String domain;

    @Override
    public CollateralPoolRequest create(List<String> collateralCodes, String branch, String clientCif, String currency) throws IOException {
        final String requestUrl = String.format("%s%s", domain, URL);
        final ResponseEntity<String> responseEntity = template.postForEntity(requestUrl, convert(collateralCodes, branch, clientCif, currency), String.class);
        if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            log.info("success {}", responseEntity.getBody());
            return mapper.readValue(responseEntity.getBody(), CollateralPoolRequest.class);
        }
        throw new InternalBusinessException(responseEntity.getBody());
    }

    private CollateralPoolRequest convert(List<String> collateralCodes, String branch, String clientCif, String currency) {
        return CollateralPoolRequest.builder()
                .branchCode(branch)
                .clientCif(clientCif)
                .collateralCodes(collateralCodes)
                .currencyCode(currency)
                .build();
    }
}
