package com.dminc.prasac.integration.domain.request;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings({"PMD.ImmutableField", "PMD.TooManyFields"})
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClientRequest implements Serializable {

    private static final long serialVersionUID = 186011941744943378L;
    private Long id;

    private String cif;

    private LocalDate dob;

    private String fullName;

    private Double incomeAmount;

    private String khmerFullName;

    private Double periodInCurrentAddress;

    private String phone;

    private Double workingPeriod;

    private String workplaceName;

    private String shortName;

    private String clientOccupation;

    private Integer wor;

    private String gender;

    private String maritalStatus;

    private String nationality;

    private Address birthAddress;

    private Address currentAddress;

    private Address workplaceAddress;

    private String branch;

    private String staffId;

    private String liabilityNo;

    private String clientType;

    private List<ClientIdentification> identifications;

    private String savingAccountNumber;
}
