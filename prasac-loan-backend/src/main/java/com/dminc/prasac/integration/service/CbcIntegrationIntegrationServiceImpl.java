package com.dminc.prasac.integration.service;

import com.dminc.common.tools.exceptions.InternalBusinessException;
import com.dminc.prasac.integration.domain.CbcResponse;
import com.dminc.prasac.integration.domain.request.Address;
import com.dminc.prasac.integration.domain.request.ClientIdentification;
import com.dminc.prasac.integration.domain.request.ClientRequest;
import com.dminc.prasac.integration.domain.request.LoanRequest;
import com.dminc.prasac.model.entity.client.ClientEntity;
import com.dminc.prasac.model.entity.client.ClientIdentificationEntity;
import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.service.ObjectsConverter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class CbcIntegrationIntegrationServiceImpl implements CbcIntegrationService {
    private static final String URL = "api/cbc";
    private final RestTemplate template;
    private final ObjectsConverter converter;
    private final ObjectMapper mapper;
    @Value("${prasac.client.service.url}")
    private String domain;

    @PostConstruct
    public void init() {
        mapper.registerModule(new JavaTimeModule());
    }

    @Override
    public CbcResponse getReport(LoanEntity loan) throws IOException {
        final LoanRequest loanRequest = convert(loan);
        final String requestUrl = String.format("%s/%s", domain, URL);
        final ResponseEntity<String> responseEntity = template.postForEntity(requestUrl, loanRequest, String.class);
        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            return mapper.readValue(responseEntity.getBody(), CbcResponse.class);
        }
        throw new InternalBusinessException(responseEntity.getBody());
    }

    private LoanRequest convert(LoanEntity loan) {
        final List<ClientRequest> clients = loan.getClientLoans().stream().map(clientLoan -> {
            final ClientRequest client = convert(clientLoan.getClient());
            client.setClientType(clientLoan.getClientType().getCode());
            return client;
        }).collect(Collectors.toList());

        return LoanRequest.builder()
                .amount(loan.getLoanAmount())
                .currency(loan.getCurrency().getCode())
                .classCode(loan.getCurrency().getClassCode())
                .purposeCode(loan.getProjects().get(0).getLoanPurpose().getCbcCode())
                .clients(clients)
                .build();
    }

    public ClientRequest convert(ClientEntity entity) {
        final ClientRequest clientRequest = converter.getObjectsMapper().map(entity, ClientRequest.class);
        clientRequest.setClientOccupation(entity.getClientOccupation() == null ? null : entity.getClientOccupation().getCbcCode());
        clientRequest.setNationality(entity.getNationality() == null ? null : entity.getNationality().getCbcCode());
        clientRequest.setMaritalStatus(entity.getMaritalStatus() == null ? null : entity.getMaritalStatus().getCode());
        clientRequest.setIdentifications(convert(entity.getIdentifications()));
        clientRequest.setBirthAddress(Address.asAddress(null, null, null, entity.getBirthVillage()));
        clientRequest.setCurrentAddress(Address.asAddress(entity.getCurrentAddressHouseNo(), entity.getCurrentAddressGroupNo(), entity.getCurrentAddressStreet(), entity.getCurrentVillage()));
        clientRequest.setWorkplaceAddress(Address.asAddress(entity.getWorkplaceHouseNo(), entity.getWorkplaceGroupNo(), entity.getWorkplaceStreet(), entity.getWorkplaceVillage()));
        return clientRequest;
    }

    public List<ClientIdentification> convert(List<ClientIdentificationEntity> entities) {
        return entities.stream().map(ClientIdentification::asClientIdentification).collect(Collectors.toList());
    }
}
