package com.dminc.prasac.integration.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CbcScore implements Serializable {
    private static final long serialVersionUID = -4909027089100224878L;
    private String scoreNum;

    private String scoreCard;

    private String scoreIndex;

    private Integer odds;

    private Double badRate;
}
