package com.dminc.prasac.integration.service;

import com.dminc.common.tools.exceptions.InternalBusinessException;
import com.dminc.prasac.integration.domain.request.CollateralRequest;
import com.dminc.prasac.model.entity.LoanCollateralEntity;
import com.dminc.prasac.model.entity.client.ClientEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class CollateralServiceImpl implements CollateralService {
    private static final String URL = "/api/v1/collateral";
    private final RestTemplate template;
    private final ObjectMapper mapper;

    @Value("${prasac.client.service.url}")
    private String domain;

    @Override
    public void create(LoanCollateralEntity entity, ClientEntity borrower, String currencyCode, String branchCode) {
        final String requestUrl = String.format("%s%s", domain, URL);

        final ResponseEntity<String> responseEntity = template.postForEntity(requestUrl, convertCollateral(entity, borrower, currencyCode, branchCode), String.class);
        if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            log.info("success");
        } else {
            throw new InternalBusinessException(responseEntity.getBody());
        }
    }

    private CollateralRequest convertCollateral(LoanCollateralEntity entity, ClientEntity borrower, String currencyCode, String branchCode) {
        return CollateralRequest.builder()
                .branchCode(branchCode)
                .clientCif(borrower.getCif())
                .collateralCode(entity.getCollateralTitleDeedNo())
                .liabId(borrower.getLiabilityNo())
                .currencyCode(currencyCode)
                .value(calculateValue(entity))
                .build();
    }

    private Double calculateValue(LoanCollateralEntity entity) {
        return entity.getMarketValueAnalyses().stream().mapToDouble(e -> e.getLandPrice() * e.getSize()).sum();
    }
}
