package com.dminc.prasac.integration.domain;

import com.dminc.prasac.model.dto.misc.Gender;
import com.dminc.prasac.model.dto.misc.SearchResultClient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Locale;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("PMD.TooManyFields")
public class CrmClient implements Serializable {

    private static final long serialVersionUID = 4605086547325395322L;

    private String cif;

    private String fullName;

    private String street;

    private String village;

    private String commune;

    private String district;

    private String province;

    private String identityType;

    private String identityNumber;

    private LocalDate dob;

    private String phone;

    private String country;

    private String nationality;

    private String khmerFullName;

    private String villageCode;

    private String communeCode;

    private String districtCode;

    private String provinceCode;

    private String gender;

    private String branch;

    private String maritalStatus;

    private String shortName;

    public SearchResultClient toClient() {
        return SearchResultClient.builder()

                .cif(cif)
                .fullName(fullName)
                .dob(dob)
                .branch(branch)
                .khmerFullName(khmerFullName)
                .phone(phone)
                .idNumber(identityNumber)
                .gender(getClientGender())
                .address(getAddress())
                .idType(identityType)
                .rawMaritalStatus(maritalStatus)
                .build();
    }

    private String getAddress() {
        return String.format("%s, %s %s %s %s", street, village, commune, district, province);
    }

    private Gender getClientGender() {
        if (StringUtils.isEmpty(gender)) {
            return null;
        }
        switch (StringUtils.upperCase(gender, Locale.ENGLISH)) {
            case "F":
                return Gender.FEMALE;
            case "M":
                return Gender.MALE;
            default:
                return null;
        }
    }
}
