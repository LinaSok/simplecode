package com.dminc.prasac.integration.domain.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoanRequest implements Serializable {
    private static final long serialVersionUID = 640153662989909229L;

    private Double amount;

    private String currency;

    private String purposeCode;

    private String classCode;

    private List<ClientRequest> clients;
}
