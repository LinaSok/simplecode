package com.dminc.prasac.integration.domain.request;

import com.dminc.prasac.model.entity.selection.VillageEntity;
import com.dminc.prasac.util.PrasacHelper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Address implements Serializable {

    private static final long serialVersionUID = -8791004788666481449L;
    private String groupNo;
    private String houseNo;
    private String streetNo;
    private String village;
    private String commune;
    private String district;
    private String province;
    private String country;
    private String countryCbcCode;
    private String villageCode;
    private String communeCode;
    private String districtCode;
    private String provinceCode;

    public static Address asAddress(String houseNo, Integer groupNo, String streetNo, VillageEntity villageEntity) {
        if (villageEntity != null) {
            return Address.builder()
                    .houseNo(houseNo)
                    .groupNo(String.valueOf(groupNo))
                    .streetNo(streetNo)
                    .village(villageEntity.getName())
                    .villageCode(PrasacHelper.format(villageEntity.getCode(), 2))
                    .commune(villageEntity.getCommune().getName())
                    .communeCode(PrasacHelper.format(villageEntity.getCommune().getCode(), 2))
                    .district(villageEntity.getCommune().getDistrict().getName())
                    .districtCode(PrasacHelper.format(villageEntity.getCommune().getDistrict().getCode(), 2))
                    .province(villageEntity.getCommune().getDistrict().getProvince().getName())
                    .provinceCode(PrasacHelper.format(villageEntity.getCommune().getDistrict().getProvince().getCode(), 2))
                    .country(villageEntity.getCommune().getDistrict().getProvince().getCountry().getCode())
                    .countryCbcCode(villageEntity.getCommune().getDistrict().getProvince().getCountry().getCbcCode())
                    .build();
        }
        return null;
    }
}
