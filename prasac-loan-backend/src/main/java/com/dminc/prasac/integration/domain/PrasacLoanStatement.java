package com.dminc.prasac.integration.domain;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings({"PMD.TooManyFields"})
@JsonIgnoreProperties(ignoreUnknown = true)

public class PrasacLoanStatement implements Serializable {

    //"BRANCH_CODE": "434",
    @JsonAlias("BRANCH_CODE")
    private String branchCode;
    //"BRANCH": "434",
    @JsonAlias("BRANCH")
    @JsonProperty("branch")
    private String branch;

    //"REPORT_DATE": "2018-12-26T00:00:00.000+0000",
    @JsonAlias("REPORT_DATE")
    private Date reportDate;

    //"LOAN_ACCOUNT": "434-7202-0001048",
    @JsonAlias("LOAN_ACCOUNT")
    private String loanAccount;

    //"CUSTOMER_NO": "00085055",
    @JsonAlias("CUSTOMER_NO")
    private String customerNo;

    //"CURRENCY": "USD",
    @JsonAlias("CURRENCY")
    private String currency;

    //"VALUE_DATE": "13-Jun-17",
    @JsonAlias("VALUE_DATE")
    private String valueDate;

    //"INT_RATE": 18,
    @JsonAlias("INT_RATE")
    private Long intRate;

    //"PNL_RATE": 24,
    @JsonAlias("PNL_RATE")
    private Long pnlRate;

    //"MT_DATE": "11-Oct-19",
    @JsonAlias("MT_DATE")
    private String mtDate;

    //"DISB_DATE": "11-Apr-17",
    @JsonAlias("DISB_DATE")
    private String disbDate;

    //"EVENT_SEQ_NO": 53,
    @JsonAlias("EVENT_SEQ_NO")
    private Long eventSeqNo;

    //"DISB_AMOUNT": 0,
    @JsonAlias("DISB_AMOUNT")
    private Double disbAmount;

    //"PRINCIPAL": 67,
    @JsonAlias("PRINCIPAL")
    private Double principal;

    //"INTEREST": 30.96,
    @JsonAlias("INTEREST")
    private Double interest;

    //"PENALTY": 0,
    @JsonAlias("PENALTY")
    private Double penalty;
    //"BALANCE": 1866,
    @JsonAlias("BALANCE")
    private Double balance;
    //"FULL_NAME": "Sov Samath",
    @JsonAlias("FULL_NAME")
    private String fullName;
    //"ADDRESS": "Pouthi Mitt / Leuk Daek / Kaoh Thum / Kandal"
    @JsonAlias("ADDRESS")
    private String address;
}
