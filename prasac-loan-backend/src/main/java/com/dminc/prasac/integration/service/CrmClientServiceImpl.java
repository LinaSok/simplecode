package com.dminc.prasac.integration.service;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.common.tools.exceptions.InternalBusinessException;
import com.dminc.prasac.integration.domain.CrmClient;
import com.dminc.prasac.integration.domain.request.Address;
import com.dminc.prasac.integration.domain.request.ClientIdentification;
import com.dminc.prasac.integration.domain.request.ClientRequest;
import com.dminc.prasac.model.entity.client.ClientEntity;
import com.dminc.prasac.model.entity.client.ClientIdentificationEntity;
import com.dminc.prasac.model.request.SearchClientRequest;
import com.dminc.prasac.service.BusinessError;
import com.dminc.prasac.service.ObjectsConverter;
import com.dminc.prasac.service.selection.ClientIdentificationTypeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class CrmClientServiceImpl implements CrmClientService {

    private static final String SERVICE_PATH = "/api/v1/clients";
    private static final String SEARCH_PATH = "/search?";
    private static final String SEARCH_BY_IDENTITY_PATH = "/searchByIdentity?";
    private final RestTemplate restTemplate;
    private final ClientIdentificationTypeService idTypeService;
    private final ObjectsConverter converter;
    private final ObjectMapper objectMapper;
    @Value("${prasac.client.service.url}")
    private String domain;

    @Override
    public List<CrmClient> searchClient(SearchClientRequest request) {
        Map<String, Object> params = new HashMap<>();
        // if cif is sent, ignore other params
        if (StringUtils.isNotEmpty(request.getClientCif())) {
            params.putIfAbsent("cif", request.getClientCif());
        } else {
            params.putIfAbsent("name", request.getName());
            params.putIfAbsent("khmerName", request.getKhmerName());
            params.putIfAbsent("dob", request.getDob());
            params.putIfAbsent("idNumber", request.getIdNumber());
        }

        final String requestUrl = String
                .format("%s%s%s%s", domain, SERVICE_PATH, SEARCH_PATH, generateParamsUrl(params.keySet()));
        try {
            final ResponseEntity<List<CrmClient>> exchange = restTemplate.exchange(requestUrl, HttpMethod.GET, null, new ParameterizedTypeReference<List<CrmClient>>() {
            }, params);
            return exchange.getBody();
        } catch (HttpClientErrorException | ResourceAccessException e) {
            BusinessError.CAN_NOT_CONNECT_TO_ORACLE.throwExceptionHttpStatus500();
            return null;
        }
    }

    @Override
    public Optional<CrmClient> getByCif(String cif) {
        final String requestUrl = String.format("%s%s/%s", domain, SERVICE_PATH, cif);

        try {
            return Optional.ofNullable(restTemplate.getForObject(requestUrl, CrmClient.class));
        } catch (HttpClientErrorException e) {
            if (e.getRawStatusCode() == HttpStatus.NOT_FOUND.value()) {
                return Optional.empty();
            } else {
                BusinessError.CAN_NOT_CONNECT_TO_ORACLE.throwExceptionHttpStatus500();
                return Optional.empty();
            }
        } catch (ResourceAccessException e) {
            BusinessError.CAN_NOT_CONNECT_TO_ORACLE.throwExceptionHttpStatus500();
            return Optional.empty();
        }
    }

    public Optional<CrmClient> searchByIdentity(String idNumber, Long identificationType) throws BusinessException {
        Map<String, Object> params = new HashMap<>();
        params.putIfAbsent("identityNumber", idNumber);
        params.putIfAbsent("identityType", idTypeService.getById(identificationType).getCode());
        final String requestUrl = String
                .format("%s%s%s%s", domain, SERVICE_PATH, SEARCH_BY_IDENTITY_PATH, generateParamsUrl(params.keySet()));

        try {
            final CrmClient crmClient = restTemplate.getForObject(requestUrl, CrmClient.class, params);
            return Objects.nonNull(crmClient) && StringUtils.isNotBlank(crmClient.getCif()) ? Optional.of(crmClient) : Optional.empty();
        } catch (HttpClientErrorException e) {
            if (e.getRawStatusCode() == HttpStatus.NOT_FOUND.value()) {
                return Optional.empty();
            } else {
                BusinessError.CAN_NOT_CONNECT_TO_ORACLE.throwExceptionHttpStatus500();
                return Optional.empty();
            }
        } catch (ResourceAccessException e) {
            BusinessError.CAN_NOT_CONNECT_TO_ORACLE.throwExceptionHttpStatus500();
            return Optional.empty();
        }
    }

    @Override
    public boolean exist(String cif) {
        return getByCif(cif).isPresent();
    }

    @Override
    public ClientEntity create(ClientEntity clientEntity, String staffId) throws IOException {
        final ClientRequest clientRequest = convert(clientEntity, staffId);
        final String requestUrl = String.format("%s%s", domain, SERVICE_PATH);
        final ResponseEntity<String> responseEntity = restTemplate.postForEntity(requestUrl, clientRequest, String.class);
        if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            final ClientRequest response = objectMapper.readValue(responseEntity.getBody(), ClientRequest.class);
            clientEntity.setShortName(response.getShortName());
            clientEntity.setCif(response.getCif());
            clientEntity.setLiabilityNo(response.getLiabilityNo());
            return clientEntity;
        }
        throw new InternalBusinessException(responseEntity.getBody());
    }

    public ClientRequest convert(ClientEntity entity, String staffId) {
        final ClientRequest clientRequest = converter.getObjectsMapper().map(entity, ClientRequest.class);
        clientRequest.setBranch(entity.getBranch().getCode());
        clientRequest.setStaffId(staffId);
        clientRequest.setClientOccupation(entity.getClientOccupation() == null ? null : entity.getClientOccupation().getCode());
        clientRequest.setNationality(entity.getNationality() == null ? null : entity.getNationality().getCode());
        clientRequest.setMaritalStatus(entity.getMaritalStatus() == null ? null : entity.getMaritalStatus().getCode());
        clientRequest.setIdentifications(convert(entity.getIdentifications()));
        clientRequest.setBirthAddress(Address.asAddress(null, null, null, entity.getBirthVillage()));
        clientRequest.setCurrentAddress(Address.asAddress(entity.getCurrentAddressHouseNo(), entity.getCurrentAddressGroupNo(), entity.getCurrentAddressStreet(), entity.getCurrentVillage()));
        clientRequest.setWorkplaceAddress(Address.asAddress(entity.getWorkplaceHouseNo(), entity.getWorkplaceGroupNo(), entity.getWorkplaceStreet(), entity.getWorkplaceVillage()));
        return clientRequest;
    }

    public List<ClientIdentification> convert(List<ClientIdentificationEntity> entities) {
        return entities.stream().map(ClientIdentification::asClientIdentification).collect(Collectors.toList());
    }

    private String generateParamsUrl(Set<String> paramsKey) {
        return paramsKey.stream().map(key -> String.format("%s={%s}", key, key)).collect(Collectors.joining("&"));
    }
}
