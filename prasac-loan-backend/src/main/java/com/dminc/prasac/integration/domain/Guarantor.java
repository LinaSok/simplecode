package com.dminc.prasac.integration.domain;

import com.dminc.prasac.util.LoanStatusHelper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Guarantor implements Serializable {

    private static final long serialVersionUID = 7286561906042993565L;
    private String creditor;

    private String purposeCode;

    private String currencyCode;

    private Double disbursementAmount;

    private LocalDate disbursementDate;

    private LocalDate maturityDate;

    private String status;

    private LocalDate closeDate;

    private Double outstanding;

    private String payment;

    public String getFullStatusLabel() {
        return LoanStatusHelper.getLoanStatusLabel(status);
    }
}
