package com.dminc.prasac.integration.domain.cbc.response;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "ITEM")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Item {

    @XmlElement(name = "ENQUIRY_REFERENCE")
    private String enquiryRef;

    @XmlElement(name = "RSP_REPORT", type = Report.class)
    private Report report;

    @XmlElement(name = "ERROR", type = Error.class)
    private List<Error> errors;

    @XmlElement(name = "NO_ERRORS")
    private int numberOfError;
}
