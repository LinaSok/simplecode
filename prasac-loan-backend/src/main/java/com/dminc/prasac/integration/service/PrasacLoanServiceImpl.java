package com.dminc.prasac.integration.service;

import com.dminc.common.tools.exceptions.InternalBusinessException;
import com.dminc.prasac.integration.domain.CbcReport;
import com.dminc.prasac.integration.domain.PrasacLoanHistory;
import com.dminc.prasac.integration.domain.PrasacLoanHistoryInfo;
import com.dminc.prasac.integration.domain.PrasacLoanRepaymentSchedule;
import com.dminc.prasac.integration.domain.PrasacLoanRequestHistory;
import com.dminc.prasac.integration.domain.PrasacLoanStatement;
import com.dminc.prasac.integration.domain.request.ClientRequest;
import com.dminc.prasac.integration.domain.request.FCLoanRequest;
import com.dminc.prasac.model.entity.FinancialStatementIncomeOperatingExpenseEntity;
import com.dminc.prasac.model.entity.client.ClientEntity;
import com.dminc.prasac.model.entity.loan.ClientLoan;
import com.dminc.prasac.model.entity.loan.LoanEntity;
import com.dminc.prasac.model.entity.selection.ClientTypeEntity;
import com.dminc.prasac.service.BusinessError;
import com.dminc.prasac.service.CbcService;
import com.dminc.prasac.util.PrasacHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class PrasacLoanServiceImpl implements PrasacLoanService {

    private static final String SERVICE_PATH = "/api/v1/loan";
    private static final String GET_BY_CLIENT_CIF = "/history/";
    private static final String CREATE_LOAN_PATH = "/fc";
    private static final String GET_REQUEST_HISTORY_BY_CLIENT_CIF = "/request-history/";
    private static final String GET_LOAN_HISTORY_INFO = "/history-info?clientCifs";
    private static final String GET_LOAN_STATEMENT = "/statement?clientCif";
    private static final String GET_LOAN_REPAYMENT_SCHEDULE = "/repayment-schedule?clientCif";
    private final RestTemplate restTemplate;
    private final CbcService cbcService;
    private final ObjectMapper mapper;
    @Value("${prasac.client.service.url}")
    private String domain;

    @Override
    public List<PrasacLoanHistory> getLoanHistoriesByClientCif(String clientCif) {
        final String requestUrl = String.format("%s%s%s%s", domain, SERVICE_PATH, GET_BY_CLIENT_CIF, clientCif);
        try {
            return getResponseAsList(requestUrl, new ParameterizedTypeReference<List<PrasacLoanHistory>>() {
            });
        } catch (HttpClientErrorException | ResourceAccessException e) {
            BusinessError.CAN_NOT_CONNECT_TO_ORACLE.throwExceptionHttpStatus500();
            return Collections.emptyList();
        }
    }

    @Override
    @Cacheable(value = "getLoanRequestHistoryByClientCif", key = "#clientCif")
    public List<PrasacLoanRequestHistory> getLoanRequestHistoryByClientCif(String clientCif) {
        final String requestUrl = String.format("%s%s%s%s", domain, SERVICE_PATH, GET_REQUEST_HISTORY_BY_CLIENT_CIF, clientCif);
        try {
            return getResponseAsList(requestUrl, new ParameterizedTypeReference<List<PrasacLoanRequestHistory>>() {
            });
        } catch (HttpClientErrorException | ResourceAccessException e) {
            BusinessError.CAN_NOT_CONNECT_TO_ORACLE.throwExceptionHttpStatus500();
            return Collections.emptyList();
        }
    }

    @Override
    public List<PrasacLoanHistoryInfo> getLoanHistoriesInfoByClients(List<String> clientCifs) {
        final String requestUrl = String.format("%s%s%s=%s", domain, SERVICE_PATH, GET_LOAN_HISTORY_INFO, StringUtils.join(clientCifs, ","));
        try {
            return getResponseAsList(requestUrl, new ParameterizedTypeReference<List<PrasacLoanHistoryInfo>>() {
            });
        } catch (HttpClientErrorException | ResourceAccessException e) {
            BusinessError.CAN_NOT_CONNECT_TO_ORACLE.throwExceptionHttpStatus500();
            return Collections.emptyList();
        }
    }

    @Override
    @Cacheable(value = "getLoanStatementByClientCif", key = "#clientCif")
    public List<PrasacLoanStatement> getLoanStatementByClientCif(String clientCif) {
        final String requestUrl = String.format("%s%s%s=%s", domain, SERVICE_PATH, GET_LOAN_STATEMENT, clientCif);
        try {
            return getResponseAsList(requestUrl, new ParameterizedTypeReference<List<PrasacLoanStatement>>() {
            });
        } catch (HttpClientErrorException | ResourceAccessException e) {
            BusinessError.CAN_NOT_CONNECT_TO_ORACLE.throwExceptionHttpStatus500();
            return Collections.emptyList();
        }
    }

    @Cacheable(value = "getLoanRepyamentsSchedule", key = "#clientCif")
    public List<PrasacLoanRepaymentSchedule> getLoanRepaymentsSchedule(String clientCif) {

        final String requestUrl = String.format("%s%s%s=%s", domain, SERVICE_PATH, GET_LOAN_REPAYMENT_SCHEDULE, clientCif);
        try {
            final List<PrasacLoanRepaymentSchedule> prasacLoanRepaymentsSchedule = getResponseAsList(requestUrl, new ParameterizedTypeReference<List<PrasacLoanRepaymentSchedule>>() {
            });
            prasacLoanRepaymentsSchedule.sort(Comparator.comparing(PrasacLoanRepaymentSchedule::getNoInst));

            return prasacLoanRepaymentsSchedule;
        } catch (HttpClientErrorException | ResourceAccessException e) {
            BusinessError.CAN_NOT_CONNECT_TO_ORACLE.throwExceptionHttpStatus500();
            return Collections.emptyList();
        }
    }

    @Override
    public FCLoanRequest createLoan(LoanEntity loanEntity) throws IOException {
        final FCLoanRequest fcLoanRequest = convertLoan(loanEntity);
        final String requestUrl = String.format("%s%s%s", domain, SERVICE_PATH, CREATE_LOAN_PATH);
        final ResponseEntity<String> responseEntity = restTemplate.postForEntity(requestUrl, fcLoanRequest, String.class);
        if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            log.info(responseEntity.getBody());
            return mapper.readValue(responseEntity.getBody(), FCLoanRequest.class);
        }
        throw new InternalBusinessException(responseEntity.getBody());
    }

    private String getCbcScore(long loanId) throws IOException {
        final CbcReport cbcReport = cbcService.cbcReport(loanId);
        return cbcReport.getClients().get(0).getScore().getScoreNum();
    }

    private FCLoanRequest convertLoan(LoanEntity loanEntity) throws IOException {
        final ClientEntity borrower = loanEntity.getClientLoans().stream()
                .filter(clientLoan -> ClientTypeEntity.TYPE_BORROWER_ID.equals(clientLoan.getClientType().getId()))
                .findFirst()
                .get()
                .getClient();
        return FCLoanRequest.builder()
                .branchCode(loanEntity.getBranch().getCode())
                .currencyCode(loanEntity.getCurrency().getCode())
                .loanAmount(loanEntity.getLoanAmount())
                .tenure(loanEntity.getTenure())
                .interestRate(loanEntity.getMonthlyInterestRate())
                .staffId(loanEntity.getAssignedUser().getUserProfile().getStaffId())
                .loanPurposeCode(loanEntity.getProjects().get(0).getLoanPurpose().getCode())
                .repaymentModeCode(loanEntity.getRepaymentMode().getCode())
                .lockInPeriod(loanEntity.getLockInPeriod())
                .clients(convertClient(loanEntity.getClientLoans()))
                .collateralPoolCode(borrower.getCollateralPoolCode())
                .cbcScoring(getCbcScore(loanEntity.getId()))
                .loanRequestDate(loanEntity.getRequestDate().toLocalDate())
                .loanFee(loanEntity.getLoanFee())
                .netIncome(calculateNetIncome(loanEntity))
                .build();
    }

    /**
     * Calculation formation:
     * Total current - Cost of Goods sold - Total Expense - Tax Expense - Depreciation expense
     */
    private Double calculateNetIncome(LoanEntity loanEntity) {
        Double totalIncome = loanEntity.getBusinesses().stream().mapToDouble(buss -> {
            final Double currentMonth = buss.getFinancialStatementIncomeRevenue().getCurrentMonth();
            return currentMonth - (currentMonth * (buss.getFinancialStatementIncomeRevenue().getCostOfGoodsSold() / 100));
        }).sum();
        final FinancialStatementIncomeOperatingExpenseEntity expense = loanEntity.getFinancialStatementIncome().getFinancialStatementIncomeOperatingExpense();
        final double totalExpense = PrasacHelper.getTotal(expense.getCurrentMonthAdministrativeExpense(),
                expense.getCurrentMonthDepreciationExpense(),
                expense.getCurrentMonthFamilyExpense(),
                expense.getCurrentMonthMaintenanceExpense(),
                expense.getCurrentMonthOthersExpense(),
                expense.getCurrentMonthRentalExpense(),
                expense.getCurrentMonthSalaryExpense(),
                expense.getCurrentMonthUtilityExpense());
        totalIncome -= totalExpense;
        totalIncome -= totalIncome * (loanEntity.getFinancialStatementIncome().getTaxExpenses() / 100);
        totalIncome -= loanEntity.getFinancialStatementIncome().getFinancialStatementIncomeOperatingExpense().getCurrentMonthDepreciationExpense();
        return totalIncome;
    }

    private List<ClientRequest> convertClient(List<ClientLoan> clientLoans) {
        return clientLoans.stream().map(clientLoan -> ClientRequest.builder()
                .cif(clientLoan.getClient().getCif())
                .clientType(clientLoan.getClientType().getCode())
                .savingAccountNumber(clientLoan.getClient().getSavingAccountNumber())
                .gender(clientLoan.getClient().getGender().toString())
                .fullName(clientLoan.getClient().getFullName())
                .build())
                .collect(Collectors.toList());
    }

    private <T> List<T> getResponseAsList(String requestUrl, ParameterizedTypeReference ref) {
        final ResponseEntity<List<T>> exchange = restTemplate.exchange(requestUrl, HttpMethod.GET, null, ref);
        return exchange.getBody();
    }
}
