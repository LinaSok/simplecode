package com.dminc.prasac.integration.service;

import com.dminc.prasac.integration.domain.request.CollateralPoolRequest;

import java.io.IOException;
import java.util.List;

public interface CollateralPoolService {

    CollateralPoolRequest create(List<String> collateralCode, String branch, String clientCif, String currency) throws IOException;
}
