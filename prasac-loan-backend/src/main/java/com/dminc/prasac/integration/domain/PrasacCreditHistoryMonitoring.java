package com.dminc.prasac.integration.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PrasacCreditHistoryMonitoring implements Serializable {

    private String clientCif;

    private LocalDate enqDate;

    private LocalDate date;

    private String event;

    private String currency;

    private Double amount;

    private String lender;

    private String reference;
}
