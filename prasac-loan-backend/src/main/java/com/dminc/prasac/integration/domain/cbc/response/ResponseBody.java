package com.dminc.prasac.integration.domain.cbc.response;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "MESSAGE")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class ResponseBody {

    @XmlElement(name = "ITEM", type = Item.class)
    private Item item;

}
