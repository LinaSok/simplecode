package com.dminc.prasac.integration.service;

import com.dminc.prasac.integration.domain.PrasacLoanHistory;
import com.dminc.prasac.integration.domain.PrasacLoanHistoryInfo;
import com.dminc.prasac.integration.domain.PrasacLoanRepaymentSchedule;
import com.dminc.prasac.integration.domain.PrasacLoanRequestHistory;
import com.dminc.prasac.integration.domain.PrasacLoanStatement;
import com.dminc.prasac.integration.domain.request.FCLoanRequest;
import com.dminc.prasac.model.entity.loan.LoanEntity;

import java.io.IOException;
import java.util.List;

public interface PrasacLoanService {

    List<PrasacLoanHistory> getLoanHistoriesByClientCif(String userCif);

    List<PrasacLoanRequestHistory> getLoanRequestHistoryByClientCif(String clientCif);

    List<PrasacLoanHistoryInfo> getLoanHistoriesInfoByClients(List<String> clientCifs);

    List<PrasacLoanStatement> getLoanStatementByClientCif(String clientCif);

    List<PrasacLoanRepaymentSchedule> getLoanRepaymentsSchedule(String clientCif);

    FCLoanRequest createLoan(LoanEntity loanEntity) throws IOException;

}
