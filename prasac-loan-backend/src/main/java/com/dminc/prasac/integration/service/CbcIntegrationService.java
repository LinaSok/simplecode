package com.dminc.prasac.integration.service;

import com.dminc.prasac.integration.domain.CbcResponse;
import com.dminc.prasac.model.entity.loan.LoanEntity;

import java.io.IOException;

public interface CbcIntegrationService {
    CbcResponse getReport(LoanEntity request) throws IOException;
}
