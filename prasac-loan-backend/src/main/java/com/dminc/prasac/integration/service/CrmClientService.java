package com.dminc.prasac.integration.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import com.dminc.common.tools.exceptions.BusinessException;
import com.dminc.prasac.integration.domain.CrmClient;
import com.dminc.prasac.model.entity.client.ClientEntity;
import com.dminc.prasac.model.request.SearchClientRequest;

public interface CrmClientService {
    List<CrmClient> searchClient(SearchClientRequest request) throws BusinessException;

    Optional<CrmClient> getByCif(String cif);

    Optional<CrmClient> searchByIdentity(String idNumber, Long identificationType) throws BusinessException;

    boolean exist(String cif);

    ClientEntity create(ClientEntity clientEntity, String staffId) throws IOException;

}
