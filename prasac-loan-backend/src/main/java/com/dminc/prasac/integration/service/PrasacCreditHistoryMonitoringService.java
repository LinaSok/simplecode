package com.dminc.prasac.integration.service;

import com.dminc.prasac.integration.domain.PrasacCreditHistoryMonitoring;

import java.util.List;

public interface PrasacCreditHistoryMonitoringService {

    List<PrasacCreditHistoryMonitoring> getCreditHistoryMonitoringsByClient(String clientCif);
}
