package com.dminc.prasac.integration.domain.cbc.response;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(namespace = "ERROR")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Error {

    @XmlElement(name = "CONSUMER_SEQ")
    private Integer consumerSeq;

    @XmlElement(name = "RSP_MSG")
    private String message;

    @XmlElement(name = "DATA")
    private String data;
}
