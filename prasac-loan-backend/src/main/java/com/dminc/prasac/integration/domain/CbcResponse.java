package com.dminc.prasac.integration.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CbcResponse implements Serializable {
    private static final long serialVersionUID = -6177139945707655670L;
    private String responseXML;
    private String requestXML;
}
