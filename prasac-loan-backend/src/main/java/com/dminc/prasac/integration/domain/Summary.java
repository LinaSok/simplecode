package com.dminc.prasac.integration.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "SUMMARY")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@SuppressWarnings("PMD.TooManyFields")
public class Summary implements Serializable {
    @XmlElement(name = "CNT_ACC")
    private int totalAccountNumber;

    @XmlElement(name = "CNT_ACC_NORM")
    private int totalNormalAccountNumber;

    @XmlElement(name = "CNT_ACC_DELQ")
    private int totalDelinquentAccountNumber;

    @XmlElement(name = "CNT_ACC_CLO")
    private int totalClosedAccountNumber;

    @XmlElement(name = "CNT_ACC_WRO")
    private int totalWriteOffAccountNumber;

    @XmlElement(name = "CNT_GACC")
    private int totalGuarantorNumber;

    @XmlElement(name = "CNT_GACC_NORM")
    private int totalNormalGuarantorNumber;

    @XmlElement(name = "CNT_GACC_DELQ")
    private int totalDelinquentGuarantorNumber;

    @XmlElement(name = "CNT_GACC_ACC_CLO")
    private int totalClosedGuarantorNumber;

    @XmlElement(name = "CNT_GACC_WRO")
    private int totalWriteOffGuarantorNumber;

    @XmlElement(name = "CNT_PE")
    private int totalPreviousEnquiryNumber;

    @XmlElement(name = "CNT_MTDE")
    private int totalPreviousEnquiryIn30daysNumber;

}
