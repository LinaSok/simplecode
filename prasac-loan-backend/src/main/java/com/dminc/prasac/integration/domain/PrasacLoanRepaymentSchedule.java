package com.dminc.prasac.integration.domain;

import java.io.Serializable;
import java.util.Locale;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings({"PMD.TooManyFields"})
@JsonIgnoreProperties(ignoreUnknown = true)

public class PrasacLoanRepaymentSchedule implements Serializable {

    //"LOAN_NUMBER": "434-7202-0001048",
    @JsonAlias("LOAN_NUMBER")
    private String loanNumber;

    //"BRANCH_CODE": "434",
    @JsonAlias("BRANCH_CODE")
    private String branchCode;

    // "BOOK_DATE": "11-04-17",
    @JsonAlias("BOOK_DATE")
    private String bookDate;
    //"CUSTOMER_ID": "00085055",
    @JsonAlias("CUSTOMER_ID")
    private String customerId;
    //"VALUE_DATE": "11-04-17",
    @JsonAlias("VALUE_DATE")
    private String valueDate;
    //"MATURITY_DATE": "11-10-19",
    @JsonAlias("MATURITY_DATE")
    private String maturityDate;
    //"AMOUNT_FINANCED": 2000,
    @JsonAlias("AMOUNT_FINANCED")
    private Double amountFinanced;

    //"CURRENCY": "USD",
    @JsonAlias("CURRENCY")
    private String currency;
    //"CUSTOMER_NAME1": "Sov Samath",
    @JsonAlias("CUSTOMER_NAME1")
    private String customerName1;

    //"CUSTOMER_NAME_LEADER": null,
    @JsonAlias("CUSTOMER_NAME_LEADER")
    private String customerNameLeader;
    //"KHMER_FULL_NAME": "សូវ  សំអាត",
    @JsonAlias("KHMER_FULL_NAME")
    private String khmerFullName;
    //"TELEPHONE": null,
    @JsonAlias("TELEPHONE")
    private String telephone;
    //"ADDRESS_LINE1": "N/A",
    @JsonAlias("ADDRESS_LINE1")
    private String addressLine1;
    //"ADDRESS_LINE5": "Pouthi Mitt",
    @JsonAlias("ADDRESS_LINE5")
    private String addressLine5;
    //"ADDRESS_LINE4": "Leuk Daek",
    @JsonAlias("ADDRESS_LINE4")
    private String addressLine4;

    //"ADDRESS_LINE3": "Kaoh Thum",
    @JsonAlias("ADDRESS_LINE3")
    private String addressLine3;
    //"ADDRESS_LINE2": "Kandal",
    @JsonAlias("ADDRESS_LINE2")
    private String addressLine2;
    //"CO_CODE": "8916",
    @JsonAlias("CO_CODE")
    private String coCode;
    //"CREDIT_OFFICER": "នុន ឆៃលី",
    @JsonAlias("CREDIT_OFFICER")
    private String creditOfficer;

    //"COLLATERAL": "លិខិតបញ្ជាក់ដី/ផ្ទះ និង មានអ្នកធានា",
    @JsonAlias("COLLATERAL")
    private String collateral;

    //"PURPOSE": "កែលំអរផ្ទះ-ជួសជុល សាងសង់ផ្ទះបន្ថែម សង់បន្ទប់ទឹក សាងសង់សួនច្បារ, ជួសជុល ឬធ្វើរបងផ្ទះ",
    @JsonAlias("PURPOSE")
    private String purpose;
    //"DR_PROD_AC": "000850550037",
    @JsonAlias("DR_PROD_AC")
    private String drProdAC;
    //"CR_PROD_AC": "000850550037",
    @JsonAlias("CR_PROD_AC")
    private Long crProdAC;
    //"LEN_DR": 12,
    @JsonAlias("LEN_DR")
    private Long lenDr;
    //"LEN_CR": 12,
    @JsonAlias("LEN_CR")
    private String lenCr;
    // "INT_RATE": 18,
    @JsonAlias("INT_RATE")
    private Long intRate;
    //"PNL_ORATE": 24,
    @JsonAlias("PNL_ORATE")
    private Long pnlOrate;
    //"LOAN_CYCLE": 6,
    @JsonAlias("LOAN_CYCLE")
    private Long loanCycle;
    //"MIN_LN_TERM": 12,
    @JsonAlias("MIN_LN_TERM")
    private Long minLnTerm;
    //"C_AC": "00085055-003-7",
    @JsonAlias("C_AC")
    private String cac;
    //"ACCOUNT_NUMBER": "43472020001048",
    @JsonAlias("ACCOUNT_NUMBER")
    private String accountNumber;
    //"NO_INST": 1,
    @JsonAlias("NO_INST")
    private Long noInst;
    //"DUE_DATE": "11-05-17",
    @JsonAlias("DUE_DATE")
    private String dueDate;
    //"DY": "THU",
    @JsonAlias("DY")
    private String dy;

    //"NO_DATE": 30,
    @JsonAlias("NO_DATE")
    private Integer noDate;
    //"BEG_BALANCE": 2000,
    @JsonAlias("BEG_BALANCE")
    private Double begBalance;
    //"PRIN_DUE": 67,
    @JsonAlias("PRIN_DUE")
    private Long prinDue;
    //"INT_DUE": 30,
    @JsonAlias("INT_DUE")
    private Long intDue;
    //"TOT_DUE": 97,
    @JsonAlias("TOT_DUE")
    private Long totDue;
    //"END_BAL": 1933,
    @JsonAlias("END_BAL")
    private Long endBal;
    //"CBC_FEE": 2.86,
    @JsonAlias("CBC_FEE")
    private Double cbcFee;
    //"FC_KH": "434_កំពង់កុង",
    @JsonAlias("FC_KH")
    private String fckh;
    //"LOV_DESC_EN": "Nun Chaily",
    @JsonAlias("LOV_DESC_EN")
    private String lovDescEn;
    //"LOAN_FEE": 31,
    @JsonAlias("LOAN_FEE")
    private Double loanFee;
    //"CUST_AC": "00085055-003-7",
    @JsonAlias("CUST_AC")
    private String custAc;
    //"CUST_1": "00085055-003-7",
    @JsonAlias("CUST_1")
    private String cust1;
    //"ACC_LINKED": "NO"
    @JsonAlias("ACC_LINKED")
    private String accLinked;

    public String getDy() {
        String khmerWeekDay;
        switch (dy.toUpperCase(Locale.ENGLISH)) {
            case "MON":
                khmerWeekDay = "ចន្ទ";
                break;
            case "TUE":
                khmerWeekDay = "អង្គារ";
                break;
            case "WED":
                khmerWeekDay = "ពុធ";
                break;
            case "THU":
                khmerWeekDay = "ព្រហ";
                break;
            case "FRI":
                khmerWeekDay = "សុក្រ";
                break;
            case "SAT":
                khmerWeekDay = "សៅរ៍";
                break;
            case "SUN":
                khmerWeekDay = "អាទិត្យ";
                break;
            default:
                khmerWeekDay = "";
                break;
        }

        return khmerWeekDay;
    }

    public String getCurrency() {
        String currencyInKhmer;
        switch (currency.toUpperCase(Locale.ENGLISH)) {
            case "USD":
                currencyInKhmer = "ដុល្លារ";
                break;
            case "KHR":
                currencyInKhmer = "រៀល";
                break;
            case "THB":
                currencyInKhmer = "បាត";
                break;
            default:
                currencyInKhmer = "";
                break;
        }

        return currencyInKhmer;
    }
}
