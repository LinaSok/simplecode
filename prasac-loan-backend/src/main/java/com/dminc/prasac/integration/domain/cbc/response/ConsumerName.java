package com.dminc.prasac.integration.domain.cbc.response;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "PCNAM")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class ConsumerName {

    @XmlElement(name = "PCNMFA")
    private String khmerFirstName;

    @XmlElement(name = "PCNM1A")
    private String khmerLastName;

    @XmlElement(name = "PCNMFE")
    private String firstName;

    @XmlElement(name = "PCNM1E")
    private String lastName;

}
