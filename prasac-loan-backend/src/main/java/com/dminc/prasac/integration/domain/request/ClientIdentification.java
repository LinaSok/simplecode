package com.dminc.prasac.integration.domain.request;

import com.dminc.prasac.model.entity.client.ClientIdentificationEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClientIdentification implements Serializable {

    public static final long IDENTIFICATION_PASSPORT_TYPE_ID = 5L;

    private static final long serialVersionUID = -1761733874731713556L;

    private Long id;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate expiryDate;

    private String idNumber;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate issueDate;

    private String clientIdentificationType;

    private String clientIdentificationCbc;

    private Long clientIdentificationTypeId;

    public static ClientIdentification asClientIdentification(ClientIdentificationEntity entity) {
        if (entity != null) {
            return ClientIdentification.builder()
                    .id(entity.getId())
                    .expiryDate(entity.getExpiryDate())
                    .issueDate(entity.getIssueDate())
                    .idNumber(entity.getIdNumber())
                    .clientIdentificationCbc(entity.getClientIdentificationType().getCbcCode())
                    .clientIdentificationType(entity.getClientIdentificationType().getCode())
                    .clientIdentificationTypeId(entity.getClientIdentificationType().getId())
                    .build();
        }
        return null;
    }
}
