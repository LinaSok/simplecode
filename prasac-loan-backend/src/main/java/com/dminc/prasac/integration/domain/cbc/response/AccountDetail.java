package com.dminc.prasac.integration.domain.cbc.response;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;

@XmlRootElement(name = "ACC_DETAIL")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class AccountDetail {

    @XmlElement(name = "ACC_CRDTR")
    private String creditor;

    @XmlElement(name = "ACC_PRD")
    private String purposeCode;

    @XmlElement(name = "ACC_CURR")
    private String currencyCode;

    @XmlElement(name = "ACC_LIMIT")
    private Double disbursementAmount;

    @XmlElement(name = "ACC_ISSU_DT")
    @XmlJavaTypeAdapter(DateAdapter.class)
    private LocalDate disbursementDate;

    @XmlElement(name = "ACC_PROD_EXP_DT")
    @XmlJavaTypeAdapter(DateAdapter.class)
    private LocalDate maturityDate;

    @XmlElement(name = "ACC_STATUS")
    private String status;

    @XmlElement(name = "ACC_CLSD_DT")
    @XmlJavaTypeAdapter(DateAdapter.class)
    private LocalDate closeDate;

    @XmlElement(name = "ACC_SEC")
    private String securityType;

    @XmlElement(name = "ACC_CUB")
    private String outstanding;

    @XmlElement(name = "ACC_LAST_AMT_PD")
    private Double lastAMTPaid;

    @XmlElement(name = "ACC_SUMMRY")
    private String payment;
}
