package com.dminc.prasac.integration.domain.cbc.response;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "PREV_ENQUIRIES")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PreviousEnquiryWrapper {

    @XmlElement(name = "PREV_ENQUIRY", type = PreviousEnquiry.class)
    private List<PreviousEnquiry> enquiries;
}
