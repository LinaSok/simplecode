package com.dminc.prasac.integration.domain.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CollateralRequest implements Serializable {
    private static final long serialVersionUID = -1046886877797179782L;

    private String collateralCode;
    private String clientCif;
    private String currencyCode;
    private String liabId;
    private String branchCode;
    private Double value;
}
