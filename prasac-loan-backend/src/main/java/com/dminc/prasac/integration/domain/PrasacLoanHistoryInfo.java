package com.dminc.prasac.integration.domain;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@SuppressWarnings({"PMD.TooManyFields"})
public class PrasacLoanHistoryInfo implements Serializable {

    private String accountNumber;

    private String institutionName;

    private Integer loanCycle;

    private LocalDate disbursementDate;

    private LocalDate maturityDate;

    private String rawLoanPurpose;

    private String loanTypeCode;

    private Double loanLimit;

    private Double loanOutStanding;

    private String currencyCode;

    private Integer tenureMonthly;

    private Integer lengthOfUsing;

    private Integer lockInPeriodMonthly;

    private String interestRateMonthly;

    private String rawRepaymentMode;

    private String mortgagedCollateralNumber;

    private Double firstPrincipalRepayment;

    private Double monthlyInterestRepayment;

    private Integer numberOfLateDays;

    private Integer numberOfLateInstallments;

    private Double penaltyAmountForPaidOff;

    private String clientCif;

    private String loanPurposeCode;

    private String repaymentCode;

    private String loanStatus;
}
