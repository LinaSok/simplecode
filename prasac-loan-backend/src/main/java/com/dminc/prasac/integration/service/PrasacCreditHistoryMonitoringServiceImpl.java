package com.dminc.prasac.integration.service;

import com.dminc.prasac.integration.domain.PrasacCreditHistoryMonitoring;
import com.dminc.prasac.service.BusinessError;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PrasacCreditHistoryMonitoringServiceImpl implements PrasacCreditHistoryMonitoringService {

    private static final String SERVICE_PATH = "/api/v1/credit";
    private static final String HISTORY_MONITORING = "/history-monitoring/";
    private final RestTemplate restTemplate;
    @Value("${prasac.client.service.url}")
    private String domain;

    @Override
    public List<PrasacCreditHistoryMonitoring> getCreditHistoryMonitoringsByClient(String clientCif) {
        final String requestUrl = String.format("%s%s%s%s", domain, SERVICE_PATH, HISTORY_MONITORING, clientCif);
        try {
            final ResponseEntity<List<PrasacCreditHistoryMonitoring>> response = restTemplate.exchange(requestUrl, HttpMethod.GET, null, new ParameterizedTypeReference<List<PrasacCreditHistoryMonitoring>>() {
            });
            return response.getBody();
        } catch (HttpClientErrorException | ResourceAccessException e) {
            BusinessError.CAN_NOT_CONNECT_TO_ORACLE.throwExceptionHttpStatus500();
            return Collections.emptyList();
        }

    }
}
