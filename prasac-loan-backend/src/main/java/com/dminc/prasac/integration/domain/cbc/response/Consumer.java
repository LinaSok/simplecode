package com.dminc.prasac.integration.domain.cbc.response;

import com.dminc.prasac.integration.domain.Summary;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "CONSUMER")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Consumer {

    @XmlElement(name = "CAPL")
    private String clientType;

    @XmlElement(name = "CID", type = Identification.class)
    private Identification identification;

    @XmlElement(name = "PROVIDED", type = ConsumerInfo.class)
    private ConsumerInfo info;

    @XmlElement(name = "PREV_ENQUIRIES", type = PreviousEnquiryWrapper.class)
    private PreviousEnquiryWrapper enquiryWrapper;

    @XmlElement(name = "ACC_DETAILS", type = AccountDetailWrapper.class)
    private AccountDetailWrapper detailWrapper;

    @XmlElement(name = "GUARANTEED_ACCOUNTS", type = GuarantorDetailWrapper.class)
    private GuarantorDetailWrapper guarantorDetailWrapper;

    @XmlElement(name = "SCORE", type = Score.class)
    private Score score;

    @XmlElement(name = "SUMMARY", type = Summary.class)
    private Summary summary;
}
