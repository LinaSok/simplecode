package com.dminc.prasac.integration.domain;

import com.dminc.prasac.util.LoanStatusHelper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountDetail implements Serializable {

    private static final long serialVersionUID = 4926370144661260191L;
    private String creditor;

    private String purposeCode;

    private String currencyCode;

    private Double disbursementAmount;

    private LocalDate disbursementDate;

    private LocalDate maturityDate;

    private String status;

    private LocalDate closeDate;

    private String securityType;

    private String outstanding;

    private Double lastAMTPaid;

    private String payment;

    public String getFullStatusLabel() {
        return LoanStatusHelper.getLoanStatusLabel(status);
    }
}
