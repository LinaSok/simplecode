package com.dminc.prasac.integration.service;

import com.dminc.common.tools.exceptions.InternalBusinessException;
import com.dminc.prasac.integration.domain.SavingAccount;
import com.dminc.prasac.integration.domain.request.Address;
import com.dminc.prasac.integration.domain.request.ClientRequest;
import com.dminc.prasac.model.entity.client.ClientEntity;
import com.dminc.prasac.model.entity.selection.CurrencyEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class SavingAccountServiceImpl implements SavingAccountService {
    private static final String URL = "/api/v1/saving-account";
    private final RestTemplate template;
    private final ObjectMapper mapper;

    @Value("${prasac.client.service.url}")
    private String domain;

    @Override
    public SavingAccount create(ClientEntity client, List<ClientEntity> joiners, CurrencyEntity currency) throws IOException {
        final String requestUrl = String.format("%s%s", domain, URL);
        SavingAccount savingAccount = convertSavingAccount(client, currency);
        savingAccount.setClients(convertJoiners(joiners));
        final ResponseEntity<String> response = template.postForEntity(requestUrl, savingAccount, String.class);
        if (HttpStatus.OK.equals(response.getStatusCode())) {
            return mapper.readValue(response.getBody(), SavingAccount.class);
        }
        throw new InternalBusinessException(response.getBody());
    }

    private List<ClientRequest> convertJoiners(List<ClientEntity> joiners) {
        return joiners.stream().map(joiner ->
                ClientRequest.builder()
                        .cif(joiner.getCif())
                        .build()).collect(Collectors.toList());
    }

    private SavingAccount convertSavingAccount(ClientEntity client, CurrencyEntity currency) {
        return SavingAccount.builder()
                .branchCode(client.getBranch().getCode())
                .accountClassCode(currency.getClassCode())
                .currencyCode(currency.getCode())
                .customerNo(client.getCif())
                .accountDesc(client.getFullName())
                .address(Address.asAddress(client.getCurrentAddressHouseNo(),
                        client.getCurrentAddressGroupNo(),
                        client.getCurrentAddressStreet(),
                        client.getCurrentVillage()))
                .build();
    }
}
