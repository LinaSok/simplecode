package com.dminc.prasac.security;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OauthConfig {
    private ClientConfig client;
    private List<CustomOAuthMappings> mappings;
}
