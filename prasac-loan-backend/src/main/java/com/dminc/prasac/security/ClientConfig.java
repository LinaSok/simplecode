package com.dminc.prasac.security;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ClientConfig {
    private String clientId;
    private String clientSecret;
    private String[] grantTypes;
    private String[] scopes;
    private int accessTokenValidity;
    private int refreshTokenValidity;

}
