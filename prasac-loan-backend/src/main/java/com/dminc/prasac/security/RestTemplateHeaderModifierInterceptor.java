package com.dminc.prasac.security;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

public class RestTemplateHeaderModifierInterceptor implements ClientHttpRequestInterceptor {

    public static final String X_AUTHORIZATION = "X-Authorization";

    private final String xauthorization;

    public RestTemplateHeaderModifierInterceptor(String xauthorization) {
        this.xauthorization = xauthorization;
    }

    @Override
    public ClientHttpResponse intercept(
            HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
            throws IOException {
        HttpHeaders headers = request.getHeaders();
        headers.add(X_AUTHORIZATION, xauthorization);
        return execution.execute(request, body);
    }
}
