package com.dminc.prasac.security;

import java.io.IOException;
import java.util.Map;

import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.common.exceptions.OAuth2ExceptionJackson2Serializer;
import org.springframework.web.util.HtmlUtils;

import com.dminc.prasac.service.BusinessError;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;

public class CustomInvalidJackson2Serializer extends OAuth2ExceptionJackson2Serializer {

    @Override
    public void serialize(OAuth2Exception value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {
        jgen.writeStartObject();
        jgen.writeStringField("error", value.getOAuth2ErrorCode());

        CustomOAuth2Exception exception = (CustomOAuth2Exception) value;

        String errorMessage = value.getMessage();
        if (errorMessage != null) {
            errorMessage = HtmlUtils.htmlEscape(errorMessage);
        }
        jgen.writeStringField("message", errorMessage);
        jgen.writeNumberField("status", exception.getHttpErrorCode());
        jgen.writeNumberField("errorCode", BusinessError.INVALID_TOKEN.getCode());
        jgen.writeStringField("error_description", errorMessage);
        if (value.getAdditionalInformation() != null) {
            for (Map.Entry<String, String> entry : value.getAdditionalInformation().entrySet()) {
                String key = entry.getKey();
                String add = entry.getValue();
                jgen.writeStringField(key, add);
            }
        }
        jgen.writeEndObject();
    }

}
