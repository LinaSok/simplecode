package com.dminc.prasac.security;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import com.dminc.prasac.service.BusinessError;
import com.dminc.prasac.util.PrasacHelper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CustomJdbcTokenStore extends JdbcTokenStore {

    public CustomJdbcTokenStore(DataSource dataSource) {
        super(dataSource);
    }

    public OAuth2AccessToken readAccessToken(String tokenValue) {
        log.debug("=====readAccessToken -  {}", tokenValue);
        final OAuth2AccessToken accessToken = super.readAccessToken(tokenValue);
        //ignore the /api/oauth2/token login api - generate new token with old one is expired or gone
        final HttpServletRequest request = PrasacHelper.getCurrentRequest();
        if (accessToken == null && request != null && !"/api/oauth/token".equals(request.getRequestURI())) {
            throwError();
        }
        return accessToken;
    }

    @Override
    public OAuth2Authentication readAuthentication(String token) {
        log.debug("=====readAuthentication -  {}", token);
        final OAuth2Authentication authentication = super.readAuthentication(token);
        if (authentication == null) {
            throwError();
        }
        return authentication;
    }

    @Override
    public OAuth2RefreshToken readRefreshToken(String token) {
        log.debug("=====readRefreshToken -  {}", token);
        final OAuth2RefreshToken refreshToken = super.readRefreshToken(token);
        if (refreshToken == null) {
            BusinessError.INVALID_TOKEN.throwExceptionHttpStatus401();
        }
        return refreshToken;
    }

    private void throwError() {
        throw new CustomOAuth2Exception(BusinessError.INVALID_TOKEN.getMessage());
    }
}
