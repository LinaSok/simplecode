package com.dminc.prasac.security;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

public class MyAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e)
            throws IOException {
        request.setAttribute("exception", e);
        request.setAttribute("trace", e.getStackTrace());
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
                e.getMessage());

    }
}
