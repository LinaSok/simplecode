package com.dminc.prasac.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;

import lombok.extern.slf4j.Slf4j;

@Configuration
@EnableAuthorizationServer
@Slf4j
@SuppressWarnings("PMD.SignatureDeclareThrowsException")
public class OAuth2Configuration extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private SecurityConfig securityConfig;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    @Qualifier("userDetailsService")
    private UserDetailsService userDetailsService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private DataSource dataSource;

    @Bean
    public TokenStore tokenStore() {
        return new CustomJdbcTokenStore(dataSource);
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer) {
        oauthServer.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints.tokenStore(tokenStore()).authenticationManager(authenticationManager).userDetailsService(userDetailsService);
        //  .exceptionTranslator(webResponseExceptionTranslator());

        this.securityConfig.getOauth2().getMappings().forEach(mapping -> {
            endpoints.pathMapping(mapping.getOriginalMapping(), mapping.getCustomMapping());
        });
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        final OauthConfig oauth2config = this.securityConfig.getOauth2();

        clients.inMemory().withClient(oauth2config.getClient().getClientId())
                .authorizedGrantTypes(oauth2config.getClient().getGrantTypes()).scopes(oauth2config.getClient().getScopes())
                .autoApprove(true).secret(passwordEncoder.encode(oauth2config.getClient().getClientSecret()))
                .accessTokenValiditySeconds(oauth2config.getClient().getAccessTokenValidity())
                .refreshTokenValiditySeconds(oauth2config.getClient().getRefreshTokenValidity());
    }

//    private WebResponseExceptionTranslator webResponseExceptionTranslator() {
//        return new DefaultWebResponseExceptionTranslator() {
//
//            @Override
//            public ResponseEntity<OAuth2Exception> translate(Exception e) throws Exception {
//                final ResponseEntity<OAuth2Exception> responseEntity = super.translate(e);
//                final OAuth2Exception body = responseEntity.getBody();
//                final HttpStatus status = responseEntity.getStatusCode();
//                HttpStatus httpStatus = status;
//                if (body != null) {
//                    log.info("error- {}", body.getSummary());
//
//                    switch (status) {
//                        case BAD_REQUEST:
//                        case UNAUTHORIZED:
//                            httpStatus = HttpStatus.UNAUTHORIZED;
//                           // body.addAdditionalInformation("errorCode", "1");
//                          //  body.addAdditionalInformation("message", "The username or password is invalid");
//                            BusinessError.BAD_CREDENTIAL.throwException();
//
//                            break;
//                        case FORBIDDEN:
//                            httpStatus = HttpStatus.FORBIDDEN;
//                            body.addAdditionalInformation("status", "403");
//                            break;
//                        default:
//                            //do nothing
//                    }
//
//                   // body.addAdditionalInformation("status", String.valueOf(status.value()));
//                   // body.addAdditionalInformation("exception", e.toString());
//                    if (ACCOUNT_IS_LOCKED.equals(body.getMessage())) {
//                       // body.addAdditionalInformation("errorCode", "2");
//                       // body.addAdditionalInformation("message", "Your account is locked");
//                        BusinessError.ACCOUNT_LOCKED.throwException();
//                    }
//                    body.addAdditionalInformation("status", "401");
//                }
//
//
//                return new ResponseEntity<>(body, httpStatus);
//            }
//        };
//    }
}
