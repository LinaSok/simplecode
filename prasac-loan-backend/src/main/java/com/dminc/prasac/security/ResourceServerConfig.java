package com.dminc.prasac.security;

import com.dminc.common.tools.filter.MobileClientAuthenticationFilter;
import com.dminc.prasac.controller.rest.ClientController;
import com.dminc.prasac.controller.rest.DocumentController;
import com.dminc.prasac.controller.rest.LoanController;
import com.dminc.prasac.controller.rest.PrasacController;
import com.dminc.prasac.controller.rest.selection.CurrencyController;
import com.dminc.prasac.controller.rest.selection.LocationController;
import com.dminc.prasac.model.misc.LocalConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Slf4j
@Configuration
@EnableResourceServer
@SuppressWarnings("PMD.SignatureDeclareThrowsException")
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
    private static final String API_V1_LOCATIONS = LocalConstants.API_V1 + LocationController.API_LOCATIONS;
    private static final String API_V1_CLIENTS = LocalConstants.API_V1 + ClientController.API_CLIENTS;
    private static final String API_V1_DOCUMENTS = LocalConstants.API_V1 + DocumentController.API_DOCUMENTS;
    private static final String[] USER_ACCESS_ROLES = {"MOBILE_CLIENT", "USER", "ADMIN"};


    @Autowired
    private Environment env;

    private final AuthenticationEntryPoint myAuthencitatoinEntryPoint = new MyAuthenticationEntryPoint();

    @Bean
    public MobileClientAuthenticationFilter mobileClientAuthenticationFilter() {

        return new MobileClientAuthenticationFilter(env.getProperty("security.oauth2.client.clientId"), env.getProperty("security.oauth2.client.clientSecret"));
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .exceptionHandling()
                .authenticationEntryPoint(myAuthencitatoinEntryPoint)
                .and()
                .authorizeRequests()

                .antMatchers("swagger-ui.html", "api/swagger-ui.html", "/api", "/error").permitAll()

                .antMatchers(HttpMethod.GET, API_V1_DOCUMENTS + DocumentController.API_DOCUMENT_TYPES).hasAnyRole(USER_ACCESS_ROLES)
                .antMatchers(HttpMethod.GET, API_V1_DOCUMENTS + DocumentController.API_CLASSIFICATION_TYPES).hasAnyRole(USER_ACCESS_ROLES)

                .antMatchers(HttpMethod.GET, LocalConstants.API_V1 + CurrencyController.API_CURRENCIES).hasAnyRole(USER_ACCESS_ROLES)

                .antMatchers(HttpMethod.GET, API_V1_LOCATIONS + LocationController.API_CAMBODIA).hasAnyRole(USER_ACCESS_ROLES)
                .antMatchers(HttpMethod.GET, API_V1_LOCATIONS + "/country/**/provinces").hasAnyRole(USER_ACCESS_ROLES)
                //.antMatchers(HttpMethod.GET, API_V1_CLIENTS + ClientController.API_RELATIONSHIPS).hasAnyRole(USER_ACCESS_ROLES)
                .antMatchers(HttpMethod.GET, API_V1_CLIENTS + ClientController.API_RESIDENT_STATUSES).hasAnyRole(USER_ACCESS_ROLES)
                .antMatchers(HttpMethod.GET, API_V1_CLIENTS + ClientController.API_CLIENT_TYPES).hasAnyRole(USER_ACCESS_ROLES)
                .antMatchers(HttpMethod.GET, API_V1_CLIENTS + ClientController.API_WORKING_TIMES).hasAnyRole(USER_ACCESS_ROLES)
                .antMatchers(HttpMethod.GET, API_V1_CLIENTS + ClientController.API_MARITAL_STATUSES).hasAnyRole(USER_ACCESS_ROLES)
                .antMatchers(HttpMethod.GET, API_V1_CLIENTS + ClientController.API_NATIONALITIES).hasAnyRole(USER_ACCESS_ROLES)
                .antMatchers(HttpMethod.GET, API_V1_CLIENTS + ClientController.API_OCCUPATIONS).hasAnyRole(USER_ACCESS_ROLES)
                .antMatchers(HttpMethod.GET, API_V1_CLIENTS + ClientController.API_IDENTIFICATION_TYPES).hasAnyRole(USER_ACCESS_ROLES)
                .antMatchers(HttpMethod.GET, PrasacController.API_V1_PRASAC + PrasacController.API_BRANCHES).hasAnyRole(USER_ACCESS_ROLES)
                .antMatchers(HttpMethod.GET, PrasacController.API_V1_PRASAC + PrasacController.API_POSITIONS).hasAnyRole(USER_ACCESS_ROLES)
                .antMatchers(HttpMethod.GET, LocalConstants.API_V1 + LoanController.API_LOANS + LoanController.API_LOAN_STATUSES).hasAnyRole(USER_ACCESS_ROLES)

                .antMatchers("/api/**", "/clients/**").authenticated().and()
                .exceptionHandling().authenticationEntryPoint(myAuthencitatoinEntryPoint);

        http.addFilterBefore(mobileClientAuthenticationFilter(), BasicAuthenticationFilter.class);
    }


}