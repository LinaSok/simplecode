package com.dminc.prasac.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomOAuthMappings {
    private String originalMapping;
    private String customMapping;
}
