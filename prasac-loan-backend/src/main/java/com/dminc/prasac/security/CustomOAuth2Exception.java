package com.dminc.prasac.security;

import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.common.exceptions.OAuth2ExceptionJackson1Deserializer;
import org.springframework.security.oauth2.common.exceptions.OAuth2ExceptionJackson1Serializer;
import org.springframework.security.oauth2.common.exceptions.OAuth2ExceptionJackson2Deserializer;

@org.codehaus.jackson.map.annotate.JsonSerialize(using = OAuth2ExceptionJackson1Serializer.class)
@org.codehaus.jackson.map.annotate.JsonDeserialize(using = OAuth2ExceptionJackson1Deserializer.class)
@com.fasterxml.jackson.databind.annotation.JsonSerialize(using = CustomInvalidJackson2Serializer.class)
@com.fasterxml.jackson.databind.annotation.JsonDeserialize(using = OAuth2ExceptionJackson2Deserializer.class)
public class CustomOAuth2Exception extends InvalidTokenException {

    public CustomOAuth2Exception(String msg) {
        super(msg);
    }

}
