package com.dminc.prasac.security;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.dminc.prasac.model.entity.User;
import com.dminc.prasac.model.entity.UserLock;
import com.dminc.prasac.service.BusinessError;
import com.dminc.prasac.service.UserLockService;
import com.dminc.prasac.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CustomDaoAuthenticationProvider extends DaoAuthenticationProvider {

    private static final int MAX_ATTEMPT = 3;

    public static final Long LOCKED_PERIOD = 15L;

    public static final Long TIME_FRAME_PERIOD = 15L;

    @Autowired
    private UserLockService userLockService;

    @Autowired
    private UserService userService;

    CustomDaoAuthenticationProvider(final UserDetailsService userDetailsService, PasswordEncoder passwordEncoder) {
        super.setUserDetailsService(userDetailsService);
        super.setPasswordEncoder(passwordEncoder);
    }


    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        final String username = authentication.getPrincipal().toString();

        if (authentication.getCredentials() == null) {
            logger.debug("Authentication failed: no credentials provided");
            //throw new BadCredentialsException("Bad Credential");
            BusinessError.BAD_CREDENTIAL.throwExceptionHttpStatus401();
        }

        String presentedPassword = authentication.getCredentials().toString();

        if (!getPasswordEncoder().matches(presentedPassword, userDetails.getPassword())) {
            logger.debug("Authentication failed: password does not match stored value");
            this.lockingUser(username);
            //throw new BadCredentialsException("Username or password is invalid");
            BusinessError.BAD_CREDENTIAL.throwExceptionHttpStatus401();
        }

        //remove record if no error happened
        userLockService.unlockUser(username);

    }

    /**
     * it will just reach the {@link com.dminc.prasac.service.UserDetailsServiceImpl}
     */
    private void lockingUser(final String username) {
        log.debug("==== Start locking user ====");
        /**
         * If existing record and start > lock peroid. Reset attempt count to 0
         */
        UserLock lockedUser = userLockService.findLockedUser(username);
        Integer attemptCount = Optional.ofNullable(lockedUser.getAttemptCount()).orElse(0);

        //Not lock yet, check if the login timeframe is in 15min
        if (lockedUser.getCreatedDate() != null && LocalDateTime.now().compareTo(lockedUser.getCreatedDate().plusMinutes(TIME_FRAME_PERIOD)) > 0) {
            lockedUser.setCreatedDate(LocalDateTime.now());
            attemptCount = 0;
        }

        //If user already locked
        if (lockedUser.getStartLockedPeriod() != null
                && LocalDateTime.now().compareTo(lockedUser.getStartLockedPeriod().plusMinutes(LOCKED_PERIOD)) > 0) {
            //reset the lock count and period
            lockedUser.setStartLockedPeriod(null);
            attemptCount = 0;
        }

        if (attemptCount < MAX_ATTEMPT) {
            lockedUser.setAttemptCount(attemptCount + 1);
        } else {
            lockedUser.setStartLockedPeriod(LocalDateTime.now());
            //lock account
            User user = (User) getUserDetailsService().loadUserByUsername(username);
            user.setLocked(true);
            userService.updateUser(user);
        }
        if (lockedUser.getUsername() == null) {
            lockedUser.setUsername(username);
        }
        userLockService.saveLockedUser(lockedUser);
    }

}
