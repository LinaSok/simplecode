(function (exports) {
    "use strict";

    var apiPrefix = "/api/v1/";
    var staticJsonFilesCache = {};

    function getStaticJsonFile(filename, processingCallback) {
        if (typeof filename == 'object') {
            if (processingCallback) {
                return Promise.resolve(processingCallback(filename));
            } else {
                return Promise.resolve(filename);
            }
        }

        function processContent(content) {
            staticJsonFilesCache[filename] = JSON.parse(JSON.stringify(content)); // Store a clone in the cache before processing callback acts.
            if (processingCallback) {
                return processingCallback(content);
            } else {
                return content;
            }
        }

        var cachedValue = staticJsonFilesCache[filename];
        if (cachedValue) {
            return Promise.resolve(processContent(cachedValue));
        } else {
            return $.ajax("v1/" + filename + ".json").promise().then(processContent);
        }
    }

    function ajax(option) {
        $.extend(option, {
            beforeSend: function (request) {
                request.setRequestHeader("authorization", "Bearer " + accessToken);
            },

        });
        return $.ajax(option).fail(function (response) {
            if (response.status == 401) {
                throw new Error("Invalid credentials.");
            }
        });
        ;
    }

    //=============================

    function UserMgt() {
    }

    UserMgt.prototype = {
        login: function () {
            return $.ajax({
                url: "/api/oauth/token",
                method: 'post',
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", "Basic " + btoa($("#client_id").val() + ":" + $("#client_secret").val()));
                },
                data: {
                    grant_type: "password",
                    username: $("#username").val(),
                    password: $("#password").val()
                }
            }).promise();
        },
    };

    exports.UserMgt = UserMgt;

    //=============================

    function SelectionMgt() {
    }

    SelectionMgt.prototype = {
        getSelection: function () {
            return ajax({
                url: apiPrefix + "selections",
                method: 'get'
            }).promise();
        },
    };

    exports.SelectionMgt = SelectionMgt;

    //=============================

    function LoanMgt() {
    }

    LoanMgt.prototype = {
        submitPreScreenByCO: function (data) {
            return ajax({
                url: apiPrefix + "loans",
                method: "POST",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
            }).promise();
        },

        submitPreScreenByAdmin: function (data) {
            return ajax({
                url: apiPrefix + "loans/admin",
                method: "POST",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
            }).promise();
        },

        submitLoanAppraisal: function (data) {
            return ajax({
                url: apiPrefix + "loans/appraisal",
                method: "POST",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
            }).promise();
        },
    };

    exports.LoanMgt = LoanMgt;

    //=============================

    function DocumentMgt() {
    }

    $.extend(DocumentMgt, {
        baseApi: apiPrefix + "documents"
    });

    DocumentMgt.prototype = {
        uploadLoanDoc: function (data) {
            return ajax({
                url: DocumentMgt.baseApi + "/loan",
                method: "POST",
                cache: false,
                contentType: false,
                processData: false,
                data: data,
            }).promise();
        },

        uploadClientDoc: function (data, clientId) {
            return ajax({
                url: DocumentMgt.baseApi + "/client/" + clientId,
                method: "POST",
                cache: false,
                contentType: false,
                processData: false,
                data: data,
            }).promise();
        },

        uploadBusinessDoc: function (data, bizId) {
            return ajax({
                url: DocumentMgt.baseApi + "/business/" + bizId,
                method: "POST",
                cache: false,
                contentType: false,
                processData: false,
                data: data,
            }).promise();
        },

        uploadCollateralDoc: function (data, id) {
            return ajax({
                url: DocumentMgt.baseApi + "/collateral/" + id,
                method: "POST",
                cache: false,
                contentType: false,
                processData: false,
                data: data,
            }).promise();
        },

        uploadLiabilityDoc: function (data, id) {
            return ajax({
                url: DocumentMgt.baseApi + "/liability/" + id,
                method: "POST",
                cache: false,
                contentType: false,
                processData: false,
                data: data,
            }).promise();
        },

        getById: function (id) {
            return ajax({
                url: DocumentMgt.baseApi + "/" + id,
                method: 'get'
            }).promise();
        },

        getLoanDocsByLoanId: function (loanId) {
            return ajax({
                url: DocumentMgt.baseApi + "/loan/" + loanId,
                method: 'get'
            }).promise();
        },

        getClientDocsByClientId: function (clientId, loanId) {
            return ajax({
                url: DocumentMgt.baseApi + "/client/" + clientId + "?loanId=" + loanId,
                method: 'get'
            }).promise();
        },

        deleteById: function (id) {
            return ajax({
                url: DocumentMgt.baseApi + "/" + id,
                method: 'DELETE'
            }).promise();
        },
    };

    exports.DocumentMgt = DocumentMgt;


})(this);