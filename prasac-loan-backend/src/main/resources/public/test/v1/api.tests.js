var expect = chai.expect;

var datetimeFormat = 'YYYY-MM-DD hh:mm';
var dateFormat = 'YYYY-MM-DD';

var globalTimeout = 10000;

function getCurrentTimeStamp() {
    return new Date().getTime();
}

var userMgt = new UserMgt();
var selectionMgt = new SelectionMgt();
var loanMgt = new LoanMgt();
var documentMgt = new DocumentMgt();

describe("User management", function () {
    this.timeout(globalTimeout);

    describe("Authentication", function () {
        it("should be able to login", function (done) {
            userMgt.login().then(function (response) {
                try {
                    expect(response).to.not.be.undefined;
                    expect(response.access_token).to.not.be.undefined;
                    done();
                } catch (error) {
                    done(error);
                }
                accessToken = response.access_token;
                done();
            }, getErrorHandler(200, done));
        });
    });

});

describe("Predefined values management", function () {
    this.timeout(globalTimeout);

    it("should return valid predefined values", function (done) {
        selectionMgt.getSelection().then(function (response) {
            try {
                expect(response).to.not.be.undefined;
                expect(response.loanStatuses).to.not.be.undefined;
                expect(response.loanStatuses).to.not.have.lengthOf(0);

                done();
            } catch (error) {
                done(error);
            }

            done();
        });
    });

});

var newLoan;
describe("Loan management", function () {
    this.timeout(globalTimeout);

    var LOAN_STATUS_NEW_PRE_SCREEN = 3;

    describe("Submit Pre Screening", function () {
        var request = {
            "branch": 1,
            "clients": [
                {
                    "clientType": 1,
                    "currentAddressGroupNo": 10,
                    "currentAddressHouseNo": "string",
                    "currentAddressStreet": "string",
                    "currentVillage": 1,
                    "dob": "1987-10-04",
                    "fullName": "Bo rrower",
                    "gender": "MALE",
                    "identifications": [
                        {
                            "clientIdentificationType": 1,
                            "idNumber": getCurrentTimeStamp()
                        }
                    ],
                    "khmerFullName": "Bo rrower",
                    "nationality": 1,
                    "phone": "string"
                },
                {
                    "clientType": 2,
                    "currentAddressGroupNo": 10,
                    "currentAddressHouseNo": "string",
                    "currentAddressStreet": "string",
                    "currentVillage": 1,
                    "dob": "1987-10-04",
                    "fullName": "Co borrower",
                    "gender": "MALE",
                    "identifications": [
                        {
                            "clientIdentificationType": 1,
                            "idNumber": getCurrentTimeStamp()
                        }
                    ],
                    "khmerFullName": "Co borrower",
                    "nationality": 1,
                    "phone": "string"
                },
                {
                    "clientType": 2,
                    "currentAddressGroupNo": 10,
                    "currentAddressHouseNo": "string",
                    "currentAddressStreet": "string",
                    "currentVillage": 1,
                    "dob": "1987-10-04",
                    "fullName": "Co borrower2",
                    "gender": "MALE",
                    "identifications": [
                        {
                            "clientIdentificationType": 1,
                            "idNumber": getCurrentTimeStamp()
                        }
                    ],
                    "khmerFullName": "Co borrower2",
                    "nationality": 1,
                    "phone": "string"
                },
                {
                    "clientType": 2,
                    "currentAddressGroupNo": 10,
                    "currentAddressHouseNo": "string",
                    "currentAddressStreet": "string",
                    "currentVillage": 1,
                    "dob": "1987-10-04",
                    "fullName": "Co borrower3",
                    "gender": "MALE",
                    "identifications": [
                        {
                            "clientIdentificationType": 1,
                            "idNumber": getCurrentTimeStamp()
                        }
                    ],
                    "khmerFullName": "Co borrower3",
                    "nationality": 1,
                    "phone": "string"
                },
                {
                    "clientType": 3,
                    "currentAddressGroupNo": 10,
                    "currentAddressHouseNo": "string",
                    "currentAddressStreet": "string",
                    "currentVillage": 1,
                    "dob": "1987-10-04",
                    "fullName": "Guaranor Guaranor",
                    "gender": "MALE",
                    "identifications": [
                        {
                            "clientIdentificationType": 1,
                            "idNumber": getCurrentTimeStamp()
                        }
                    ],
                    "khmerFullName": "Guaranor Guaranor",
                    "nationality": 1,
                    "phone": "string"
                }
            ],
            "currency": 1,
            "loanAmount": 1,
            "loanStatus": 1
        };

        it("should be able to submit Pre Screening by CO", function (done) {
            loanMgt.submitPreScreenByCO(request).then(function (response) {
                try {
                    newLoan = response;
                    expect(response.id).to.not.be.undefined;
                    done();
                } catch (error) {
                    done(error);
                }
            }, getErrorHandler(200, done));
        });

        it("should be able to submit Pre Screening by Admin", function (done) {
            request.clients[0].identifications[0].idNumber = getCurrentTimeStamp() + 1;
            request.loanStatus = LOAN_STATUS_NEW_PRE_SCREEN;

            loanMgt.submitPreScreenByAdmin(request).then(function (response) {
                try {
                    expect(response.id).to.not.be.undefined;
                    expect(response.loanStatus).to.equal(LOAN_STATUS_NEW_PRE_SCREEN);
                    done();
                } catch (error) {
                    done(error);
                }
            }, getErrorHandler(200, done))
        });
    });

    describe("Submit Loan Appraisal", function () {
        var request = {
            "clients": [
                {
                    "birthVillage": 1,
                    "clientOccupation": 1,
                    "clientRelationship": 1,
                    "clientResidentStatus": 1,
                    "clientType": 1,
                    "clientWorkingTime": 2,
                    "currentAddressGroupNo": 1,
                    "currentAddressHouseNo": "string",
                    "currentAddressStreet": "string",
                    "currentVillage": 1,
                    "dob": "1987-10-04",
                    "fullName": "Bo rrower",
                    "gender": "MALE",

                    "identifications": [
                        {
                            "expiryDate": "2020-10-04",
                            "issueDate": "1987-10-04",
                            "idNumber": getCurrentTimeStamp() + 1,
                            "clientIdentificationType": 1
                        }
                    ],
                    "incomeAmount": 10,
                    "khmerFullName": "Bo rrower",
                    "maritalStatus": 1,
                    "nationality": 1,
                    "noOfActiveChildren": 10,
                    "noOfChildren": 10,
                    "noOfChildrenAgeGreaterThan18": 10,
                    "periodInCurrentAddress": 10,
                    "phone": "string",
                    "workingPeriod": 1,
                    "workplaceGroupNo": 10,
                    "workplaceHouseNo": "string",
                    "workplaceName": "string",
                    "workplaceStreet": "string",
                    "workplaceVillage": 1
                },
                {
                    "birthVillage": 1,
                    "clientOccupation": 1,
                    "clientRelationship": 1,
                    "clientResidentStatus": 1,
                    "clientType": 2,
                    "clientWorkingTime": 2,
                    "currentAddressGroupNo": 1,
                    "currentAddressHouseNo": "string",
                    "currentAddressStreet": "string",
                    "currentVillage": 1,
                    "dob": "1987-10-04",
                    "fullName": "Co borrower",
                    "gender": "MALE",

                    "identifications": [
                        {
                            "expiryDate": "2020-10-04",
                            "issueDate": "1987-10-04",
                            "idNumber": getCurrentTimeStamp() + 10,
                            "clientIdentificationType": 1
                        }
                    ],
                    "incomeAmount": 10,
                    "khmerFullName": "Co borrower",
                    "maritalStatus": 1,
                    "nationality": 1,
                    "noOfActiveChildren": 10,
                    "noOfChildren": 10,
                    "noOfChildrenAgeGreaterThan18": 10,
                    "periodInCurrentAddress": 10,
                    "phone": "string",
                    "workingPeriod": 1,
                    "workplaceGroupNo": 10,
                    "workplaceHouseNo": "string",
                    "workplaceName": "string",
                    "workplaceStreet": "string",
                    "workplaceVillage": 1
                },
                {
                    "birthVillage": 1,
                    "clientOccupation": 1,
                    "clientRelationship": 1,
                    "clientResidentStatus": 1,
                    "clientType": 2,
                    "clientWorkingTime": 2,
                    "currentAddressGroupNo": 1,
                    "currentAddressHouseNo": "string",
                    "currentAddressStreet": "string",
                    "currentVillage": 1,
                    "dob": "1987-10-04",
                    "fullName": "Co borrower2",
                    "gender": "MALE",

                    "identifications": [
                        {
                            "expiryDate": "2020-10-04",
                            "issueDate": "1987-10-04",
                            "idNumber": getCurrentTimeStamp() + 100,
                            "clientIdentificationType": 1
                        }
                    ],
                    "incomeAmount": 10,
                    "khmerFullName": "Co borrower2",
                    "maritalStatus": 1,
                    "nationality": 1,
                    "noOfActiveChildren": 10,
                    "noOfChildren": 10,
                    "noOfChildrenAgeGreaterThan18": 10,
                    "periodInCurrentAddress": 10,
                    "phone": "string",
                    "workingPeriod": 1,
                    "workplaceGroupNo": 10,
                    "workplaceHouseNo": "string",
                    "workplaceName": "string",
                    "workplaceStreet": "string",
                    "workplaceVillage": 1
                },
                {
                    "birthVillage": 1,
                    "clientOccupation": 1,
                    "clientRelationship": 1,
                    "clientResidentStatus": 1,
                    "clientType": 3,
                    "clientWorkingTime": 2,
                    "currentAddressGroupNo": 1,
                    "currentAddressHouseNo": "string",
                    "currentAddressStreet": "string",
                    "currentVillage": 1,
                    "dob": "1987-10-04",
                    "fullName": "Guaranor Guaranor",
                    "gender": "MALE",

                    "identifications": [
                        {
                            "expiryDate": "2020-10-04",
                            "issueDate": "1987-10-04",
                            "idNumber": getCurrentTimeStamp() + 1000,
                            "clientIdentificationType": 1
                        }
                    ],
                    "incomeAmount": 10,
                    "khmerFullName": "Guaranor Guaranor",
                    "maritalStatus": 1,
                    "nationality": 1,
                    "noOfActiveChildren": 10,
                    "noOfChildren": 10,
                    "noOfChildrenAgeGreaterThan18": 10,
                    "periodInCurrentAddress": 10,
                    "phone": "string",
                    "workingPeriod": 1,
                    "workplaceGroupNo": 10,
                    "workplaceHouseNo": "string",
                    "workplaceName": "string",
                    "workplaceStreet": "string",
                    "workplaceVillage": 1
                }
            ],
            "conclusion": "string",
            "currency": 1,
            "cycle": 1,
            "gracePeriod": 1,
            "loanAmount": 100,
            "loanFee": 1,
            "loanRequestType": 1,
            "lockInPeriod": 1,
            "monthlyInterestRate": 1,
            "refinancingFee": 1,
            "paidOffFee": 1,
            "registrationFee": 1,
            "adminFee": 1,
            "repaymentMode": 1,
            "tenure": 1,
            "projects": [
                {

                    "loanPurpose": 1,
                    "loanUtilizationProject": "string",
                    "pricePerUnit": 10,
                    "unit": 10
                },
                {

                    "loanPurpose": 1,
                    "loanUtilizationProject": "string",
                    "pricePerUnit": 10,
                    "unit": 10
                }
            ],
            "liabilityCredits": [
                {
                    "accountNumber": "string",
                    "currency": 1,
                    "disbursementDate": "2019-04-17",
                    "firstPrincipalRepayment": 10,

                    "institutionName": "string",
                    "interestRateMonthly": 10,
                    "lengthOfUsing": 10,

                    "loanAmount": 10,
                    "loanCycle": 10,
                    "loanLimit": 10,
                    "loanPurpose": 1,
                    "loanType": 1,
                    "lockInPeriodMonthly": 10,
                    "maturityDate": "2019-04-17",
                    "monthlyInterestRepayment": 10,
                    "mortgagedCollateralNumber": 10,
                    "numberOfLateDays": 1,
                    "numberOfLateInstallments": 1,
                    "payOffAfterNewLoan": true,
                    "penaltyAmountForPaidOff": 1,
                    "repaymentMode": 1
                }
            ],

            "businesses": [
                {
                    "addressGroupNo": 1,
                    "businessActivitiesDetail": "string",
                    "businessExperience": 1,
                    "businessOwner": 1,
                    "clientBusinessCashFlow": 1,
                    "clientBusinessLocationStatus": 1,
                    "clientBusinessPeriod": 1,
                    "clientBusinessProgress": 1,
                    "houseNo": "string",

                    "latitude": "string",
                    "licenseContractValidity": 1,
                    "longitude": "string",
                    "managerName": "string",
                    "name": "string",
                    "numberOfManager": 1,
                    "street": "string",
                    "village": 1,
                    "financialStatementIncomeRevenue": {
                        "average": 1,
                        "costOfGoodsSold": 1,
                        "currentMonth": 1,
                        "eighth": 1,
                        "eleventh": 1,
                        "fifth": 1,
                        "first": 1,
                        "fourth": 1,
                        "ninth": 1,
                        "second": 1,
                        "seventh": 1,
                        "sixth": 1,
                        "startDate": "2019-08-08",
                        "tenth": 1,
                        "third": 1,
                        "total": 1,
                        "twelfth": 1
                    }
                }
            ],
            "collateralList": [
                {
                    "collateralArea": 1,
                    "collateralDescription": 1,
                    "collateralGroupNo": 1,
                    "collateralRegistration": 1,
                    "collateralTitleDeedNo": getCurrentTimeStamp(),
                    "collateralTitleType": 1,

                    "east": "string",
                    "houseNumber": "string",

                    "latitude": "string",
                    "longitude": "string",
                    "marketValueAnalyses": [
                        {
                            "floorNo": "string",

                            "landPrice": 1,
                            "length": 1,
                            "marketValue": 1,
                            "marketValueAnalyseDescription": 1,
                            "size": 1,
                            "width": 1
                        }
                    ],
                    "north": "string",
                    "propertyOwner": "string",
                    "propertyType": 1,

                    "south": "string",
                    "street": "string",
                    "village": 1,
                    "west": "string"
                },
                {
                    "collateralArea": 1,
                    "collateralDescription": 1,
                    "collateralGroupNo": 1,
                    "collateralRegistration": 1,
                    "collateralTitleDeedNo": getCurrentTimeStamp() + 1,
                    "collateralTitleType": 1,

                    "east": "string",
                    "houseNumber": "string",

                    "latitude": "string",
                    "longitude": "string",
                    "marketValueAnalyses": [
                        {
                            "floorNo": "string",

                            "landPrice": 1,
                            "length": 1,
                            "marketValue": 1,
                            "marketValueAnalyseDescription": 1,
                            "size": 1,
                            "width": 1
                        }
                    ],
                    "north": "string",
                    "propertyOwner": "string",
                    "propertyType": 1,

                    "south": "string",
                    "street": "string",
                    "village": 1,
                    "west": "string"
                }, {
                    "collateralArea": 1,
                    "collateralDescription": 1,
                    "collateralGroupNo": 1,
                    "collateralRegistration": 1,
                    "collateralTitleDeedNo": getCurrentTimeStamp() + 2,
                    "collateralTitleType": 1,

                    "east": "string",
                    "houseNumber": "string",

                    "latitude": "string",
                    "longitude": "string",
                    "marketValueAnalyses": [
                        {
                            "floorNo": "string",

                            "landPrice": 1,
                            "length": 1,
                            "marketValue": 1,
                            "marketValueAnalyseDescription": 1,
                            "size": 1,
                            "width": 1
                        }
                    ],
                    "north": "string",
                    "propertyOwner": "string",
                    "propertyType": 1,

                    "south": "string",
                    "street": "string",
                    "village": 1,
                    "west": "string"
                }
            ],
            "socialEnvironmentImpactAssessment": {
                "adviceConsultation": "string",
                "airPollution": 1,
                "businessActivity": 1,
                "conversationWithBorrower": "8822",
                "customerImprovementNotice": "99",
                "energy": 1,
                "healthSafety": 1,

                "laborForce": 1,

                "naturalResource": 1,
                "productWaste": 1,
                "waterPollution": 1
            },
            "financialStatementBalance": {
                "financialStatementBalanceAsset": {
                    "currentYear": 1,
                    "currentYearAccountReceivable": 1,
                    "currentYearCash": 1,
                    "currentYearMaterialInventory": 1,
                    "currentYearOthers": 1,
                    "forecastYear": 1,
                    "forecastYearAccountReceivable": 1,
                    "forecastYearCash": 1,
                    "forecastYearMaterialInventory": 1,
                    "forecastYearOthers": 1

                },
                "financialStatementBalanceLiability": {
                    "capitalInvestmentToLoanUtilizationProject": 1,
                    "currentYear": 1,
                    "currentYearAccountPayable": 1,
                    "currentYearAdvanceDeposit": 1,
                    "currentYearInformalMoneyLenders": 1,
                    "currentYearShortTermLoan": 1,
                    "forecastYear": 1,
                    "forecastYearAccountPayable": 1,
                    "forecastYearAdvanceDeposit": 1,
                    "forecastYearInformalMoneyLenders": 1,
                    "forecastYearShortTermLoan": 1

                }
            },
            "financialStatementIncome": {
                "taxExpenses": 1,
                "financialStatementIncomeBankPayment": {
                    "average": 1,
                    "currentMonth": 1,
                    "eighth": 1,
                    "eleventh": 1,
                    "fifth": 1,
                    "first": 1,
                    "fourth": 1,
                    "ninth": 1,
                    "second": 1,
                    "seventh": 1,
                    "sixth": 1,
                    "startDate": "2019-08-08",
                    "tenth": 1,
                    "third": 1,
                    "total": 1,
                    "twelfth": 1
                },
                "financialStatementIncomeOperatingExpense": {
                    "currentMonthAdministrativeExpense": 1,
                    "currentMonthDepreciationExpense": 1,
                    "currentMonthFamilyExpense": 1,
                    "currentMonthMaintenanceExpense": 1,
                    "currentMonthOthersExpense": 1,
                    "currentMonthRentalExpense": 1,
                    "currentMonthSalaryExpense": 1,
                    "currentMonthTotal": 1,
                    "currentMonthUtilityExpense": 1,
                    "forecastMonthAdministrativeExpense": 1,
                    "forecastMonthDepreciationExpense": 1,
                    "forecastMonthFamilyExpense": 1,
                    "forecastMonthMaintenanceExpense": 1,
                    "forecastMonthOthersExpense": 1,
                    "forecastMonthRentalExpense": 1,
                    "forecastMonthSalaryExpense": 1,
                    "forecastMonthUtilityExpense": 1,
                    "nextMonthTotal": 1
                }
            },
            "financialStatementGuarantor": {
                "financialStatementGuarantorEmployments": [
                    {
                        "activity": "string",
                        "annualExpense": 1,
                        "annualIncome": 1
                    }
                ],
                "financialStatementGuarantorExpense": {
                    "familyExpense": 1,

                    "otherExpense": 1,
                    "utilityExpense": 1
                }
            }
        };

        it("should be able to submit loan appraisal", function (done) {
            request.id = newLoan.id;
            loanMgt.submitLoanAppraisal(request).then(function (response) {
                try {
                    newLoan = response;
                    expect(response.id).to.not.be.undefined;
                    done();
                } catch (error) {
                    done(error);
                }
            }, getErrorHandler(200, done));
        });
    });

});

var docMgtTitle = 'Document management';
describe(docMgtTitle, function () {
    this.timeout(globalTimeout);

    var DOC_TYPE_CLIENT_PIC = 19;
    var DOC_TYPE_BIZ_PIC = 2;
    var DOC_TYPE_COLLATERAL_PIC = 17;
    var DOC_TYPE_LIABILITY_PIC = 14;
    var result = {};

    function isTesting() {
        return $('.file').val();
    }

    it("should be able to upload the loan document", function (done) {
        if (isTesting()) {
            $('#loan').val(newLoan.id);
            var request = new FormData($('#upload-form')[0]);

            documentMgt.uploadLoanDoc(request).then(function (response) {
                result = response;
                done();
            }, getErrorHandler(200, done));
        } else {
            $("a:contains('" + docMgtTitle + "')").closest('.suite').hide();
            done();
        }
    });

    it("should be able to get the uploaded loan document", function (done) {
        if (isTesting()) {
            var documentId = result[0].id;
            documentMgt.getById(documentId).then(function (response) {
                done();
            }, getErrorHandler(200, done));
        }
    });

    it("should be able to get a list of loan documents", function (done) {
        if (isTesting()) {
            var loanId = newLoan.id;
            documentMgt.getLoanDocsByLoanId(loanId).then(function (response) {
                done();
            }, getErrorHandler(200, done));
        }
    });

    it("should be able to delete the uploaded loan document", function (done) {
        if (isTesting()) {
            var documentId = result[0].id;
            documentMgt.deleteById(documentId).then(function (response) {
                done();
            }, getErrorHandler(200, done));
        }
    });

    it("should be able to upload the business document", function (done) {
        if (isTesting()) {
            $('#loan').val(newLoan.id);
            $('.documentType').val(DOC_TYPE_BIZ_PIC);
            var request = new FormData($('#upload-form')[0]);
            var bizId = newLoan.businesses[0].id;

            documentMgt.uploadBusinessDoc(request, bizId).then(function (response) {
                result = response;
                done();
            }, getErrorHandler(200, done));
        }
    });

    it("should be able to delete the uploaded business document", function (done) {
        if (isTesting()) {
            var documentId = result[0].id;
            documentMgt.deleteById(documentId).then(function (response) {
                done();
            }, getErrorHandler(200, done));
        }
    });

    it("should be able to upload the collateral document", function (done) {
        if (isTesting()) {
            $('#loan').val(newLoan.id);
            $('.documentType').val(DOC_TYPE_COLLATERAL_PIC);
            var request = new FormData($('#upload-form')[0]);
            var id = newLoan.collateralList[0].id;

            documentMgt.uploadCollateralDoc(request, id).then(function (response) {
                result = response;
                done();
            }, getErrorHandler(200, done));
        }
    });

    it("should be able to delete the uploaded collateral document", function (done) {
        if (isTesting()) {
            var documentId = result[0].id;
            documentMgt.deleteById(documentId).then(function (response) {
                done();
            }, getErrorHandler(200, done));
        }
    });

    it("should be able to upload the liability document", function (done) {
        if (isTesting()) {
            $('#loan').val(newLoan.id);
            $('.documentType').val(DOC_TYPE_LIABILITY_PIC);
            var request = new FormData($('#upload-form')[0]);
            var id = newLoan.liabilityCredits[0].id;

            documentMgt.uploadLiabilityDoc(request, id).then(function (response) {
                result = response;
                done();
            }, getErrorHandler(200, done));
        }
    });

    it("should be able to delete the uploaded liability document", function (done) {
        if (isTesting()) {
            var documentId = result[0].id;
            documentMgt.deleteById(documentId).then(function (response) {
                done();
            }, getErrorHandler(200, done));
        }
    });

    it("should be able to upload the client document", function (done) {
        if (isTesting()) {
            $('#loan').val(newLoan.id);
            $('.documentType').val(DOC_TYPE_CLIENT_PIC);
            var request = new FormData($('#upload-form')[0]);
            var clientId = newLoan.clients[0].id;

            documentMgt.uploadClientDoc(request, clientId).then(function (response) {
                result = response;
                done();
            }, getErrorHandler(200, done));
        }
    });

    it("should be able to get the uploaded client document", function (done) {
        if (isTesting()) {
            var documentId = result[0].id;
            documentMgt.getById(documentId).then(function (response) {
                done();
            }, getErrorHandler(200, done));
        }
    });


    it("should be able to get a list of client documents", function (done) {
        if (isTesting()) {
            var clientId = newLoan.clients[0].id;
            var loanId = newLoan.id;

            documentMgt.getClientDocsByClientId(clientId, loanId).then(function (response) {
                done();
            }, getErrorHandler(200, done));
        }
    });

    it("should be able to delete the uploaded client document", function (done) {
        if (isTesting()) {
            var documentId = result[0].id;
            documentMgt.deleteById(documentId).then(function (response) {
                done();
            }, getErrorHandler(200, done));
        }
    });
});
