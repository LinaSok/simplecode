function getErrorHandler(expectedCode, done) {
    return function (response) {
        if (response.status == 401) {
            done(new Error("Invalid access token."));
        }
        if (response.status == expectedCode) {
            done();
        } else {
            var errorMessage = "Expecting response with code " + expectedCode + ", got " + response.status + " instead.";
            if (response.responseJSON && response.responseJSON.message) {
                errorMessage += "\nAdditional details: " + response.responseJSON.message + "\n";
            }
            done(new Error(errorMessage));
        }
    }
}