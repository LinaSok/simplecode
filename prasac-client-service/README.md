## Getting Started

This project act as Middleware application for prasac micro-fiance bank connecting external service such as

- Oracle Database service
- FlexCube Soap Service
- CBC services
- others 

### Prerequisites
* Git (optional)
* Jdk 8
* springboot + spring5
* Oracle as database

### Clone

```
git clone ssh://your-domain-name@gerrit.dminc-gtc.com:29418/prasac-client-service
cd prasac-client-service
```


### Run local environment
./gradlew clean bootRun

### open browser 
http://localhost:8181

http://localhost:8181/api/healcheck/v1

#### generic error json 

{
timestamp: "2019-02-25T16:18:09.228+0000",
status: 500,
error: "Internal Server Error",
message: "my error",
trace: -  ["....."],
path: "/api/healthcheck/v2/error",
errorCode: 4
}

