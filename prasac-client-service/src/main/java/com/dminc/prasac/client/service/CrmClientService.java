package com.dminc.prasac.client.service;

import com.dminc.prasac.client.model.dto.CrmClient;
import com.dminc.prasac.client.model.dto.IdentityRequest;
import com.dminc.prasac.client.model.dto.SearchClientRequest;
import com.dminc.rest.exceptions.BusinessException;

import java.util.List;

public interface CrmClientService {

    List<CrmClient> search(SearchClientRequest request) throws BusinessException;

    CrmClient getOneByCif(String cif) throws BusinessException;

    CrmClient findByIdNumberAndIdType(IdentityRequest request) throws BusinessException;
}
