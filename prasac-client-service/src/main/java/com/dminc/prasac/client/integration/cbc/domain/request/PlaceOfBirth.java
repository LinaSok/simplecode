package com.dminc.prasac.client.integration.cbc.domain.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "CPLB")
@XmlAccessorType(XmlAccessType.FIELD)
public class PlaceOfBirth {

    @XmlElement(name = "CPLBC")
    private String countryCode;

    @XmlElement(name = "CPLBP")
    private String provinceCode;

    @XmlElement(name = "CPLBD")
    private String districtCode;

    @XmlElement(name = "CPLBCM")
    private String communeCode;


}
