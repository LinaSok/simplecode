package com.dminc.prasac.client.respository;

import com.dminc.prasac.client.model.entity.ClientLinkageEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientLinkageRepository extends ReadOnlyRepository<ClientLinkageEntity, String> {

    List<ClientLinkageEntity> findByClientCif(String cif);
}
