package com.dminc.prasac.client.integration.cbc.domain.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@XmlRootElement(name = "EADR")
@XmlAccessorType(XmlAccessType.FIELD)
public class WorkingAddress {

    @XmlElement(name = "EADT")
    private String addressType;

    @XmlElement(name = "EADP")
    private String provinceCode;

    @XmlElement(name = "EADD")
    private String districtCode;

    @XmlElement(name = "EADC")
    private String communeCode;

    @XmlElement(name = "EADV")
    private String villageCode;

    @XmlElement(name = "EAD1E")
    private String fullAddress;

    @XmlElement(name = "EAD8E")
    private String province;

    @XmlElement(name = "EAD9")
    private String countryCode;


}
