package com.dminc.prasac.client.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

@Configuration
public class DatasourceConfig {

    @Autowired
    private Environment env;

    @Bean
    public DataSource restDataSource() {
        HikariConfig hikariConfig = HikariDatasourceDefaultConfigurationFactory.builder().build().getDefaultConfig(env);
        return new HikariDataSource(hikariConfig);
    }
}
