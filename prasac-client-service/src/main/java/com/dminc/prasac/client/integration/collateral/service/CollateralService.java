package com.dminc.prasac.client.integration.collateral.service;

import com.dminc.prasac.client.model.dto.Collateral;
import com.dminc.rest.exceptions.BusinessException;

public interface CollateralService {

    void createOrUpdate(Collateral collateral) throws BusinessException;
}
