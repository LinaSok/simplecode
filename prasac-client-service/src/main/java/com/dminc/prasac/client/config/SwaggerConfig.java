package com.dminc.prasac.client.config;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("com.dmi"))
                .paths(PathSelectors.any()).build()
                .apiInfo(apiInfo());
    }


    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Prasac Web Service REST API",
                "Prasac Web Service of API. Each API requires to send the Header 'X-Authorization'."
                        + "The Development server the X-Authorization value is UsnSWbnYK26hHc7KUcaM",
                "API",
                "Terms of service",
                new Contact("DMI", "-", "smak@dminc.com"),
                "License of API", "API license URL", Collections.emptyList());
    }
}
