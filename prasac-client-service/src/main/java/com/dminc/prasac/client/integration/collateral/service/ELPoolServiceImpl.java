package com.dminc.prasac.client.integration.collateral.service;

import com.dminc.prasac.client.integration.collateral.request.NewCollateralPoolRequest;
import com.dminc.prasac.client.integration.collateral.request.QueryCollateralPoolRequest;
import com.dminc.prasac.client.integration.collateral.request.UpdateCollateralPoolRequest;
import com.dminc.prasac.client.integration.config.DefaultNamespacePrefixMapper;
import com.dminc.prasac.client.model.dto.ClientLinkage;
import com.dminc.prasac.client.model.dto.CollateralPool;
import com.dminc.prasac.client.model.dto.CrmClient;
import com.dminc.prasac.client.model.dto.User;
import com.dminc.prasac.client.service.ClientLinkageService;
import com.dminc.prasac.client.service.CrmClientService;
import com.dminc.prasac.client.service.FlexCubeUserService;
import com.dminc.rest.exceptions.BusinessException;
import com.dminc.rest.exceptions.InternalBusinessException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;
import prasac.webapi.wsdl.elpool.CREATECOLLATERALPOOLFSFSRES;
import prasac.webapi.wsdl.elpool.CollateralpoolFullType;
import prasac.webapi.wsdl.elpool.ERRORDETAILSType;
import prasac.webapi.wsdl.elpool.ERRORType;
import prasac.webapi.wsdl.elpool.MODIFYCOLLATERALPOOLFSFSRES;
import prasac.webapi.wsdl.elpool.QUERYCOLLATERALPOOLIOFSRES;

import javax.annotation.PostConstruct;
import javax.xml.bind.Marshaller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class ELPoolServiceImpl implements ELPoolService {

    public static final String STATE_OPEN = "O";

    private static final String SOAP_PATH = "/FLEXCUBE-ELCMWeb/ELPoolService";
    private final DefaultNamespacePrefixMapper defaultNamespacePrefixMapper;
    private final FlexCubeUserService userService;
    private final CrmClientService clientService;
    private final ClientLinkageService linkageService;

    @Value("${prasac.soap.service.url}")
    private String domain;
    @Value("${prasac.soap.service.source}")
    private String source;
    private WebServiceTemplate template;
    private String soapUrl;

    @PostConstruct
    public void init() {
        soapUrl = String.format("%s%s", domain, SOAP_PATH);
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("prasac.webapi.wsdl.elpool");
        final Map<String, Object> properties = new HashMap<>();
        properties.put(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        properties.put("com.sun.xml.bind.namespacePrefixMapper", defaultNamespacePrefixMapper);
        marshaller.setMarshallerProperties(properties);
        template = new WebServiceTemplate();
        template.setDefaultUri(domain);
        template.setMarshaller(marshaller);
        template.setUnmarshaller(marshaller);
    }

    @Override
    public CollateralPool createOrUpdate(CollateralPool pool) throws BusinessException {

        final User user = userService.getUserByBranch(pool.getBranchCode());
        final CrmClient client = clientService.getOneByCif(pool.getClientCif());

        final List<String> poolCodes = findPoolCodes(pool.getClientCif());

        // no found poolCodes it means create
        if (CollectionUtils.isEmpty(poolCodes)) {
            log.info("Create mode");
            create(pool, user, client.getLiabId());
            return pool;
        }

        boolean isOpenStateFound = false;
        CollateralpoolFullType existing = null;
        //lookup for poolCode which has status Open, if the state Open the action is modified otherwise it is create new one
        // if not found response skill null and continue for has response
        for (String code : poolCodes) {
            existing = query(client.getLiabId(), code, user);
            if (existing != null && STATE_OPEN.equals(existing.getTXNSTAT())) {
                isOpenStateFound = true;
                break;
            }
        }
        CollateralpoolFullType collateralpool;
        if (isOpenStateFound) {
            log.info("update mode TXNSTAT {}", existing.getTXNSTAT());
            collateralpool = update(pool, user, existing);
        } else {
            log.info("Create mode");
            collateralpool = create(pool, user, client.getLiabId());
        }
        pool.setCollateralPoolCode(collateralpool.getPOOLCODE());
        return pool;
    }

    private List<String> findPoolCodes(String clientCif) {
        final List<ClientLinkage> clientLinkages = linkageService.getByClientCif(clientCif);
        return org.apache.commons.collections4.CollectionUtils.emptyIfNull(clientLinkages)
                .stream()
                .map(ClientLinkage::getCollateralPoolCode)
                .filter(StringUtils::isNotEmpty)
                .collect(Collectors.toList());
    }

    private CollateralpoolFullType create(CollateralPool pool, User user, String liabId) throws InternalBusinessException {
        log.info("============================ create CollateralPool ======================= \n");
        final NewCollateralPoolRequest request = NewCollateralPoolRequest.builder()
                .collateralPool(pool)
                .source(source)
                .user(user)
                .liabId(liabId)
                .build();
        final CREATECOLLATERALPOOLFSFSRES response = (CREATECOLLATERALPOOLFSFSRES) template
                .marshalSendAndReceive(soapUrl, request.getRequest());
        checkError(response.getFCUBSBODY().getFCUBSERRORRESP());
        return response.getFCUBSBODY().getCollateralsPoolFull();
    }

    private CollateralpoolFullType update(CollateralPool pool, User user, CollateralpoolFullType existing) throws InternalBusinessException {

        log.info("============================ update CollateralPool=======================");
        final UpdateCollateralPoolRequest updateCollateralRequest = UpdateCollateralPoolRequest.builder().collateralPool(pool)
                .existing(existing).source(source)
                .user(user)
                .build();
        final MODIFYCOLLATERALPOOLFSFSRES response = (MODIFYCOLLATERALPOOLFSFSRES) template
                .marshalSendAndReceive(soapUrl, updateCollateralRequest.getRequest());
        checkError(response.getFCUBSBODY().getFCUBSERRORRESP());
        return response.getFCUBSBODY().getCollateralsPoolFull();
    }

    public CollateralpoolFullType query(String liabilityId, String poolCode, User user) throws BusinessException {

        log.info("get query - CollateralpoolFullType with poolCode {}", poolCode);

        final QueryCollateralPoolRequest request = QueryCollateralPoolRequest.builder().liabId(liabilityId).source(source)
                .poolCode(poolCode).user(user).build();

        final QUERYCOLLATERALPOOLIOFSRES response = (QUERYCOLLATERALPOOLIOFSRES) template
                .marshalSendAndReceive(soapUrl, request.getRequest());
        final List<ERRORType> errorTypes = response.getFCUBSBODY().getFCUBSERRORRESP();
        for (ERRORType errorType : errorTypes) {
            for (ERRORDETAILSType e : errorType.getERROR()) {
                if ("NO-DATA".equals(e.getECODE())) {
                    return null;
                }
            }
        }
        //check error
        checkError(errorTypes);

        final CollateralpoolFullType result = response.getFCUBSBODY().getCollateralsPoolFull();
        log.info("PoolCode {} , with TXNSTAT {} ", poolCode, result.getTXNSTAT());
        return result;
    }

    private void checkError(final List<ERRORType> types) throws InternalBusinessException {
        final StringBuilder errorMessage = new StringBuilder();
        types.forEach(type -> type.getERROR().forEach(error -> {
            errorMessage.append(String.format("\n %s : %s", error.getECODE(), error.getEDESC()));
        }));
        if (StringUtils.isNotEmpty(errorMessage)) {
            throw new InternalBusinessException(errorMessage.toString(), 8000);
        }
    }
}
