package com.dminc.prasac.client.integration.cbc.domain.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "CNAM")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerName {

    @XmlElement(name = "CNMFA")
    private KhmerFirstName khmerFirstName;

    @XmlElement(name = "CNM1A")
    private KhmerLastName khmerLastName;

    @XmlElement(name = "CNMFE")
    private String firstName;

    @XmlElement(name = "CNM1E")
    private String lastName;
}
