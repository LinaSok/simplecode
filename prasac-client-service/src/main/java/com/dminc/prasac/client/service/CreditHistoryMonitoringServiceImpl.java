package com.dminc.prasac.client.service;

import com.dminc.prasac.client.model.dto.CreditHistoryMonitoring;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.List;
import java.util.Locale;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CreditHistoryMonitoringServiceImpl implements CreditHistoryMonitoringService {

    public static final String COLUMN_CUST_NO = "CUST_NO";
    public static final String COLUMN_AMOUNT = "AMOUNT";
    public static final String COLUMN_CURRENCY = "CCY";
    public static final String COLUMN_DATE = "DATE1";
    public static final String COLUMN_ENQ_DATE = "ENQ_DATE";
    public static final String COLUMN_EVENT = "EVENT";
    public static final String COLUMN_REFERENCE = "REFERENCE";
    public static final String COLUMN_LENDER = "LENDER";

    public static final DateTimeFormatter DATE_FORMATTER = new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .appendPattern("[d-MMM-yy]")
            .appendPattern("[yyyy-MM-dd hh:mm:ss]")
            .toFormatter(Locale.ENGLISH);
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public static Double parseDouble(String s) {
        if (StringUtils.isNotEmpty(s)) {
            return Double.valueOf(s.replace(",", ""));
        }
        return null;
    }

    public static LocalDate parseLocaleDate(String s) {
        if (StringUtils.isNotEmpty(s)) {
            return LocalDate.parse(s, DATE_FORMATTER);
        }
        return null;
    }

    @Override
    public List<CreditHistoryMonitoring> getCreditHistoryMonitoringsByClient(String clientCif) {
        final RowMapper<CreditHistoryMonitoring> rowMapper = (ResultSet rs, int rowNum) ->
                CreditHistoryMonitoring.builder()
                        .clientCif(rs.getString(COLUMN_CUST_NO))
                        .amount(parseDouble(rs.getString(COLUMN_AMOUNT)))
                        .currency(rs.getString(COLUMN_CURRENCY))
                        .date(parseLocaleDate(rs.getString(COLUMN_DATE)))
                        .enqDate(parseLocaleDate(rs.getString(COLUMN_ENQ_DATE)))
                        .event(rs.getString(COLUMN_EVENT))
                        .reference(rs.getString(COLUMN_REFERENCE))
                        .lender(rs.getString(COLUMN_LENDER))
                        .build();
        //TODO replace by Store Procedure once client finished
        final String sql = new StringBuilder().append(" SELECT CUST_NO,ENQ_DATE,DATE1,EVENT ,CCY,REPLACE(AMOUNT,' ','')AMOUNT ,LENDER,REFERENCE FROM (")
                .append("SELECT CUST_NO,ENQ_DATE,DATE1,EVENT, CCY,TO_CHAR(REPLACE(AMOUNT,',',''),'999,999,999,999')AMOUNT,LENDER,REFERENCE FROM (")
                .append("SELECT CUST_NO,ENQ_DATE,TO_CHAR(ENQ_DATE,'dd-Mon-yy')DATE1,EVENT, REPLACE(CASE WHEN SUBSTR(AMOUNT,-3) IN ('KHR','USD','THB') ")
                .append("THEN SUBSTR(AMOUNT,0,LENGTH(AMOUNT)-3) WHEN SUBSTR(AMOUNT,1,3) IN ('KHR','USD','THB') THEN SUBSTR(AMOUNT,4) ELSE AMOUNT END,' ','') AMOUNT, ")
                .append("CASE WHEN SUBSTR(AMOUNT,-3) IN ('KHR','USD','THB') THEN SUBSTR(AMOUNT,-3) WHEN SUBSTR(AMOUNT,1,3) IN ('KHR','USD','THB') THEN SUBSTR(AMOUNT,1,3)  ")
                .append("ELSE NULL END CCY,  LENDER,REFERENCE FROM (SELECT:CUST_ID CUST_NO,TO_DATE(ENQ_DATE)ENQ_DATE,EVENT,AMOUNT,LENDER,REFERENCE ")
                .append("FROM ( SELECT A.ACCOUNT_NUMBER,A.ENQ_DATE,A.EVENT, CASE WHEN AMOUNT LIKE '%Open%' OR AMOUNT LIKE '%Close%' ")
                .append("THEN SUBSTR(SUBSTR(AMOUNT, -INSTR(REVERSE(AMOUNT),'/')+1),6) ELSE '' END AMOUNT, LENDER,REFERENCE FROM ")
                .append("(SELECT ACCOUNT_NUMBER, CASE WHEN (ENQ_TYPE LIKE '%Open%' OR ENQ_TYPE LIKE '%Close%') ")
                .append("AND SUBSTR(REGEXP_SUBSTR(ENQ_TYPE, '[^ ]+', 1,3),1,10) <>'00/01/1900' THEN  TO_DATE(SUBSTR(REGEXP_SUBSTR(ENQ_TYPE, '[^ ]+', 1,3),1,10),'DD/MM/YYYY') ")
                .append("ELSE TO_DATE(ENQ_DATE) END ENQ_DATE, TYPE EVENT,  REPLACE(REPLACE(ENQ_TYPE,'_',' '),'\"','') AMOUNT, ")
                .append("CASE WHEN SUBSTR(ENQ_TYPE,1,2)='IC' OR SUBSTR(ENQ_TYPE,1,3)='SCD' THEN 'Amret' WHEN SUBSTR(ENQ_TYPE,1,4)='LOLC' OR SUBSTR(ENQ_TYPE,1,3)='TPC' ")
                .append("THEN 'LOLC' WHEN LENGTH(ENQ_TYPE)=9 AND SUBSTR(ENQ_TYPE,8,2)='-1' THEN 'AMK' WHEN SUBSTR(ENQ_TYPE,1,3)='VFC' THEN 'Vision Fund'")
                .append(" WHEN SUBSTR(ENQ_TYPE,4,3) IN ('KHR','THB','USD') THEN 'HKL' WHEN SUBSTR(ENQ_TYPE,1,3)='SPN' THEN 'Sathapna'")
                .append(" WHEN SUBSTR(ENQ_TYPE,1,3)='GLF' THEN 'GL Finance' WHEN SUBSTR(ENQ_TYPE,1,3)<>'SPN' AND (SUBSTR(ENQ_TYPE,4,1) ")
                .append("LIKE '-' OR (SUBSTR(ENQ_TYPE,4,1)=' ' AND SUBSTR(ENQ_TYPE,5,1)='-')) THEN 'ACLEDA' WHEN SUBSTR(ENQ_TYPE,5,3)='201' ")
                .append("THEN 'ABA' WHEN ENQ_TYPE IN ('Leasing Company','Leasing Company/Guarantor') THEN 'Leasing Company' WHEN ENQ_TYPE IN ('MDI','MDI/Guarantor') ")
                .append("THEN 'MDI' WHEN ENQ_TYPE IN ('Specialized Bank','Specialized Bank/Guarantor') THEN 'Specialized Bank' WHEN ENQ_TYPE ")
                .append("IN ('Commercial Bank','Commercial Bank/Guarantor') THEN 'Commercial Bank' WHEN ENQ_DATE IN ('MFI','MFI/Guarantor') ")
                .append("THEN 'MFI' ELSE 'Other' END LENDER, ENQ_TYPE REFERENCE FROM CLTB_CLBAL_CBC_BK WHERE ACCOUNT_NUMBER IN (SELECT ACCOUNT_NUMBER ")
                .append("FROM PRAVW_LN_DETAIL_LOANS WHERE CUSTOMER_ID=:CUST_ID))A  UNION ALL  SELECT ACCOUNT_NUMBER,ENQ_DATE, CASE WHEN RANKA=1 THEN 'New Account' ")
                .append("ELSE 'Closed Account' END EVENT,NVL(AMOUNT,'')AMOUNT, LENDER,REFERENCE FROM (WITH G AS (")
                .append("SELECT CUSTOMER_ID,ACCOUNT_NUMBER,AMOUNT,DISBURSE_DATE,LENDER,REFERENCE,LAST_TRN_DATE FROM  (")
                .append("SELECT CUSTOMER_ID,A.ACCOUNT_NUMBER, A.CURRENCY ||' '|| AMOUNT_DISBURSED AMOUNT, A.DISBURSE_DATE, 'Prasac' Lender,A.ACCOUNT_NUMBER REFERENCE, LAST_TRN_DATE ")
                .append("FROM PRAVW_LN_DETAIL_LOANS A  WHERE A.CUSTOMER_ID=:CUST_ID ) WHERE CUSTOMER_ID=:CUST_ID ) ")
                .append("SELECT RANK() OVER(PARTITION BY ACCOUNT_NUMBER ORDER BY ENQ_DATE)RANKA,ACCOUNT_NUMBER,ENQ_DATE,AMOUNT,Lender,REFERENCE ")
                .append("FROM G  UNPIVOT (ENQ_DATE FOR Y IN (DISBURSE_DATE,LAST_TRN_DATE)) ))) ORDER BY ENQ_DATE) )").toString();
        final MapSqlParameterSource parameterSource = new MapSqlParameterSource("CUST_ID", clientCif);
        return jdbcTemplate.query(sql, parameterSource, rowMapper);
    }
}
