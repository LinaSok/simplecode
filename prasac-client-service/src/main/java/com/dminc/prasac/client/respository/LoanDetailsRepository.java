package com.dminc.prasac.client.respository;

import org.springframework.stereotype.Repository;

import com.dminc.prasac.client.model.entity.LoanDetails;

@Repository
public interface LoanDetailsRepository extends ReadOnlyRepository<LoanDetails, String> {

    LoanDetails findDistinctTopByClientCifOrderByLoanCycleDesc(String cif);
}
