package com.dminc.prasac.client.integration.cbc.domain.response;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "PE_NAME")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PreviousEnquiryName {

    @XmlElement(name = "PE_NMFA")
    private String khmerFirstName;

    @XmlElement(name = "PE_NM1A")
    private String khmerLastName;

    @XmlElement(name = "PE_NMFE")
    private String firstName;

    @XmlElement(name = "PE_NM1E")
    private String lastName;
}
