package com.dminc.prasac.client.service;

import com.dminc.prasac.client.model.dto.ClientLinkage;
import com.dminc.prasac.client.model.entity.ClientLinkageEntity;
import com.dminc.prasac.client.respository.ClientLinkageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ClientLinkageServiceImpl implements ClientLinkageService {

    private final ClientLinkageRepository repository;
    private final ObjectsConverter converter;

    @Override
    public List<ClientLinkage> getByClientCif(String cif) {
        final List<ClientLinkageEntity> linkages = repository.findByClientCif(cif);
        return converter.getObjectsMapper().map(linkages, ClientLinkage.class);
    }
}
