package com.dminc.prasac.client.model.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
@SuppressWarnings({"PMD.TooManyFields"})
public class LoanRequestHistoryDTO {

    private String identityNumber;

    private String clientCif;

    private String clientType;

    private String fullName;

    private String khmerFullname;

    private LocalDate dob;

    private String identityType;

    private String address;

    private String maritalStatus;

    private Integer no;

    private LocalDate requestDate;

    private Double requestAmount;

    private String currency;

    private String requestStatus;

    private String rejectReason;

}
