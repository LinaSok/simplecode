package com.dminc.prasac.client.model.dto;


import lombok.Data;

import java.time.LocalDate;

@Data
@SuppressWarnings({"PMD.TooManyFields"})
public class LoanHistoryDTO {

    private String accountNumber;
    private String clientCif;
    private String requestStatus;
    private String clientType;
    private LocalDate disbursementDate;
    private Double loanFee;
    private String currencyType;
    private Double interestRate;
    private Integer loanTerm;
    private Integer usedTerm;
    private String loanStatus;
    private Integer lockInPeriod;
    private String repaymentMode;
    private Integer lateInstallment;
    private String loanClassification;
    private Double paidOffAmount;
    private String loanPurpose;
    private Integer totalCollateral;
    private String collateralType;
    private String prasacScoring;
    private String cbcScoring;
    private String otherApplicants;
    private Integer loanCycle;
    private String loanPurposeCode;
    private String repaymentModeCode;
    private Integer aging;
    private String multipleLoanCode;
    private String multipleLoan;
    private String otherApplicants1;
    private String no;
}
