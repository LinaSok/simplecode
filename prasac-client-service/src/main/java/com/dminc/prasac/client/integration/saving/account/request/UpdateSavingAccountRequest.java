package com.dminc.prasac.client.integration.saving.account.request;

import com.dminc.prasac.client.model.dto.SavingAccount;
import com.dminc.prasac.client.model.dto.User;
import lombok.Builder;
import prasac.webapi.wsdl.saving.account.CustAccFullType;
import prasac.webapi.wsdl.saving.account.MODIFYCUSTACCFSFSREQ;

import java.math.BigDecimal;
import java.util.List;

public class UpdateSavingAccountRequest extends SavingAccountRequest {
    public static final String OPERATION = "ModifyCustAcc";
    private final SavingAccount savingAccount;
    private final CustAccFullType existingAccount;

    @Builder
    public UpdateSavingAccountRequest(String source, User user, SavingAccount savingAccount, CustAccFullType existingAccount) {
        super(source, user, OPERATION);
        this.savingAccount = savingAccount;
        this.existingAccount = existingAccount;
    }

    public MODIFYCUSTACCFSFSREQ getRequest() {
        final MODIFYCUSTACCFSFSREQ request = new MODIFYCUSTACCFSFSREQ();
        request.setFCUBSHEADER(populateHeader());
        populateBody(request);
        return request;
    }

    private void populateBody(MODIFYCUSTACCFSFSREQ request) {
        final MODIFYCUSTACCFSFSREQ.FCUBSBODY body = new MODIFYCUSTACCFSFSREQ.FCUBSBODY();
        populateAccount(body);
        request.setFCUBSBODY(body);
    }

    private void populateAccount(MODIFYCUSTACCFSFSREQ.FCUBSBODY body) {
        existingAccount.setADESC(savingAccount.getAccountDesc());
        existingAccount.setMODNO(existingAccount.getMODNO().add(BigDecimal.ONE)); // increase modify number
        populateAddress(existingAccount, savingAccount.getAddress());
        body.setCustAccountFull(existingAccount);
        populateJoiners(existingAccount);
    }

    private void populateJoiners(CustAccFullType account) {
        List<CustAccFullType.Jointholders> joins = getJoinHolders(savingAccount);
        List<CustAccFullType.Jointholders> jointholders = account.getJointholders();
        jointholders.clear();
        account.getJointholders().addAll(joins);
    }
}
