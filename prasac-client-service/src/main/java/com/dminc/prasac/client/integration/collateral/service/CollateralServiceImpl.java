package com.dminc.prasac.client.integration.collateral.service;

import com.dminc.prasac.client.integration.collateral.request.NewCollateralRequest;
import com.dminc.prasac.client.integration.collateral.request.QueryCollateralRequest;
import com.dminc.prasac.client.integration.collateral.request.ReOpenCollateralRequest;
import com.dminc.prasac.client.integration.collateral.request.UpdateCollateralRequest;
import com.dminc.prasac.client.integration.config.DefaultNamespacePrefixMapper;
import com.dminc.prasac.client.model.dto.Collateral;
import com.dminc.prasac.client.model.dto.CrmClient;
import com.dminc.prasac.client.model.dto.User;
import com.dminc.prasac.client.service.CrmClientService;
import com.dminc.prasac.client.service.FlexCubeUserService;
import com.dminc.rest.exceptions.BusinessException;
import com.dminc.rest.exceptions.InternalBusinessException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;
import prasac.webapi.wsdl.collteral.CREATECOLLATERALFSFSRES;
import prasac.webapi.wsdl.collteral.CollateralFullType;
import prasac.webapi.wsdl.collteral.ERRORDETAILSType;
import prasac.webapi.wsdl.collteral.ERRORType;
import prasac.webapi.wsdl.collteral.MODIFYCOLLATERALFSFSRES;
import prasac.webapi.wsdl.collteral.QUERYCOLLATERALIOFSRES;
import prasac.webapi.wsdl.collteral.REOPENCOLLATERALIOPKRES;

import javax.annotation.PostConstruct;
import javax.xml.bind.Marshaller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class CollateralServiceImpl implements CollateralService {
    private static final String SOAP_PATH = "/FLEXCUBE-ELCMWeb/ELCollteralService";

    private final DefaultNamespacePrefixMapper defaultNamespacePrefixMapper;
    private final FlexCubeUserService userService;
    private final CrmClientService clientService;
    @Value("${prasac.soap.service.url}")
    private String domain;
    @Value("${prasac.soap.service.source}")
    private String source;
    private WebServiceTemplate template;
    private String soapUrl;

    @PostConstruct
    public void init() {
        soapUrl = String.format("%s%s", domain, SOAP_PATH);
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("prasac.webapi.wsdl.collteral");
        Map<String, Object> properties = new HashMap<>();
        properties.put(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        properties.put("com.sun.xml.bind.namespacePrefixMapper", defaultNamespacePrefixMapper);
        marshaller.setMarshallerProperties(properties);
        template = new WebServiceTemplate();
        template.setDefaultUri(domain);
        template.setMarshaller(marshaller);
        template.setUnmarshaller(marshaller);
    }

    @Override
    public void createOrUpdate(Collateral collateral) throws BusinessException {
        final User user = userService.getUserByBranch(collateral.getBranchCode());
        final CrmClient client = clientService.getOneByCif(collateral.getClientCif());
        final CollateralFullType existing = getExistingCollateral(user, collateral.getCollateralCode(), client.getCif());
        if (existing == null) {
            create(collateral, user, client.getLiabId());
        } else {
            update(collateral, user, existing, client.getLiabId());
        }
    }

    private void update(Collateral collateral, User user, CollateralFullType existing, String liabId) throws InternalBusinessException {
        if (existing.getTXNSTAT().equals("C")) {
            reOpen(user, collateral.getCollateralCode(), liabId);
        }
        log.info("============================ update ======================= \n");
        final UpdateCollateralRequest updateCollateralRequest = UpdateCollateralRequest.builder()
                .collateral(collateral)
                .existing(existing)
                .source(source)
                .user(user)
                .build();
        final MODIFYCOLLATERALFSFSRES response = (MODIFYCOLLATERALFSFSRES) template.marshalSendAndReceive(soapUrl, updateCollateralRequest.getRequest());
        checkError(response.getFCUBSBODY().getFCUBSERRORRESP());
    }

    private void create(Collateral collateral, User user, String liabId) throws InternalBusinessException {
        log.info("============================ create ======================= \n");
        final NewCollateralRequest collateralRequest = NewCollateralRequest.builder()
                .collateral(collateral)
                .source(source)
                .user(user)
                .liabId(liabId)
                .build();
        final CREATECOLLATERALFSFSRES response = (CREATECOLLATERALFSFSRES) template.marshalSendAndReceive(soapUrl, collateralRequest.getRequest());
        checkError(response.getFCUBSBODY().getFCUBSERRORRESP());
    }

    private void checkError(final List<ERRORType> types) throws InternalBusinessException {
        final StringBuilder errorMessage = new StringBuilder();
        types.forEach(type -> type.getERROR().forEach(error -> {
            errorMessage.append(String.format("\n %s : %s", error.getECODE(), error.getEDESC()));
        }));
        if (StringUtils.isNotEmpty(errorMessage)) {
            throw new InternalBusinessException(errorMessage.toString(), 7000);
        }
    }

    private CollateralFullType getExistingCollateral(User user, final String code, final String clientCif) throws InternalBusinessException {
        log.info("============================ get existing ======================= \n");
        final QueryCollateralRequest collateralRequest = QueryCollateralRequest.builder()
                .code(code)
                .source(source)
                .clientCif(clientCif)
                .user(user)
                .build();
        final QUERYCOLLATERALIOFSRES response = (QUERYCOLLATERALIOFSRES) template.marshalSendAndReceive(soapUrl, collateralRequest.getRequest());
        final List<ERRORType> errorTypes = response.getFCUBSBODY().getFCUBSERRORRESP();
        for (ERRORType errorType : errorTypes) {
            for (ERRORDETAILSType e : errorType.getERROR()) {
                if ("NO-DATA".equals(e.getECODE())) {
                    return null;
                }
            }
        }
        checkError(errorTypes);
        return response.getFCUBSBODY().getCollateralsFull();
    }

    private void reOpen(User user, final String code, final String liabId) throws InternalBusinessException {
        log.info("============================ re-open ======================= \n");
        final ReOpenCollateralRequest collateralRequest = ReOpenCollateralRequest.builder()
                .user(user)
                .code(code)
                .liabId(liabId)
                .source(source)
                .build();
        final REOPENCOLLATERALIOPKRES response = (REOPENCOLLATERALIOPKRES) template.marshalSendAndReceive(soapUrl, collateralRequest.getRequest());
        checkError(response.getFCUBSBODY().getFCUBSERRORRESP());
    }
}
