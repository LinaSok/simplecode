package com.dminc.prasac.client.integration.cbc.domain.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "CONSUMER")
@XmlAccessorType(XmlAccessType.FIELD)
public class Consumer {

    @XmlElement(name = "CAPL")
    private ClientType clientType;

    @XmlElement(name = "CID", type = Identification.class)
    private Identification identification;

    @XmlElement(name = "CDOB", type = CustomerDOB.class)
    private CustomerDOB customerDOB;

    @XmlElement(name = "CPLB", type = PlaceOfBirth.class)
    private PlaceOfBirth placeOfBirth;

    @XmlElement(name = "CGND")
    private String gender;

    @XmlElement(name = "CMAR")
    private String maritalStatus;

    @XmlElement(name = "CNAT")
    private String nationalityCode;

    @XmlElement(name = "CNAM", type = CustomerName.class)
    private CustomerName customerName;
    @XmlElement(name = "CADR", type = Address.class)
    private Address address;

    @XmlElement(name = "CCNT", type = CustomerContact.class)
    private CustomerContact customerContact;

    @XmlElement(name = "CEMP", type = Occupation.class)
    private Occupation occupation;

    @XmlEnum
    public enum ClientType {
        @XmlEnumValue("P")
        BORROWER("P"),
        @XmlEnumValue("P")
        CO_BORROWER("P"),
        @XmlEnumValue("G")
        GUARANTOR("G");

        private String type;

        ClientType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }
}
