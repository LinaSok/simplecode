package com.dminc.prasac.client.integration.collateral.request;

import java.math.BigDecimal;

import com.dminc.prasac.client.model.dto.User;

import lombok.Builder;
import prasac.webapi.wsdl.elpool.CollateralpoolQueryIOType;
import prasac.webapi.wsdl.elpool.QUERYCOLLATERALPOOLIOFSREQ;

public class QueryCollateralPoolRequest extends CollateralPoolRequest {
    public static final String OPERATION = "QueryCollateralPool";

    private final String poolCode;
    private final String liabId;

    @Builder
    public QueryCollateralPoolRequest(String source, User user, String poolCode, String liabId) {
        super(OPERATION, source, user);
        this.poolCode = poolCode;
        this.liabId = liabId;
    }

    public QUERYCOLLATERALPOOLIOFSREQ getRequest() {
        final QUERYCOLLATERALPOOLIOFSREQ request = new QUERYCOLLATERALPOOLIOFSREQ();
        request.setFCUBSHEADER(populateHeader());
        final QUERYCOLLATERALPOOLIOFSREQ.FCUBSBODY body = new QUERYCOLLATERALPOOLIOFSREQ.FCUBSBODY();
        final CollateralpoolQueryIOType query = new CollateralpoolQueryIOType();
        query.setPOOLCODE(poolCode);
        query.setLIABID(new BigDecimal(liabId));

        body.setCollateralsPoolIO(query);
        request.setFCUBSBODY(body);
        return request;
    }
}
