package com.dminc.prasac.client.service;

import com.dminc.prasac.client.model.dto.LoanHistoryDTO;
import com.dminc.prasac.client.model.dto.LoanHistoryInfoDTO;
import com.dminc.rest.exceptions.BusinessException;

import java.util.List;

public interface LoanHistoryService {
    List<LoanHistoryDTO> getLoanHistoryByClient(String userCif) throws BusinessException;

    List<LoanHistoryInfoDTO> getLoanHistoriesInfoByClient(List<String> clientCifs);

    LoanHistoryDTO getLastLoan(String clientCif);
}
