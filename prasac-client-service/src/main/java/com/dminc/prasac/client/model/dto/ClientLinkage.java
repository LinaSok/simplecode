package com.dminc.prasac.client.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClientLinkage implements Serializable {

    private static final long serialVersionUID = -2904896118765711394L;
    private String clientCif;

    private String accountClass;

    private String savingAccountNumber;

    private LocalDate savingAccountOpenDate;

    private String branchCode;

    private String recordSate;

    private String savingAccountState;

    private List<String> collateralInputs;

    private String collateralPoolCode;

    private String currencyCode;
}
