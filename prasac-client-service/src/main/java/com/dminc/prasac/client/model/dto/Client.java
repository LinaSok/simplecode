package com.dminc.prasac.client.model.dto;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@SuppressWarnings({"PMD.ImmutableField", "PMD.TooManyFields"})
@Data
public class Client implements Serializable {

    private static final long serialVersionUID = 8077738328458909908L;
    private Long id;

    private String cif;

    private LocalDate dob;

    private String fullName;

    private Double incomeAmount;

    private String khmerFullName;

    private Integer periodInCurrentAddress;

    private String phone;

    private Integer workingPeriod;

    private String workplaceName;

    private String shortName;

    private String clientOccupation;

    private String gender;

    private String maritalStatus;

    private String nationality;

    private Address birthAddress;

    private Address currentAddress;

    private Address workplaceAddress;

    private String branch;

    private String staffId;

    private String liabilityNo;

    private String clientType;

    private String savingAccountNumber;

    private List<ClientIdentification> identifications;
}
