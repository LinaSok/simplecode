package com.dminc.prasac.client.integration.cbc.domain.response;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;

@XmlRootElement(name = "GRT_ACC_DETAIL")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class GuarantorDetail {

    @XmlElement(name = "GRT_CRDTR")
    private String creditor;

    @XmlElement(name = "GRT_PRD")
    private String purposeCode;

    @XmlElement(name = "GRT_CURR")
    private String currencyCode;

    @XmlElement(name = "GRT_LIMIT")
    private Double disbursementAmount;

    @XmlElement(name = "GRT_ISSU_DT")
    @XmlJavaTypeAdapter(DateAdapter.class)
    private LocalDate disbursementDate;

    @XmlElement(name = "GRT_PROD_EXP_DT")
    @XmlJavaTypeAdapter(DateAdapter.class)
    private LocalDate maturityDate;

    @XmlElement(name = "GRT_STATUS")
    private String status;

    @XmlElement(name = "GRT_CLSD_DT")
    @XmlJavaTypeAdapter(DateAdapter.class)
    private LocalDate closeDate;

    @XmlElement(name = "GRT_CUB")
    private Double outstanding;

    @XmlElement(name = "GRT_SUMMRY")
    private String payment;
}
