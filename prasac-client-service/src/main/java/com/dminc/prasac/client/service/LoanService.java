package com.dminc.prasac.client.service;

import java.util.List;

import com.dminc.rest.exceptions.BusinessException;

public interface LoanService {
    List getLoanStatement(String clientCif) throws BusinessException;

    List getLoanRepaymentScheduleStatement(String clientCif);
}
