package com.dminc.prasac.client.controller;

import com.dminc.prasac.client.integration.collateral.service.CollateralService;
import com.dminc.prasac.client.model.dto.Collateral;
import com.dminc.rest.exceptions.BusinessException;
import io.swagger.annotations.ApiImplicitParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.dminc.prasac.client.config.SwaggerDocumentation.HEADER;
import static com.dminc.prasac.client.config.SwaggerDocumentation.X_AUTHORIZATION;

@RestController
@RequestMapping("api/v1/collateral")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CollateralController {

    private final CollateralService collateralService;

    @PostMapping()
    @ApiImplicitParam(paramType = HEADER, name = X_AUTHORIZATION, value = "for DevServer UsnSWbnYK26hHc7KUcaM")
    public void create(@RequestBody Collateral collateral) throws BusinessException {
        collateralService.createOrUpdate(collateral);
    }
}
