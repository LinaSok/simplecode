package com.dminc.prasac.client.controller;

import com.dminc.prasac.client.model.dto.CreditHistoryMonitoring;
import com.dminc.prasac.client.service.CreditHistoryMonitoringService;
import io.swagger.annotations.ApiImplicitParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.dminc.prasac.client.config.SwaggerDocumentation.HEADER;
import static com.dminc.prasac.client.config.SwaggerDocumentation.X_AUTHORIZATION;

@RestController
@RequestMapping("api/v1/credit")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CreditController {

    private final CreditHistoryMonitoringService service;

    @GetMapping("/history-monitoring/{clientCif}")
    @ApiImplicitParam(paramType = HEADER, name = X_AUTHORIZATION, value = "for DevServer UsnSWbnYK26hHc7KUcaM")
    public List<CreditHistoryMonitoring> getLoanHistory(@PathVariable("clientCif") String clientCif) {
        return service.getCreditHistoryMonitoringsByClient(clientCif);
    }
}
