package com.dminc.prasac.client.integration.cbc.domain.response;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "ACC_DETAILS")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class AccountDetailWrapper {

    @XmlElement(name = "ACC_DETAIL", type = AccountDetail.class)
    private List<AccountDetail> accountDetails;

}
