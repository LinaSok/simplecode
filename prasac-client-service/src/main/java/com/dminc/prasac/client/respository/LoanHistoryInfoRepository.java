package com.dminc.prasac.client.respository;

import com.dminc.prasac.client.model.entity.LoanHistoryInfo;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LoanHistoryInfoRepository extends ReadOnlyRepository<LoanHistoryInfo, String> {

    List<LoanHistoryInfo> findByClientCifInAndLoanStatusAndInstitutionName(List<String> clientCifs, String status, String institutionName, Pageable pageable);

    LoanHistoryInfo findTopByClientCifAndInstitutionNameAndLoanStatusNotOrderByLoanCycleDesc(String clientCif, String institutionName, String loanStatus);
}
