package com.dminc.prasac.client.config;

public final class SwaggerDocumentation {

    public static final String AUTH_HEADER = "Client authentication header starting by bearer ${access_token}";
    public static final String HEADER = "header";
    public static final String AUTHORIZATION = "Authorization";
    public static final String X_AUTHORIZATION = "X-Authorization";

    private SwaggerDocumentation() {
        //No createOrUpdate
    }
}
