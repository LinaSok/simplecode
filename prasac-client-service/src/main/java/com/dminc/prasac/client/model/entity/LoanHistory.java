package com.dminc.prasac.client.model.entity;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "PRAVW_LN_CREDIT_HISTORY")
@Data
@SuppressWarnings({"PMD.TooManyFields"})
public class LoanHistory {

    @Id
    @Column(name = "ACCOUNT_NUMBER")
    private String accountNumber;

    @Column(name = "CUSTOMER_ID")
    private String clientCif;

    @Column(name = "REQUEST_STATUS")
    private String requestStatus;

    @Column(name = "APPLICANT_TYPE")
    private String clientType;

    @Column(name = "DISBURSMENT_DATE")
    private LocalDate disbursementDate;

    @Column(name = "AMOUNT_DISBURSED")
    private Double loanFee;

    @Column(name = "CURRENCY")
    private String currencyType;

    @Column(name = "INT_REATE")
    private Double interestRate;

    @Column(name = "LOAN_TTERM")
    private Integer loanTerm;

    @Column(name = "USE_TERM")
    private Integer usedTerm;

    @Column(name = "LOAN_STATUS")
    private String loanStatus;

    @Column(name = "LOCK_IN_PERIOD")
    private Integer lockInPeriod;

    @Column(name = "REPAYMENT_MODE")
    private String repaymentMode;

    @Column(name = "NUMBER_OF_LATE")
    private Integer lateInstallment;

    @Column(name = "LOAN_CLASSIFICATION")
    private String loanClassification;

    @Column(name = "PAID_OFF_AMOUNT")
    private Double paidOffAmount;

    @Column(name = "PURPOSE")
    private String loanPurpose;

    @Column(name = "COLL_NUM")
    private Integer totalCollateral;

    @Column(name = "COLLATERAL_CATEGORY")
    private String collateralType;

    @Column(name = "PRASAC_SCORING")
    private String prasacScoring;

    @Column(name = "CBC_SCORING")
    private String cbcScoring;

    @Column(name = "OTHER_APPLICANTS")
    private String otherApplicants;

    @Column(name = "LOAN_CYCLE")
    private Integer loanCycle;

    @Column(name = "PURPOSE_CODE")
    private String loanPurposeCode;

    @Column(name = "REPAYMENT_CODE")
    private String repaymentModeCode;

    @Column(name = "AGING")
    private Integer aging;

    @Column(name = "MULTIPLE_LOAN_CODE")
    private String multipleLoanCode;

    @Column(name = "MULTIPLE_LOAN_DESC")
    private String multipleLoan;

    @Column(name = "OTHER_APPLICANTS_1")
    private String otherApplicants1;

    @Column(name = "NO")
    private String no;
}
