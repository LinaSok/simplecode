package com.dminc.prasac.client.controller;

import com.dminc.prasac.client.integration.client.service.ClientService;
import com.dminc.prasac.client.model.dto.Client;
import com.dminc.prasac.client.model.dto.ClientLinkage;
import com.dminc.prasac.client.model.dto.CrmClient;
import com.dminc.prasac.client.model.dto.IdentityRequest;
import com.dminc.prasac.client.model.dto.SearchClientRequest;
import com.dminc.prasac.client.service.ClientLinkageService;
import com.dminc.prasac.client.service.CrmClientService;
import com.dminc.rest.exceptions.BusinessException;
import com.dminc.rest.exceptions.ItemNotFoundBusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

import static com.dminc.prasac.client.config.SwaggerDocumentation.HEADER;
import static com.dminc.prasac.client.config.SwaggerDocumentation.X_AUTHORIZATION;

@RestController
@RequestMapping("api/v1/clients")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Api(tags = "Prasac client management", description = "Manage information about Prasac clients")
public class CrmClientController {
    private static final String HEADER_INFO = "for DevServer UsnSWbnYK26hHc7KUcaM";
    private final CrmClientService service;
    private final ClientService clientService;
    private final ClientLinkageService clientLinkageService;

    @GetMapping("/search")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = HEADER, name = X_AUTHORIZATION, value = HEADER_INFO)})
    public List<CrmClient> search(SearchClientRequest request) throws BusinessException {
        return service.search(request);
    }

    @GetMapping("/searchByIdentity")
    @ApiImplicitParam(paramType = HEADER, name = X_AUTHORIZATION, value = HEADER_INFO)
    public CrmClient search(@Valid @ModelAttribute IdentityRequest request, HttpServletResponse response) throws BusinessException {
        return service.findByIdNumberAndIdType(request);
    }

    @GetMapping("/{cif}")
    @ApiImplicitParam(paramType = HEADER, name = X_AUTHORIZATION, value = HEADER_INFO)
    public CrmClient getDetailByCif(@PathVariable("cif") String cif) throws BusinessException {
        final CrmClient clientInfo = service.getOneByCif(cif);
        if (clientInfo == null) {
            throw new ItemNotFoundBusinessException("Client not found");
        }
        return service.getOneByCif(cif);
    }

    @GetMapping("/{cif}/linkage")
    @ApiImplicitParam(paramType = HEADER, name = X_AUTHORIZATION, value = HEADER_INFO)
    public List<ClientLinkage> getLinkageByCif(@PathVariable("cif") String cif) {
        return clientLinkageService.getByClientCif(cif);
    }

    @PostMapping("")
    public Client create(@RequestBody Client client) throws BusinessException {
        return clientService.create(client);
    }
}
