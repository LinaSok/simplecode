package com.dminc.prasac.client.integration.loan;

import java.util.HashMap;
import java.util.Map;

import static com.dminc.prasac.client.model.LocalConstants.KHMER_CURRENCY;
import static com.dminc.prasac.client.model.LocalConstants.THAI_CURRENCY;
import static com.dminc.prasac.client.model.LocalConstants.US_DOLLAR;

@SuppressWarnings({"PMD"})
public final class ProductCodeHelper {
    public static final String INDIVIDUAL_SHORT_TERM_LOAN_KHR = "INDIVIDUAL SHORT TERM LOAN_KHR";
    public static final String INDIVIDUAL_SHORT_TERM_LOAN_USD = "INDIVIDUAL SHORT TERM LOAN_USD";
    public static final String INDIVIDUAL_SHORT_TERM_LOAN_THB = "INDIVIDUAL SHORT TERM LOAN_THB";
    public static final String INDIVIDUAL_SHORT_TERM_LOAN_KHR_EMI = "INDIVIDUAL SHORT TERM LOAN KHR EMI";
    public static final String INDIVIDUAL_SHORT_TERM_LOAN_USD_EMI = "INDIVIDUAL SHORT TERM LOAN USD EMI";
    public static final String INDIVIDUAL_SHORT_TERM_LOAN_THB_EMI = "INDIVIDUAL SHORT TERM LOAN THB EMI";
    public static final String INDIVIDUAL_LONG_TERM_LOANS_KHR = "INDIVIDUAL LONG TERM LOANS_KHR";
    public static final String INDIVIDUAL_LONG_TERM_LOANS_USD = "INDIVIDUAL LONG TERM LOANS_USD";
    public static final String INDIVIDUAL_LONG_TERM_LOANS_THB = "INDIVIDUAL LONG TERM LOANS_THB";
    public static final String INDIVIDUAL_LONG_TERM_LOAN_KHR_EMI = "INDIVIDUAL LONG TERM LOAN KHR EMI";
    public static final String INDIVIDUAL_LONG_TERM_LOAN_USD_EMI = "INDIVIDUAL LONG TERM LOAN USD EMI";
    public static final String INDIVIDUAL_LONG_TERM_LOAN_THB_EMI = "INDIVIDUAL LONG TERM LOAN THB EMI";
    private static final String EQUAL_MONTHLY_INSTALLMENT = "02"; // repayment mode EMI
    private static final Map<String, String> PRODUCT_CODE = new HashMap<>();

    static {
        PRODUCT_CODE.putIfAbsent(INDIVIDUAL_SHORT_TERM_LOAN_KHR, "7101");
        PRODUCT_CODE.putIfAbsent(INDIVIDUAL_SHORT_TERM_LOAN_USD, "7102");
        PRODUCT_CODE.putIfAbsent(INDIVIDUAL_SHORT_TERM_LOAN_THB, "7105");
        PRODUCT_CODE.putIfAbsent(INDIVIDUAL_SHORT_TERM_LOAN_KHR_EMI, "7131");
        PRODUCT_CODE.putIfAbsent(INDIVIDUAL_SHORT_TERM_LOAN_USD_EMI, "7132");
        PRODUCT_CODE.putIfAbsent(INDIVIDUAL_SHORT_TERM_LOAN_THB_EMI, "7135");
        PRODUCT_CODE.putIfAbsent(INDIVIDUAL_LONG_TERM_LOANS_KHR, "7201");
        PRODUCT_CODE.putIfAbsent(INDIVIDUAL_LONG_TERM_LOANS_USD, "7202");
        PRODUCT_CODE.putIfAbsent(INDIVIDUAL_LONG_TERM_LOANS_THB, "7205");
        PRODUCT_CODE.putIfAbsent(INDIVIDUAL_LONG_TERM_LOAN_KHR_EMI, "7231");
        PRODUCT_CODE.putIfAbsent(INDIVIDUAL_LONG_TERM_LOAN_USD_EMI, "7232");
        PRODUCT_CODE.putIfAbsent(INDIVIDUAL_LONG_TERM_LOAN_THB_EMI, "7235");
    }

    private ProductCodeHelper() {
        // do nothing
    }

    public static String getProductCode(Integer tenure, String currencyCode, String repaymentMode) {
        switch (currencyCode) {
            case KHMER_CURRENCY:
                if (repaymentMode.equals(EQUAL_MONTHLY_INSTALLMENT)) {
                    if (tenure < 12) {
                        return PRODUCT_CODE.get(INDIVIDUAL_SHORT_TERM_LOAN_KHR_EMI);
                    } else {
                        return PRODUCT_CODE.get(INDIVIDUAL_LONG_TERM_LOAN_KHR_EMI);
                    }
                } else {
                    if (tenure < 12) {
                        return PRODUCT_CODE.get(INDIVIDUAL_SHORT_TERM_LOAN_KHR);
                    } else {
                        return PRODUCT_CODE.get(INDIVIDUAL_LONG_TERM_LOANS_KHR);
                    }
                }
            case US_DOLLAR:
                if (repaymentMode.equals(EQUAL_MONTHLY_INSTALLMENT)) {
                    if (tenure < 12) {
                        return PRODUCT_CODE.get(INDIVIDUAL_SHORT_TERM_LOAN_USD_EMI);
                    } else {
                        return PRODUCT_CODE.get(INDIVIDUAL_LONG_TERM_LOAN_USD_EMI);
                    }
                } else {
                    if (tenure < 12) {
                        return PRODUCT_CODE.get(INDIVIDUAL_SHORT_TERM_LOAN_USD);
                    } else {
                        return PRODUCT_CODE.get(INDIVIDUAL_LONG_TERM_LOANS_USD);
                    }
                }
            case THAI_CURRENCY:
                if (repaymentMode.equals(EQUAL_MONTHLY_INSTALLMENT)) {
                    if (tenure < 12) {
                        return PRODUCT_CODE.get(INDIVIDUAL_SHORT_TERM_LOAN_THB_EMI);
                    } else {
                        return PRODUCT_CODE.get(INDIVIDUAL_LONG_TERM_LOAN_THB_EMI);
                    }
                } else {
                    if (tenure < 12) {
                        return PRODUCT_CODE.get(INDIVIDUAL_SHORT_TERM_LOAN_THB);
                    } else {
                        return PRODUCT_CODE.get(INDIVIDUAL_LONG_TERM_LOANS_THB);
                    }
                }
            default:
                return null;
        }

    }
}
