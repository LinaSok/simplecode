package com.dminc.prasac.client.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@SuppressWarnings({"PMD.TooManyFields"})
public class LoanHistoryInfoDTO {

    private String accountNumber;

    private String institutionName;

    private Integer loanCycle;

    private LocalDate disbursementDate;

    private LocalDate maturityDate;

    private String rawLoanPurpose;

    private String loanTypeCode;

    private Double loanLimit;

    private Double loanOutStanding;

    private String currencyCode;

    private Integer tenureMonthly;

    private Integer lengthOfUsing;

    private Integer lockInPeriodMonthly;

    private String interestRateMonthly;

    private String rawRepaymentMode;

    private Integer mortgagedCollateralNumber;

    private Double firstPrincipalRepayment;

    private Double monthlyInterestRepayment;

    private Integer numberOfLateDays;

    private Integer numberOfLateInstallments;

    private Double penaltyAmountForPaidOff;

    private String clientCif;

    private String loanPurposeCode;

    private String repaymentCode;

    private String loanStatus;
}
