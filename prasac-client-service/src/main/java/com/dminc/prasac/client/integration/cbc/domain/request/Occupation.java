package com.dminc.prasac.client.integration.cbc.domain.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "CEMP")
@XmlAccessorType(XmlAccessType.FIELD)
public class Occupation {

    @XmlElement(name = "ETYP")
    private String employmentType;

    @XmlElement(name = "ESLF")
    private String selfEmploy;

    @XmlElement(name = "EOCE")
    private String occupationCode;

    @XmlElement(name = "ELEN")
    private String tenure;

    @XmlElement(name = "ETMS")
    private String annualIncome;

    @XmlElement(name = "ECURR")
    private String incomeCurrency;

    @XmlElement(name = "ENME")
    private String name;

    @XmlElement(name = "EADR", type = WorkingAddress.class)
    private WorkingAddress workingAddress;


}
