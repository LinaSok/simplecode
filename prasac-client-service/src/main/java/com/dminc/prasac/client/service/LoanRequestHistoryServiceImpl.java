package com.dminc.prasac.client.service;

import com.dminc.prasac.client.model.dto.LoanRequestHistoryDTO;
import com.dminc.prasac.client.model.entity.LoanRequestHistory;
import com.dminc.prasac.client.respository.LoanRequestHistoryRepository;
import com.dminc.rest.exceptions.BusinessException;
import com.dminc.rest.mapping.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LoanRequestHistoryServiceImpl implements LoanRequestHistoryService {

    private final LoanRequestHistoryRepository repository;

    private final ObjectsConverter converter;

    @Override
    public List<LoanRequestHistoryDTO> getByClientCif(String clientCif) throws BusinessException {
        final List<LoanRequestHistory> loans = repository.findByClientCif(clientCif);
        return converter.runServiceTask((Mapper mapper) -> mapper.map(loans, LoanRequestHistoryDTO.class));
    }
}
