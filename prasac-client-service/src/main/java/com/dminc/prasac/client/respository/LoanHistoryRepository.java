package com.dminc.prasac.client.respository;

import com.dminc.prasac.client.model.entity.LoanHistory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LoanHistoryRepository extends ReadOnlyRepository<LoanHistory, String> {

    List<LoanHistory> findByClientCifOrderByNo(String clientCif);

    LoanHistory findTopByClientCifOrderByLoanCycleDesc(String cif);
}
