package com.dminc.prasac.client.respository;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.internal.OracleTypes;

@Repository
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LoanPaymentRepositoryImpl implements LoanPaymentRepository {
    public static final String P_ACCOUNT_NUMBER = "P_ACCOUNT_NUMBER";
    public static final String REF_CURSOR_NAME = "PRC";

    //
    //    private static final RowMapper<LoanRepaymentSchedule> REPAYMENT_MAPPER = (rs, rowNum) -> LoanRepaymentSchedule.builder()
    //            .customerId(rs.getString("CUSTOMER_ID"))
    //            .loanNumber(rs.getString("LOAN_NUMBER"))
    //            .build();

    private final DataSource dataSource;

    //
    //    PROCEDURE LN_LOAN_STATEMENT
    //    Argument Name Type In/Out Default?
    //            ---------------- ---------- ------ --------
    //    PRC REF CURSOR OUT unknown
    //    P_ACCOUNT_NUMBER VARCHAR2 IN unknown
    //
    public List triggerLoanStatemment(String accountNumber) {
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("LN_LOAN_STATEMENT")
                .withoutProcedureColumnMetaDataAccess().useInParameterNames(P_ACCOUNT_NUMBER)
                .declareParameters(new SqlOutParameter(REF_CURSOR_NAME, OracleTypes.CURSOR),
                        new SqlParameter(P_ACCOUNT_NUMBER, Types.VARCHAR));

        final SqlParameterSource in = new MapSqlParameterSource(P_ACCOUNT_NUMBER, accountNumber);

        return executeProcedure(jdbcCall, in, REF_CURSOR_NAME);
    }

    private List executeProcedure(SimpleJdbcCall jdbcCall, SqlParameterSource in, String outputName) {
        final Map<String, Object> out = jdbcCall.execute(in);
        return (List) out.get(outputName);
    }

    /*
       PRC REF CURSOR OUT unknown
       P_ACCOUNT_NUMBER VARCHAR2 IN unknown
       PROCEDURE LN_REPAYMENT_SCHEDULED*/
    public List triggerRepaymentSchedule(String accountNumber) {
        final SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("LN_REPAYMENT_SCHEDULED")
                .withoutProcedureColumnMetaDataAccess().useInParameterNames(P_ACCOUNT_NUMBER)
                .declareParameters(new SqlOutParameter(REF_CURSOR_NAME, OracleTypes.CURSOR),
                        new SqlParameter(P_ACCOUNT_NUMBER, Types.VARCHAR));

        final SqlParameterSource in = new MapSqlParameterSource(P_ACCOUNT_NUMBER, accountNumber);
        return executeProcedure(jdbcCall, in, REF_CURSOR_NAME);

    }
}
