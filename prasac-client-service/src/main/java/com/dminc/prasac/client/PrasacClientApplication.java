package com.dminc.prasac.client;

import com.dminc.prasac.client.config.filter.BasicAuthenticationFilter;
import com.dminc.prasac.client.config.filter.ClientAuthenticationFilter;
import com.dminc.prasac.client.config.filter.TracerRequestFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

import javax.servlet.Filter;
import java.util.TimeZone;

@SpringBootApplication
public class PrasacClientApplication {

    @Autowired
    private Environment env;

    public static void main(String[] args) {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        SpringApplication.run(PrasacClientApplication.class, args);
    }


    @Bean
    public FilterRegistrationBean requestDumperFilter() {
        final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setName("tracingLogfilter");
        final Filter filter = new TracerRequestFilter();
        registrationBean.setFilter(filter);
        registrationBean.addUrlPatterns("/api/*");
        registrationBean.setOrder(1);
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean clientAuthenticationFilter() {
        final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setName("clientAuthenticationFilter");
        final Filter filter = new ClientAuthenticationFilter(env.getProperty("x-authorization"));
        registrationBean.setFilter(filter);
        registrationBean.addUrlPatterns("/api/*");
        registrationBean.setOrder(2);
        return registrationBean;
    }


    @Bean()
    public FilterRegistrationBean basicAuthenticationFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        final Filter filter = new BasicAuthenticationFilter();
        registration.addInitParameter("username", env.getProperty("spring.security.username"));
        registration.addInitParameter("secret", env.getProperty("spring.security.password"));
        registration.setFilter(filter);
        registration.addUrlPatterns("/swagger-ui.html");
        registration.addUrlPatterns("/actuator/*");
        registration.setOrder(4);

        return registration;
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
