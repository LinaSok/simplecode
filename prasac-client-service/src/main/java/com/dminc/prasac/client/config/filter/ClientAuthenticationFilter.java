package com.dminc.prasac.client.config.filter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.OncePerRequestFilter;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class ClientAuthenticationFilter extends OncePerRequestFilter {
    public static final String AUTHORIZATION = "X-Authorization";

    String secret;

    @Override
    @SneakyThrows
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) {
        final String xAuth = request.getHeader(AUTHORIZATION);
        final String uriPath = request.getRequestURI();
        if (!"/api/healthcheck/v1".equals(uriPath) && !isValid(xAuth)) {
            response.sendError(HttpServletResponse.SC_FORBIDDEN, "403 Forbidden, The Header is missing or the key value is invalid.");
            return;

        }
        filterChain.doFilter(request, response);
    }

    private boolean isValid(String xAuth) {
        return secret.equals(xAuth);
    }
}
