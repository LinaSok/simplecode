package com.dminc.prasac.client.integration.config;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class DefaultNamespacePrefixMapper extends NamespacePrefixMapper {
    private final Map<String, String> namespaceMap;

    public DefaultNamespacePrefixMapper() {
        namespaceMap = new HashMap<>();
        namespaceMap.put("http://fcubs.ofss.com/service/FCUBSCustomerService", "fcub");
        namespaceMap.put("http://fcubs.ofss.com/service/ELCollteralService", "elc");
        namespaceMap.put("http://fcubs.ofss.com/service/FCUBSAccService", "fcub");
        namespaceMap.put("http://fcubs.ofss.com/service/ELPoolService", "elp");
        namespaceMap.put("http://fcubs.ofss.com/service/FCUBSCLService", "fcub");
    }

    @Override
    public String getPreferredPrefix(String nameUrl, String suggestion, boolean required) {
        return namespaceMap.getOrDefault(nameUrl, suggestion);
    }
}
