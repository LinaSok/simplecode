package com.dminc.prasac.client.model.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "PRAVW_LN_CREDIT_HISTORY_INFO")
@Data
@SuppressWarnings({"PMD.TooManyFields"})
public class LoanHistoryInfo {
    @Id
    @Column(name = "ACCOUNT_NUMBER")
    private String accountNumber;

    @Column(name = "INSTITUTION_NAME")
    private String institutionName;

    @Column(name = "LOAN_CYCLE")
    private Integer loanCycle;

    @Column(name = "DISBURSMENT_DATE")
    private LocalDate disbursementDate;

    @Column(name = "MATURITY_DATE")
    private LocalDate maturityDate;

    @Column(name = "LOAN_PURPOSE")
    private String rawLoanPurpose;

    @Column(name = "LOAN_TYPE")
    private String loanTypeCode;

    @Column(name = "LOAN_SIZE")
    private Double loanLimit;

    @Column(name = "LOAN_OUTSTANDING")
    private Double loanOutStanding;

    @Column(name = "CURRENCY")
    private String currencyCode;

    @Column(name = "LOAN_TENURE")
    private Integer tenureMonthly;

    @Column(name = "LENGTH_OF_USING")
    private Integer lengthOfUsing;

    @Column(name = "LOCK_IN_PERIOD")
    private Integer lockInPeriodMonthly;

    @Column(name = "INTEREST_RATE")
    private String interestRateMonthly;

    @Column(name = "REPAYMENT_MODE")
    private String rawRepaymentMode;

    @Column(name = "COLLATERAL_NUMBER")
    private Integer mortgagedCollateralNumber;

    @Column(name = "FIRST_PRINCIPAL_PAYMENT")
    private Double firstPrincipalRepayment;

    @Column(name = "MONTHLY_INTEREST_PAYMENT")
    private Double monthlyInterestRepayment;

    @Column(name = "NUMBER_OF_DAY_LATE")
    private Integer numberOfLateDays;

    @Column(name = "NUMBER_OF_LATE_INSTALLMENT")
    private Integer numberOfLateInstallments;

    @Column(name = "PENALTY_AMOUNT_FOR_PAID_OFF")
    private Double penaltyAmountForPaidOff;

    @Column(name = "CUSTOMER_ID")
    private String clientCif;

    @Column(name = "LOANPURPOSECODE")
    private String loanPurposeCode;

    @Column(name = "REPAYMENTCODE")
    private String repaymentCode;

    @Column(name = "loan_status")
    private String loanStatus;
}

