package com.dminc.prasac.client.integration.saving.account.request;

import com.dminc.prasac.client.model.dto.User;
import lombok.Builder;
import prasac.webapi.wsdl.saving.account.CustAccQueryIOType;
import prasac.webapi.wsdl.saving.account.QUERYCUSTACCIOFSREQ;

public class QuerySavingAccountRequest extends SavingAccountRequest {

    public static final String OPERATION = "QueryCustAcc";
    private final String accountNumber;
    private final String branch;

    @Builder
    public QuerySavingAccountRequest(String source, User user, String accountNumber, String branch) {
        super(source, user, OPERATION);
        this.accountNumber = accountNumber;
        this.branch = branch;
    }

    public QUERYCUSTACCIOFSREQ getRequest() {
        final QUERYCUSTACCIOFSREQ request = new QUERYCUSTACCIOFSREQ();
        request.setFCUBSHEADER(populateHeader());
        final QUERYCUSTACCIOFSREQ.FCUBSBODY body = new QUERYCUSTACCIOFSREQ.FCUBSBODY();
        final CustAccQueryIOType account = new CustAccQueryIOType();
        account.setACC(accountNumber);
        account.setBRN(branch);
        body.setCustAccountIO(account);
        request.setFCUBSBODY(body);
        return request;
    }
}
