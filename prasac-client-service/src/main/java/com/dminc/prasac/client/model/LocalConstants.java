package com.dminc.prasac.client.model;

public final class LocalConstants {

    public static final String KHMER_CURRENCY = "KHR";
    public static final String THAI_CURRENCY = "THB";
    public static final String US_DOLLAR = "USD";
    public static final String BORROWER_CODE = "BRW";
    public static final String CO_BORROWER_CODE = "CBW";
    public static final String GUARANTOR_CODE = "GUA";

    private LocalConstants() {
        //to prevent creating instances
    }

}
