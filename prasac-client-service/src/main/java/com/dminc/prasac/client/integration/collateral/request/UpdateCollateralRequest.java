package com.dminc.prasac.client.integration.collateral.request;

import com.dminc.prasac.client.model.dto.Collateral;
import com.dminc.prasac.client.model.dto.User;
import lombok.Builder;
import prasac.webapi.wsdl.collteral.CollateralFullType;
import prasac.webapi.wsdl.collteral.MODIFYCOLLATERALFSFSREQ;

import java.math.BigDecimal;

public class UpdateCollateralRequest extends CollateralRequest {

    public static final String OPERATION = "ModifyCollateral";
    private final Collateral collateral;
    private final CollateralFullType existing;

    @Builder
    public UpdateCollateralRequest(String source, User user, Collateral collateral, CollateralFullType existing) {
        super(OPERATION, source, user);
        this.collateral = collateral;
        this.existing = existing;
    }

    public MODIFYCOLLATERALFSFSREQ getRequest() {
        final MODIFYCOLLATERALFSFSREQ request = new MODIFYCOLLATERALFSFSREQ();
        request.setFCUBSHEADER(populateHeader());
        final CollateralFullType collateral = populateCollateral();
        final MODIFYCOLLATERALFSFSREQ.FCUBSBODY body = new MODIFYCOLLATERALFSFSREQ.FCUBSBODY();
        body.setCollateralsFull(collateral);
        request.setFCUBSBODY(body);
        return request;
    }

    private CollateralFullType populateCollateral() {
        existing.setCOLLATERALVALUE(BigDecimal.valueOf(collateral.getValue()));
        existing.setCOLLATERALCURRENCY(collateral.getCurrencyCode());
        existing.setBRANCHCODE(collateral.getBranchCode());
        existing.setTXNSTAT("O");
        return existing;
    }

}
