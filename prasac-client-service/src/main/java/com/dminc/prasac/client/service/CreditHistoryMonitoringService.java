package com.dminc.prasac.client.service;

import com.dminc.prasac.client.model.dto.CreditHistoryMonitoring;

import java.util.List;

public interface CreditHistoryMonitoringService {

    List<CreditHistoryMonitoring> getCreditHistoryMonitoringsByClient(String clientCif);
}
