package com.dminc.prasac.client.integration.client.request;

import com.dminc.prasac.client.model.dto.Address;
import com.dminc.prasac.client.model.dto.Client;
import com.dminc.prasac.client.model.dto.User;
import com.dminc.prasac.client.util.Helper;
import lombok.Builder;
import prasac.webapi.wsdl.client.CustomerFullType;
import prasac.webapi.wsdl.client.MODIFYCUSTOMERFSFSREQ;

import java.math.BigDecimal;

public class UpdateClientRequest extends ClientRequest {

    public static final String UPDATE_CUSTOMER_OPERATION = "ModifyCustomer";
    private final CustomerFullType existingClient;

    @Builder
    public UpdateClientRequest(String source, User user, Client clientEntity, CustomerFullType existingClient) {
        super(source, user, clientEntity, UPDATE_CUSTOMER_OPERATION);
        this.existingClient = existingClient;
    }

    public MODIFYCUSTOMERFSFSREQ getRequest() {
        final MODIFYCUSTOMERFSFSREQ request = new MODIFYCUSTOMERFSFSREQ();
        final MODIFYCUSTOMERFSFSREQ.FCUBSBODY body = new MODIFYCUSTOMERFSFSREQ.FCUBSBODY();
        request.setFCUBSHEADER(populateHeader());
        request.setFCUBSBODY(body);
        final CustomerFullType client = populateClient();
        body.setCustomerFull(client);
        return request;
    }

    @Override
    protected void populateClientProfile(CustomerFullType client) {
        final Client clientEntity = getClientEntity();
        final CustomerFullType.Custpersonal clientProfile = existingClient.getCustpersonal();
        clientProfile.setMOBNUM(Helper.normalizePhoneNumber(clientEntity.getPhone()));
        clientProfile.setDOB(Helper.getXmlGregorianCalendar(clientEntity.getDob()));
        final String[] names = splitName(clientEntity.getKhmerFullName());
        clientProfile.setLSTNAME(names[0]);
        clientProfile.setFSTNAME(getFirstName(names));
        clientProfile.setGENDR(clientEntity.getGender());
        clientProfile.setTITLE(Helper.getClientPrefix(clientEntity.getGender()));
        clientProfile.setMOBISDNO(BigDecimal.valueOf(855));
        if (clientEntity.getBirthAddress() != null) {
            clientProfile.setPLACEOFBIRTH(clientEntity.getBirthAddress().getProvince());
            clientProfile.setPLACEOFBIRTH(clientEntity.getBirthAddress().getCountry());
        }
        populatePassport(clientProfile, clientEntity.getIdentifications());
        populateIdentification(clientProfile, clientEntity.getIdentifications());
        populateClientRelationShip(clientProfile);
        populateClientJob(clientProfile);
        client.setCustpersonal(clientProfile);
    }

    @Override
    protected CustomerFullType populateClient() {
        final Client clientEntity = getClientEntity();
        existingClient.setNAME(clientEntity.getFullName());
        existingClient.setLBRN(clientEntity.getBranch());
        final Address currentAddress = clientEntity.getCurrentAddress();
        existingClient.setADDRS5(currentAddress.getVillage());
        existingClient.setADDRLN4(currentAddress.getCommune());
        existingClient.setADDRLN3(currentAddress.getDistrict());
        existingClient.setADDRLN2(currentAddress.getProvince());
        existingClient.setADDRLN1(populateAddress1(clientEntity.getCurrentAddress()));
        existingClient.setCOUNTRY(currentAddress.getCountry());
        existingClient.setFULLNAME(clientEntity.getFullName());
        existingClient.setNLTY(clientEntity.getNationality());
        existingClient.setMODNO(null);
        populateClientProfile(existingClient);
        return existingClient;
    }
}

