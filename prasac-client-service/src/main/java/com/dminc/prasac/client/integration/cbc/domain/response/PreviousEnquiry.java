package com.dminc.prasac.client.integration.cbc.domain.response;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;

@XmlRootElement(name = "PREV_ENQUIRY")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PreviousEnquiry {

    @XmlElement(name = "PE_DATE")
    @XmlJavaTypeAdapter(DateAdapter.class)
    private LocalDate date;

    @XmlElement(name = "PE_ENQR")
    private String enquirer;

    @XmlElement(name = "PE_TYPE")
    private String type;

    @XmlElement(name = "PE_ACCT")
    private String accountType;

    @XmlElement(name = "PE_MEMB_REF")
    private String memberRef;

    @XmlElement(name = "PE_PRD")
    private String purposeCode;

    @XmlElement(name = "PE_AMOUNT")
    private Double amount;

    @XmlElement(name = "PE_CURR")
    private String currencyCode;

    @XmlElement(name = "PE_APPL")
    private String clientType;

    @XmlElement(name = "PE_NAME", type = PreviousEnquiryName.class)
    private PreviousEnquiryName name;

}
