package com.dminc.prasac.client.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.GregorianCalendar;

@Slf4j
public final class Helper {

    private Helper() {
    }

    public static String getRemoteAddress(HttpServletRequest req) {
        String ipAddress = req.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = req.getRemoteAddr();
        }
        return ipAddress;
    }

    public static BigDecimal convertStringToBigDecimal(String s) {
        if (StringUtils.isEmpty(s)) {
            return BigDecimal.ZERO;
        }
        DecimalFormat decimalFormat = new DecimalFormat();
        decimalFormat.setParseBigDecimal(true);
        try {
            return (BigDecimal) decimalFormat.parse(s);
        } catch (ParseException e) {
            return BigDecimal.ZERO;
        }
    }

    public static String getClientPrefix(String gender) {
        switch (gender) {
            case "M":
                return "Mr.";
            case "F":
                return "Ms.";
            default:
                return null;
        }
    }

    public static String getEffectiveMethod(HttpServletRequest request) {
        return request.getMethod();
    }

    public static XMLGregorianCalendar getXmlGregorianCalendar(LocalDate date) {
        if (date == null) {
            return null;
        }
        try {
            GregorianCalendar gcal = new GregorianCalendar(date.getYear(), date.getMonthValue() - 1, date.getDayOfMonth());
            XMLGregorianCalendar xcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcal);
            xcal.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
            return xcal;
        } catch (DatatypeConfigurationException e) {
            return null;
        }
    }

    /**
     * Remove leading Zeros
     */
    public static String normalizePhoneNumber(String s) {
        if (StringUtils.isNotEmpty(s)) {
            return s.replaceFirst("^0+(?!$)", "");
        }
        return null;
    }

    public static String[] splitName(String name) {
        final String normalizeName = StringUtils.normalizeSpace(name);
        return StringUtils.splitPreserveAllTokens(normalizeName);
    }

    /**
     * name index 0 is last name
     */
    public static String getFirstName(String... names) {
        final StringBuilder name = new StringBuilder();
        for (int i = 1; i < names.length; i++) {
            name.append(names[i]);
        }
        return name.toString();
    }

    public static String format(Number n, int digit) {
        final String format = String.format("%%0%dd", digit);
        return String.format(format, n);
    }
}
