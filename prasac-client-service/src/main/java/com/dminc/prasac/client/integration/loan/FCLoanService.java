package com.dminc.prasac.client.integration.loan;

import com.dminc.prasac.client.model.dto.FCLoanRequest;
import com.dminc.rest.exceptions.BusinessException;

public interface FCLoanService {

    FCLoanRequest createFCLoan(FCLoanRequest fcLoanRequest) throws BusinessException;
}
