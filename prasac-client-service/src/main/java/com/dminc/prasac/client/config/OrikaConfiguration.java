package com.dminc.prasac.client.config;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Carlos Aller
 * @since 13/01/17
 */
@Configuration
public class OrikaConfiguration {

    @Bean
    public MapperFactory getMapper() {
        return new DefaultMapperFactory.Builder().build();
    }
}
