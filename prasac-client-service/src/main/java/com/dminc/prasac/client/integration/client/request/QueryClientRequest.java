package com.dminc.prasac.client.integration.client.request;

import com.dminc.prasac.client.model.dto.Client;
import com.dminc.prasac.client.model.dto.User;
import lombok.Builder;
import prasac.webapi.wsdl.client.CustomerQueryIOType;
import prasac.webapi.wsdl.client.QUERYCUSTOMERIOFSREQ;

public class QueryClientRequest extends ClientRequest {
    public static final String OPERATION = "QueryCustomer";

    private final String cif;

    @Builder
    public QueryClientRequest(String source, User user, Client clientEntity, String cif) {
        super(source, user, clientEntity, OPERATION);
        this.cif = cif;
    }

    public QUERYCUSTOMERIOFSREQ getRequest() {
        final QUERYCUSTOMERIOFSREQ request = new QUERYCUSTOMERIOFSREQ();
        request.setFCUBSHEADER(populateHeader());
        final CustomerQueryIOType client = new CustomerQueryIOType();
        client.setCUSTNO(cif);
        final QUERYCUSTOMERIOFSREQ.FCUBSBODY body = new QUERYCUSTOMERIOFSREQ.FCUBSBODY();
        body.setCustomerIO(client);
        request.setFCUBSBODY(body);
        return request;
    }
}
