package com.dminc.prasac.client.config;

import org.springframework.core.env.Environment;

import com.zaxxer.hikari.HikariConfig;

import lombok.Builder;

@Builder
public class HikariDatasourceDefaultConfigurationFactory {
    public HikariConfig getDefaultConfig(Environment env) {
        final HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(env.getProperty("spring.datasource.driver-class-name"));
        hikariConfig.setJdbcUrl(env.getProperty("spring.datasource.url"));
        hikariConfig.setUsername(env.getProperty("spring.datasource.username"));
        hikariConfig.setPassword(env.getProperty("spring.datasource.password"));
        hikariConfig.setMaxLifetime(Long.parseLong(env.getProperty("spring.datasource.hikari.maxLifetime", "50000")));
        hikariConfig.setMaximumPoolSize(Integer.parseInt(env.getProperty("spring.datasource.hikari.maximum-pool-size", "40")));
        hikariConfig.setMinimumIdle(Integer.parseInt(env.getProperty("spring.datasource.hikari.minimumIdle", "10")));
        hikariConfig.addDataSourceProperty("cachePrepStmts", env.getProperty("spring.datasource.hikari.cachePrepStmts", "true"));
        hikariConfig.addDataSourceProperty("prepStmtCacheSize", env.getProperty("spring.datasource.hikari.prepStmtCacheSize", "250"));
        hikariConfig.addDataSourceProperty("prepStmtCacheSqlLimit", env.getProperty("spring.datasource.hikari.prepStmtCacheSqlLimit", "2048"));
        hikariConfig.addDataSourceProperty("useServerPrepStmts", env.getProperty("spring.datasource.hikari.useServerPrepStmts", "true"));
        return hikariConfig;
    }
}
