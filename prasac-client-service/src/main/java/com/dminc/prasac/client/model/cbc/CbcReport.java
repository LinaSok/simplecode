package com.dminc.prasac.client.model.cbc;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CbcReport implements Serializable {
    private static final long serialVersionUID = -3295508522599075757L;

    private String enquiryType;

    private LocalDate reportDate;

    private String enquiryNo;

    private String purposeCode;

    private Integer numberOfApplicants;

    private String accountType;

    private String enquiryRef;

    private Double amount;

    private String currencyCode;

    private List<CbcClient> clients;
}
