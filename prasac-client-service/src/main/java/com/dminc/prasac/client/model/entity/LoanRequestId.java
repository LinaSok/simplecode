package com.dminc.prasac.client.model.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
public class LoanRequestId implements Serializable {
    @Column(name = "LOAN_ID")
    protected Long loanId;
    @Column(name = "CIF_NUMBER")
    private String clientCif;
}
