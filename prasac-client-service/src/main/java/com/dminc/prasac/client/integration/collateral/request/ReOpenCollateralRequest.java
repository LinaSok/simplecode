package com.dminc.prasac.client.integration.collateral.request;

import com.dminc.prasac.client.model.dto.User;
import lombok.Builder;
import prasac.webapi.wsdl.collteral.CollateralReopenIOType;
import prasac.webapi.wsdl.collteral.REOPENCOLLATERALIOPKREQ;

import java.math.BigDecimal;

public class ReOpenCollateralRequest extends CollateralRequest {

    public static final String OPERATION = "ReopenCollateral";
    private final String code;
    private final String liabId;

    @Builder
    public ReOpenCollateralRequest(String source, User user, String code, String liabId) {
        super(OPERATION, source, user);
        this.code = code;
        this.liabId = liabId;
    }

    public REOPENCOLLATERALIOPKREQ getRequest() {
        final REOPENCOLLATERALIOPKREQ request = new REOPENCOLLATERALIOPKREQ();

        request.setFCUBSHEADER(populateHeader());

        final REOPENCOLLATERALIOPKREQ.FCUBSBODY body = new REOPENCOLLATERALIOPKREQ.FCUBSBODY();
        final CollateralReopenIOType collateral = new CollateralReopenIOType();
        collateral.setCOLLATERALCODE(code);
        collateral.setLIABID(new BigDecimal(liabId));
        body.setCollateralsIO(collateral);
        request.setFCUBSBODY(body);
        return request;
    }
}
