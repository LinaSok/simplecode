package com.dminc.prasac.client.model.dto;


import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class CreditHistoryMonitoring {

    private String clientCif;

    private LocalDate enqDate;

    private LocalDate date;

    private String event;

    private String currency;

    private Double amount;

    private String lender;

    private String reference;
}
