package com.dminc.prasac.client.integration.collateral.request;

import com.dminc.prasac.client.integration.collateral.service.ELPoolServiceImpl;
import com.dminc.prasac.client.model.dto.CollateralPool;
import com.dminc.prasac.client.model.dto.User;
import lombok.Builder;
import prasac.webapi.wsdl.elpool.CollateralpoolFullType;
import prasac.webapi.wsdl.elpool.MODIFYCOLLATERALPOOLFSFSREQ;

import java.math.BigDecimal;
import java.util.List;

public class UpdateCollateralPoolRequest extends CollateralPoolRequest {

    public static final String OPERATION = "Modifycollateralpool";
    private final CollateralPool collateralPool;
    private final CollateralpoolFullType existing;

    @Builder
    public UpdateCollateralPoolRequest(String source, User user, CollateralPool collateralPool, CollateralpoolFullType existing) {
        super(OPERATION, source, user);
        this.collateralPool = collateralPool;
        this.existing = existing;
    }

    public MODIFYCOLLATERALPOOLFSFSREQ getRequest() {
        final MODIFYCOLLATERALPOOLFSFSREQ request = new MODIFYCOLLATERALPOOLFSFSREQ();
        request.setFCUBSHEADER(populateHeader());
        final CollateralpoolFullType pool = populateCollateralPool();
        final MODIFYCOLLATERALPOOLFSFSREQ.FCUBSBODY body = new MODIFYCOLLATERALPOOLFSFSREQ.FCUBSBODY();
        body.setCollateralsPoolFull(pool);
        request.setFCUBSBODY(body);
        return request;
    }

    private CollateralpoolFullType populateCollateralPool() {
        existing.setPOOLCCY(collateralPool.getCurrencyCode());
        existing.setBRANCHCODE(collateralPool.getBranchCode());
        existing.setTXNSTAT(ELPoolServiceImpl.STATE_OPEN);
        addCollateralToPool();
        return existing;
    }

    private void addCollateralToPool() {
        final List<CollateralpoolFullType.PoolCollateralsLinkage> linkages = existing.getPoolCollateralsLinkage();
        linkages.clear();
        collateralPool.getCollateralCodes().forEach(code -> {
            final CollateralpoolFullType.PoolCollateralsLinkage linkage = new CollateralpoolFullType.PoolCollateralsLinkage();
            linkage.setLINKEDPERCENTNUMBER(new BigDecimal("100")); //default value
            linkage.setCOLLATERALCODE(code);
            linkages.add(linkage);
        });
    }

}
