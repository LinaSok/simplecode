package com.dminc.prasac.client.integration.cbc.domain.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "CADR")
@XmlAccessorType(XmlAccessType.FIELD)
public class Address {

    @XmlElement(name = "CADT")
    private String addressType;

    @XmlElement(name = "CADPR")
    private String provinceCode;

    @XmlElement(name = "CADDS")
    private String districtCode;
    @XmlElement(name = "CADCM")
    private String communeCode;
    @XmlElement(name = "CADVL")
    private String villageCode;

    @XmlElement(name = "CAD1E")
    private String fullAddress;

    @XmlElement(name = "CAD8E")
    private String province;

    @XmlElement(name = "CAD9")
    private String countryCode;

}
