package com.dminc.prasac.client.integration.cbc.domain.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "REQUEST")
@XmlAccessorType(XmlAccessType.FIELD)
public class Request {

    @XmlAttribute(name = "xsi:noNamespaceSchemaLocation")
    protected static final String XIS = "Enquiry.xsd";

    @XmlElement(name = "SERVICE")
    private String service;

    @XmlElement(name = "ACTION")
    private String action;


    @XmlElement(name = "HEADER", type = RequestHeader.class)
    private RequestHeader requestHeader;

    @XmlElement(name = "MESSAGE", type = RequestBody.class)
    private RequestBody requestBody;
}
