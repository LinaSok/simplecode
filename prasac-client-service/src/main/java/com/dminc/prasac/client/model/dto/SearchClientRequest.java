package com.dminc.prasac.client.model.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
public class SearchClientRequest {
    private String cif;
    private String name;
    private String khmerName;
    private String idNumber;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate dob;
}
