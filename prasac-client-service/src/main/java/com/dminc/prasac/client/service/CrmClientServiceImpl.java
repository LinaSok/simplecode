package com.dminc.prasac.client.service;

import com.dminc.prasac.client.model.dto.CrmClient;
import com.dminc.prasac.client.model.dto.IdentityRequest;
import com.dminc.prasac.client.model.dto.SearchClientRequest;
import com.dminc.prasac.client.model.entity.CrmClientEntity;
import com.dminc.prasac.client.respository.CrmClientRepository;
import com.dminc.rest.exceptions.BusinessException;
import com.dminc.rest.exceptions.ItemNotFoundBusinessException;
import com.dminc.rest.mapping.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CrmClientServiceImpl implements CrmClientService {

    private final CrmClientRepository repository;
    private final ObjectsConverter converter;

    @Override
    @Transactional(readOnly = true)
    public List<CrmClient> search(SearchClientRequest request) {
        final List<CrmClientEntity> clientInfo = repository.searchClient(request);
        return converter.getObjectsMapper().map(clientInfo, CrmClient.class);
    }

    @Override
    public CrmClient getOneByCif(String cif) throws BusinessException {
        return converter.runServiceTask((Mapper mapper) -> mapper.map(repository.findTopByCif(cif), CrmClient.class));
    }

    @Override
    public CrmClient findByIdNumberAndIdType(IdentityRequest request) throws BusinessException {
        final CrmClientEntity result = repository
                .findFirstByIdentityNumberAndIdentityTypeContainingIgnoreCase(request.getIdentityNumber(), request.getIdentityType());
        if (result == null) {
            throw new ItemNotFoundBusinessException("Client not found by requested identity");
        }
        return converter.runServiceTask((Mapper mapper) -> mapper.map(result, CrmClient.class));
    }
}
