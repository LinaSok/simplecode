package com.dminc.prasac.client.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class SavingAccount implements Serializable {
    private static final long serialVersionUID = 8027166710492483128L;

    private String customerNo; // Borrower cif
    private String currencyCode;
    private String accountClassCode;
    private String accountDesc; // Borrower full name
    private String branchCode;
    private Address address;
    private String accountNumber;
    private List<Client> clients; // Co-borrower
}
