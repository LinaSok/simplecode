package com.dminc.prasac.client.integration.client.request;

import com.dminc.prasac.client.model.dto.Client;
import com.dminc.prasac.client.model.dto.User;
import com.dminc.rest.exceptions.InternalBusinessException;
import lombok.Builder;
import prasac.webapi.wsdl.client.CREATECUSTOMERFSFSREQ;
import prasac.webapi.wsdl.client.CustomerFullType;

public class NewClientRequest extends ClientRequest {
    public static final String CREATE_CUSTOMER_OPERATION = "CreateCustomer";

    @Builder
    public NewClientRequest(String source, User user, Client clientEntity) {
        super(source, user, clientEntity, CREATE_CUSTOMER_OPERATION);
    }

    public CREATECUSTOMERFSFSREQ getRequest() throws InternalBusinessException {
        final CREATECUSTOMERFSFSREQ request = new CREATECUSTOMERFSFSREQ();
        final CREATECUSTOMERFSFSREQ.FCUBSBODY body = new CREATECUSTOMERFSFSREQ.FCUBSBODY();
        request.setFCUBSHEADER(populateHeader());
        request.setFCUBSBODY(body);
        final CustomerFullType client = populateClient();
        body.setCustomerFull(client);
        return request;
    }
}

