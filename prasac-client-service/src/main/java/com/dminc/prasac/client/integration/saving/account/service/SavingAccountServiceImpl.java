package com.dminc.prasac.client.integration.saving.account.service;

import com.dminc.prasac.client.integration.config.DefaultNamespacePrefixMapper;
import com.dminc.prasac.client.integration.saving.account.request.NewSavingAccountRequest;
import com.dminc.prasac.client.integration.saving.account.request.QuerySavingAccountRequest;
import com.dminc.prasac.client.integration.saving.account.request.UpdateSavingAccountRequest;
import com.dminc.prasac.client.model.dto.ClientLinkage;
import com.dminc.prasac.client.model.dto.SavingAccount;
import com.dminc.prasac.client.model.dto.User;
import com.dminc.prasac.client.service.ClientLinkageService;
import com.dminc.prasac.client.service.FlexCubeUserService;
import com.dminc.rest.exceptions.InternalBusinessException;
import com.dminc.rest.exceptions.ItemNotFoundBusinessException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;
import prasac.webapi.wsdl.saving.account.CREATECUSTACCFSFSRES;
import prasac.webapi.wsdl.saving.account.CustAccFullType;
import prasac.webapi.wsdl.saving.account.ERRORType;
import prasac.webapi.wsdl.saving.account.MODIFYCUSTACCFSFSRES;
import prasac.webapi.wsdl.saving.account.QUERYCUSTACCIOFSRES;

import javax.annotation.PostConstruct;
import javax.xml.bind.Marshaller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class SavingAccountServiceImpl implements SavingAccountService {

    private static final String SOAP_PATH = "/FCUBSAccService/FCUBSAccService";

    private final DefaultNamespacePrefixMapper defaultNamespacePrefixMapper;
    private final ClientLinkageService linkageService;
    private final FlexCubeUserService userService;

    @Value("${prasac.soap.service.url}")
    private String domain;
    @Value("${prasac.soap.service.source}")
    private String source;

    private WebServiceTemplate template;
    private String soapUrl;

    @PostConstruct
    public void init() {
        soapUrl = String.format("%s%s", domain, SOAP_PATH);
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("prasac.webapi.wsdl.saving.account");
        Map<String, Object> properties = new HashMap<>();
        properties.put(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        properties.put("com.sun.xml.bind.namespacePrefixMapper", defaultNamespacePrefixMapper);
        marshaller.setMarshallerProperties(properties);
        template = new WebServiceTemplate();
        template.setDefaultUri(domain);
        template.setMarshaller(marshaller);
        template.setUnmarshaller(marshaller);
    }

    @Override
    public SavingAccount createOrUpdate(SavingAccount savingAccount) throws ItemNotFoundBusinessException, InternalBusinessException {
        final List<ClientLinkage> clientLinkages = linkageService.getByClientCif(savingAccount.getCustomerNo());
        final Optional<ClientLinkage> existing = getExisting(savingAccount, clientLinkages);
        final User user = userService.getUserByBranch(savingAccount.getBranchCode());
        if (existing.isPresent()) {
            return update(savingAccount, existing.get(), user);
        } else {
            return create(savingAccount, user);
        }
    }

    private SavingAccount update(SavingAccount savingAccount, ClientLinkage clientLinkage, User user) throws InternalBusinessException {
        final CustAccFullType existingAccount = getExistingAccount(clientLinkage.getSavingAccountNumber(), clientLinkage.getBranchCode(), user);
        final UpdateSavingAccountRequest accountRequest = UpdateSavingAccountRequest.builder()
                .existingAccount(existingAccount)
                .savingAccount(savingAccount)
                .source(source)
                .user(user)
                .build();
        final MODIFYCUSTACCFSFSRES response = (MODIFYCUSTACCFSFSRES) template.marshalSendAndReceive(soapUrl, accountRequest.getRequest());
        checkError(response.getFCUBSBODY().getFCUBSERRORRESP());
        savingAccount.setAccountNumber(response.getFCUBSBODY().getCustAccountFull().getACC());
        return savingAccount;
    }

    private SavingAccount create(SavingAccount savingAccount, User user) throws InternalBusinessException {
        final NewSavingAccountRequest accountRequest = NewSavingAccountRequest.builder()
                .savingAccount(savingAccount)
                .source(source)
                .user(user)
                .build();
        final CREATECUSTACCFSFSRES response = (CREATECUSTACCFSFSRES) template.marshalSendAndReceive(soapUrl, accountRequest.getRequest());
        checkError(response.getFCUBSBODY().getFCUBSERRORRESP());
        savingAccount.setAccountNumber(response.getFCUBSBODY().getCustAccountFull().getACC());
        return savingAccount;
    }

    private void checkError(final List<ERRORType> types) throws InternalBusinessException {
        final StringBuilder errorMessage = new StringBuilder();
        types.forEach(type -> type.getERROR().forEach(error -> {
            errorMessage.append(String.format("\n %s : %s", error.getECODE(), error.getEDESC()));
        }));
        if (StringUtils.isNotEmpty(errorMessage)) {
            throw new InternalBusinessException(errorMessage.toString());
        }
    }

    /**
     * Create New :
     * - No data in oracle/Flexcube
     * - Existing data in oracle/Flexcube (Record status = "O") and Account currency different from currency of loan request
     * - Existing data but in difference branch
     */
    private Optional<ClientLinkage> getExisting(SavingAccount savingAccount, List<ClientLinkage> clientLinkages) {
        return clientLinkages.stream().filter(clientLinkage ->
                clientLinkage.getBranchCode().equals(savingAccount.getBranchCode())
                        && clientLinkage.getCurrencyCode().equals(savingAccount.getCurrencyCode())
                        && clientLinkage.getRecordSate().equals("O")
        ).findFirst();
    }

    private CustAccFullType getExistingAccount(String accountNumber, String branch, User user) {
        final QuerySavingAccountRequest accountRequest = QuerySavingAccountRequest.builder()
                .user(user)
                .source(source)
                .accountNumber(accountNumber)
                .branch(branch)
                .build();
        final QUERYCUSTACCIOFSRES response = (QUERYCUSTACCIOFSRES) template.marshalSendAndReceive(soapUrl, accountRequest.getRequest());
        return response.getFCUBSBODY().getCustAccountFull();
    }

}
