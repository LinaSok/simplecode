package com.dminc.prasac.client.model.cbc;

import com.dminc.prasac.client.model.dto.Client;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Data
@ToString
public class Loan implements Serializable {
    private static final long serialVersionUID = 640153662989909229L;

    private Double amount;

    private String currency;

    private String classCode;

    private String purposeCode;

    private List<Client> clients;
}
