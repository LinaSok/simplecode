package com.dminc.prasac.client.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/error")
public class JsonErrorController implements ErrorController {
    private static final String TRACE = "trace";

    private final ErrorAttributes errorAttributes;

    @Autowired
    public JsonErrorController(ErrorAttributes errorAttributes) {
        Assert.notNull(errorAttributes, "ErrorAttributes must not be null");
        this.errorAttributes = errorAttributes;
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }

    @RequestMapping
    public Map<String, Object> error(HttpServletRequest aRequest, WebRequest webRequest) {
        Map<String, Object> body = getErrorAttributes(webRequest, getTraceParameter(aRequest));
        String trace = (String) body.get(TRACE);
        if (trace != null) {
            String[] lines = trace.split("\n\t");
            body.put(TRACE, lines);
        }
        return body;
    }

    private boolean getTraceParameter(HttpServletRequest request) {
        String parameter = request.getParameter(TRACE);
        if (parameter == null) {
            return false;
        }
        return !"false".equalsIgnoreCase(parameter);
    }

    private Map<String, Object> getErrorAttributes(WebRequest webRequest, boolean includeStackTrace) {

        return errorAttributes.getErrorAttributes(webRequest, includeStackTrace);
    }
}
