package com.dminc.prasac.client.service;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dminc.prasac.client.model.entity.LoanDetails;
import com.dminc.prasac.client.respository.LoanDetailsRepository;
import com.dminc.prasac.client.respository.LoanPaymentRepository;
import com.dminc.rest.exceptions.BusinessException;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class LoanServiceImpl implements LoanService {

    private final LoanPaymentRepository loanPaymentRepositoryDao;
    private final LoanDetailsRepository loanDetailsRepositoryDao;

    public List getLoanStatement(String clientCif) throws BusinessException {

        //lookup LoanDetail by clientCif to get the last update accountNumber
        final LoanDetails loanDetails = loanDetailsRepositoryDao.findDistinctTopByClientCifOrderByLoanCycleDesc(clientCif);

        if (loanDetails == null) {
            return Collections.emptyList();
        }
        log.info("getLoanStatement with account Number {}", loanDetails.getAccountNumber());
        return loanPaymentRepositoryDao.triggerLoanStatemment(loanDetails.getAccountNumber());
    }

    public List getLoanRepaymentScheduleStatement(String clientCif) {
        //lookup LoanDetail by clientCif to get the last update accountNumber
        final LoanDetails loanDetails = loanDetailsRepositoryDao.findDistinctTopByClientCifOrderByLoanCycleDesc(clientCif);

        if (loanDetails == null) {
            return Collections.emptyList();
        }
        log.info("getLoanRepaymentScheduleStatement with account Number {}", loanDetails.getAccountNumber());
        return loanPaymentRepositoryDao.triggerRepaymentSchedule(loanDetails.getAccountNumber());
    }
}
