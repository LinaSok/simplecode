package com.dminc.prasac.client.service;

import com.dminc.prasac.client.model.dto.ClientLinkage;

import java.util.List;

public interface ClientLinkageService {

    List<ClientLinkage> getByClientCif(String cif);
}
