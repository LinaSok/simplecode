package com.dminc.prasac.client.integration.client.service;

import com.dminc.prasac.client.integration.client.request.NewClientRequest;
import com.dminc.prasac.client.integration.client.request.QueryClientRequest;
import com.dminc.prasac.client.integration.client.request.UpdateClientRequest;
import com.dminc.prasac.client.integration.config.DefaultNamespacePrefixMapper;
import com.dminc.prasac.client.model.dto.Client;
import com.dminc.prasac.client.model.dto.User;
import com.dminc.prasac.client.service.FlexCubeUserService;
import com.dminc.rest.exceptions.InternalBusinessException;
import com.dminc.rest.exceptions.ItemNotFoundBusinessException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;
import prasac.webapi.wsdl.client.CREATECUSTOMERFSFSRES;
import prasac.webapi.wsdl.client.CustomerFullType;
import prasac.webapi.wsdl.client.ERRORType;
import prasac.webapi.wsdl.client.MODIFYCUSTOMERFSFSRES;
import prasac.webapi.wsdl.client.QUERYCUSTOMERIOFSRES;

import javax.annotation.PostConstruct;
import javax.xml.bind.Marshaller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ClientServiceImpl implements ClientService {
    private static final String SOAP_PATH = "/FCUBSCustomerService/FCUBSCustomerService";

    private final DefaultNamespacePrefixMapper defaultNamespacePrefixMapper;
    private final FlexCubeUserService userService;
    @Value("${prasac.soap.service.url}")
    private String domain;
    @Value("${prasac.soap.service.source}")
    private String source;
    private WebServiceTemplate template;
    private String soapUrl;

    @PostConstruct
    public void init() {
        soapUrl = String.format("%s%s", domain, SOAP_PATH);
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("prasac.webapi.wsdl.client");
        Map<String, Object> properties = new HashMap<>();
        properties.put(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        properties.put("com.sun.xml.bind.namespacePrefixMapper", defaultNamespacePrefixMapper);
        marshaller.setMarshallerProperties(properties);
        template = new WebServiceTemplate();
        template.setDefaultUri(soapUrl);
        template.setMarshaller(marshaller);
        template.setUnmarshaller(marshaller);
    }

    @Override
    public Client create(Client clientEntity) throws InternalBusinessException, ItemNotFoundBusinessException {
        final User user = userService.getUserByBranch(clientEntity.getBranch());
        CustomerFullType customerFullType;
        if (StringUtils.isNotEmpty(clientEntity.getCif())) {
            final CustomerFullType existing = getByCif(clientEntity.getCif(), user);
            UpdateClientRequest request = UpdateClientRequest.builder()
                    .clientEntity(clientEntity)
                    .existingClient(existing)
                    .source(source)
                    .user(user)
                    .build();
            customerFullType = update(request);
        } else {
            final NewClientRequest request = NewClientRequest.builder()
                    .clientEntity(clientEntity)
                    .source(source)
                    .user(user)
                    .build();
            customerFullType = create(request);
        }
        clientEntity.setCif(customerFullType.getCUSTNO());
        clientEntity.setShortName(customerFullType.getSNAME());
        clientEntity.setLiabilityNo(customerFullType.getLIABID());
        return clientEntity;
    }

    private CustomerFullType getByCif(String cif, User user) throws InternalBusinessException {
        final QueryClientRequest queryClientRequest = QueryClientRequest.builder()
                .cif(cif)
                .source(source)
                .user(user)
                .build();
        final QUERYCUSTOMERIOFSRES receive = (QUERYCUSTOMERIOFSRES) template.marshalSendAndReceive(soapUrl, queryClientRequest.getRequest());
        final List<ERRORType> errorTypes = receive.getFCUBSBODY().getFCUBSERRORRESP();
        if (CollectionUtils.isNotEmpty(errorTypes)) {
            final StringBuilder errorMessage = new StringBuilder();
            for (ERRORType errorType : errorTypes) {
                errorMessage.append(errorType.getERROR().stream().map(e -> String.format("%s : %s", e.getECODE(), e.getEDESC())).collect(Collectors.joining("\n")));
            }

            throw new InternalBusinessException(errorMessage.toString());
        }
        return receive.getFCUBSBODY().getCustomerFull();
    }

    private CustomerFullType update(UpdateClientRequest request) throws InternalBusinessException {

        final MODIFYCUSTOMERFSFSRES response = (MODIFYCUSTOMERFSFSRES) template.marshalSendAndReceive(soapUrl, request.getRequest());
        final MODIFYCUSTOMERFSFSRES.FCUBSBODY body = response.getFCUBSBODY();
        checkError(response.getFCUBSBODY().getFCUBSERRORRESP());
        return body.getCustomerFull();
    }

    private void checkError(final List<ERRORType> types) throws InternalBusinessException {
        final StringBuilder errorMessage = new StringBuilder();
        types.forEach(type -> type.getERROR().forEach(error -> {
            errorMessage.append(String.format("\n %s : %s", error.getECODE(), error.getEDESC()));
        }));
        if (StringUtils.isNotEmpty(errorMessage)) {
            throw new InternalBusinessException(errorMessage.toString());
        }
    }

    private CustomerFullType create(NewClientRequest request) throws InternalBusinessException {
        final CREATECUSTOMERFSFSRES response = (CREATECUSTOMERFSFSRES) template.marshalSendAndReceive(soapUrl, request.getRequest());
        checkError(response.getFCUBSBODY().getFCUBSERRORRESP());
        return response.getFCUBSBODY().getCustomerFull();
    }
}
