package com.dminc.prasac.client.integration.cbc.domain.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "CDOB")
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerDOB {

    @XmlElement(name = "CDBD")
    private String day;

    @XmlElement(name = "CDBM")
    private String month;

    @XmlElement(name = "CDBY")
    private String year;


}
