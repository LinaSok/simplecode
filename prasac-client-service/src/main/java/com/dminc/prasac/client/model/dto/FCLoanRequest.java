package com.dminc.prasac.client.model.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@SuppressWarnings({"PMD.TooManyFields"})
public class FCLoanRequest {

    private String loanNumber;
    private String branchCode;
    private String currencyCode;
    private Double loanAmount;
    private Integer tenure;
    private List<Client> clients;
    private Double interestRate;
    private String staffId;
    private String loanPurposeCode;
    private String repaymentModeCode;
    private Integer lockInPeriod;
    private String cbcScoring;
    private Double netIncome;
    private String collateralPoolCode;
    private LocalDate loanRequestDate;
    private Double loanFee;
}
