package com.dminc.prasac.client.integration.loan;

import com.dminc.prasac.client.integration.config.DefaultNamespacePrefixMapper;
import com.dminc.prasac.client.model.LocalConstants;
import com.dminc.prasac.client.model.dto.Client;
import com.dminc.prasac.client.model.dto.FCLoanRequest;
import com.dminc.prasac.client.model.dto.LoanHistoryDTO;
import com.dminc.prasac.client.model.dto.User;
import com.dminc.prasac.client.service.CrmClientService;
import com.dminc.prasac.client.service.FlexCubeUserService;
import com.dminc.prasac.client.service.LoanHistoryService;
import com.dminc.rest.exceptions.BusinessException;
import com.dminc.rest.exceptions.InternalBusinessException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;
import prasac.webapi.wsdl.loan.CREATEACCOUNTFSFSRES;
import prasac.webapi.wsdl.loan.ERRORType;

import javax.annotation.PostConstruct;
import javax.xml.bind.Marshaller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class FCLoanServiceImpl implements FCLoanService {

    private static final String SOAP_PATH = "/FCUBSCLService/FCUBSCLService";
    private final DefaultNamespacePrefixMapper defaultNamespacePrefixMapper;
    private final FlexCubeUserService userService;
    private final CrmClientService clientService;
    private final LoanHistoryService loanHistoryService;

    @Value("${prasac.soap.service.url}")
    private String domain;
    @Value("${prasac.soap.service.source}")
    private String source;
    private WebServiceTemplate template;
    private String soapUrl;

    @PostConstruct
    public void init() {
        soapUrl = String.format("%s%s", domain, SOAP_PATH);
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("prasac.webapi.wsdl.loan");
        final Map<String, Object> properties = new HashMap<>();
        properties.put(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        properties.put("com.sun.xml.bind.namespacePrefixMapper", defaultNamespacePrefixMapper);
        marshaller.setMarshallerProperties(properties);
        template = new WebServiceTemplate();
        template.setDefaultUri(domain);
        template.setMarshaller(marshaller);
        template.setUnmarshaller(marshaller);
    }


    @Override
    public FCLoanRequest createFCLoan(FCLoanRequest fcLoanRequest) throws BusinessException {
        log.info("Create FlexCube loan");
        final Client borrower = fcLoanRequest.getClients().stream()
                .filter(client -> LocalConstants.BORROWER_CODE.equals(client.getClientType()))
                .findFirst()
                .get();

        final LoanHistoryDTO lastLoan = loanHistoryService.getLastLoan(borrower.getCif());
        final User user = userService.getUserByBranch(fcLoanRequest.getBranchCode());

        final AccountMasterRequest request = AccountMasterRequest.builder()
                .source(source)
                .fcLoanRequest(fcLoanRequest)
                .user(user)
                .lastLoanCycle(lastLoan == null ? 0 : lastLoan.getLoanCycle())
                .build();

        final CREATEACCOUNTFSFSRES response = (CREATEACCOUNTFSFSRES) template
                .marshalSendAndReceive(soapUrl, request.getRequest());

        checkError(response.getFCUBSBODY().getFCUBSERRORRESP());
        fcLoanRequest.setLoanNumber(response.getFCUBSBODY().getAccountMasterFull().getACCNO());
        return fcLoanRequest;
    }

    private void checkError(final List<ERRORType> types) throws InternalBusinessException {
        final StringBuilder errorMessage = new StringBuilder();
        types.forEach(type -> type.getERROR().forEach(error -> {
            errorMessage.append(String.format("\n %s : %s", error.getECODE(), error.getEDESC()));
        }));
        if (StringUtils.isNotEmpty(errorMessage)) {
            throw new InternalBusinessException(errorMessage.toString(), 9000);
        }
    }
}
