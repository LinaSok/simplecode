package com.dminc.prasac.client.service;

import com.dminc.prasac.client.model.dto.LoanRequestHistoryDTO;
import com.dminc.rest.exceptions.BusinessException;

import java.util.List;

public interface LoanRequestHistoryService {

    List<LoanRequestHistoryDTO> getByClientCif(String clientCif) throws BusinessException;
}
