package com.dminc.prasac.client.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * This entity use for get
 * - saving account
 * - collateral input
 * - collateral pool
 * from existing client
 */

@Entity
@Table(name = "PRAVW_CIF_PRO_LINKAGES")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClientLinkageEntity {

    @Id
    @Column(name = "SAVING")
    private String savingAccountNumber;

    @Column(name = "CIF")
    private String clientCif;

    @Column(name = "ACCOUNT_CLASS")
    private String accountClass;


    @Column(name = "AC_OPEN_DATE")
    private LocalDate savingAccountOpenDate;

    @Column(name = "BRANCH_CODE_SV")
    private String branchCode;

    @Column(name = "RECORD_STAT")
    private String recordSate;

    @Column(name = "AC_STAT_DORMANT")
    private String savingAccountState;

    @Column(name = "COLLATERAL_INPUT")
    private String collateralInputs;

    @Column(name = "COLLATERAL_POOL")
    private String collateralPoolCode;

    @Column(name = "CCY")
    private String currencyCode;

    public List<String> getCollateralInputs() {
        if (StringUtils.isNotEmpty(this.collateralInputs)) {
            return Arrays.asList(StringUtils.split(this.collateralInputs, ","));
        } else {
            return Collections.emptyList();
        }
    }
}
