package com.dminc.prasac.client.config;

import com.dminc.rest.exceptions.BusinessException;
import com.netflix.hystrix.exception.HystrixRuntimeException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author Sophea Mak <a href='mailto:smak@dminc.com'> sophea </a>
 * @since Feb 6, 2018
 */

@Component
@Slf4j
public class ErrorJsonResolver extends DefaultErrorAttributes {

    private static final String ERRORS = "errors";
    private static final String ERROR = "error";
    private static final String EXCEPTION = "exception";
    private static final String TRACE = "trace";
    private static final String MESSAGE = "message";
    private static final String STATUS = "status";
    private static final String ERROR_CODE = "errorCode";

    @Value("${json.error.showallfields:true}")
    private boolean showAllFields;

    @Value("${json.error.msg}")
    private String message;

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        request.setAttribute(EXCEPTION, ex);
        ModelAndView result = super.resolveException(request, response, handler, ex);
        if (ex instanceof BusinessException) {
            ((BusinessException) ex).customizeResponse(request, response);
        }
        return result;
    }

    @Override
    public Map<String, Object> getErrorAttributes(WebRequest webRequest, boolean includeStackTrace) {
        final Map<String, Object> errorAttributes = super.getErrorAttributes(webRequest, true);
        final Throwable throwable = (Throwable) webRequest.getAttribute(EXCEPTION, RequestAttributes.SCOPE_REQUEST);

        String stackTrace = (String) errorAttributes.get(TRACE);
        String errorMessage = (String) errorAttributes.get(MESSAGE);
        Integer httpCode = (Integer) errorAttributes.get(STATUS);
        Integer errorCode = null;
        // check throwable is not null
        if (throwable != null) {
            stackTrace = getStackTrace(throwable);
            errorMessage = StringUtils.isNotEmpty(throwable.getLocalizedMessage()) ? throwable.getLocalizedMessage()
                    : errorMessage;
            errorAttributes.put(MESSAGE, errorMessage);
            // Only BusinessExceptions have possible errorCodes
            if (throwable instanceof BusinessException) {
                errorCode = ((BusinessException) throwable).getErrorCode();
            }
        }

        // visible some error files to end-users
        if (isShowJsonAllErrorFields()) {
            errorAttributes.put(TRACE, stackTrace);
        } else {
            /* remove fields */
            errorAttributes.remove(EXCEPTION);
            errorAttributes.put(ERROR, getAdequateErrorCodeDescription(httpCode, errorAttributes.get(ERROR)));
            errorAttributes.remove(TRACE);
            errorAttributes.remove(ERRORS);

            final String message = getMessage();

            //  override the generic message when errorCode is not defined
            if (StringUtils.isNotEmpty(message) && errorCode == null) {
                errorAttributes.put(MESSAGE, message);

            }
        }

        String errorLog = String.format("Error: {httpCode:%s, message:%s}", httpCode, errorMessage);
        if (errorCode != null) {
            errorAttributes.put(ERROR_CODE, errorCode);
            errorLog = String.format("Error: {httpCode:%s, message:%s, errorCode: %s}", httpCode, errorMessage, errorCode);
        }

        log.info(errorLog);

        return errorAttributes;
    }

    private Object getAdequateErrorCodeDescription(Integer httpCode, Object defaultValue) {
        if (httpCode != null) {
            for (HttpStatus status : HttpStatus.values()) {
                if (status.value() == httpCode) {
                    return status.getReasonPhrase();
                }
            }
        }
        return defaultValue;
    }

    private String getStackTrace(Throwable e) {
        if (e instanceof HystrixRuntimeException) {
            return getStackTrace(e.getCause());
        }

        StackTraceElement[] trace;
        final StringBuilder sb = new StringBuilder(100);

        if (e.getCause() == null) {
            trace = e.getStackTrace();
            sb.append("Exception - ").append(e.getClass().getName());
            if (StringUtils.isNotEmpty(e.getLocalizedMessage())) {
                sb.append("--").append(e.getLocalizedMessage());
            }
            sb.append("\r\nStack -\r\n");
        } else {
            trace = e.getCause().getStackTrace();
            sb.append("\r\nException Cause -\r\n");
        }

        for (StackTraceElement element : trace) {
            sb.append("\r\n\tat ").append(element.toString());
        }
        return sb.toString();

    }

    // #json.error.showallfields=false|true
    private boolean isShowJsonAllErrorFields() {
        return showAllFields;
    }

    // #json.error.msg=Sorry, something went wrong. Please try again later
    private String getMessage() {
        return message;
    }

}
