package com.dminc.prasac.client.respository;

import java.util.List;

public interface LoanPaymentRepository {

    List triggerRepaymentSchedule(String accountNumber);

    List triggerLoanStatemment(String accountNumber);
}
