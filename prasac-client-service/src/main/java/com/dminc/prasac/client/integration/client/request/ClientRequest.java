package com.dminc.prasac.client.integration.client.request;

import com.dminc.prasac.client.model.dto.Address;
import com.dminc.prasac.client.model.dto.Client;
import com.dminc.prasac.client.model.dto.ClientIdentification;
import com.dminc.prasac.client.model.dto.User;
import com.dminc.prasac.client.util.Helper;
import org.apache.commons.lang3.StringUtils;
import prasac.webapi.wsdl.client.CustomerFullType;
import prasac.webapi.wsdl.client.FCUBSHEADERType;
import prasac.webapi.wsdl.client.UBSCOMPType;
import prasac.webapi.wsdl.client.UDFDETAILSType2;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@SuppressWarnings({"PMD.ImmutableField", "PMD.DataClass", "PMD.TooManyMethods", "PMD.AbstractClassWithoutAbstractMethod"})
public abstract class ClientRequest {

    public static final String FCUBS_CUSTOMER_SERVICE = "FCUBSCustomerService";
    public static final String MOCK_LIABILITY_CURRENCY = "KHR";
    public static final String MOCK_LIABILITY_OVER_LIMIT = "999999999999";
    public static final String CLIENT_CLASS_FN = "IN_DEPOSIT_CUST";
    public static final String TRACK_LIMIT = "Y";
    public static final String MOCK_EXPIRED_DATE = "2018-12-30";
    public static final String MOCK_ISSUE_DATE = "2018-01-01";
    public static final String MOCK_UDF_NAME_1 = "07NUMBER_EMPLOYEE";
    public static final String MOCK_UDF_VALUE_1 = "2";
    public static final String MOCK_UDF_NAME_2 = "09OFFICER";
    public static final String MOCK_UDF_NAME_3 = "10EMPLOYMENT_SECTOR";
    public static final String MOCK_UDF_VALUE_3 = "99";
    public static final String MOCK_UDF_NAME_6 = "11CIF_FORM";
    public static final String SALARY_FREQUENCY = "A";
    public static final String MOCK_EMPLOYEE_CURRENCY = "USD";
    public static final String MOCK_RESIDENT_STATUS = "R";
    public static final String TYPE_OF_CIF = "I";
    public static final String CLIENT_CATEGORY = "IN";
    public static final String LANGUAGE = "ENG";

    private final String source;
    private final User user;
    private final Client clientEntity;
    private final String operation;

    public ClientRequest(String source, User user, Client clientEntity, String operation) {
        this.source = source;
        this.user = user;
        this.clientEntity = clientEntity;
        this.operation = operation;
    }

    public static String populateAddress1(Address address) {
        return String.format("%s,%s,%s", address.getHouseNo(), address.getStreetNo(), address.getGroupNo());
    }

    public static String[] splitName(String name) {
        final String normalizeName = StringUtils.normalizeSpace(name);
        return StringUtils.splitPreserveAllTokens(normalizeName);
    }

    /**
     * name index 0 is last name
     */
    protected static String getFirstName(String... names) {
        final StringBuilder name = new StringBuilder();
        for (int i = 1; i < names.length; i++) {
            name.append(names[i]);
        }
        return name.toString();
    }

    public Client getClientEntity() {
        return clientEntity;
    }

    public FCUBSHEADERType populateHeader() {
        final FCUBSHEADERType header = new FCUBSHEADERType();
        header.setSOURCE(source);
        header.setUBSCOMP(UBSCOMPType.FCUBS);
        header.setUSERID(user.getUserId());
        header.setBRANCH(user.getBranch());
        header.setSERVICE(FCUBS_CUSTOMER_SERVICE);
        header.setOPERATION(operation);
        header.setSOURCEOPERATION(operation);
        return header;
    }

    protected CustomerFullType populateClient() {
        final CustomerFullType client = new CustomerFullType();
        if (StringUtils.isNotEmpty(clientEntity.getCif())) {
            client.setCUSTNO(clientEntity.getCif());
        }
        if (StringUtils.isNotEmpty(clientEntity.getShortName())) {
            client.setSNAME(clientEntity.getShortName());
        } else {
            client.setSNAME(String.valueOf(clientEntity.getId()));
        }
        client.setNAME(clientEntity.getFullName());
        client.setCTYPE(TYPE_OF_CIF);
        client.setLBRN(clientEntity.getBranch());
        client.setCCATEG(CLIENT_CATEGORY);
        final Address currentAddress = clientEntity.getCurrentAddress();
        client.setADDRS5(currentAddress.getVillage());
        client.setADDRLN4(currentAddress.getCommune());
        client.setADDRLN3(currentAddress.getDistrict());
        client.setADDRLN2(currentAddress.getProvince());
        client.setADDRLN1(populateAddress1(clientEntity.getCurrentAddress()));
        client.setCOUNTRY(currentAddress.getCountry());
        client.setFULLNAME(clientEntity.getFullName());
        client.setCUSTCLASSFN(CLIENT_CLASS_FN);
        client.setTRACKLIMITS(TRACK_LIMIT);
        client.setNLTY(clientEntity.getNationality());

        populateClientProfile(client);
        populateClientLiability(client);
        populateUdfDetails(client);
        return client;
    }

    protected void populateClientRelationShip(CustomerFullType.Custpersonal clientProfile) {
        final CustomerFullType.Custpersonal.Custdomestic relationship = new CustomerFullType.Custpersonal.Custdomestic();
        relationship.setMARITALSTAT(clientEntity.getMaritalStatus());
        clientProfile.setCustdomestic(relationship);
    }

    protected void populateClientProfile(CustomerFullType client) {
        final CustomerFullType.Custpersonal clientProfile = new CustomerFullType.Custpersonal();
        clientProfile.setMOBNUM(Helper.normalizePhoneNumber(clientEntity.getPhone()));
        clientProfile.setDOB(Helper.getXmlGregorianCalendar(clientEntity.getDob()));
        final String[] names = splitName(clientEntity.getKhmerFullName());
        clientProfile.setLSTNAME(names[0]);
        clientProfile.setFSTNAME(getFirstName(names));
        clientProfile.setGENDR(clientEntity.getGender());
        clientProfile.setRESSTATUS(MOCK_RESIDENT_STATUS);
        clientProfile.setLANG(LANGUAGE);
        clientProfile.setTITLE(Helper.getClientPrefix(clientEntity.getGender()));
        clientProfile.setMOBISDNO(BigDecimal.valueOf(855));
        if (clientEntity.getBirthAddress() != null) {
            clientProfile.setPLACEOFBIRTH(clientEntity.getBirthAddress().getProvince());
            clientProfile.setPLACEOFBIRTH(clientEntity.getBirthAddress().getCountry());
        }
        populatePassport(clientProfile, clientEntity.getIdentifications());
        populateIdentification(clientProfile, clientEntity.getIdentifications());
        populateClientRelationShip(clientProfile);
        populateClientJob(clientProfile);
        client.setCustpersonal(clientProfile);
    }

    protected void populateIdentification(CustomerFullType.Custpersonal clientProfile, List<ClientIdentification> entities) {
        final Optional<ClientIdentification> optionalId = entities.stream().filter(id -> id.getClientIdentificationTypeId() != ClientIdentification.IDENTIFICATION_PASSPORT_TYPE_ID).findFirst();
        final ClientIdentification identification = optionalId.orElse(entities.get(0));
        clientProfile.setNATIONID(identification.getIdNumber());
        clientProfile.setNATIONIDTYPE(identification.getClientIdentificationType());
        final XMLGregorianCalendar expiredDate = identification.getExpiryDate() == null ? Helper.getXmlGregorianCalendar(LocalDate.parse(MOCK_EXPIRED_DATE))
                : Helper.getXmlGregorianCalendar(identification.getExpiryDate());
        clientProfile.setNIDEXPDT(expiredDate);
        XMLGregorianCalendar issueDate = identification.getIssueDate() == null ? Helper.getXmlGregorianCalendar(LocalDate.parse(MOCK_ISSUE_DATE))
                : Helper.getXmlGregorianCalendar(identification.getIssueDate());
        clientProfile.setNIDISSDT(issueDate);
    }

    protected void populatePassport(CustomerFullType.Custpersonal clientProfile, List<ClientIdentification> entities) {
        final Optional<ClientIdentification> optionalPassport = entities.stream().filter(id -> id.getClientIdentificationTypeId() == ClientIdentification.IDENTIFICATION_PASSPORT_TYPE_ID).findFirst();
        if (optionalPassport.isPresent()) {
            final ClientIdentification password = optionalPassport.get();
            clientProfile.setPPTNO(password.getIdNumber());
            final XMLGregorianCalendar expiredDate = password.getExpiryDate() == null ? Helper.getXmlGregorianCalendar(LocalDate.parse(MOCK_EXPIRED_DATE))
                    : Helper.getXmlGregorianCalendar(password.getExpiryDate());
            clientProfile.setPPTEXPDT(expiredDate);
            XMLGregorianCalendar issueDate = password.getIssueDate() == null ? Helper.getXmlGregorianCalendar(LocalDate.parse(MOCK_ISSUE_DATE))
                    : Helper.getXmlGregorianCalendar(password.getIssueDate());
            clientProfile.setPPTISSDT(issueDate);
        }
    }

    protected void populateClientLiability(CustomerFullType client) {
        CustomerFullType.CustLiab clientLiability = new CustomerFullType.CustLiab();
        clientLiability.setLIABILTYNAME(clientEntity.getFullName());
        clientLiability.setLIABBRANCH(clientEntity.getBranch());
        clientLiability.setLIABCCY(MOCK_LIABILITY_CURRENCY);
        clientLiability.setOVERLIMIT(Helper.convertStringToBigDecimal(MOCK_LIABILITY_OVER_LIMIT));
        client.setCustLiab(clientLiability);
    }

    protected void populateClientJob(CustomerFullType.Custpersonal clientProfile) {
        CustomerFullType.Custpersonal.Custprof clientEmployment = new CustomerFullType.Custpersonal.Custprof();
        clientEmployment.setEMPSTAT(clientEntity.getClientOccupation());
        clientEmployment.setEMPTENURE(BigDecimal.valueOf(clientEntity.getWorkingPeriod()));
        clientEmployment.setAMTCCY1(MOCK_EMPLOYEE_CURRENCY);
        clientEmployment.setEMPLOYERDESC(clientEntity.getWorkplaceName());
        clientEmployment.setSALARY(BigDecimal.valueOf(clientEntity.getIncomeAmount()));
        clientEmployment.setSALARYFREQ(SALARY_FREQUENCY);
        final Address workplaceAddress = clientEntity.getWorkplaceAddress();
        clientEmployment.setADD1(populateAddress1(workplaceAddress));
        clientEmployment.setADD2(workplaceAddress.getProvince());
        clientEmployment.setADD3(workplaceAddress.getDistrict());
        clientEmployment.setEADDRESS4(workplaceAddress.getCommune());
        clientEmployment.setADD5(workplaceAddress.getVillage());
        clientProfile.setCustprof(clientEmployment);
    }

    protected void populateUdfDetails(CustomerFullType client) {
        UDFDETAILSType2 udf1 = new UDFDETAILSType2();
        udf1.setFLDNAM(MOCK_UDF_NAME_1);
        udf1.setFLDVAL(MOCK_UDF_VALUE_1);

        UDFDETAILSType2 udf2 = new UDFDETAILSType2();
        udf2.setFLDNAM(MOCK_UDF_NAME_2);
        udf2.setFLDVAL(clientEntity.getStaffId());

        UDFDETAILSType2 udf3 = new UDFDETAILSType2();
        udf3.setFLDNAM(MOCK_UDF_NAME_3);
        udf3.setFLDVAL(MOCK_UDF_VALUE_3);

        UDFDETAILSType2 udf6 = new UDFDETAILSType2();
        udf6.setFLDNAM(MOCK_UDF_NAME_6);
        udf6.setFLDVAL(this.clientEntity.getBranch());

        client.getUDFDETAILS().add(udf1);
        client.getUDFDETAILS().add(udf2);
        client.getUDFDETAILS().add(udf3);
        client.getUDFDETAILS().add(udf6);
    }
}
