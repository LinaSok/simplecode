package com.dminc.prasac.client.integration.client.service;

import com.dminc.prasac.client.model.dto.Client;
import com.dminc.rest.exceptions.InternalBusinessException;
import com.dminc.rest.exceptions.ItemNotFoundBusinessException;

public interface ClientService {

    Client create(Client client) throws InternalBusinessException, ItemNotFoundBusinessException;
}
