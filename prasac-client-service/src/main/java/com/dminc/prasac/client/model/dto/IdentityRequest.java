package com.dminc.prasac.client.model.dto;

import lombok.Data;

@Data
public class IdentityRequest {
    private String identityNumber;
    private String identityType;
}
