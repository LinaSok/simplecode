package com.dminc.prasac.client.controller;

import com.dminc.prasac.client.integration.saving.account.service.SavingAccountService;
import com.dminc.prasac.client.model.dto.SavingAccount;
import com.dminc.rest.exceptions.BusinessException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/saving-account")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SavingAccountController {

    private final SavingAccountService service;

    @PostMapping()
    public SavingAccount createOrUpdate(@RequestBody SavingAccount account) throws BusinessException {
        return service.createOrUpdate(account);
    }

}
