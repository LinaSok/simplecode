package com.dminc.prasac.client.integration.loan;

import com.dminc.prasac.client.model.LocalConstants;
import com.dminc.prasac.client.model.dto.Client;

import java.util.List;

@SuppressWarnings({"PMD"})
public final class CBCFeeHelper {

    private CBCFeeHelper() {
        // do nothing
    }

    public static Double getAmount(String currencyCode, Double loanAmount, List<Client> clients) {
        final long borrower = clients.stream().filter(client -> LocalConstants.BORROWER_CODE.equals(client.getClientType())).count();
        final long coBorrower = clients.stream().filter(client -> LocalConstants.CO_BORROWER_CODE.equals(client.getClientType())).count();
        final long guarantor = clients.stream().filter(client -> LocalConstants.GUARANTOR_CODE.equals(client.getClientType())).count();
        switch (currencyCode) {
            case LocalConstants.US_DOLLAR:
                if (loanAmount <= 500) {
                    return (borrower * 0.18) + (coBorrower * 0.18) + (guarantor * 0.18);
                } else if (loanAmount <= 1000) {
                    return (borrower * 1.1) + (coBorrower * 0.6) + (guarantor * 0.6);
                } else if (loanAmount <= 3000) {
                    return (borrower * 2.3) + (coBorrower * 0.6) + (guarantor * 0.6);
                } else if (loanAmount <= 10000) {
                    return (borrower * 2.6) + (coBorrower * 0.8) + (guarantor * 0.8);
                } else {
                    return (borrower * 3.6) + (coBorrower * 1.3) + (guarantor * 1.3);
                }
            case LocalConstants.KHMER_CURRENCY:
                if (loanAmount <= 2000000) {
                    return (double) ((borrower * 700) + (coBorrower * 700) + (guarantor * 700));
                } else if (loanAmount <= 4000000) {
                    return (double) ((borrower * 4000) + (coBorrower * 2000) + (guarantor * 2000));
                } else if (loanAmount <= 12000000) {
                    return (double) ((borrower * 9000) + (coBorrower * 2000) + (guarantor * 2000));
                } else if (loanAmount <= 40000000) {
                    return (double) ((borrower * 10000) + (coBorrower * 3000) + (guarantor * 3000));
                } else {
                    return (double) ((borrower * 14000) + (coBorrower * 5000) + (guarantor * 5000));
                }
            case LocalConstants.THAI_CURRENCY:
                if (loanAmount <= 15500) {
                    return (double) ((borrower * 7) + (coBorrower * 7) + (guarantor * 7));
                } else if (loanAmount <= 31000) {
                    return (double) ((borrower * 40) + (coBorrower * 20) + (guarantor * 20));
                } else if (loanAmount <= 93000) {
                    return (double) ((borrower * 90) + (coBorrower * 20) + (guarantor * 20));
                } else if (loanAmount <= 310000) {
                    return (double) ((borrower * 100) + (coBorrower * 30) + (guarantor * 30));
                } else {
                    return (double) ((borrower * 140) + (coBorrower * 50) + (guarantor * 50));
                }
            default:
                return null;
        }
    }
}
