package com.dminc.prasac.client.integration.cbc.service;

import com.dminc.prasac.client.integration.cbc.domain.Cbc;
import com.dminc.prasac.client.model.cbc.Loan;
import com.dminc.rest.exceptions.InternalBusinessException;

public interface CbcService {
    Cbc getReport(Loan loan) throws InternalBusinessException;
}
