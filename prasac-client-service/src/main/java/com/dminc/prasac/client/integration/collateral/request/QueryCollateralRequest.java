package com.dminc.prasac.client.integration.collateral.request;

import com.dminc.prasac.client.model.dto.User;
import lombok.Builder;
import prasac.webapi.wsdl.collteral.CollateralQueryIOType;
import prasac.webapi.wsdl.collteral.QUERYCOLLATERALIOFSREQ;

public class QueryCollateralRequest extends CollateralRequest {
    public static final String OPERATION = "QueryCollateral";

    private final String clientCif;
    private final String code;

    @Builder
    public QueryCollateralRequest(String source, User user, String clientCif, String code) {
        super(OPERATION, source, user);
        this.clientCif = clientCif;
        this.code = code;
    }

    public QUERYCOLLATERALIOFSREQ getRequest() {
        final QUERYCOLLATERALIOFSREQ request = new QUERYCOLLATERALIOFSREQ();
        request.setFCUBSHEADER(populateHeader());
        final QUERYCOLLATERALIOFSREQ.FCUBSBODY body = new QUERYCOLLATERALIOFSREQ.FCUBSBODY();
        final CollateralQueryIOType query = new CollateralQueryIOType();
        query.setCOLLATERALCODE(code);
        query.setLIABNO(clientCif);
        body.setCollateralsIO(query);
        request.setFCUBSBODY(body);
        return request;
    }
}
