package com.dminc.prasac.client.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClientIdentification implements Serializable {

    public static final long IDENTIFICATION_PASSPORT_TYPE_ID = 5L;

    private static final long serialVersionUID = -1761733874731713556L;

    private Long id;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate expiryDate;

    private String idNumber;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate issueDate;

    private String clientIdentificationCbc;
    private String clientIdentificationType;
    private Long clientIdentificationTypeId;
}
