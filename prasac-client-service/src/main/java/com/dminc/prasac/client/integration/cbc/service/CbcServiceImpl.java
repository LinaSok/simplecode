package com.dminc.prasac.client.integration.cbc.service;

import com.dminc.prasac.client.integration.cbc.domain.Cbc;
import com.dminc.prasac.client.integration.cbc.domain.CbcRequest;
import com.dminc.prasac.client.integration.cbc.domain.request.Request;
import com.dminc.prasac.client.model.cbc.Loan;
import com.dminc.rest.exceptions.InternalBusinessException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CbcServiceImpl implements CbcService {
    public static final Charset CHARSET_UTF_8 = Charset.forName("UTF-8");

    private static final String PATH = "/enquiry/inthttp.pgm";
    private final RestTemplate template;
    @Value("${cbc.url}")
    private String domain;
    @Value("${cbc.token}")
    private String token;

    @Value("${cbc.use-mock:true}")
    private boolean usedMock;

    @Value("${cbc.user-id}")
    private String userId;

    @Override
    public Cbc getReport(Loan loan) throws InternalBusinessException {

        final CbcRequest cbcRequest = CbcRequest.builder().loan(loan).userId(userId).build();
        final HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, token);
        headers.setContentType(MediaType.APPLICATION_XML);
        template.getMessageConverters().add(0, new StringHttpMessageConverter(CHARSET_UTF_8));
        final HttpEntity<Request> entity = new HttpEntity<>(cbcRequest.getRequest(), headers);
        final String requestUrl = String.format("%s%s", domain, PATH);
        final ResponseEntity<String> responseEntity = template.postForEntity(requestUrl, entity, String.class);
        try {
            final String xmlRequest = convertJaxbToXML(cbcRequest);
            final Cbc cbc = Cbc.builder()
                    .requestXML(xmlRequest)
                    .build();
            if (usedMock) {
                cbc.setResponseXML(readMockData());
            } else {
                cbc.setResponseXML(responseEntity.getBody());
            }
            return cbc;
        } catch (IOException | JAXBException e) {
            throw new InternalBusinessException(e.getMessage(), 6000, e);
        }
    }

    private String readMockData() throws IOException {
        log.info("Apply the MockData");
        final InputStream inputStream = new ClassPathResource("static/cbcResponseMock.xml").getInputStream();
        return IOUtils.toString(inputStream, CHARSET_UTF_8);
    }

    private String convertJaxbToXML(CbcRequest request) throws JAXBException {
        final JAXBContext jaxbContext = JAXBContext.newInstance(Request.class);
        final Marshaller marshaller = jaxbContext.createMarshaller();
        final StringWriter stringWriter = new StringWriter();
        marshaller.marshal(request.getRequest(), stringWriter);
        return stringWriter.toString();
    }
}
