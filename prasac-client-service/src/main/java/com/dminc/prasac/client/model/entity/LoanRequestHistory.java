package com.dminc.prasac.client.model.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "PRAVW_LN_REQUEST_HISTORY")
@Data
@SuppressWarnings({"PMD.TooManyFields"})
public class LoanRequestHistory {

    @EmbeddedId
    private LoanRequestId id;

    @Column(name = "CIF_NUMBER", insertable = false, updatable = false)
    private String clientCif;

    @Column(name = "CLIENT_TYPE")
    private String clientType;

    @Column(name = "FULL_NAME")
    private String fullName;

    @Column(name = "LOCAL_NMAE_KH")
    private String khmerFullname;

    @Column(name = "DOB")
    private LocalDate dob;

    @Column(name = "ID_TYPE")
    private String identityType;

    @Column(name = "ID_NUMBER")
    private String identityNumber;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "MARITAL_STATUS")
    private String maritalStatus;

    @Column(name = "NO")
    private Integer no;

    @Column(name = "REQUEST_DATE")
    private LocalDate requestDate;

    @Column(name = "REQUEST_AMOUNT")
    private Double requestAmount;

    @Column(name = "CURRENCY")
    private String currency;

    @Column(name = "REQUEST_STATUS")
    private String requestStatus;

    @Column(name = "REJECT_REASONS")
    private String rejectReason;
}
