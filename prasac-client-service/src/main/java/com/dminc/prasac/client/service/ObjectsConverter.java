package com.dminc.prasac.client.service;

import com.dminc.rest.mapping.DefaultOrikaObjectsConverter;
import ma.glasnost.orika.MapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by carlos on 14/11/17.
 */
@Service
public class ObjectsConverter extends DefaultOrikaObjectsConverter {

    @Autowired
    public ObjectsConverter(MapperFactory orikaMapperFactory) {
        super(orikaMapperFactory);
    }
}
