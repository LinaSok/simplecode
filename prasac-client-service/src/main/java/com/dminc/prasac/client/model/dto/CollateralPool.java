package com.dminc.prasac.client.model.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CollateralPool implements Serializable {
    private static final long serialVersionUID = -1046886877797179782L;

    private String collateralPoolCode;

    /**
     * 432
     */
    private String branchCode;
    /**
     * USD
     */
    private String currencyCode;
    /**
     * 1731
     */
    private List<String> collateralCodes;

    private String clientCif;

}
