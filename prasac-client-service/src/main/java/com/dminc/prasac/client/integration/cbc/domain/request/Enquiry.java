package com.dminc.prasac.client.integration.cbc.domain.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "ENQUIRY")
@XmlAccessorType(XmlAccessType.FIELD)
public class Enquiry {

    @XmlElement(name = "ENQUIRY_TYPE")
    private String enquiryType;

    @XmlElement(name = "LANGUAGE")
    private String language;

    @XmlElement(name = "PRODUCT_TYPE")
    private String purposeCode;

    @XmlElement(name = "NO_OF_APPLICANTS")
    private String numberOfApplicant;

    @XmlElement(name = "ACCOUNT_TYPE")
    private AccountType accountType;

    @XmlElement(name = "ENQUIRY_REFERENCE")
    private String reference;

    @XmlElement(name = "AMOUNT")
    private String amount;
    @XmlElement(name = "CURRENCY")
    private String currency;

    @XmlElements({@XmlElement(name = "CONSUMER", type = Consumer.class)})
    private List<Consumer> consumer;

    @XmlElement(name = "SCORECARD")
    private String scoreCard;

    @XmlEnum
    public enum AccountType {
        @XmlEnumValue("S")
        SINGLE("S"),
        @XmlEnumValue("J")
        JOIN("J");

        private String type;

        AccountType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }

    }

}
