package com.dminc.prasac.client.integration.loan;

import com.dminc.prasac.client.model.LocalConstants;
import com.dminc.prasac.client.model.dto.Client;
import com.dminc.prasac.client.model.dto.FCLoanRequest;
import com.dminc.prasac.client.model.dto.User;
import com.dminc.prasac.client.util.Helper;
import lombok.Builder;
import prasac.webapi.wsdl.loan.AccountFullType;
import prasac.webapi.wsdl.loan.CREATEACCOUNTFSFSREQ;
import prasac.webapi.wsdl.loan.FCUBSHEADERType;
import prasac.webapi.wsdl.loan.UBSCOMPType;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.dminc.prasac.client.integration.loan.CBCFeeHelper.getAmount;

@SuppressWarnings({"PMD.TooManyMethods"})
public class AccountMasterRequest {

    public static final String SERVICE = "FCUBSCLService";
    public static final String OPERATION = "CreateAccount";
    public static final String PAYMENT_MODE_ACC = "ACC";
    public static final String PRINCIPAL = "PRINCIPAL";
    private final String source;
    private final User user;
    private final FCLoanRequest fcLoanRequest;
    private final int lastLoanCycle;

    @Builder
    public AccountMasterRequest(String source, User user, FCLoanRequest fcLoanRequest, int lastLoanCycle) {
        this.source = source;
        this.user = user;
        this.fcLoanRequest = fcLoanRequest;
        this.lastLoanCycle = lastLoanCycle;
    }

    private FCUBSHEADERType populateHeader() {
        final FCUBSHEADERType header = new FCUBSHEADERType();
        header.setBRANCH(user.getBranch());
        header.setSERVICE(SERVICE);
        header.setOPERATION(OPERATION);
        header.setSOURCEOPERATION(OPERATION);
        header.setSOURCE(source);
        header.setUBSCOMP(UBSCOMPType.FCUBS);
        header.setUSERID(user.getUserId());
        return header;
    }

    public CREATEACCOUNTFSFSREQ getRequest() {
        final CREATEACCOUNTFSFSREQ request = new CREATEACCOUNTFSFSREQ();
        request.setFCUBSHEADER(populateHeader());

        final AccountFullType account = populateAccountMaster();
        final CREATEACCOUNTFSFSREQ.FCUBSBODY body = new CREATEACCOUNTFSFSREQ.FCUBSBODY();
        body.setAccountMasterFull(account);
        request.setFCUBSBODY(body);
        return request;
    }

    @SuppressWarnings("PMD")
    private AccountFullType populateAccountMaster() {
        final AccountFullType request = new AccountFullType();
        request.setCCY(fcLoanRequest.getCurrencyCode());
        request.setBRN(fcLoanRequest.getBranchCode());
        request.setPROD(ProductCodeHelper.getProductCode(fcLoanRequest.getTenure(), fcLoanRequest.getCurrencyCode(), fcLoanRequest.getRepaymentModeCode()));
        request.setCUSTID(getBorrower(fcLoanRequest.getClients()).getCif());
        request.setAMTFINANCED(BigDecimal.valueOf(fcLoanRequest.getLoanAmount()));
        final Client borrower = getBorrower(fcLoanRequest.getClients());
        request.setCRPRODAC(borrower.getSavingAccountNumber());
        request.setDRPRODAC(borrower.getSavingAccountNumber());
        request.setDRACCBRN(fcLoanRequest.getBranchCode());
        request.setCRACCBRN(fcLoanRequest.getBranchCode());
        request.setNETPRIN(BigDecimal.valueOf(fcLoanRequest.getLoanAmount()));
        request.setDRPAYMODE(PAYMENT_MODE_ACC);
        request.setCRPAYMODE(PAYMENT_MODE_ACC);
        request.setCUSTNAME(borrower.getFullName());
        request.setMATDT(getMaturityDate(fcLoanRequest.getTenure()));
        request.getEffecDate().add(populateEffecDate());
        request.getOthrApplicants().addAll(populateOtherApplicant());
        request.getClvwsAccountUdfChar().addAll(populateAccountUDFChars());
        request.getClvwsAccountUdfNum().addAll(populateAccountUDFNums());
        request.getCollLinkages().add(populateLinkage());
        request.getComponents().add(populatePenalPreComponent());
        request.getComponents().add(populateMainInt2Component());
        request.getComponents().add(populateMainIntComponent());
        request.getComponents().add(populatePenalOvrComponent());
        request.getComponents().add(populatePrincipalComponent());

        request.getChgComp().addAll(populateComponents());
        request.getChgComp().add(populateProChargeComponent());
        request.getChgComp().add(populateLoanChargeComponent());
        return request;
    }

    private AccountFullType.EffecDate populateEffecDate() {
        final AccountFullType.EffecDate effecDate = new AccountFullType.EffecDate();
        effecDate.setEFFDT(Helper.getXmlGregorianCalendar(getNow()));

        final AccountFullType.EffecDate.UdeVals cbcFee = new AccountFullType.EffecDate.UdeVals();
        cbcFee.setUDEID("FEE_CBC_AMOUNT");
        cbcFee.setRESOLVEDVAL(BigDecimal.valueOf(44));
        cbcFee.setRATEBASIS("A");
        cbcFee.setUDEVAL(BigDecimal.valueOf(getAmount(fcLoanRequest.getCurrencyCode(), fcLoanRequest.getLoanAmount(), fcLoanRequest.getClients())));
        effecDate.getUdeVals().add(cbcFee);

        final AccountFullType.EffecDate.UdeVals loanAmount = new AccountFullType.EffecDate.UdeVals();
        loanAmount.setUDEID("FEE_LOAN_AMOUNT");
        loanAmount.setUDEVAL(BigDecimal.valueOf(fcLoanRequest.getLoanFee()));
        loanAmount.setRESOLVEDVAL(BigDecimal.valueOf(48));
        loanAmount.setRATEBASIS("N");
        effecDate.getUdeVals().add(loanAmount);

        final AccountFullType.EffecDate.UdeVals interestRate = new AccountFullType.EffecDate.UdeVals();
        interestRate.setUDEID("INTEREST_RATE");
        interestRate.setUDEVAL(BigDecimal.valueOf(fcLoanRequest.getInterestRate()));
        interestRate.setRESOLVEDVAL(BigDecimal.valueOf(52));
        interestRate.setRATEBASIS("A");
        effecDate.getUdeVals().add(interestRate);

        final AccountFullType.EffecDate.UdeVals interestRate2 = new AccountFullType.EffecDate.UdeVals();
        interestRate2.setUDEID("INTEREST_RATE2");
        interestRate2.setUDEVAL(BigDecimal.valueOf(fcLoanRequest.getInterestRate()));
        interestRate2.setRESOLVEDVAL(BigDecimal.valueOf(56));
        interestRate2.setRATEBASIS("A");
        effecDate.getUdeVals().add(interestRate2);

        final AccountFullType.EffecDate.UdeVals orate = new AccountFullType.EffecDate.UdeVals();
        orate.setUDEID("PNL_ORATE");
        orate.setUDEVAL(BigDecimal.valueOf(24));
        orate.setRESOLVEDVAL(BigDecimal.valueOf(60));
        orate.setRATEBASIS("A");
        effecDate.getUdeVals().add(orate);

        final AccountFullType.EffecDate.UdeVals rate = new AccountFullType.EffecDate.UdeVals();
        rate.setUDEID("PNL_RATE");
        rate.setRESOLVEDVAL(BigDecimal.ZERO);
        rate.setRATEBASIS("A");
        rate.setUDEVAL(BigDecimal.ZERO);
        effecDate.getUdeVals().add(rate);
        return effecDate;
    }

    // TODO return LocalDate.now() in Production, because UAT servser current date is 26-12-2018
    private LocalDate getNow() {
        return LocalDate.of(2018, Month.DECEMBER, 26);
    }

    private AccountFullType.Components populateMainIntComponent() {
        final AccountFullType.Components component = new AccountFullType.Components();
        final Client borrower = getBorrower(fcLoanRequest.getClients());
        component.setCOMPNAME("MAIN_INT");
        component.setDRACCBRN(fcLoanRequest.getBranchCode());
        component.setCRACCBRN(fcLoanRequest.getBranchCode());
        component.setCRPRODAC(borrower.getSavingAccountNumber());
        component.setDRPRODAC(borrower.getSavingAccountNumber());
        component.setSETTLCCY(fcLoanRequest.getCurrencyCode());
        component.setCMPTYPE("I");
        component.setMAINCOMP("Y");
        component.setDRPAYMENTMODE(PAYMENT_MODE_ACC);
        component.setCRPAYMENTMODE(PAYMENT_MODE_ACC);
        component.setVERIFYFUNDS("Y");
        final AccountFullType.Components.CompSch compSch = new AccountFullType.Components.CompSch();
        compSch.setUNIT("M");
        compSch.setCAPITALIZED("N");
        compSch.setWAIVERFLG("N");
        compSch.setFREQ(BigDecimal.ONE);
        compSch.setFORMULANAME("MAIN_INT_FRM_1");
        compSch.setFIRSTDUEDT(getFirstDueDate());
        compSch.setSCHTYP("P");
        compSch.setSCHFLG("N");
        compSch.setNOOFSCH(BigDecimal.valueOf(2));
        component.getCompSch().add(compSch);
        return component;
    }

    private AccountFullType.Components populateMainInt2Component() {
        final AccountFullType.Components component = new AccountFullType.Components();
        final Client borrower = getBorrower(fcLoanRequest.getClients());
        component.setCOMPNAME("MAIN_INT2");
        component.setDRACCBRN(fcLoanRequest.getBranchCode());
        component.setCRACCBRN(fcLoanRequest.getBranchCode());
        component.setCRPRODAC(borrower.getSavingAccountNumber());
        component.setDRPRODAC(borrower.getSavingAccountNumber());
        component.setVERIFYFUNDS("Y");
        component.setSETTLCCY(fcLoanRequest.getCurrencyCode());
        component.setCMPTYPE("P");
        component.setPENALBASISCOMP(PRINCIPAL);
        component.setDRPAYMENTMODE(PAYMENT_MODE_ACC);
        component.setCRPAYMENTMODE(PAYMENT_MODE_ACC);
        return component;
    }

    private AccountFullType.Components populatePenalOvrComponent() {
        final AccountFullType.Components component = new AccountFullType.Components();
        component.setCOMPNAME("PENAL_OVR");
        final Client borrower = getBorrower(fcLoanRequest.getClients());
        component.setDRACCBRN(fcLoanRequest.getBranchCode());
        component.setCRACCBRN(fcLoanRequest.getBranchCode());
        component.setCRPRODAC(borrower.getSavingAccountNumber());
        component.setDRPRODAC(borrower.getSavingAccountNumber());
        component.setVERIFYFUNDS("Y");
        component.setSETTLCCY(fcLoanRequest.getCurrencyCode());
        component.setPENALBASISCOMP(PRINCIPAL);
        component.setDRPAYMENTMODE(PAYMENT_MODE_ACC);
        component.setCRPAYMENTMODE(PAYMENT_MODE_ACC);
        return component;
    }

    private AccountFullType.Components populatePenalPreComponent() {
        final AccountFullType.Components component = new AccountFullType.Components();
        component.setCOMPNAME("PENAL_PRE");
        final Client borrower = getBorrower(fcLoanRequest.getClients());
        component.setDRACCBRN(fcLoanRequest.getBranchCode());
        component.setCRACCBRN(fcLoanRequest.getBranchCode());
        component.setCRPRODAC(borrower.getSavingAccountNumber());
        component.setDRPRODAC(borrower.getSavingAccountNumber());
        component.setVERIFYFUNDS("Y");
        component.setSETTLCCY(fcLoanRequest.getCurrencyCode());
        component.setPENALBASISCOMP(PRINCIPAL);
        component.setDRPAYMENTMODE(PAYMENT_MODE_ACC);
        component.setCRPAYMENTMODE(PAYMENT_MODE_ACC);
        return component;
    }

    private AccountFullType.Components populatePrincipalComponent() {
        final AccountFullType.Components component = new AccountFullType.Components();
        component.setCOMPNAME(PRINCIPAL);
        final Client borrower = getBorrower(fcLoanRequest.getClients());
        component.setDRACCBRN(fcLoanRequest.getBranchCode());
        component.setCRACCBRN(fcLoanRequest.getBranchCode());
        component.setCRPRODAC(borrower.getSavingAccountNumber());
        component.setDRPRODAC(borrower.getSavingAccountNumber());
        component.setVERIFYFUNDS("Y");
        component.setSETTLCCY(fcLoanRequest.getCurrencyCode());
        component.setDRPAYMENTMODE(PAYMENT_MODE_ACC);
        component.setCRPAYMENTMODE(PAYMENT_MODE_ACC);

        final AccountFullType.Components.CompSch compSch = new AccountFullType.Components.CompSch();
        compSch.setUNIT("D");
        compSch.setAMT(BigDecimal.valueOf(fcLoanRequest.getLoanAmount()));
        compSch.setFIRSTDUEDT(Helper.getXmlGregorianCalendar(getNow()));
        compSch.setFREQ(BigDecimal.ONE);
        compSch.setSCHSTARTDT(Helper.getXmlGregorianCalendar(getNow()));
        compSch.setNOOFSCH(BigDecimal.ONE);
        compSch.setSCHTYP("D");
        compSch.setSCHFLG("N");

        final AccountFullType.Components.CompSch compSch1 = new AccountFullType.Components.CompSch();
        compSch1.setUNIT("M");
        compSch1.setAMT(BigDecimal.ZERO);
        compSch1.setCAPITALIZED("N");
        compSch1.setFIRSTDUEDT(getFirstDueDate());
        compSch1.setWAIVERFLG("N");
        compSch1.setFREQ(BigDecimal.ONE);
        compSch1.setNOOFSCH(BigDecimal.ONE);
        compSch1.setSCHTYP("P");
        compSch1.setSCHFLG("N");
        component.getCompSch().add(compSch);
        component.getCompSch().add(compSch1);

        return component;
    }

    /**
     * Generate Component for "ADHOC", "ADHOC_PNL", "GEN_PROV", "PROV_PRIN"
     * Because they have the same field value
     */
    private List<AccountFullType.ChgComp> populateComponents() {
        final List<String> components = Arrays.asList("ADHOC", "ADHOC_PNL", "GEN_PROV", "PROV_PRIN");
        final Client borrower = getBorrower(fcLoanRequest.getClients());
        return components.stream().map(name -> {
            final AccountFullType.ChgComp comp = new AccountFullType.ChgComp();
            comp.setCOMPNAME(name);
            comp.setDRACCBRN(fcLoanRequest.getBranchCode());
            comp.setCRACCBRN(fcLoanRequest.getBranchCode());
            comp.setCRPRODAC(borrower.getSavingAccountNumber());
            comp.setDRPRODAC(borrower.getSavingAccountNumber());
            comp.setDRPAYMODE(PAYMENT_MODE_ACC);
            comp.setCRPAYMODE(PAYMENT_MODE_ACC);
            comp.setSETTLCHGCCY(fcLoanRequest.getCurrencyCode());
            return comp;
        }).collect(Collectors.toList());
    }

    private AccountFullType.ChgComp populateLoanChargeComponent() {
        final AccountFullType.ChgComp loanCharge = new AccountFullType.ChgComp();
        final Client borrower = getBorrower(fcLoanRequest.getClients());
        loanCharge.setCOMPNAME("LOAN_CHARGE");
        loanCharge.setDRACCBRN(fcLoanRequest.getBranchCode());
        loanCharge.setCRACCBRN(fcLoanRequest.getBranchCode());
        loanCharge.setCRPRODAC(borrower.getSavingAccountNumber());
        loanCharge.setDRPRODAC(borrower.getSavingAccountNumber());
        loanCharge.setDRPAYMODE(PAYMENT_MODE_ACC);
        loanCharge.setCRPAYMODE(PAYMENT_MODE_ACC);
        loanCharge.setSETTLCHGCCY(fcLoanRequest.getCurrencyCode());
        final AccountFullType.ChgComp.ChgSch chgSch = new AccountFullType.ChgComp.ChgSch();
        chgSch.setFORMULANAME("LOAN_CHARGE_FRM_1");
        loanCharge.getChgSch().add(chgSch);

        final AccountFullType.ChgComp.ChgDtls chgDtls = new AccountFullType.ChgComp.ChgDtls();
        chgDtls.setEVENTCD("DSBR");
        chgDtls.setCOMPNAME("LOAN_CHARGE");
        loanCharge.getChgDtls().add(chgDtls);

        return loanCharge;
    }

    private AccountFullType.ChgComp populateProChargeComponent() {
        final AccountFullType.ChgComp proCharge = new AccountFullType.ChgComp();
        final Client borrower = getBorrower(fcLoanRequest.getClients());
        proCharge.setCOMPNAME("PRO_CHARGE");
        proCharge.setDRACCBRN(fcLoanRequest.getBranchCode());
        proCharge.setCRACCBRN(fcLoanRequest.getBranchCode());
        proCharge.setCRPRODAC(borrower.getSavingAccountNumber());
        proCharge.setDRPRODAC(borrower.getSavingAccountNumber());
        proCharge.setDRPAYMODE(PAYMENT_MODE_ACC);
        proCharge.setCRPAYMODE(PAYMENT_MODE_ACC);
        proCharge.setSETTLCHGCCY(fcLoanRequest.getCurrencyCode());
        final AccountFullType.ChgComp.ChgSch chgSch = new AccountFullType.ChgComp.ChgSch();
        chgSch.setFORMULANAME("PRO_CHARGE_FRM_1");
        proCharge.getChgSch().add(chgSch);

        final AccountFullType.ChgComp.ChgDtls chgDtls = new AccountFullType.ChgComp.ChgDtls();
        chgDtls.setEVENTCD("DSBR");
        chgDtls.setCOMPNAME("PRO_CHARGE");
        proCharge.getChgDtls().add(chgDtls);

        return proCharge;
    }

    private List<AccountFullType.OthrApplicants> populateOtherApplicant() {
        return fcLoanRequest.getClients().stream().map(client -> {
            final AccountFullType.OthrApplicants otherApplicant = new AccountFullType.OthrApplicants();
            otherApplicant.setCUSTID(client.getCif());
            otherApplicant.setCUSTNAME(client.getFullName());
            otherApplicant.setRESPONSIBILITY(client.getClientType());
            otherApplicant.setLIABILITY(BigDecimal.ZERO);
            otherApplicant.setEFFECDATE(Helper.getXmlGregorianCalendar(getNow()));
            return otherApplicant;
        }).collect(Collectors.toList());
    }

    private List<AccountFullType.ClvwsAccountUdfChar> populateAccountUDFChars() {

        final List<AccountFullType.ClvwsAccountUdfChar> accountUdfs = new ArrayList<>();

        final AccountFullType.ClvwsAccountUdfChar co = new AccountFullType.ClvwsAccountUdfChar();
        co.setFIELDNAME("CREDIT OFFICER");
        co.setFIELDCHAR1(fcLoanRequest.getStaffId());
        accountUdfs.add(co);

        final AccountFullType.ClvwsAccountUdfChar loanPurpose = new AccountFullType.ClvwsAccountUdfChar();
        loanPurpose.setFIELDNAME("LOAN PURPOSE");
        loanPurpose.setFIELDCHAR1(fcLoanRequest.getLoanPurposeCode());
        accountUdfs.add(loanPurpose);

        final AccountFullType.ClvwsAccountUdfChar loanCollateral = new AccountFullType.ClvwsAccountUdfChar();
        loanCollateral.setFIELDNAME("LOAN COLLATERAL");
        loanCollateral.setFIELDCHAR1("206");
        accountUdfs.add(loanCollateral);

        final AccountFullType.ClvwsAccountUdfChar creditScore = new AccountFullType.ClvwsAccountUdfChar();
        creditScore.setFIELDNAME("CREDIT SCORING");
        creditScore.setFIELDCHAR1("100");
        accountUdfs.add(creditScore);

        final AccountFullType.ClvwsAccountUdfChar creditCommittee = new AccountFullType.ClvwsAccountUdfChar();
        creditCommittee.setFIELDNAME("CREDIT COMMITTEE");
        creditCommittee.setFIELDCHAR1("99");
        accountUdfs.add(creditCommittee);

        final AccountFullType.ClvwsAccountUdfChar repaymentMode = new AccountFullType.ClvwsAccountUdfChar();
        repaymentMode.setFIELDNAME("REPAYMENT MODE");
        repaymentMode.setFIELDCHAR1(fcLoanRequest.getRepaymentModeCode());
        accountUdfs.add(repaymentMode);

        final AccountFullType.ClvwsAccountUdfChar repaymentSource = new AccountFullType.ClvwsAccountUdfChar();
        repaymentSource.setFIELDNAME("REPAYMENT SOURCE");
        repaymentSource.setFIELDCHAR1("99");
        accountUdfs.add(repaymentSource);

        final AccountFullType.ClvwsAccountUdfChar responseCo = new AccountFullType.ClvwsAccountUdfChar();
        responseCo.setFIELDNAME("RESPONSE CO");
        accountUdfs.add(responseCo);

        final AccountFullType.ClvwsAccountUdfChar creditor = new AccountFullType.ClvwsAccountUdfChar();
        creditor.setFIELDNAME("CREDITOR");
        creditor.setFIELDCHAR1("NA");
        accountUdfs.add(creditor);

        final AccountFullType.ClvwsAccountUdfChar rejection = new AccountFullType.ClvwsAccountUdfChar();
        rejection.setFIELDNAME("REJECTION");
        accountUdfs.add(rejection);

        final AccountFullType.ClvwsAccountUdfChar acquired = new AccountFullType.ClvwsAccountUdfChar();
        acquired.setFIELDNAME("ACQUIRED BY");
        acquired.setFIELDCHAR1("NA");
        accountUdfs.add(acquired);

        return accountUdfs;
    }

    private AccountFullType.CollLinkages populateLinkage() {
        final AccountFullType.CollLinkages collLinkages = new AccountFullType.CollLinkages();
        collLinkages.setLINKAGETYPE("P");
        collLinkages.setLINKEDREFNO(fcLoanRequest.getCollateralPoolCode());
        collLinkages.setLINKEDPER(BigDecimal.valueOf(100));
        collLinkages.setUTILIZATIONNO(BigDecimal.ONE);
        return collLinkages;
    }

    private List<AccountFullType.ClvwsAccountUdfNum> populateAccountUDFNums() {
        final List<AccountFullType.ClvwsAccountUdfNum> accountUdfNums = new ArrayList<>();

        final AccountFullType.ClvwsAccountUdfNum loanCycle = new AccountFullType.ClvwsAccountUdfNum();
        loanCycle.setLBLFIELDNUM("LOAN CYCLE");
        loanCycle.setFIELDVALUE(BigDecimal.valueOf(lastLoanCycle + 1));
        accountUdfNums.add(loanCycle);

        final AccountFullType.ClvwsAccountUdfNum totalClient = new AccountFullType.ClvwsAccountUdfNum();
        totalClient.setLBLFIELDNUM("TOTAL CLIENT");
        totalClient.setFIELDVALUE(BigDecimal.ONE);
        accountUdfNums.add(totalClient);

        final AccountFullType.ClvwsAccountUdfNum femaleClient = new AccountFullType.ClvwsAccountUdfNum();
        femaleClient.setLBLFIELDNUM("FEMALE CLIENT");
        femaleClient.setFIELDVALUE(getBorrowerGender(fcLoanRequest.getClients()));
        accountUdfNums.add(femaleClient);

        final AccountFullType.ClvwsAccountUdfNum subBranch = new AccountFullType.ClvwsAccountUdfNum();
        subBranch.setLBLFIELDNUM("SUB-BRANCH OFFICE");
        accountUdfNums.add(subBranch);

        final AccountFullType.ClvwsAccountUdfNum loanWithOther = new AccountFullType.ClvwsAccountUdfNum();
        loanWithOther.setLBLFIELDNUM("LOAN WITH OTHER");
        loanWithOther.setFIELDVALUE(BigDecimal.valueOf(9));
        accountUdfNums.add(loanWithOther);

        final AccountFullType.ClvwsAccountUdfNum loanTerm = new AccountFullType.ClvwsAccountUdfNum();
        loanTerm.setLBLFIELDNUM("MINIMUM LOAN TERM");
        loanTerm.setFIELDVALUE(BigDecimal.valueOf(fcLoanRequest.getLockInPeriod()));
        accountUdfNums.add(loanTerm);

        final AccountFullType.ClvwsAccountUdfNum lastLoanSize = new AccountFullType.ClvwsAccountUdfNum();
        lastLoanSize.setLBLFIELDNUM("LAST LOAN SIZE");
        lastLoanSize.setFIELDVALUE(BigDecimal.ZERO);
        accountUdfNums.add(lastLoanSize);

        final AccountFullType.ClvwsAccountUdfNum netIncome = new AccountFullType.ClvwsAccountUdfNum();
        netIncome.setLBLFIELDNUM("FAMILY NET INCOME");
        netIncome.setFIELDVALUE(BigDecimal.valueOf(fcLoanRequest.getNetIncome()));
        accountUdfNums.add(netIncome);

        final AccountFullType.ClvwsAccountUdfNum cbcScore = new AccountFullType.ClvwsAccountUdfNum();
        cbcScore.setLBLFIELDNUM("CBC SCORING");
        cbcScore.setFIELDVALUE(Helper.convertStringToBigDecimal(fcLoanRequest.getCbcScoring()));
        accountUdfNums.add(cbcScore);


        return accountUdfNums;
    }

    private Client getBorrower(List<Client> clients) {
        return clients.stream().filter(client -> LocalConstants.BORROWER_CODE.equals(client.getClientType()))
                .findFirst()
                .get();
    }

    private BigDecimal getBorrowerGender(List<Client> clients) {
        final Client borrower = getBorrower(clients);
        return borrower.getGender().equals("FEMALE") ? BigDecimal.ONE : BigDecimal.ZERO;
    }


    private XMLGregorianCalendar getMaturityDate(final Integer tenure) {
        final LocalDate date = getNow().plusMonths(tenure);
        return Helper.getXmlGregorianCalendar(date);
    }

    private XMLGregorianCalendar getFirstDueDate() {
        final LocalDate plusMonth = getNow().plusMonths(1L);
        return Helper.getXmlGregorianCalendar(plusMonth);
    }
}
