package com.dminc.prasac.client.integration.saving.account.service;

import com.dminc.prasac.client.model.dto.SavingAccount;
import com.dminc.rest.exceptions.InternalBusinessException;
import com.dminc.rest.exceptions.ItemNotFoundBusinessException;

public interface SavingAccountService {

    SavingAccount createOrUpdate(SavingAccount savingAccount) throws ItemNotFoundBusinessException, InternalBusinessException;
}
