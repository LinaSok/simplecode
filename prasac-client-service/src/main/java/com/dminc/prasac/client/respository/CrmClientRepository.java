package com.dminc.prasac.client.respository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.dminc.prasac.client.model.entity.CrmClientEntity;

@Repository
public interface CrmClientRepository extends ReadOnlyRepository<CrmClientEntity, String>, JpaSpecificationExecutor, CrmClientRepositoryCustom {
    CrmClientEntity findTopByCif(String cif);

    CrmClientEntity findFirstByIdentityNumberAndIdentityTypeContainingIgnoreCase(String identityNumber, String identityType);
}
