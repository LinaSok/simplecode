package com.dminc.prasac.client.integration.cbc.domain.response;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "GUARANTEED_ACCOUNTS")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class GuarantorDetailWrapper {

    @XmlElement(name = "GRT_ACC_DETAIL", type = GuarantorDetail.class)
    private List<GuarantorDetail> guarantorDetails;
}
