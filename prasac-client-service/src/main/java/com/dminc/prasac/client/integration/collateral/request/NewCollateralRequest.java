package com.dminc.prasac.client.integration.collateral.request;

import com.dminc.prasac.client.model.dto.Collateral;
import com.dminc.prasac.client.model.dto.User;
import lombok.Builder;
import prasac.webapi.wsdl.collteral.CREATECOLLATERALFSFSREQ;
import prasac.webapi.wsdl.collteral.CollateralFullType;
import prasac.webapi.wsdl.collteral.UDFDETAILSType2;

import java.math.BigDecimal;

public class NewCollateralRequest extends CollateralRequest {

    public static final String OPERATION = "CollateralCreate";
    public static final String COLLATERAL_STATUS = "01COLLATERAL_STATUS";
    public static final String COLLATERAL_SUPPORT = "02COLLATERAL_SUPPORT";
    public static final String DATE_RETURN = "03DATE_RETURN";
    public static final String DATE_RECEIVE = "04DATE_RECEIVE";
    public static final String CATEGORY_NAME = "0301_SALARY_BASED";

    private final Collateral collateral;
    private final String liabId;

    @Builder
    public NewCollateralRequest(String source, User user, Collateral collateral, String liabId) {
        super(OPERATION, source, user);
        this.collateral = collateral;
        this.liabId = liabId;
    }

    public CREATECOLLATERALFSFSREQ getRequest() {
        final CREATECOLLATERALFSFSREQ request = new CREATECOLLATERALFSFSREQ();
        request.setFCUBSHEADER(populateHeader());
        final CollateralFullType collateral = populateCollateral();
        final CREATECOLLATERALFSFSREQ.FCUBSBODY body = new CREATECOLLATERALFSFSREQ.FCUBSBODY();
        body.setCollateralsFull(collateral);
        request.setFCUBSBODY(body);
        return request;
    }

    private CollateralFullType populateCollateral() {
        CollateralFullType collateralFullType = new CollateralFullType();
        collateralFullType.setLIABID(new BigDecimal(liabId));
        collateralFullType.setCOLLATERALCODE(collateral.getCollateralCode());
        collateralFullType.setCUSTOMERNO(collateral.getClientCif());
        collateralFullType.setCOLLATERALCURRENCY(collateral.getCurrencyCode());
        collateralFullType.setCATEGORYNAME(CATEGORY_NAME);
        collateralFullType.setSENIORITYOFCLAIM("First Charge");
        collateralFullType.setCHARGETYPE("Hypothecation");
        collateralFullType.setCOLLATERALVALUE(BigDecimal.valueOf(collateral.getValue()));
        collateralFullType.setHAIRCUT2(BigDecimal.ZERO);
        collateralFullType.setLIABNO(collateral.getClientCif());
        collateralFullType.setBRANCHCODE(collateral.getBranchCode());
        populateUdf(collateralFullType);
        return collateralFullType;
    }

    private void populateUdf(CollateralFullType collateral) {
        final UDFDETAILSType2 udf1 = new UDFDETAILSType2();
        udf1.setFLDNAM(COLLATERAL_STATUS);
        final UDFDETAILSType2 udf2 = new UDFDETAILSType2();
        udf2.setFLDNAM(COLLATERAL_SUPPORT);
        final UDFDETAILSType2 udf3 = new UDFDETAILSType2();
        udf3.setFLDNAM(DATE_RETURN);
        final UDFDETAILSType2 udf4 = new UDFDETAILSType2();
        udf4.setFLDNAM(DATE_RECEIVE);
        collateral.getUDFDETAILS().add(udf1);
        collateral.getUDFDETAILS().add(udf2);
        collateral.getUDFDETAILS().add(udf3);
        collateral.getUDFDETAILS().add(udf4);
    }
}
