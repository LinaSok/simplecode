package com.dminc.prasac.client.model.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
@SuppressWarnings("PMD.TooManyFields")
public class CrmClient {

    private String cif;

    private String liabId;

    private String fullName;

    private String street;

    private String village;

    private String commune;

    private String district;

    private String province;

    private String identityType;

    private String identityNumber;

    private LocalDate dob;

    private String phone;

    private String country;

    private String nationality;

    private String khmerFullName;

    private String villageCode;

    private String communeCode;

    private String districtCode;

    private String provinceCode;

    private String gender;

    private String branch;

    private String maritalStatus;

    private String shortName;
}
