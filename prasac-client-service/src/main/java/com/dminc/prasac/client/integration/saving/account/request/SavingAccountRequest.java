package com.dminc.prasac.client.integration.saving.account.request;

import com.dminc.prasac.client.model.dto.Address;
import com.dminc.prasac.client.model.dto.SavingAccount;
import com.dminc.prasac.client.model.dto.User;
import prasac.webapi.wsdl.saving.account.CustAccFullType;
import prasac.webapi.wsdl.saving.account.FCUBSHEADERType;
import prasac.webapi.wsdl.saving.account.UBSCOMPType;

import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings({"PMD.AbstractClassWithoutAbstractMethod"})
public class SavingAccountRequest {

    public static final String JOIN_AND_OTHER = "JAO";
    public static final String SERVICE = "FCUBSAccService";
    private final String source;
    private final User user;
    private final String operation;

    public SavingAccountRequest(String source, User user, String operation) {
        this.source = source;
        this.user = user;
        this.operation = operation;
    }

    public static String populateAddress1(Address address) {
        return String.format("%s,%s,%s", address.getHouseNo(), address.getStreetNo(), address.getGroupNo());
    }

    public FCUBSHEADERType populateHeader() {
        final FCUBSHEADERType header = new FCUBSHEADERType();
        header.setUBSCOMP(UBSCOMPType.FCUBS);
        header.setUSERID(user.getUserId());
        header.setSOURCE(source);
        header.setBRANCH(user.getBranch());
        header.setSERVICE(SERVICE);
        header.setOPERATION(operation);
        header.setSOURCEOPERATION(operation);
        return header;
    }

    protected void populateAddress(CustAccFullType account, Address address) {
        account.setADDRS5(address.getVillage());
        account.setADDRESS4(address.getCommune());
        account.setADDRESS3(address.getDistrict());
        account.setADDRESS2(address.getProvince());
        account.setADDRESS1(populateAddress1(address));
    }

    public List<CustAccFullType.Jointholders> getJoinHolders(SavingAccount savingAccount) {
        return savingAccount.getClients().stream().map(client -> {
            final CustAccFullType.Jointholders join = new CustAccFullType.Jointholders();
            join.setCUSTNO1(client.getCif());
            join.setJNTHLDTYP(JOIN_AND_OTHER);
            return join;
        }).collect(Collectors.toList());
    }
}
