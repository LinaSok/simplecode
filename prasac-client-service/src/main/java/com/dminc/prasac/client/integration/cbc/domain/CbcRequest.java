package com.dminc.prasac.client.integration.cbc.domain;

import com.dminc.prasac.client.integration.cbc.domain.request.Address;
import com.dminc.prasac.client.integration.cbc.domain.request.Consumer;
import com.dminc.prasac.client.integration.cbc.domain.request.CustomerContact;
import com.dminc.prasac.client.integration.cbc.domain.request.CustomerDOB;
import com.dminc.prasac.client.integration.cbc.domain.request.CustomerName;
import com.dminc.prasac.client.integration.cbc.domain.request.Enquiry;
import com.dminc.prasac.client.integration.cbc.domain.request.Identification;
import com.dminc.prasac.client.integration.cbc.domain.request.IdentificationExpire;
import com.dminc.prasac.client.integration.cbc.domain.request.KhmerFirstName;
import com.dminc.prasac.client.integration.cbc.domain.request.KhmerLastName;
import com.dminc.prasac.client.integration.cbc.domain.request.Occupation;
import com.dminc.prasac.client.integration.cbc.domain.request.PlaceOfBirth;
import com.dminc.prasac.client.integration.cbc.domain.request.Request;
import com.dminc.prasac.client.integration.cbc.domain.request.RequestBody;
import com.dminc.prasac.client.integration.cbc.domain.request.RequestHeader;
import com.dminc.prasac.client.integration.cbc.domain.request.WorkingAddress;
import com.dminc.prasac.client.model.LocalConstants;
import com.dminc.prasac.client.model.cbc.Loan;
import com.dminc.prasac.client.model.dto.Client;
import com.dminc.prasac.client.model.dto.ClientIdentification;
import com.dminc.prasac.client.util.Helper;
import lombok.Builder;
import lombok.Getter;
import org.apache.commons.collections.CollectionUtils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Getter
@Builder
@SuppressWarnings({"PMD.TooManyMethods"})
public class CbcRequest {

    public static final String SERVICE = "ENQUIRYV3";
    public static final String LARGE_LOAN_SIZE_ACTION = "A_SC";
    public static final String SMALL_LOAN_SIZE_ACTION = "L_SC";
    public static final int KHR_LARGE_LOAN_SIZE = 2000000;
    public static final int THB_LARGE_LOAN_SIZE = 15500;
    public static final int USD_LARGE_LOAN_SIZE = 500;
    public static final String MEMBER_ID = "242";
    public static final String RUN_NO = "000001";
    public static final String TOP_ITEMS = "1";
    public static final String SCORE_CARD = "MFI01";
    public static final String ADDRESS_TYPE = "RESID";
    public static final String PHONE_NUMBER_TYPE = "M";
    public static final String PHONE_NUMBER_CODE = "855";
    public static final String KHMER_LANGUAGE_CODE = "kh";

    private Loan loan;
    private String userId;

    public Request getRequest() {
        return Request.builder()
                .service(SERVICE)
                .requestHeader(populateHeader())
                .requestBody(populateBody(loan))
                .action(getAction(loan))
                .build();
    }

    /**
     * "Value can be ""A_SC"" or ""L_SC"" bast on XML tags AMOUNT, CURRENCY:
     * - if CURRENCY ='KHR' and AMOUNT >=2000000 THEN 'A_SC' ELSE 'L_SC'
     * - if CURRENCY ='THB' and AMOUNT >=15500 THEN 'A_SC' ELSE 'L_SC'
     * - if CURRENCY ='USD' and AMOUNT >=500 THEN 'A_SC' ELSE 'L_SC'"
     */
    private String getAction(Loan loan) {
        switch (loan.getCurrency()) {
            case LocalConstants.KHMER_CURRENCY:
                if (loan.getAmount() >= KHR_LARGE_LOAN_SIZE) {
                    return LARGE_LOAN_SIZE_ACTION;
                } else {
                    return SMALL_LOAN_SIZE_ACTION;
                }
            case LocalConstants.THAI_CURRENCY:
                if (loan.getAmount() >= THB_LARGE_LOAN_SIZE) {
                    return LARGE_LOAN_SIZE_ACTION;
                } else {
                    return SMALL_LOAN_SIZE_ACTION;
                }
            case LocalConstants.US_DOLLAR:
                if (loan.getAmount() >= USD_LARGE_LOAN_SIZE) {
                    return LARGE_LOAN_SIZE_ACTION;
                } else {
                    return SMALL_LOAN_SIZE_ACTION;
                }
            default:
                return null;
        }
    }

    private RequestHeader populateHeader() {
        return RequestHeader.builder()
                .memberId(MEMBER_ID)
                .userId(userId)
                .no(RUN_NO)
                .topItems(TOP_ITEMS)
                .build();
    }

    private RequestBody populateBody(Loan loan) {
        final NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setGroupingUsed(false);
        final String loanAmount = numberFormat.format(loan.getAmount());
        final Enquiry enquiry = Enquiry.builder()
                .enquiryType("NA")
                .language("Null")
                .purposeCode(loan.getPurposeCode())
                .numberOfApplicant(String.valueOf(CollectionUtils.size(loan.getClients())))
                .accountType(CollectionUtils.size(loan.getClients()) > 1 ? Enquiry.AccountType.JOIN : Enquiry.AccountType.SINGLE)
                .amount(loanAmount)
                .currency(loan.getCurrency())
                .consumer(populateConsumers(loan.getClients()))
                .scoreCard(SCORE_CARD)
                .reference(populateReferenceCode())
                .build();

        return RequestBody.builder()
                .enquiry(enquiry)
                .build();
    }

    /**
     * Format: FCBrandCodeHHmmssddmmyyyy
     * sample: FC22204233426032019
     */
    private String populateReferenceCode() {
        return String.format("FC222%s%s%s%s%s%s",
                Calendar.getInstance().get(Calendar.HOUR_OF_DAY),
                Calendar.getInstance().get(Calendar.MINUTE),
                Calendar.getInstance().get(Calendar.SECOND),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.YEAR));
    }

    private List<Consumer> populateConsumers(List<Client> clients) {
        return clients.stream().map(this::populateConsumer).collect(Collectors.toList());
    }

    private Consumer populateConsumer(Client client) {
        final Consumer.ClientType clientType = Objects.equals(client.getClientType(), LocalConstants.BORROWER_CODE)
                || Objects.equals(client.getClientType(), LocalConstants.CO_BORROWER_CODE)
                ? Consumer.ClientType.BORROWER : Consumer.ClientType.GUARANTOR;
        return Consumer.builder()
                .clientType(clientType)
                .gender(client.getGender())
                .maritalStatus(client.getMaritalStatus())
                .nationalityCode(client.getNationality())
                .identification(populateIdentification(client.getIdentifications()))
                .customerDOB(populateDOB(client))
                .placeOfBirth(populatePlaceOfBirth(client))
                .customerName(populateName(client))
                .address(populateAddress(client))
                .customerContact(populateContact(client))
                .occupation(populateOccupation(client))
                .build();

    }

    private Occupation populateOccupation(Client client) {
        final NumberFormat formatter = new DecimalFormat("#0.00");
        return Occupation.builder()
                .employmentType("C")
                .selfEmploy("Y")
                .occupationCode(client.getClientOccupation())
                .tenure(String.valueOf(client.getWorkingPeriod()))
                .annualIncome(formatter.format(client.getIncomeAmount() / 12))
                .incomeCurrency(LocalConstants.US_DOLLAR)
                .workingAddress(populateWorkingAddress(client))
                .name(client.getFullName())
                .build();
    }

    private WorkingAddress populateWorkingAddress(Client client) {
        final com.dminc.prasac.client.model.dto.Address workplaceAddress = client.getWorkplaceAddress();
        return WorkingAddress.builder()
                .countryCode(workplaceAddress.getCountryCbcCode())
                .addressType(ADDRESS_TYPE)
                .villageCode(workplaceAddress.getVillageCode())
                .communeCode(workplaceAddress.getCommuneCode())
                .districtCode(workplaceAddress.getDistrictCode())
                .provinceCode(workplaceAddress.getProvinceCode())
                .province(workplaceAddress.getProvince())
                .fullAddress(String.format("%s, %s, %s",
                        workplaceAddress.getCommune(),
                        workplaceAddress.getDistrict(),
                        workplaceAddress.getProvince()))
                .build();
    }

    private Address populateAddress(Client client) {
        final com.dminc.prasac.client.model.dto.Address currentAddress = client.getCurrentAddress();
        return Address.builder()
                .addressType(ADDRESS_TYPE)
                .villageCode(currentAddress.getVillageCode())
                .communeCode(currentAddress.getCommuneCode())
                .districtCode(currentAddress.getDistrictCode())
                .provinceCode(currentAddress.getProvinceCode())
                .province(currentAddress.getProvince())
                .countryCode(currentAddress.getCountryCbcCode())
                .fullAddress(String.format("%s, %s, %s",
                        currentAddress.getCommune(),
                        currentAddress.getDistrict(),
                        currentAddress.getProvince()))
                .build();
    }

    private CustomerContact populateContact(Client client) {
        return CustomerContact.builder()
                .phoneNumberType(PHONE_NUMBER_TYPE)
                .phoneNumberCode(PHONE_NUMBER_CODE)
                .phoneNumber(client.getPhone())
                .build();
    }

    private CustomerName populateName(Client client) {
        final String[] name = Helper.splitName(client.getFullName());
        final String[] khmerName = Helper.splitName(client.getKhmerFullName());
        final KhmerFirstName khmerFirstName = KhmerFirstName.builder()
                .firstName(Helper.getFirstName(khmerName))
                .language(KHMER_LANGUAGE_CODE)
                .build();
        final KhmerLastName khmerLastName = KhmerLastName.builder()
                .lastName(khmerName[0])
                .language(KHMER_LANGUAGE_CODE)
                .build();
        return CustomerName.builder()
                .firstName(Helper.getFirstName(name))
                .lastName(name[0])
                .khmerLastName(khmerLastName)
                .khmerFirstName(khmerFirstName)
                .build();
    }

    private Identification populateIdentification(List<ClientIdentification> identifications) {
        if (CollectionUtils.isNotEmpty(identifications)) {
            final ClientIdentification clientIdentification = identifications.get(0);
            return Identification.builder()
                    .idType(clientIdentification.getClientIdentificationCbc())
                    .idNumber(clientIdentification.getIdNumber())
                    .identificationExpire(populateIdentificationExpire(clientIdentification))
                    .build();
        }
        return null;
    }

    private PlaceOfBirth populatePlaceOfBirth(Client client) {
        if (client == null || client.getBirthAddress() == null) {
            return PlaceOfBirth.builder().build();
        }
        return PlaceOfBirth.builder()
                .countryCode(client.getBirthAddress().getCountryCbcCode())
                .communeCode(client.getBirthAddress().getCommuneCode())
                .districtCode(client.getBirthAddress().getDistrictCode())
                .provinceCode(client.getBirthAddress().getProvinceCode())
                .build();
    }

    //mandatory
    private CustomerDOB populateDOB(Client client) {
        return CustomerDOB.builder()
                .day(Helper.format(client.getDob().getDayOfMonth(), 2))
                .month(Helper.format(client.getDob().getMonthValue(), 2))
                .year(String.valueOf(client.getDob().getYear()))
                .build();
    }

    private IdentificationExpire populateIdentificationExpire(ClientIdentification identification) {
        if (identification == null || identification.getExpiryDate() == null) {
            return IdentificationExpire.builder().build();
        }

        return IdentificationExpire.builder()
                .day(Helper.format(identification.getExpiryDate().getDayOfMonth(), 2))
                .month(Helper.format(identification.getExpiryDate().getMonthValue(), 2))
                .year(String.valueOf(identification.getExpiryDate().getYear()))
                .build();
    }
}
