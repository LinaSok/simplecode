package com.dminc.prasac.client.integration.collateral.service;

import com.dminc.prasac.client.model.dto.CollateralPool;
import com.dminc.rest.exceptions.BusinessException;

public interface ELPoolService {

    CollateralPool createOrUpdate(CollateralPool pool) throws BusinessException;
}
