package com.dminc.prasac.client.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Address implements Serializable {
    private static final long serialVersionUID = -8620268672552193862L;
    private String groupNo;
    private String houseNo;
    private String streetNo;
    private String village;
    private String commune;
    private String district;
    private String province;
    private String country;
    private String villageCode;
    private String communeCode;
    private String districtCode;
    private String provinceCode;
    private String countryCbcCode;
}
