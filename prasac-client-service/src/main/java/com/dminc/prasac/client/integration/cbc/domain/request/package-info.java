@javax.xml.bind.annotation.XmlSchema(xmlns = {
        @XmlNs(prefix = "", namespaceURI = "http://www.w3.org/2001/XMLSchema-instance")
})

package com.dminc.prasac.client.integration.cbc.domain.request;

import javax.xml.bind.annotation.XmlNs;
