package com.dminc.prasac.client.respository;

import com.dminc.prasac.client.model.dto.SearchClientRequest;
import com.dminc.prasac.client.model.entity.CrmClientEntity;
import com.dminc.prasac.client.util.QueryEscapeUtils;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.List;

import static com.dminc.prasac.client.service.CreditHistoryMonitoringServiceImpl.parseLocaleDate;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@SuppressWarnings({"PMD.InsufficientStringBufferDeclaration"})
public class CrmClientRepositoryCustomImpl implements CrmClientRepositoryCustom {

    private static final RowMapper<CrmClientEntity> CLIENT_MAPPER = (rs, rowNum) -> CrmClientEntity.builder()
            .cif(rs.getString("CUSTOMER_NO"))
            .branch(rs.getString("LOCAL_BRANCH"))
            .fullName(rs.getString("FULL_NAME"))
            .street(rs.getString("STREET"))
            .village(rs.getString("VILLAGE"))
            .commune(rs.getString("COMMUNE"))
            .district(rs.getString("DISTRICT"))
            .province(rs.getString("PROVINCE"))
            .identityType(rs.getString("ID_TYPE"))
            .identityNumber(rs.getString("ID_NUMBER"))
            .phone(rs.getString("PHONE"))
            .country(rs.getString("COUNTRY"))
            .nationality(rs.getString("NATIONALITY"))
            .khmerFullName(rs.getString("LOCAL_NMAE"))
            .villageCode(rs.getString("VCODE"))
            .communeCode(rs.getString("CCODE"))
            .districtCode(rs.getString("DCODE"))
            .provinceCode(rs.getString("PCODE"))
            .gender(rs.getString("GENDER"))
            .maritalStatus(rs.getString("MARITAL_STATUS"))
            .shortName(rs.getString("SHORT_NAME"))
            .dob(parseLocaleDate(rs.getString("DOB")))
            .build();
    private final NamedParameterJdbcTemplate template;

    @Override
    public List<CrmClientEntity> searchClient(SearchClientRequest request) {

        final StringBuilder queryBuilder = new StringBuilder(" from PRAVW_DETAIL_CIF_INFO where 1=1");
        final MapSqlParameterSource params = new MapSqlParameterSource();
        if (StringUtils.isNotEmpty(request.getCif())) {
            queryBuilder.append(" and CUSTOMER_NO = :cif");
            params.addValue("cif", request.getCif());
        }
        if (StringUtils.isNotEmpty(request.getName())) {
            final String normalizeName = StringUtils.normalizeSpace(request.getName());
            queryBuilder.append(" and LOWER(FULL_NAME) like :name");
            params.addValue("name", StringUtils.lowerCase(QueryEscapeUtils.toContainingCondition(normalizeName)));
        }
        if (StringUtils.isNotEmpty(request.getKhmerName())) {
            queryBuilder.append(" and LOCAL_NMAE like :khmerName");
            final String normalizeName = StringUtils.normalizeSpace(request.getKhmerName());
            params.addValue("khmerName", QueryEscapeUtils.toContainingCondition(normalizeName));
        }
        if (StringUtils.isNotEmpty(request.getIdNumber())) {
            queryBuilder.append(" and ID_NUMBER = :idNumber");
            params.addValue("idNumber", request.getIdNumber());
        }
        if (request.getDob() != null) {
            queryBuilder.append(" and DOB = :dob");
            params.addValue("dob", request.getDob());
        }
        final String selectQuery = String.format("select * %s ", queryBuilder.toString());
        return template.query(selectQuery, params, CLIENT_MAPPER);
    }
}
