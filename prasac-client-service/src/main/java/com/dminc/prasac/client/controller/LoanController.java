package com.dminc.prasac.client.controller;

import com.dminc.prasac.client.integration.loan.FCLoanService;
import com.dminc.prasac.client.model.dto.FCLoanRequest;
import com.dminc.prasac.client.model.dto.LoanHistoryDTO;
import com.dminc.prasac.client.model.dto.LoanHistoryInfoDTO;
import com.dminc.prasac.client.model.dto.LoanRequestHistoryDTO;
import com.dminc.prasac.client.service.LoanHistoryService;
import com.dminc.prasac.client.service.LoanRequestHistoryServiceImpl;
import com.dminc.prasac.client.service.LoanService;
import com.dminc.rest.exceptions.BusinessException;
import io.swagger.annotations.ApiImplicitParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.dminc.prasac.client.config.SwaggerDocumentation.HEADER;
import static com.dminc.prasac.client.config.SwaggerDocumentation.X_AUTHORIZATION;

@RestController
@RequestMapping("api/v1/loan")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LoanController {

    public static final String X_AUTH_DESC = "for DevServer UsnSWbnYK26hHc7KUcaM";
    public static final String CLIENT_CIF = "clientCif";
    private final LoanHistoryService service;
    private final LoanRequestHistoryServiceImpl loanRequestHistoryService;
    private final FCLoanService loanServiceFlexCube;
    private final LoanService loanService;

    @GetMapping("/history/{clientCif}")
    @ApiImplicitParam(paramType = HEADER, name = X_AUTHORIZATION, value = X_AUTH_DESC)
    public List<LoanHistoryDTO> getLoanHistory(@PathVariable(CLIENT_CIF) String clientCif) throws BusinessException {
        return service.getLoanHistoryByClient(clientCif);
    }

    @GetMapping("/history-info")
    @ApiImplicitParam(paramType = HEADER, name = X_AUTHORIZATION, value = X_AUTH_DESC)
    public List<LoanHistoryInfoDTO> getLoanHistoryInfo(@RequestParam(name = "clientCifs") List<String> clientCifs) {
        return service.getLoanHistoriesInfoByClient(clientCifs);
    }

    @GetMapping("/request-history/{clientCif}")
    @ApiImplicitParam(paramType = HEADER, name = X_AUTHORIZATION, value = X_AUTH_DESC)
    public List<LoanRequestHistoryDTO> getLoanRequestHistory(@PathVariable(CLIENT_CIF) String clientCif) throws BusinessException {
        return loanRequestHistoryService.getByClientCif(clientCif);
    }


    @GetMapping("/repayment-schedule")
    @ApiImplicitParam(paramType = HEADER, name = X_AUTHORIZATION, value = X_AUTH_DESC)
    public List getLoanRepaymentScheduleStatement(@RequestParam(CLIENT_CIF) String clientCif) throws BusinessException {
        return loanService.getLoanRepaymentScheduleStatement(clientCif);
    }


    @GetMapping("/statement")
    @ApiImplicitParam(paramType = HEADER, name = X_AUTHORIZATION, value = X_AUTH_DESC)
    public List getLoanStatement(@RequestParam(CLIENT_CIF) String clientCif) throws BusinessException {
        return loanService.getLoanStatement(clientCif);
    }


    /**
     * create loan in flex cube service
     */
    @PostMapping("/fc")
    @ApiImplicitParam(paramType = HEADER, name = X_AUTHORIZATION, value = X_AUTH_DESC)
    public FCLoanRequest createFlexCubeLoan(@RequestBody FCLoanRequest request) throws BusinessException {
        return loanServiceFlexCube.createFCLoan(request);
    }

}
