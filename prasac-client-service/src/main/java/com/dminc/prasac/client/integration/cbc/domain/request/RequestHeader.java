package com.dminc.prasac.client.integration.cbc.domain.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "HEADER")
@XmlAccessorType(XmlAccessType.FIELD)
public class RequestHeader {

    @XmlElement(name = "MEMBER_ID")
    private String memberId;

    @XmlElement(name = "USER_ID")
    private String userId;

    @XmlElement(name = "RUN_NO")
    private String no;

    @XmlElement(name = "TOT_ITEMS")
    private String topItems;

}
