package com.dminc.prasac.client.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "PRAVW_DETAIL_CIF_INFO")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("PMD.TooManyFields")
public class CrmClientEntity {

    public static final String CIF_FIELD_NAME = "cif";
    public static final String FULL_NAME_FIELD_NAME = "fullName";
    public static final String KHMER_FULL_NAME_FIELD_NAME = "khmerFullName";
    public static final String DOB_FIELD_NAME = "dob";
    public static final String IDENTITY_TYPE_FIELD_NAME = "identityType";
    public static final String IDENTITY_NUMBER_FIELD_NAME = "identityNumber";

    @Id
    @Column(name = "CUSTOMER_NO")
    private String cif;

    @Column(name = "ID")
    private String liabId;

    @Column(name = "FULL_NAME")
    private String fullName;

    @Column(name = "STREET")
    private String street;

    @Column(name = "VILLAGE")
    private String village;

    @Column(name = "COMMUNE")
    private String commune;

    @Column(name = "DISTRICT")
    private String district;

    @Column(name = "PROVINCE")
    private String province;

    @Column(name = "ID_TYPE")
    private String identityType;

    @Column(name = "ID_NUMBER")
    private String identityNumber;

    @Column(name = "DOB")
    private LocalDate dob;

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "COUNTRY")
    private String country;

    @Column(name = "NATIONALITY")
    private String nationality;

    @Column(name = "LOCAL_NMAE")
    private String khmerFullName;

    @Column(name = "VCODE")
    private String villageCode;

    @Column(name = "CCODE")
    private String communeCode;

    @Column(name = "DCODE")
    private String districtCode;

    @Column(name = "PCODE")
    private String provinceCode;

    @Column(name = "GENDER")
    private String gender;

    @Column(name = "LOCAL_BRANCH")
    private String branch;

    @Column(name = "MARITAL_STATUS")
    private String maritalStatus;

    @Column(name = "SHORT_NAME")
    private String shortName;
}
