package com.dminc.prasac.client.integration.saving.account.request;

import com.dminc.prasac.client.model.dto.SavingAccount;
import com.dminc.prasac.client.model.dto.User;
import lombok.Builder;
import prasac.webapi.wsdl.saving.account.CREATECUSTACCFSFSREQ;
import prasac.webapi.wsdl.saving.account.CustAccFullType;


public class NewSavingAccountRequest extends SavingAccountRequest {
    public static final String OPERATION = "CreateCustAcc";
    public static final String JOIN_TYPE = "J";
    public static final String DUMMY = "DUMMY";
    public static final String MAIL_MEDIA_TYPE = "MAIL";
    private final SavingAccount savingAccount;

    @Builder
    public NewSavingAccountRequest(String source, User user, SavingAccount savingAccount) {
        super(source, user, OPERATION);
        this.savingAccount = savingAccount;
    }

    public CREATECUSTACCFSFSREQ getRequest() {
        final CREATECUSTACCFSFSREQ request = new CREATECUSTACCFSFSREQ();
        request.setFCUBSHEADER(populateHeader());
        populateBody(request);
        return request;
    }

    private void populateBody(CREATECUSTACCFSFSREQ request) {
        final CREATECUSTACCFSFSREQ.FCUBSBODY body = new CREATECUSTACCFSFSREQ.FCUBSBODY();
        populateAccount(body);
        request.setFCUBSBODY(body);
    }

    private void populateAccount(CREATECUSTACCFSFSREQ.FCUBSBODY body) {
        final CustAccFullType account = new CustAccFullType();
        account.setBRN(savingAccount.getBranchCode());
        account.setCCY(savingAccount.getCurrencyCode());
        account.setACCLS(savingAccount.getAccountClassCode());
        account.setADESC(savingAccount.getAccountDesc());
        account.setCUSTNO(savingAccount.getCustomerNo());
        account.setACCTYPE(JOIN_TYPE);
        account.setLOC(savingAccount.getBranchCode());
        account.setACC(DUMMY);
        account.setMEDIA(MAIL_MEDIA_TYPE);
        populateAddress(account, savingAccount.getAddress());
        body.setCustAccountFull(account);
        populateJoiners(account);
    }

    private void populateJoiners(CustAccFullType account) {
        account.getJointholders().addAll(getJoinHolders(savingAccount));
    }
}
