package com.dminc.prasac.client.model.cbc;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PreviousEnquiry implements Serializable {

    private static final long serialVersionUID = 471754502335476198L;
    private LocalDate date;

    private String enquirer;

    private String type;

    private String accountType;

    private String memberRef;

    private String purposeCode;

    private Double amount;

    private String currencyCode;

    private String clientType;

    private String firstName;

    private String lastName;

    private String khmerLastName;

    private String khmerFirstName;

}
