package com.dminc.prasac.client.integration.cbc.domain.response;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "PCPLB")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PlaceOfBirth {

    @XmlElement(name = "PCPLBC")
    private String countryCode;

    @XmlElement(name = "PCPLBP")
    private String provinceCode;

    @XmlElement(name = "PCPLBD")
    private String districtCode;

    @XmlElement(name = "PCPLBCM")
    private String communeCode;
}
