package com.dminc.prasac.client.controller;

import com.dminc.prasac.client.integration.cbc.domain.Cbc;
import com.dminc.prasac.client.integration.cbc.service.CbcService;
import com.dminc.prasac.client.model.cbc.Loan;
import com.dminc.rest.exceptions.InternalBusinessException;
import io.swagger.annotations.ApiImplicitParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.dminc.prasac.client.config.SwaggerDocumentation.HEADER;
import static com.dminc.prasac.client.config.SwaggerDocumentation.X_AUTHORIZATION;

@RestController
@RequestMapping("api/cbc")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CbcController {

    private final CbcService service;

    @PostMapping
    @ApiImplicitParam(paramType = HEADER, name = X_AUTHORIZATION, value = "for DevServer UsnSWbnYK26hHc7KUcaM")
    public Cbc generateReport(@RequestBody Loan loan) throws InternalBusinessException {
        return service.getReport(loan);
    }
}
