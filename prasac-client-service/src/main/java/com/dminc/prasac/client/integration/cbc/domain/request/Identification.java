package com.dminc.prasac.client.integration.cbc.domain.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "CID")
public class Identification {
    @XmlElement(name = "CID1")
    private String idType;

    @XmlElement(name = "CID2")
    private String idNumber;

    @XmlElement(name = "CID3", type = IdentificationExpire.class)
    private IdentificationExpire identificationExpire;


}
