package com.dminc.prasac.client.integration.cbc.domain.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "CID3")
@XmlAccessorType(XmlAccessType.FIELD)
public class IdentificationExpire {

    @XmlElement(name = "CID3D")
    private String day;

    @XmlElement(name = "CID3M")
    private String month;

    @XmlElement(name = "CID3Y")
    private String year;


}

