package com.dminc.prasac.client.service;

import com.dminc.prasac.client.model.dto.LoanHistoryDTO;
import com.dminc.prasac.client.model.dto.LoanHistoryInfoDTO;
import com.dminc.prasac.client.model.entity.LoanHistory;
import com.dminc.prasac.client.model.entity.LoanHistoryInfo;
import com.dminc.prasac.client.respository.LoanHistoryInfoRepository;
import com.dminc.prasac.client.respository.LoanHistoryRepository;
import com.dminc.rest.exceptions.BusinessException;
import com.dminc.rest.mapping.Mapper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LoanHistoryServiceImpl implements LoanHistoryService {

    private static final String PRASAC_INSTITUTION_NAME = "Prasac";
    private static final String LOAN_ACTIVE_STATUS = "ACTIVE";
    private static final int MAXIMUM_HISTORY_COUNT = 5;
    private final LoanHistoryRepository repository;
    private final ObjectsConverter converter;
    private final LoanHistoryInfoRepository loanHistoryInfoRepository;

    @Override
    public List<LoanHistoryDTO> getLoanHistoryByClient(String clientCif) throws BusinessException {
        final List<LoanHistory> loans = repository.findByClientCifOrderByNo(clientCif);
        return converter.runServiceTask((Mapper mapper) -> mapper.map(loans, LoanHistoryDTO.class));
    }

    /**
     * get all active loans, if no active loan get 1 latest cycle loan for each client
     * maximum is 5
     */

    @Override
    public List<LoanHistoryInfoDTO> getLoanHistoriesInfoByClient(List<String> clientCifs) {
        final List<LoanHistoryInfoDTO> loans = getActiveLoans(clientCifs);
        for (int i = 0; i < CollectionUtils.size(clientCifs) && CollectionUtils.size(loans) < MAXIMUM_HISTORY_COUNT; i++) {
            String cif = clientCifs.get(i);
            // to check the client's loan history is already in the list
            final Optional<LoanHistoryInfoDTO> clientLoan = loans.stream().filter(loan -> cif.equals(loan.getClientCif())).findFirst();
            if (!clientLoan.isPresent()) {
                final LoanHistoryInfoDTO latestLoan = getLatestLoanCycleByClient(cif);
                if (latestLoan != null) {
                    loans.add(latestLoan);
                }
            }
        }
        return loans;
    }

    @Override
    public LoanHistoryDTO getLastLoan(String clientCif) {
        return converter.getObjectsMapper().map(repository.findTopByClientCifOrderByLoanCycleDesc(clientCif), LoanHistoryDTO.class);
    }

    private List<LoanHistoryInfoDTO> getActiveLoans(List<String> clientCifs) {
        final PageRequest pageRequest = PageRequest.of(0, MAXIMUM_HISTORY_COUNT);
        final List<LoanHistoryInfo> loans = loanHistoryInfoRepository.findByClientCifInAndLoanStatusAndInstitutionName(clientCifs, LOAN_ACTIVE_STATUS, PRASAC_INSTITUTION_NAME, pageRequest);
        return converter.getObjectsMapper().map(loans, LoanHistoryInfoDTO.class);
    }

    private LoanHistoryInfoDTO getLatestLoanCycleByClient(String cif) {
        final LoanHistoryInfo loans = loanHistoryInfoRepository.findTopByClientCifAndInstitutionNameAndLoanStatusNotOrderByLoanCycleDesc(cif, PRASAC_INSTITUTION_NAME, LOAN_ACTIVE_STATUS);
        return converter.getObjectsMapper().map(loans, LoanHistoryInfoDTO.class);
    }
}
