package com.dminc.prasac.client.respository;

import com.dminc.prasac.client.model.entity.LoanRequestHistory;
import com.dminc.prasac.client.model.entity.LoanRequestId;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LoanRequestHistoryRepository extends ReadOnlyRepository<LoanRequestHistory, LoanRequestId> {

    List<LoanRequestHistory> findByClientCif(String cif);
}
