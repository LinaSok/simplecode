package com.dminc.prasac.client.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "PRAVW_LN_DETAIL_LOANS")
@Data
public class LoanDetails {

    @Id
    @Column(name = "ACCOUNT_NUMBER")
    private String accountNumber;

    @Column(name = "CUSTOMER_ID")
    private String clientCif;

    @Column(name = "LOAN_CYCLE")
    private Integer loanCycle;
}
