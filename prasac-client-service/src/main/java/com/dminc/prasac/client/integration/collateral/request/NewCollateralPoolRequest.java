package com.dminc.prasac.client.integration.collateral.request;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang3.time.DateFormatUtils;

import com.dminc.prasac.client.model.dto.CollateralPool;
import com.dminc.prasac.client.model.dto.User;

import lombok.Builder;
import prasac.webapi.wsdl.elpool.CREATECOLLATERALPOOLFSFSREQ;
import prasac.webapi.wsdl.elpool.CollateralpoolFullType;

public class NewCollateralPoolRequest extends CollateralPoolRequest {

    public static final String OPERATION = "CreateCollateralPool";

    private final CollateralPool collateralPool;
    private final String liabId;

    @Builder
    public NewCollateralPoolRequest(String source, User user, CollateralPool collateralPool, String liabId) {
        super(OPERATION, source, user);
        this.collateralPool = collateralPool;
        this.liabId = liabId;
    }

    public CREATECOLLATERALPOOLFSFSREQ getRequest() {
        final CREATECOLLATERALPOOLFSFSREQ request = new CREATECOLLATERALPOOLFSFSREQ();
        request.setFCUBSHEADER(populateHeader());
        final CollateralpoolFullType collateralPool = populateCollateralPool();
        final CREATECOLLATERALPOOLFSFSREQ.FCUBSBODY body = new CREATECOLLATERALPOOLFSFSREQ.FCUBSBODY();
        body.setCollateralsPoolFull(collateralPool);
        request.setFCUBSBODY(body);
        return request;
    }

    private CollateralpoolFullType populateCollateralPool() {
        final CollateralpoolFullType request = new CollateralpoolFullType();
        request.setPOOLCODE(generatePoolCode());
        request.setPOOLCCY(collateralPool.getCurrencyCode());
        request.setBRANCHCODE(collateralPool.getBranchCode());
        request.setLIABID(new BigDecimal(liabId));

        collateralPool.getCollateralCodes().forEach(code -> {
            final CollateralpoolFullType.PoolCollateralsLinkage linkage = new CollateralpoolFullType.PoolCollateralsLinkage();
            linkage.setLINKEDPERCENTNUMBER(new BigDecimal("100")); //default value
            linkage.setCOLLATERALCODE(code);
            request.getPoolCollateralsLinkage().add(linkage);
        });

        return request;
    }

    //get system current time and format to 190517104535 yyMMddHHmmss
    private String generatePoolCode() {
        return DateFormatUtils.format(new Date(), "yyMMddHHmmss");
    }

}
