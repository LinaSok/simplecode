package com.dminc.prasac.client.respository;

import com.dminc.prasac.client.model.dto.SearchClientRequest;
import com.dminc.prasac.client.model.entity.CrmClientEntity;

import java.util.List;

public interface CrmClientRepositoryCustom {
    List<CrmClientEntity> searchClient(SearchClientRequest request);
}
