package com.dminc.prasac.client.integration.cbc.domain.response;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "SCORE")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Score {

    @XmlElement(name = "SC_SCORE")
    private String scoreNum;

    @XmlElement(name = "SC_SCORECARD")
    private String scoreCard;

    @XmlElement(name = "SC_SCOREINDEX")
    private String scoreIndex;

    @XmlElement(name = "SC_ODDS")
    private Integer odds;

    @XmlElement(name = "SC_BAD")
    private Double badRate;
}
