package com.dminc.prasac.client.model.cbc;

import com.dminc.prasac.client.model.dto.Address;
import com.dminc.prasac.client.model.dto.ClientIdentification;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CbcClient implements Serializable {
    private static final long serialVersionUID = 4444407139205011500L;

    private String clientType;

    private String gender;

    private String maritalStatusCode;

    private String nationalityCode;

    private String khmerFirstName;

    private String khmerLastName;

    private String firstName;

    private String lastName;

    private LocalDate dob;

    private Address placeOfBirth;

    private CbcScore score;

    private List<Guarantor> guarantors;

    private List<AccountDetail> accountDetails;

    private List<PreviousEnquiry> previousEnquiries;

    private ClientIdentification clientIdentification;
}
