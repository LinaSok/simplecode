package com.dminc.prasac.client.integration.cbc.domain.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "CCNT")
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerContact {

    @XmlElement(name = "CCN1")
    private String phoneNumberType;

    @XmlElement(name = "CCN2")
    private String phoneNumberCode;

    @XmlElement(name = "CCN4")
    private String phoneNumber;
}
