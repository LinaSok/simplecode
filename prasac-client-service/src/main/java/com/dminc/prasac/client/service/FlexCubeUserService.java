package com.dminc.prasac.client.service;

import com.dminc.prasac.client.model.dto.User;
import com.dminc.rest.exceptions.ItemNotFoundBusinessException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class FlexCubeUserService {

    private final ObjectMapper mapper;
    private List<User> users;

    @PostConstruct
    public void init() throws IOException {
        final ClassPathResource classPathResource = new ClassPathResource("static/defaultFlexcubeUser.json");
        users = Arrays.asList(mapper.readValue(classPathResource.getInputStream(), User[].class));
    }

    public User getUserByBranch(String branch) throws ItemNotFoundBusinessException {
        final Optional<User> optionalUser = users.stream().filter(user -> branch.equals(user.getBranch())).findFirst();
        return optionalUser.orElseThrow(() -> new ItemNotFoundBusinessException("User not found"));
    }
}
