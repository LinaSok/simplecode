package com.dminc.prasac.client.integration.collateral.request;

import com.dminc.prasac.client.model.dto.User;
import prasac.webapi.wsdl.collteral.FCUBSHEADERType;

@SuppressWarnings({"PMD.AbstractClassWithoutAbstractMethod"})
public abstract class CollateralRequest {

    public static final String SERVICE = "ELCollteralService";
    private final String operation;
    private final String source;
    private final User user;

    public CollateralRequest(String operation, String source, User user) {
        this.operation = operation;
        this.source = source;
        this.user = user;
    }

    public FCUBSHEADERType populateHeader() {
        final FCUBSHEADERType header = new FCUBSHEADERType();
        header.setBRANCH(user.getBranch());
        header.setSERVICE(SERVICE);
        header.setOPERATION(operation);
        header.setSOURCEOPERATION(operation);
        header.setSOURCE(source);
        header.setFUNCTIONID("");
        header.setUSERID(user.getUserId());
        return header;
    }
}
