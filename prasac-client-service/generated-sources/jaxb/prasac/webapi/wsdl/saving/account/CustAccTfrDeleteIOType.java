//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-558 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.05.06 at 02:52:22 PM ICT 
//


package prasac.webapi.wsdl.saving.account;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CustAccTfr-Delete-IO-Type complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CustAccTfr-Delete-IO-Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ACC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ACCLS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EFFECTIVEDATE" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="BRN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustAccTfr-Delete-IO-Type", propOrder = {
        "acc",
        "accls",
        "effectivedate",
        "brn"
})
public class CustAccTfrDeleteIOType {

    @XmlElement(name = "ACC", required = true)
    protected String acc;
    @XmlElement(name = "ACCLS", required = true)
    protected String accls;
    @XmlElement(name = "EFFECTIVEDATE")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar effectivedate;
    @XmlElement(name = "BRN")
    protected String brn;

    /**
     * Gets the value of the acc property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getACC() {
        return acc;
    }

    /**
     * Sets the value of the acc property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setACC(String value) {
        this.acc = value;
    }

    /**
     * Gets the value of the accls property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getACCLS() {
        return accls;
    }

    /**
     * Sets the value of the accls property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setACCLS(String value) {
        this.accls = value;
    }

    /**
     * Gets the value of the effectivedate property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getEFFECTIVEDATE() {
        return effectivedate;
    }

    /**
     * Sets the value of the effectivedate property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setEFFECTIVEDATE(XMLGregorianCalendar value) {
        this.effectivedate = value;
    }

    /**
     * Gets the value of the brn property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getBRN() {
        return brn;
    }

    /**
     * Sets the value of the brn property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setBRN(String value) {
        this.brn = value;
    }

}
