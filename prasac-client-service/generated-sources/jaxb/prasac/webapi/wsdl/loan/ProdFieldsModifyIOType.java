//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-558 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.05.17 at 10:44:26 AM ICT 
//


package prasac.webapi.wsdl.loan;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProdFields-Modify-IO-Type complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ProdFields-Modify-IO-Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PRDCD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Prd-Udf-Num" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="FLDNAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="FLDDESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DATA_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="FLDNO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Prd-Udf-Char" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="FLDNAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="FLDDESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DATA_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="FIELDNO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Prd-Udf-Dt" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="FLDNAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="FLDDESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DATA_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="FLDNO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProdFields-Modify-IO-Type", propOrder = {
        "prdcd",
        "prdUdfNum",
        "prdUdfChar",
        "prdUdfDt"
})
public class ProdFieldsModifyIOType {

    @XmlElement(name = "PRDCD")
    protected String prdcd;
    @XmlElement(name = "Prd-Udf-Num")
    protected List<ProdFieldsModifyIOType.PrdUdfNum> prdUdfNum;
    @XmlElement(name = "Prd-Udf-Char")
    protected List<ProdFieldsModifyIOType.PrdUdfChar> prdUdfChar;
    @XmlElement(name = "Prd-Udf-Dt")
    protected List<ProdFieldsModifyIOType.PrdUdfDt> prdUdfDt;

    /**
     * Gets the value of the prdcd property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPRDCD() {
        return prdcd;
    }

    /**
     * Sets the value of the prdcd property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPRDCD(String value) {
        this.prdcd = value;
    }

    /**
     * Gets the value of the prdUdfNum property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prdUdfNum property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrdUdfNum().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProdFieldsModifyIOType.PrdUdfNum }
     */
    public List<ProdFieldsModifyIOType.PrdUdfNum> getPrdUdfNum() {
        if (prdUdfNum == null) {
            prdUdfNum = new ArrayList<ProdFieldsModifyIOType.PrdUdfNum>();
        }
        return this.prdUdfNum;
    }

    /**
     * Gets the value of the prdUdfChar property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prdUdfChar property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrdUdfChar().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProdFieldsModifyIOType.PrdUdfChar }
     */
    public List<ProdFieldsModifyIOType.PrdUdfChar> getPrdUdfChar() {
        if (prdUdfChar == null) {
            prdUdfChar = new ArrayList<ProdFieldsModifyIOType.PrdUdfChar>();
        }
        return this.prdUdfChar;
    }

    /**
     * Gets the value of the prdUdfDt property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prdUdfDt property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrdUdfDt().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProdFieldsModifyIOType.PrdUdfDt }
     */
    public List<ProdFieldsModifyIOType.PrdUdfDt> getPrdUdfDt() {
        if (prdUdfDt == null) {
            prdUdfDt = new ArrayList<ProdFieldsModifyIOType.PrdUdfDt>();
        }
        return this.prdUdfDt;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="FLDNAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FLDDESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DATA_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FIELDNO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "fldname",
            "flddesc",
            "datatype",
            "fieldno"
    })
    public static class PrdUdfChar {

        @XmlElement(name = "FLDNAME")
        protected String fldname;
        @XmlElement(name = "FLDDESC")
        protected String flddesc;
        @XmlElement(name = "DATA_TYPE")
        protected String datatype;
        @XmlElement(name = "FIELDNO")
        protected BigDecimal fieldno;

        /**
         * Gets the value of the fldname property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getFLDNAME() {
            return fldname;
        }

        /**
         * Sets the value of the fldname property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setFLDNAME(String value) {
            this.fldname = value;
        }

        /**
         * Gets the value of the flddesc property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getFLDDESC() {
            return flddesc;
        }

        /**
         * Sets the value of the flddesc property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setFLDDESC(String value) {
            this.flddesc = value;
        }

        /**
         * Gets the value of the datatype property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getDATATYPE() {
            return datatype;
        }

        /**
         * Sets the value of the datatype property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setDATATYPE(String value) {
            this.datatype = value;
        }

        /**
         * Gets the value of the fieldno property.
         *
         * @return possible object is
         * {@link BigDecimal }
         */
        public BigDecimal getFIELDNO() {
            return fieldno;
        }

        /**
         * Sets the value of the fieldno property.
         *
         * @param value allowed object is
         *              {@link BigDecimal }
         */
        public void setFIELDNO(BigDecimal value) {
            this.fieldno = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="FLDNAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FLDDESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DATA_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FLDNO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "fldname",
            "flddesc",
            "datatype",
            "fldno"
    })
    public static class PrdUdfDt {

        @XmlElement(name = "FLDNAME")
        protected String fldname;
        @XmlElement(name = "FLDDESC")
        protected String flddesc;
        @XmlElement(name = "DATA_TYPE")
        protected String datatype;
        @XmlElement(name = "FLDNO")
        protected BigDecimal fldno;

        /**
         * Gets the value of the fldname property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getFLDNAME() {
            return fldname;
        }

        /**
         * Sets the value of the fldname property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setFLDNAME(String value) {
            this.fldname = value;
        }

        /**
         * Gets the value of the flddesc property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getFLDDESC() {
            return flddesc;
        }

        /**
         * Sets the value of the flddesc property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setFLDDESC(String value) {
            this.flddesc = value;
        }

        /**
         * Gets the value of the datatype property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getDATATYPE() {
            return datatype;
        }

        /**
         * Sets the value of the datatype property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setDATATYPE(String value) {
            this.datatype = value;
        }

        /**
         * Gets the value of the fldno property.
         *
         * @return possible object is
         * {@link BigDecimal }
         */
        public BigDecimal getFLDNO() {
            return fldno;
        }

        /**
         * Sets the value of the fldno property.
         *
         * @param value allowed object is
         *              {@link BigDecimal }
         */
        public void setFLDNO(BigDecimal value) {
            this.fldno = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="FLDNAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FLDDESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DATA_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FLDNO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "fldname",
            "flddesc",
            "datatype",
            "fldno"
    })
    public static class PrdUdfNum {

        @XmlElement(name = "FLDNAME")
        protected String fldname;
        @XmlElement(name = "FLDDESC")
        protected String flddesc;
        @XmlElement(name = "DATA_TYPE")
        protected String datatype;
        @XmlElement(name = "FLDNO")
        protected BigDecimal fldno;

        /**
         * Gets the value of the fldname property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getFLDNAME() {
            return fldname;
        }

        /**
         * Sets the value of the fldname property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setFLDNAME(String value) {
            this.fldname = value;
        }

        /**
         * Gets the value of the flddesc property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getFLDDESC() {
            return flddesc;
        }

        /**
         * Sets the value of the flddesc property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setFLDDESC(String value) {
            this.flddesc = value;
        }

        /**
         * Gets the value of the datatype property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getDATATYPE() {
            return datatype;
        }

        /**
         * Sets the value of the datatype property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setDATATYPE(String value) {
            this.datatype = value;
        }

        /**
         * Gets the value of the fldno property.
         *
         * @return possible object is
         * {@link BigDecimal }
         */
        public BigDecimal getFLDNO() {
            return fldno;
        }

        /**
         * Sets the value of the fldno property.
         *
         * @param value allowed object is
         *              {@link BigDecimal }
         */
        public void setFLDNO(BigDecimal value) {
            this.fldno = value;
        }

    }

}
