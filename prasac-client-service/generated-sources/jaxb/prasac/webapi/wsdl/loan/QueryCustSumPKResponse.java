//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-558 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.05.17 at 10:44:26 AM ICT 
//


package prasac.webapi.wsdl.loan;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QueryCustSumPKResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="QueryCustSumPKResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="XREF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CPTYSUMMARY" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ACCNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="BRN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CPTY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryCustSumPKResponse", propOrder = {
        "xref",
        "cptysummary"
})
public class QueryCustSumPKResponse {

    @XmlElement(name = "XREF")
    protected String xref;
    @XmlElement(name = "CPTYSUMMARY", required = true)
    protected List<QueryCustSumPKResponse.CPTYSUMMARY> cptysummary;

    /**
     * Gets the value of the xref property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getXREF() {
        return xref;
    }

    /**
     * Sets the value of the xref property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setXREF(String value) {
        this.xref = value;
    }

    /**
     * Gets the value of the cptysummary property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cptysummary property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCPTYSUMMARY().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryCustSumPKResponse.CPTYSUMMARY }
     */
    public List<QueryCustSumPKResponse.CPTYSUMMARY> getCPTYSUMMARY() {
        if (cptysummary == null) {
            cptysummary = new ArrayList<QueryCustSumPKResponse.CPTYSUMMARY>();
        }
        return this.cptysummary;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ACCNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="BRN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CPTY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "accno",
            "brn",
            "cpty"
    })
    public static class CPTYSUMMARY {

        @XmlElement(name = "ACCNO")
        protected String accno;
        @XmlElement(name = "BRN")
        protected String brn;
        @XmlElement(name = "CPTY")
        protected String cpty;

        /**
         * Gets the value of the accno property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getACCNO() {
            return accno;
        }

        /**
         * Sets the value of the accno property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setACCNO(String value) {
            this.accno = value;
        }

        /**
         * Gets the value of the brn property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getBRN() {
            return brn;
        }

        /**
         * Sets the value of the brn property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setBRN(String value) {
            this.brn = value;
        }

        /**
         * Gets the value of the cpty property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getCPTY() {
            return cpty;
        }

        /**
         * Sets the value of the cpty property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setCPTY(String value) {
            this.cpty = value;
        }

    }

}
