//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-558 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.05.17 at 10:44:26 AM ICT 
//


package prasac.webapi.wsdl.loan;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SaveUdeventRequestIO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="SaveUdeventRequestIO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ACCNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BRN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EVNTCODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="VALUEDATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Clevent-Detail" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AMOUNTTAG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AMTPAID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="SETTLACCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SETTLBRN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Clevent-Charges" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="COMPNM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="STLCCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="STLBRN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="STLACC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SaveUdeventRequestIO", propOrder = {
        "accno",
        "brn",
        "evntcode",
        "valuedate",
        "cleventDetail",
        "cleventCharges"
})
public class SaveUdeventRequestIO {

    @XmlElement(name = "ACCNO", required = true)
    protected String accno;
    @XmlElement(name = "BRN", required = true)
    protected String brn;
    @XmlElement(name = "EVNTCODE", required = true)
    protected String evntcode;
    @XmlElement(name = "VALUEDATE", required = true)
    protected String valuedate;
    @XmlElement(name = "Clevent-Detail")
    protected List<SaveUdeventRequestIO.CleventDetail> cleventDetail;
    @XmlElement(name = "Clevent-Charges")
    protected List<SaveUdeventRequestIO.CleventCharges> cleventCharges;

    /**
     * Gets the value of the accno property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getACCNO() {
        return accno;
    }

    /**
     * Sets the value of the accno property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setACCNO(String value) {
        this.accno = value;
    }

    /**
     * Gets the value of the brn property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getBRN() {
        return brn;
    }

    /**
     * Sets the value of the brn property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setBRN(String value) {
        this.brn = value;
    }

    /**
     * Gets the value of the evntcode property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getEVNTCODE() {
        return evntcode;
    }

    /**
     * Sets the value of the evntcode property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setEVNTCODE(String value) {
        this.evntcode = value;
    }

    /**
     * Gets the value of the valuedate property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getVALUEDATE() {
        return valuedate;
    }

    /**
     * Sets the value of the valuedate property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setVALUEDATE(String value) {
        this.valuedate = value;
    }

    /**
     * Gets the value of the cleventDetail property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cleventDetail property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCleventDetail().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SaveUdeventRequestIO.CleventDetail }
     */
    public List<SaveUdeventRequestIO.CleventDetail> getCleventDetail() {
        if (cleventDetail == null) {
            cleventDetail = new ArrayList<SaveUdeventRequestIO.CleventDetail>();
        }
        return this.cleventDetail;
    }

    /**
     * Gets the value of the cleventCharges property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cleventCharges property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCleventCharges().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SaveUdeventRequestIO.CleventCharges }
     */
    public List<SaveUdeventRequestIO.CleventCharges> getCleventCharges() {
        if (cleventCharges == null) {
            cleventCharges = new ArrayList<SaveUdeventRequestIO.CleventCharges>();
        }
        return this.cleventCharges;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="COMPNM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="STLCCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="STLBRN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="STLACC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "compnm",
            "stlccy",
            "stlbrn",
            "stlacc"
    })
    public static class CleventCharges {

        @XmlElement(name = "COMPNM")
        protected String compnm;
        @XmlElement(name = "STLCCY")
        protected String stlccy;
        @XmlElement(name = "STLBRN")
        protected String stlbrn;
        @XmlElement(name = "STLACC")
        protected String stlacc;

        /**
         * Gets the value of the compnm property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getCOMPNM() {
            return compnm;
        }

        /**
         * Sets the value of the compnm property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setCOMPNM(String value) {
            this.compnm = value;
        }

        /**
         * Gets the value of the stlccy property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getSTLCCY() {
            return stlccy;
        }

        /**
         * Sets the value of the stlccy property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setSTLCCY(String value) {
            this.stlccy = value;
        }

        /**
         * Gets the value of the stlbrn property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getSTLBRN() {
            return stlbrn;
        }

        /**
         * Sets the value of the stlbrn property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setSTLBRN(String value) {
            this.stlbrn = value;
        }

        /**
         * Gets the value of the stlacc property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getSTLACC() {
            return stlacc;
        }

        /**
         * Sets the value of the stlacc property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setSTLACC(String value) {
            this.stlacc = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AMOUNTTAG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AMTPAID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *         &lt;element name="SETTLACCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SETTLBRN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "amounttag",
            "ccy",
            "amtpaid",
            "settlaccount",
            "settlbrn"
    })
    public static class CleventDetail {

        @XmlElement(name = "AMOUNTTAG")
        protected String amounttag;
        @XmlElement(name = "CCY")
        protected String ccy;
        @XmlElement(name = "AMTPAID")
        protected BigInteger amtpaid;
        @XmlElement(name = "SETTLACCOUNT")
        protected String settlaccount;
        @XmlElement(name = "SETTLBRN")
        protected String settlbrn;

        /**
         * Gets the value of the amounttag property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getAMOUNTTAG() {
            return amounttag;
        }

        /**
         * Sets the value of the amounttag property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setAMOUNTTAG(String value) {
            this.amounttag = value;
        }

        /**
         * Gets the value of the ccy property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getCCY() {
            return ccy;
        }

        /**
         * Sets the value of the ccy property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setCCY(String value) {
            this.ccy = value;
        }

        /**
         * Gets the value of the amtpaid property.
         *
         * @return possible object is
         * {@link BigInteger }
         */
        public BigInteger getAMTPAID() {
            return amtpaid;
        }

        /**
         * Sets the value of the amtpaid property.
         *
         * @param value allowed object is
         *              {@link BigInteger }
         */
        public void setAMTPAID(BigInteger value) {
            this.amtpaid = value;
        }

        /**
         * Gets the value of the settlaccount property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getSETTLACCOUNT() {
            return settlaccount;
        }

        /**
         * Sets the value of the settlaccount property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setSETTLACCOUNT(String value) {
            this.settlaccount = value;
        }

        /**
         * Gets the value of the settlbrn property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getSETTLBRN() {
            return settlbrn;
        }

        /**
         * Sets the value of the settlbrn property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setSETTLBRN(String value) {
            this.settlbrn = value;
        }

    }

}
