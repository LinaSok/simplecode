//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-558 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.05.17 at 10:44:26 AM ICT 
//


package prasac.webapi.wsdl.loan;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FCUBS_HEADER" type="{http://fcubs.ofss.com/service/FCUBSCLService}FCUBS_HEADERType"/>
 *         &lt;element name="FCUBS_BODY">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="RENOG-RES-PK" type="{http://fcubs.ofss.com/service/FCUBSCLService}SaveRenogPKResponse" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="CL-RENOG-REQ-FULL" type="{http://fcubs.ofss.com/service/FCUBSCLService}SaveRenogFS" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="CL-RENOG-REQ-IO" type="{http://fcubs.ofss.com/service/FCUBSCLService}SaveRenogIO" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="CL-RENOG-REQ-PK" type="{http://fcubs.ofss.com/service/FCUBSCLService}SaveRenogPK" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="FCUBS_WARNING_RESP" type="{http://fcubs.ofss.com/service/FCUBSCLService}WARNINGType" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="FCUBS_ERROR_RESP" type="{http://fcubs.ofss.com/service/FCUBSCLService}ERRORType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "fcubsheader",
        "fcubsbody"
})
@XmlRootElement(name = "SAVERENOG_IOPK_RES")
public class SAVERENOGIOPKRES {

    @XmlElement(name = "FCUBS_HEADER", required = true)
    protected FCUBSHEADERType fcubsheader;
    @XmlElement(name = "FCUBS_BODY", required = true)
    protected SAVERENOGIOPKRES.FCUBSBODY fcubsbody;

    /**
     * Gets the value of the fcubsheader property.
     *
     * @return possible object is
     * {@link FCUBSHEADERType }
     */
    public FCUBSHEADERType getFCUBSHEADER() {
        return fcubsheader;
    }

    /**
     * Sets the value of the fcubsheader property.
     *
     * @param value allowed object is
     *              {@link FCUBSHEADERType }
     */
    public void setFCUBSHEADER(FCUBSHEADERType value) {
        this.fcubsheader = value;
    }

    /**
     * Gets the value of the fcubsbody property.
     *
     * @return possible object is
     * {@link SAVERENOGIOPKRES.FCUBSBODY }
     */
    public SAVERENOGIOPKRES.FCUBSBODY getFCUBSBODY() {
        return fcubsbody;
    }

    /**
     * Sets the value of the fcubsbody property.
     *
     * @param value allowed object is
     *              {@link SAVERENOGIOPKRES.FCUBSBODY }
     */
    public void setFCUBSBODY(SAVERENOGIOPKRES.FCUBSBODY value) {
        this.fcubsbody = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="RENOG-RES-PK" type="{http://fcubs.ofss.com/service/FCUBSCLService}SaveRenogPKResponse" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="CL-RENOG-REQ-FULL" type="{http://fcubs.ofss.com/service/FCUBSCLService}SaveRenogFS" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="CL-RENOG-REQ-IO" type="{http://fcubs.ofss.com/service/FCUBSCLService}SaveRenogIO" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="CL-RENOG-REQ-PK" type="{http://fcubs.ofss.com/service/FCUBSCLService}SaveRenogPK" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="FCUBS_WARNING_RESP" type="{http://fcubs.ofss.com/service/FCUBSCLService}WARNINGType" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="FCUBS_ERROR_RESP" type="{http://fcubs.ofss.com/service/FCUBSCLService}ERRORType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "renogrespk",
            "clrenogreqfull",
            "clrenogreqio",
            "clrenogreqpk",
            "fcubswarningresp",
            "fcubserrorresp"
    })
    public static class FCUBSBODY {

        @XmlElement(name = "RENOG-RES-PK")
        protected List<SaveRenogPKResponse> renogrespk;
        @XmlElement(name = "CL-RENOG-REQ-FULL")
        protected List<SaveRenogFS> clrenogreqfull;
        @XmlElement(name = "CL-RENOG-REQ-IO")
        protected List<SaveRenogIO> clrenogreqio;
        @XmlElement(name = "CL-RENOG-REQ-PK")
        protected List<SaveRenogPK> clrenogreqpk;
        @XmlElement(name = "FCUBS_WARNING_RESP")
        protected List<WARNINGType> fcubswarningresp;
        @XmlElement(name = "FCUBS_ERROR_RESP")
        protected List<ERRORType> fcubserrorresp;

        /**
         * Gets the value of the renogrespk property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the renogrespk property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRENOGRESPK().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SaveRenogPKResponse }
         */
        public List<SaveRenogPKResponse> getRENOGRESPK() {
            if (renogrespk == null) {
                renogrespk = new ArrayList<SaveRenogPKResponse>();
            }
            return this.renogrespk;
        }

        /**
         * Gets the value of the clrenogreqfull property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the clrenogreqfull property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCLRENOGREQFULL().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SaveRenogFS }
         */
        public List<SaveRenogFS> getCLRENOGREQFULL() {
            if (clrenogreqfull == null) {
                clrenogreqfull = new ArrayList<SaveRenogFS>();
            }
            return this.clrenogreqfull;
        }

        /**
         * Gets the value of the clrenogreqio property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the clrenogreqio property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCLRENOGREQIO().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SaveRenogIO }
         */
        public List<SaveRenogIO> getCLRENOGREQIO() {
            if (clrenogreqio == null) {
                clrenogreqio = new ArrayList<SaveRenogIO>();
            }
            return this.clrenogreqio;
        }

        /**
         * Gets the value of the clrenogreqpk property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the clrenogreqpk property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCLRENOGREQPK().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SaveRenogPK }
         */
        public List<SaveRenogPK> getCLRENOGREQPK() {
            if (clrenogreqpk == null) {
                clrenogreqpk = new ArrayList<SaveRenogPK>();
            }
            return this.clrenogreqpk;
        }

        /**
         * Gets the value of the fcubswarningresp property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the fcubswarningresp property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFCUBSWARNINGRESP().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link WARNINGType }
         */
        public List<WARNINGType> getFCUBSWARNINGRESP() {
            if (fcubswarningresp == null) {
                fcubswarningresp = new ArrayList<WARNINGType>();
            }
            return this.fcubswarningresp;
        }

        /**
         * Gets the value of the fcubserrorresp property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the fcubserrorresp property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFCUBSERRORRESP().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ERRORType }
         */
        public List<ERRORType> getFCUBSERRORRESP() {
            if (fcubserrorresp == null) {
                fcubserrorresp = new ArrayList<ERRORType>();
            }
            return this.fcubserrorresp;
        }

    }

}
