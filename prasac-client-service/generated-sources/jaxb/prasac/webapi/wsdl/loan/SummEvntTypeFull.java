//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-558 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.05.17 at 10:44:26 AM ICT 
//


package prasac.webapi.wsdl.loan;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SummEvntTypeFull complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="SummEvntTypeFull">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ACCNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BRN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ALTACCNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CUSTID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EVNTCOD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="VALDT" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="AMOUNT" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="CCY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AMTTAG" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ESN" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="VERNO" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SummEvntTypeFull", propOrder = {
        "accno",
        "brn",
        "altaccno",
        "custid",
        "evntcod",
        "valdt",
        "amount",
        "ccy",
        "amttag",
        "esn",
        "verno"
})
public class SummEvntTypeFull {

    @XmlElement(name = "ACCNO", required = true)
    protected String accno;
    @XmlElement(name = "BRN", required = true)
    protected String brn;
    @XmlElement(name = "ALTACCNO")
    protected String altaccno;
    @XmlElement(name = "CUSTID", required = true)
    protected String custid;
    @XmlElement(name = "EVNTCOD", required = true)
    protected String evntcod;
    @XmlElement(name = "VALDT", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar valdt;
    @XmlElement(name = "AMOUNT", required = true)
    protected BigDecimal amount;
    @XmlElement(name = "CCY", required = true)
    protected String ccy;
    @XmlElement(name = "AMTTAG", required = true)
    protected String amttag;
    @XmlElement(name = "ESN", required = true)
    protected BigInteger esn;
    @XmlElement(name = "VERNO", required = true)
    protected BigInteger verno;

    /**
     * Gets the value of the accno property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getACCNO() {
        return accno;
    }

    /**
     * Sets the value of the accno property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setACCNO(String value) {
        this.accno = value;
    }

    /**
     * Gets the value of the brn property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getBRN() {
        return brn;
    }

    /**
     * Sets the value of the brn property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setBRN(String value) {
        this.brn = value;
    }

    /**
     * Gets the value of the altaccno property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getALTACCNO() {
        return altaccno;
    }

    /**
     * Sets the value of the altaccno property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setALTACCNO(String value) {
        this.altaccno = value;
    }

    /**
     * Gets the value of the custid property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCUSTID() {
        return custid;
    }

    /**
     * Sets the value of the custid property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCUSTID(String value) {
        this.custid = value;
    }

    /**
     * Gets the value of the evntcod property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getEVNTCOD() {
        return evntcod;
    }

    /**
     * Sets the value of the evntcod property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setEVNTCOD(String value) {
        this.evntcod = value;
    }

    /**
     * Gets the value of the valdt property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getVALDT() {
        return valdt;
    }

    /**
     * Sets the value of the valdt property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setVALDT(XMLGregorianCalendar value) {
        this.valdt = value;
    }

    /**
     * Gets the value of the amount property.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getAMOUNT() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setAMOUNT(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the ccy property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCCY() {
        return ccy;
    }

    /**
     * Sets the value of the ccy property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCCY(String value) {
        this.ccy = value;
    }

    /**
     * Gets the value of the amttag property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getAMTTAG() {
        return amttag;
    }

    /**
     * Sets the value of the amttag property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setAMTTAG(String value) {
        this.amttag = value;
    }

    /**
     * Gets the value of the esn property.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    public BigInteger getESN() {
        return esn;
    }

    /**
     * Sets the value of the esn property.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    public void setESN(BigInteger value) {
        this.esn = value;
    }

    /**
     * Gets the value of the verno property.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    public BigInteger getVERNO() {
        return verno;
    }

    /**
     * Sets the value of the verno property.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    public void setVERNO(BigInteger value) {
        this.verno = value;
    }

}
