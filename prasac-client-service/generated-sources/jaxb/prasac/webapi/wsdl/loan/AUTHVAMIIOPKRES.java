//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-558 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.05.17 at 10:44:26 AM ICT 
//


package prasac.webapi.wsdl.loan;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FCUBS_HEADER" type="{http://fcubs.ofss.com/service/FCUBSCLService}FCUBS_HEADERType"/>
 *         &lt;element name="FCUBS_BODY">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CLAuthVamiResPK" type="{http://fcubs.ofss.com/service/FCUBSCLService}AuthVamiResPK" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="CL-Auth-Vami-Req" type="{http://fcubs.ofss.com/service/FCUBSCLService}AuthVamiRes" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="CL-Auth-Vami-Req-IO" type="{http://fcubs.ofss.com/service/FCUBSCLService}AuthVamiIO" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="FCUBS_ERROR_RESP" type="{http://fcubs.ofss.com/service/FCUBSCLService}ERRORType" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="FCUBS_WARNING_RESP" type="{http://fcubs.ofss.com/service/FCUBSCLService}WARNINGType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "fcubsheader",
        "fcubsbody"
})
@XmlRootElement(name = "AUTHVAMI_IOPK_RES")
public class AUTHVAMIIOPKRES {

    @XmlElement(name = "FCUBS_HEADER", required = true)
    protected FCUBSHEADERType fcubsheader;
    @XmlElement(name = "FCUBS_BODY", required = true)
    protected AUTHVAMIIOPKRES.FCUBSBODY fcubsbody;

    /**
     * Gets the value of the fcubsheader property.
     *
     * @return possible object is
     * {@link FCUBSHEADERType }
     */
    public FCUBSHEADERType getFCUBSHEADER() {
        return fcubsheader;
    }

    /**
     * Sets the value of the fcubsheader property.
     *
     * @param value allowed object is
     *              {@link FCUBSHEADERType }
     */
    public void setFCUBSHEADER(FCUBSHEADERType value) {
        this.fcubsheader = value;
    }

    /**
     * Gets the value of the fcubsbody property.
     *
     * @return possible object is
     * {@link AUTHVAMIIOPKRES.FCUBSBODY }
     */
    public AUTHVAMIIOPKRES.FCUBSBODY getFCUBSBODY() {
        return fcubsbody;
    }

    /**
     * Sets the value of the fcubsbody property.
     *
     * @param value allowed object is
     *              {@link AUTHVAMIIOPKRES.FCUBSBODY }
     */
    public void setFCUBSBODY(AUTHVAMIIOPKRES.FCUBSBODY value) {
        this.fcubsbody = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CLAuthVamiResPK" type="{http://fcubs.ofss.com/service/FCUBSCLService}AuthVamiResPK" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="CL-Auth-Vami-Req" type="{http://fcubs.ofss.com/service/FCUBSCLService}AuthVamiRes" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="CL-Auth-Vami-Req-IO" type="{http://fcubs.ofss.com/service/FCUBSCLService}AuthVamiIO" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="FCUBS_ERROR_RESP" type="{http://fcubs.ofss.com/service/FCUBSCLService}ERRORType" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="FCUBS_WARNING_RESP" type="{http://fcubs.ofss.com/service/FCUBSCLService}WARNINGType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "clAuthVamiResPK",
            "clAuthVamiReq",
            "clAuthVamiReqIO",
            "fcubserrorresp",
            "fcubswarningresp"
    })
    public static class FCUBSBODY {

        @XmlElement(name = "CLAuthVamiResPK")
        protected List<AuthVamiResPK> clAuthVamiResPK;
        @XmlElement(name = "CL-Auth-Vami-Req")
        protected List<AuthVamiRes> clAuthVamiReq;
        @XmlElement(name = "CL-Auth-Vami-Req-IO")
        protected List<AuthVamiIO> clAuthVamiReqIO;
        @XmlElement(name = "FCUBS_ERROR_RESP")
        protected List<ERRORType> fcubserrorresp;
        @XmlElement(name = "FCUBS_WARNING_RESP")
        protected List<WARNINGType> fcubswarningresp;

        /**
         * Gets the value of the clAuthVamiResPK property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the clAuthVamiResPK property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCLAuthVamiResPK().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AuthVamiResPK }
         */
        public List<AuthVamiResPK> getCLAuthVamiResPK() {
            if (clAuthVamiResPK == null) {
                clAuthVamiResPK = new ArrayList<AuthVamiResPK>();
            }
            return this.clAuthVamiResPK;
        }

        /**
         * Gets the value of the clAuthVamiReq property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the clAuthVamiReq property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCLAuthVamiReq().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AuthVamiRes }
         */
        public List<AuthVamiRes> getCLAuthVamiReq() {
            if (clAuthVamiReq == null) {
                clAuthVamiReq = new ArrayList<AuthVamiRes>();
            }
            return this.clAuthVamiReq;
        }

        /**
         * Gets the value of the clAuthVamiReqIO property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the clAuthVamiReqIO property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCLAuthVamiReqIO().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AuthVamiIO }
         */
        public List<AuthVamiIO> getCLAuthVamiReqIO() {
            if (clAuthVamiReqIO == null) {
                clAuthVamiReqIO = new ArrayList<AuthVamiIO>();
            }
            return this.clAuthVamiReqIO;
        }

        /**
         * Gets the value of the fcubserrorresp property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the fcubserrorresp property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFCUBSERRORRESP().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ERRORType }
         */
        public List<ERRORType> getFCUBSERRORRESP() {
            if (fcubserrorresp == null) {
                fcubserrorresp = new ArrayList<ERRORType>();
            }
            return this.fcubserrorresp;
        }

        /**
         * Gets the value of the fcubswarningresp property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the fcubswarningresp property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFCUBSWARNINGRESP().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link WARNINGType }
         */
        public List<WARNINGType> getFCUBSWARNINGRESP() {
            if (fcubswarningresp == null) {
                fcubswarningresp = new ArrayList<WARNINGType>();
            }
            return this.fcubswarningresp;
        }

    }

}
