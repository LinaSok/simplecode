//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-558 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.05.17 at 10:44:26 AM ICT 
//


package prasac.webapi.wsdl.loan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SaveRenogPK complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="SaveRenogPK">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="XREF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ACCNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BRN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EXECDT" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="VALDT" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SaveRenogPK", propOrder = {
        "scode",
        "xref",
        "accno",
        "brn",
        "execdt",
        "valdt"
})
public class SaveRenogPK {

    @XmlElement(name = "SCODE")
    protected String scode;
    @XmlElement(name = "XREF")
    protected String xref;
    @XmlElement(name = "ACCNO", required = true)
    protected String accno;
    @XmlElement(name = "BRN", required = true)
    protected String brn;
    @XmlElement(name = "EXECDT", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar execdt;
    @XmlElement(name = "VALDT", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar valdt;

    /**
     * Gets the value of the scode property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSCODE() {
        return scode;
    }

    /**
     * Sets the value of the scode property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSCODE(String value) {
        this.scode = value;
    }

    /**
     * Gets the value of the xref property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getXREF() {
        return xref;
    }

    /**
     * Sets the value of the xref property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setXREF(String value) {
        this.xref = value;
    }

    /**
     * Gets the value of the accno property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getACCNO() {
        return accno;
    }

    /**
     * Sets the value of the accno property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setACCNO(String value) {
        this.accno = value;
    }

    /**
     * Gets the value of the brn property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getBRN() {
        return brn;
    }

    /**
     * Sets the value of the brn property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setBRN(String value) {
        this.brn = value;
    }

    /**
     * Gets the value of the execdt property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getEXECDT() {
        return execdt;
    }

    /**
     * Sets the value of the execdt property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setEXECDT(XMLGregorianCalendar value) {
        this.execdt = value;
    }

    /**
     * Gets the value of the valdt property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getVALDT() {
        return valdt;
    }

    /**
     * Sets the value of the valdt property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setVALDT(XMLGregorianCalendar value) {
        this.valdt = value;
    }

}
