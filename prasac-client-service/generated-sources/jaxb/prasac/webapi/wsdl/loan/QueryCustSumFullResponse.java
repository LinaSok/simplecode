//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-558 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.05.17 at 10:44:26 AM ICT 
//


package prasac.webapi.wsdl.loan;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QueryCustSumFullResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="QueryCustSumFullResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="XREF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CPTYSUMMARY" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ACCNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="BRN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ALTACCNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PRD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CPTY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ISAVAIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryCustSumFullResponse", propOrder = {
        "xref",
        "cptysummary"
})
public class QueryCustSumFullResponse {

    @XmlElement(name = "XREF")
    protected String xref;
    @XmlElement(name = "CPTYSUMMARY", required = true)
    protected List<QueryCustSumFullResponse.CPTYSUMMARY> cptysummary;

    /**
     * Gets the value of the xref property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getXREF() {
        return xref;
    }

    /**
     * Sets the value of the xref property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setXREF(String value) {
        this.xref = value;
    }

    /**
     * Gets the value of the cptysummary property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cptysummary property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCPTYSUMMARY().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryCustSumFullResponse.CPTYSUMMARY }
     */
    public List<QueryCustSumFullResponse.CPTYSUMMARY> getCPTYSUMMARY() {
        if (cptysummary == null) {
            cptysummary = new ArrayList<QueryCustSumFullResponse.CPTYSUMMARY>();
        }
        return this.cptysummary;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ACCNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="BRN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ALTACCNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PRD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CPTY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ISAVAIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "accno",
            "brn",
            "altaccno",
            "prd",
            "cpty",
            "ccy",
            "isavail"
    })
    public static class CPTYSUMMARY {

        @XmlElement(name = "ACCNO")
        protected String accno;
        @XmlElement(name = "BRN")
        protected String brn;
        @XmlElement(name = "ALTACCNO")
        protected String altaccno;
        @XmlElement(name = "PRD")
        protected String prd;
        @XmlElement(name = "CPTY")
        protected String cpty;
        @XmlElement(name = "CCY")
        protected String ccy;
        @XmlElement(name = "ISAVAIL")
        protected String isavail;

        /**
         * Gets the value of the accno property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getACCNO() {
            return accno;
        }

        /**
         * Sets the value of the accno property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setACCNO(String value) {
            this.accno = value;
        }

        /**
         * Gets the value of the brn property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getBRN() {
            return brn;
        }

        /**
         * Sets the value of the brn property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setBRN(String value) {
            this.brn = value;
        }

        /**
         * Gets the value of the altaccno property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getALTACCNO() {
            return altaccno;
        }

        /**
         * Sets the value of the altaccno property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setALTACCNO(String value) {
            this.altaccno = value;
        }

        /**
         * Gets the value of the prd property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getPRD() {
            return prd;
        }

        /**
         * Sets the value of the prd property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setPRD(String value) {
            this.prd = value;
        }

        /**
         * Gets the value of the cpty property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getCPTY() {
            return cpty;
        }

        /**
         * Sets the value of the cpty property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setCPTY(String value) {
            this.cpty = value;
        }

        /**
         * Gets the value of the ccy property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getCCY() {
            return ccy;
        }

        /**
         * Sets the value of the ccy property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setCCY(String value) {
            this.ccy = value;
        }

        /**
         * Gets the value of the isavail property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getISAVAIL() {
            return isavail;
        }

        /**
         * Sets the value of the isavail property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setISAVAIL(String value) {
            this.isavail = value;
        }

    }

}
