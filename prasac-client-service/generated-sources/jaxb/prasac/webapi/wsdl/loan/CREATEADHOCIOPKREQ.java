//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-558 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.05.17 at 10:44:26 AM ICT 
//


package prasac.webapi.wsdl.loan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FCUBS_HEADER" type="{http://fcubs.ofss.com/service/FCUBSCLService}FCUBS_HEADERType"/>
 *         &lt;element name="FCUBS_BODY">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Clvws-Adhoc-Charges-IO" type="{http://fcubs.ofss.com/service/FCUBSCLService}CL-Adhoc-Create-IO-Type"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "fcubsheader",
        "fcubsbody"
})
@XmlRootElement(name = "CREATEADHOC_IOPK_REQ")
public class CREATEADHOCIOPKREQ {

    @XmlElement(name = "FCUBS_HEADER", required = true)
    protected FCUBSHEADERType fcubsheader;
    @XmlElement(name = "FCUBS_BODY", required = true)
    protected CREATEADHOCIOPKREQ.FCUBSBODY fcubsbody;

    /**
     * Gets the value of the fcubsheader property.
     *
     * @return possible object is
     * {@link FCUBSHEADERType }
     */
    public FCUBSHEADERType getFCUBSHEADER() {
        return fcubsheader;
    }

    /**
     * Sets the value of the fcubsheader property.
     *
     * @param value allowed object is
     *              {@link FCUBSHEADERType }
     */
    public void setFCUBSHEADER(FCUBSHEADERType value) {
        this.fcubsheader = value;
    }

    /**
     * Gets the value of the fcubsbody property.
     *
     * @return possible object is
     * {@link CREATEADHOCIOPKREQ.FCUBSBODY }
     */
    public CREATEADHOCIOPKREQ.FCUBSBODY getFCUBSBODY() {
        return fcubsbody;
    }

    /**
     * Sets the value of the fcubsbody property.
     *
     * @param value allowed object is
     *              {@link CREATEADHOCIOPKREQ.FCUBSBODY }
     */
    public void setFCUBSBODY(CREATEADHOCIOPKREQ.FCUBSBODY value) {
        this.fcubsbody = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Clvws-Adhoc-Charges-IO" type="{http://fcubs.ofss.com/service/FCUBSCLService}CL-Adhoc-Create-IO-Type"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "clvwsAdhocChargesIO"
    })
    public static class FCUBSBODY {

        @XmlElement(name = "Clvws-Adhoc-Charges-IO", required = true)
        protected CLAdhocCreateIOType clvwsAdhocChargesIO;

        /**
         * Gets the value of the clvwsAdhocChargesIO property.
         *
         * @return possible object is
         * {@link CLAdhocCreateIOType }
         */
        public CLAdhocCreateIOType getClvwsAdhocChargesIO() {
            return clvwsAdhocChargesIO;
        }

        /**
         * Sets the value of the clvwsAdhocChargesIO property.
         *
         * @param value allowed object is
         *              {@link CLAdhocCreateIOType }
         */
        public void setClvwsAdhocChargesIO(CLAdhocCreateIOType value) {
            this.clvwsAdhocChargesIO = value;
        }

    }

}
