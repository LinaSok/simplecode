//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-558 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.05.17 at 10:44:26 AM ICT 
//


package prasac.webapi.wsdl.loan;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ProdTrfr-Close-IO-Type complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ProdTrfr-Close-IO-Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PRODUCT_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CURRENT_BRANCH_CODESS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TARGET_BRANCH_CODESS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TRANSFER_DATE" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="TRANSFER_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MAKER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MAKERSTAMP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MODNO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProdTrfr-Close-IO-Type", propOrder = {
        "productcode",
        "currentbranchcodess",
        "targetbranchcodess",
        "transferdate",
        "transferid",
        "maker",
        "makerstamp",
        "modno"
})
public class ProdTrfrCloseIOType {

    @XmlElement(name = "PRODUCT_CODE")
    protected String productcode;
    @XmlElement(name = "CURRENT_BRANCH_CODESS")
    protected String currentbranchcodess;
    @XmlElement(name = "TARGET_BRANCH_CODESS")
    protected String targetbranchcodess;
    @XmlElement(name = "TRANSFER_DATE")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar transferdate;
    @XmlElement(name = "TRANSFER_ID", required = true)
    protected String transferid;
    @XmlElement(name = "MAKER")
    protected String maker;
    @XmlElement(name = "MAKERSTAMP")
    protected String makerstamp;
    @XmlElement(name = "MODNO")
    protected BigDecimal modno;

    /**
     * Gets the value of the productcode property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPRODUCTCODE() {
        return productcode;
    }

    /**
     * Sets the value of the productcode property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPRODUCTCODE(String value) {
        this.productcode = value;
    }

    /**
     * Gets the value of the currentbranchcodess property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCURRENTBRANCHCODESS() {
        return currentbranchcodess;
    }

    /**
     * Sets the value of the currentbranchcodess property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCURRENTBRANCHCODESS(String value) {
        this.currentbranchcodess = value;
    }

    /**
     * Gets the value of the targetbranchcodess property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTARGETBRANCHCODESS() {
        return targetbranchcodess;
    }

    /**
     * Sets the value of the targetbranchcodess property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTARGETBRANCHCODESS(String value) {
        this.targetbranchcodess = value;
    }

    /**
     * Gets the value of the transferdate property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getTRANSFERDATE() {
        return transferdate;
    }

    /**
     * Sets the value of the transferdate property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setTRANSFERDATE(XMLGregorianCalendar value) {
        this.transferdate = value;
    }

    /**
     * Gets the value of the transferid property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTRANSFERID() {
        return transferid;
    }

    /**
     * Sets the value of the transferid property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTRANSFERID(String value) {
        this.transferid = value;
    }

    /**
     * Gets the value of the maker property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMAKER() {
        return maker;
    }

    /**
     * Sets the value of the maker property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMAKER(String value) {
        this.maker = value;
    }

    /**
     * Gets the value of the makerstamp property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMAKERSTAMP() {
        return makerstamp;
    }

    /**
     * Sets the value of the makerstamp property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMAKERSTAMP(String value) {
        this.makerstamp = value;
    }

    /**
     * Gets the value of the modno property.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getMODNO() {
        return modno;
    }

    /**
     * Sets the value of the modno property.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setMODNO(BigDecimal value) {
        this.modno = value;
    }

}
