//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-558 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.05.17 at 10:44:26 AM ICT 
//


package prasac.webapi.wsdl.loan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SaveCLAdhocIO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="SaveCLAdhocIO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ACCNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BRN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="COMP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CHGINCURDT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="REASON" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AMT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHGTYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="INTREFNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CRACC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CCY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EXCHRT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AUTHSTAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SaveCLAdhocIO", propOrder = {
        "accno",
        "brn",
        "comp",
        "chgincurdt",
        "reason",
        "amt",
        "chgtype",
        "intrefno",
        "cracc",
        "ccy",
        "exchrt",
        "authstat"
})
public class SaveCLAdhocIO {

    @XmlElement(name = "ACCNO", required = true)
    protected String accno;
    @XmlElement(name = "BRN", required = true)
    protected String brn;
    @XmlElement(name = "COMP", required = true)
    protected String comp;
    @XmlElement(name = "CHGINCURDT")
    protected String chgincurdt;
    @XmlElement(name = "REASON", required = true)
    protected String reason;
    @XmlElement(name = "AMT")
    protected String amt;
    @XmlElement(name = "CHGTYPE", required = true)
    protected String chgtype;
    @XmlElement(name = "INTREFNO", required = true)
    protected String intrefno;
    @XmlElement(name = "CRACC", required = true)
    protected String cracc;
    @XmlElement(name = "CCY", required = true)
    protected String ccy;
    @XmlElement(name = "EXCHRT")
    protected String exchrt;
    @XmlElement(name = "AUTHSTAT")
    protected String authstat;

    /**
     * Gets the value of the accno property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getACCNO() {
        return accno;
    }

    /**
     * Sets the value of the accno property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setACCNO(String value) {
        this.accno = value;
    }

    /**
     * Gets the value of the brn property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getBRN() {
        return brn;
    }

    /**
     * Sets the value of the brn property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setBRN(String value) {
        this.brn = value;
    }

    /**
     * Gets the value of the comp property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCOMP() {
        return comp;
    }

    /**
     * Sets the value of the comp property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCOMP(String value) {
        this.comp = value;
    }

    /**
     * Gets the value of the chgincurdt property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCHGINCURDT() {
        return chgincurdt;
    }

    /**
     * Sets the value of the chgincurdt property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCHGINCURDT(String value) {
        this.chgincurdt = value;
    }

    /**
     * Gets the value of the reason property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getREASON() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setREASON(String value) {
        this.reason = value;
    }

    /**
     * Gets the value of the amt property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getAMT() {
        return amt;
    }

    /**
     * Sets the value of the amt property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setAMT(String value) {
        this.amt = value;
    }

    /**
     * Gets the value of the chgtype property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCHGTYPE() {
        return chgtype;
    }

    /**
     * Sets the value of the chgtype property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCHGTYPE(String value) {
        this.chgtype = value;
    }

    /**
     * Gets the value of the intrefno property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getINTREFNO() {
        return intrefno;
    }

    /**
     * Sets the value of the intrefno property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setINTREFNO(String value) {
        this.intrefno = value;
    }

    /**
     * Gets the value of the cracc property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCRACC() {
        return cracc;
    }

    /**
     * Sets the value of the cracc property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCRACC(String value) {
        this.cracc = value;
    }

    /**
     * Gets the value of the ccy property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCCY() {
        return ccy;
    }

    /**
     * Sets the value of the ccy property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCCY(String value) {
        this.ccy = value;
    }

    /**
     * Gets the value of the exchrt property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getEXCHRT() {
        return exchrt;
    }

    /**
     * Sets the value of the exchrt property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setEXCHRT(String value) {
        this.exchrt = value;
    }

    /**
     * Gets the value of the authstat property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getAUTHSTAT() {
        return authstat;
    }

    /**
     * Sets the value of the authstat property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setAUTHSTAT(String value) {
        this.authstat = value;
    }

}
