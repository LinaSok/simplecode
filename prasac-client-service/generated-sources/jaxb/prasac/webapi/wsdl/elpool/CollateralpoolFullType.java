//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-558 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.05.16 at 11:03:32 AM ICT 
//


package prasac.webapi.wsdl.elpool;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for collateralpool-Full-Type complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="collateralpool-Full-Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LIAB_ID" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="POOL_CODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="POOL_DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="POOL_CCY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="POOL_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="POOL_UTIL" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="LIAB_BRANCH" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MORTGAGE_INITIATED" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AUTOFACILITY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AVAILABLE_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="LIAB_NO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BLOCK_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="LIAB_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BRANCH_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AUTO_FACILITY_CUSTNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MAKER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MAKERSTAMP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHECKER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHECKERSTAMP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MODNO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TXNSTAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AUTHSTAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Pool-Collaterals-Linkage" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="FUNCTION_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="COLLATERAL_CCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="COLLATERAL_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="LINKEDAMT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="LINKDAMTPOOL" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="COLLATUTIL" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="LINKEDPERCENTNUMBER" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="AVAIL_INT_RATE" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="INT_SPREAD" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="RATE_OF_INT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="EXPIRY_DATE" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="POOL_ID" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="ORDER_NO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="BRANCH_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="COLLATERAL_CODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="COLLATERAL_DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SHARING_PERCENTAGE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="LIMIT_CONTRIBUTION" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="COLLATERAL_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="UDFDETAILS" type="{http://fcubs.ofss.com/service/ELPoolService}UDFDETAILSType2" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "collateralpool-Full-Type", propOrder = {
        "liabid",
        "poolcode",
        "pooldescription",
        "poolccy",
        "poolamt",
        "poolutil",
        "liabbranch",
        "mortgageinitiated",
        "autofacility",
        "availableamount",
        "liabno",
        "blockamt",
        "liabname",
        "branchcode",
        "autofacilitycustno",
        "maker",
        "makerstamp",
        "checker",
        "checkerstamp",
        "modno",
        "txnstat",
        "authstat",
        "poolCollateralsLinkage",
        "udfdetails"
})
public class CollateralpoolFullType {

    @XmlElement(name = "LIAB_ID")
    protected BigDecimal liabid;
    @XmlElement(name = "POOL_CODE", required = true)
    protected String poolcode;
    @XmlElement(name = "POOL_DESCRIPTION")
    protected String pooldescription;
    @XmlElement(name = "POOL_CCY", required = true)
    protected String poolccy;
    @XmlElement(name = "POOL_AMT")
    protected BigDecimal poolamt;
    @XmlElement(name = "POOL_UTIL")
    protected BigDecimal poolutil;
    @XmlElement(name = "LIAB_BRANCH")
    protected String liabbranch;
    @XmlElement(name = "MORTGAGE_INITIATED")
    protected String mortgageinitiated;
    @XmlElement(name = "AUTOFACILITY")
    protected String autofacility;
    @XmlElement(name = "AVAILABLE_AMOUNT")
    protected BigDecimal availableamount;
    @XmlElement(name = "LIAB_NO", required = true)
    protected String liabno;
    @XmlElement(name = "BLOCK_AMT")
    protected BigDecimal blockamt;
    @XmlElement(name = "LIAB_NAME")
    protected String liabname;
    @XmlElement(name = "BRANCH_CODE")
    protected String branchcode;
    @XmlElement(name = "AUTO_FACILITY_CUSTNO")
    protected String autofacilitycustno;
    @XmlElement(name = "MAKER")
    protected String maker;
    @XmlElement(name = "MAKERSTAMP")
    protected String makerstamp;
    @XmlElement(name = "CHECKER")
    protected String checker;
    @XmlElement(name = "CHECKERSTAMP")
    protected String checkerstamp;
    @XmlElement(name = "MODNO")
    protected BigDecimal modno;
    @XmlElement(name = "TXNSTAT")
    protected String txnstat;
    @XmlElement(name = "AUTHSTAT")
    protected String authstat;
    @XmlElement(name = "Pool-Collaterals-Linkage")
    protected List<CollateralpoolFullType.PoolCollateralsLinkage> poolCollateralsLinkage;
    @XmlElement(name = "UDFDETAILS")
    protected List<UDFDETAILSType2> udfdetails;

    /**
     * Gets the value of the liabid property.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getLIABID() {
        return liabid;
    }

    /**
     * Sets the value of the liabid property.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setLIABID(BigDecimal value) {
        this.liabid = value;
    }

    /**
     * Gets the value of the poolcode property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPOOLCODE() {
        return poolcode;
    }

    /**
     * Sets the value of the poolcode property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPOOLCODE(String value) {
        this.poolcode = value;
    }

    /**
     * Gets the value of the pooldescription property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPOOLDESCRIPTION() {
        return pooldescription;
    }

    /**
     * Sets the value of the pooldescription property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPOOLDESCRIPTION(String value) {
        this.pooldescription = value;
    }

    /**
     * Gets the value of the poolccy property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPOOLCCY() {
        return poolccy;
    }

    /**
     * Sets the value of the poolccy property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPOOLCCY(String value) {
        this.poolccy = value;
    }

    /**
     * Gets the value of the poolamt property.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getPOOLAMT() {
        return poolamt;
    }

    /**
     * Sets the value of the poolamt property.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setPOOLAMT(BigDecimal value) {
        this.poolamt = value;
    }

    /**
     * Gets the value of the poolutil property.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getPOOLUTIL() {
        return poolutil;
    }

    /**
     * Sets the value of the poolutil property.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setPOOLUTIL(BigDecimal value) {
        this.poolutil = value;
    }

    /**
     * Gets the value of the liabbranch property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLIABBRANCH() {
        return liabbranch;
    }

    /**
     * Sets the value of the liabbranch property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLIABBRANCH(String value) {
        this.liabbranch = value;
    }

    /**
     * Gets the value of the mortgageinitiated property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMORTGAGEINITIATED() {
        return mortgageinitiated;
    }

    /**
     * Sets the value of the mortgageinitiated property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMORTGAGEINITIATED(String value) {
        this.mortgageinitiated = value;
    }

    /**
     * Gets the value of the autofacility property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getAUTOFACILITY() {
        return autofacility;
    }

    /**
     * Sets the value of the autofacility property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setAUTOFACILITY(String value) {
        this.autofacility = value;
    }

    /**
     * Gets the value of the availableamount property.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getAVAILABLEAMOUNT() {
        return availableamount;
    }

    /**
     * Sets the value of the availableamount property.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setAVAILABLEAMOUNT(BigDecimal value) {
        this.availableamount = value;
    }

    /**
     * Gets the value of the liabno property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLIABNO() {
        return liabno;
    }

    /**
     * Sets the value of the liabno property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLIABNO(String value) {
        this.liabno = value;
    }

    /**
     * Gets the value of the blockamt property.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getBLOCKAMT() {
        return blockamt;
    }

    /**
     * Sets the value of the blockamt property.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setBLOCKAMT(BigDecimal value) {
        this.blockamt = value;
    }

    /**
     * Gets the value of the liabname property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLIABNAME() {
        return liabname;
    }

    /**
     * Sets the value of the liabname property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLIABNAME(String value) {
        this.liabname = value;
    }

    /**
     * Gets the value of the branchcode property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getBRANCHCODE() {
        return branchcode;
    }

    /**
     * Sets the value of the branchcode property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setBRANCHCODE(String value) {
        this.branchcode = value;
    }

    /**
     * Gets the value of the autofacilitycustno property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getAUTOFACILITYCUSTNO() {
        return autofacilitycustno;
    }

    /**
     * Sets the value of the autofacilitycustno property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setAUTOFACILITYCUSTNO(String value) {
        this.autofacilitycustno = value;
    }

    /**
     * Gets the value of the maker property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMAKER() {
        return maker;
    }

    /**
     * Sets the value of the maker property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMAKER(String value) {
        this.maker = value;
    }

    /**
     * Gets the value of the makerstamp property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMAKERSTAMP() {
        return makerstamp;
    }

    /**
     * Sets the value of the makerstamp property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMAKERSTAMP(String value) {
        this.makerstamp = value;
    }

    /**
     * Gets the value of the checker property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCHECKER() {
        return checker;
    }

    /**
     * Sets the value of the checker property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCHECKER(String value) {
        this.checker = value;
    }

    /**
     * Gets the value of the checkerstamp property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCHECKERSTAMP() {
        return checkerstamp;
    }

    /**
     * Sets the value of the checkerstamp property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCHECKERSTAMP(String value) {
        this.checkerstamp = value;
    }

    /**
     * Gets the value of the modno property.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getMODNO() {
        return modno;
    }

    /**
     * Sets the value of the modno property.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setMODNO(BigDecimal value) {
        this.modno = value;
    }

    /**
     * Gets the value of the txnstat property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTXNSTAT() {
        return txnstat;
    }

    /**
     * Sets the value of the txnstat property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTXNSTAT(String value) {
        this.txnstat = value;
    }

    /**
     * Gets the value of the authstat property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getAUTHSTAT() {
        return authstat;
    }

    /**
     * Sets the value of the authstat property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setAUTHSTAT(String value) {
        this.authstat = value;
    }

    /**
     * Gets the value of the poolCollateralsLinkage property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the poolCollateralsLinkage property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPoolCollateralsLinkage().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CollateralpoolFullType.PoolCollateralsLinkage }
     */
    public List<CollateralpoolFullType.PoolCollateralsLinkage> getPoolCollateralsLinkage() {
        if (poolCollateralsLinkage == null) {
            poolCollateralsLinkage = new ArrayList<CollateralpoolFullType.PoolCollateralsLinkage>();
        }
        return this.poolCollateralsLinkage;
    }

    /**
     * Gets the value of the udfdetails property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the udfdetails property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUDFDETAILS().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UDFDETAILSType2 }
     */
    public List<UDFDETAILSType2> getUDFDETAILS() {
        if (udfdetails == null) {
            udfdetails = new ArrayList<UDFDETAILSType2>();
        }
        return this.udfdetails;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="FUNCTION_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="COLLATERAL_CCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="COLLATERAL_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="LINKEDAMT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="LINKDAMTPOOL" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="COLLATUTIL" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="LINKEDPERCENTNUMBER" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="AVAIL_INT_RATE" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="INT_SPREAD" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="RATE_OF_INT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="EXPIRY_DATE" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="POOL_ID" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="ORDER_NO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="BRANCH_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="COLLATERAL_CODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="COLLATERAL_DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SHARING_PERCENTAGE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="LIMIT_CONTRIBUTION" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="COLLATERAL_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "functiontype",
            "collateralccy",
            "collateralamount",
            "linkedamt",
            "linkdamtpool",
            "collatutil",
            "linkedpercentnumber",
            "availintrate",
            "intspread",
            "rateofint",
            "expirydate",
            "poolid",
            "orderno",
            "type",
            "branchcode",
            "collateralcode",
            "collateraldescription",
            "sharingpercentage",
            "limitcontribution",
            "collateraltype"
    })
    public static class PoolCollateralsLinkage {

        @XmlElement(name = "FUNCTION_TYPE")
        protected String functiontype;
        @XmlElement(name = "COLLATERAL_CCY")
        protected String collateralccy;
        @XmlElement(name = "COLLATERAL_AMOUNT")
        protected BigDecimal collateralamount;
        @XmlElement(name = "LINKEDAMT")
        protected BigDecimal linkedamt;
        @XmlElement(name = "LINKDAMTPOOL")
        protected BigDecimal linkdamtpool;
        @XmlElement(name = "COLLATUTIL")
        protected BigDecimal collatutil;
        @XmlElement(name = "LINKEDPERCENTNUMBER")
        protected BigDecimal linkedpercentnumber;
        @XmlElement(name = "AVAIL_INT_RATE")
        protected BigDecimal availintrate;
        @XmlElement(name = "INT_SPREAD")
        protected BigDecimal intspread;
        @XmlElement(name = "RATE_OF_INT")
        protected BigDecimal rateofint;
        @XmlElement(name = "EXPIRY_DATE")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar expirydate;
        @XmlElement(name = "POOL_ID")
        protected BigDecimal poolid;
        @XmlElement(name = "ORDER_NO")
        protected BigDecimal orderno;
        @XmlElement(name = "TYPE")
        protected String type;
        @XmlElement(name = "BRANCH_CODE")
        protected String branchcode;
        @XmlElement(name = "COLLATERAL_CODE", required = true)
        protected String collateralcode;
        @XmlElement(name = "COLLATERAL_DESCRIPTION")
        protected String collateraldescription;
        @XmlElement(name = "SHARING_PERCENTAGE")
        protected String sharingpercentage;
        @XmlElement(name = "LIMIT_CONTRIBUTION")
        protected BigDecimal limitcontribution;
        @XmlElement(name = "COLLATERAL_TYPE")
        protected String collateraltype;

        /**
         * Gets the value of the functiontype property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getFUNCTIONTYPE() {
            return functiontype;
        }

        /**
         * Sets the value of the functiontype property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setFUNCTIONTYPE(String value) {
            this.functiontype = value;
        }

        /**
         * Gets the value of the collateralccy property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getCOLLATERALCCY() {
            return collateralccy;
        }

        /**
         * Sets the value of the collateralccy property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setCOLLATERALCCY(String value) {
            this.collateralccy = value;
        }

        /**
         * Gets the value of the collateralamount property.
         *
         * @return possible object is
         * {@link BigDecimal }
         */
        public BigDecimal getCOLLATERALAMOUNT() {
            return collateralamount;
        }

        /**
         * Sets the value of the collateralamount property.
         *
         * @param value allowed object is
         *              {@link BigDecimal }
         */
        public void setCOLLATERALAMOUNT(BigDecimal value) {
            this.collateralamount = value;
        }

        /**
         * Gets the value of the linkedamt property.
         *
         * @return possible object is
         * {@link BigDecimal }
         */
        public BigDecimal getLINKEDAMT() {
            return linkedamt;
        }

        /**
         * Sets the value of the linkedamt property.
         *
         * @param value allowed object is
         *              {@link BigDecimal }
         */
        public void setLINKEDAMT(BigDecimal value) {
            this.linkedamt = value;
        }

        /**
         * Gets the value of the linkdamtpool property.
         *
         * @return possible object is
         * {@link BigDecimal }
         */
        public BigDecimal getLINKDAMTPOOL() {
            return linkdamtpool;
        }

        /**
         * Sets the value of the linkdamtpool property.
         *
         * @param value allowed object is
         *              {@link BigDecimal }
         */
        public void setLINKDAMTPOOL(BigDecimal value) {
            this.linkdamtpool = value;
        }

        /**
         * Gets the value of the collatutil property.
         *
         * @return possible object is
         * {@link BigDecimal }
         */
        public BigDecimal getCOLLATUTIL() {
            return collatutil;
        }

        /**
         * Sets the value of the collatutil property.
         *
         * @param value allowed object is
         *              {@link BigDecimal }
         */
        public void setCOLLATUTIL(BigDecimal value) {
            this.collatutil = value;
        }

        /**
         * Gets the value of the linkedpercentnumber property.
         *
         * @return possible object is
         * {@link BigDecimal }
         */
        public BigDecimal getLINKEDPERCENTNUMBER() {
            return linkedpercentnumber;
        }

        /**
         * Sets the value of the linkedpercentnumber property.
         *
         * @param value allowed object is
         *              {@link BigDecimal }
         */
        public void setLINKEDPERCENTNUMBER(BigDecimal value) {
            this.linkedpercentnumber = value;
        }

        /**
         * Gets the value of the availintrate property.
         *
         * @return possible object is
         * {@link BigDecimal }
         */
        public BigDecimal getAVAILINTRATE() {
            return availintrate;
        }

        /**
         * Sets the value of the availintrate property.
         *
         * @param value allowed object is
         *              {@link BigDecimal }
         */
        public void setAVAILINTRATE(BigDecimal value) {
            this.availintrate = value;
        }

        /**
         * Gets the value of the intspread property.
         *
         * @return possible object is
         * {@link BigDecimal }
         */
        public BigDecimal getINTSPREAD() {
            return intspread;
        }

        /**
         * Sets the value of the intspread property.
         *
         * @param value allowed object is
         *              {@link BigDecimal }
         */
        public void setINTSPREAD(BigDecimal value) {
            this.intspread = value;
        }

        /**
         * Gets the value of the rateofint property.
         *
         * @return possible object is
         * {@link BigDecimal }
         */
        public BigDecimal getRATEOFINT() {
            return rateofint;
        }

        /**
         * Sets the value of the rateofint property.
         *
         * @param value allowed object is
         *              {@link BigDecimal }
         */
        public void setRATEOFINT(BigDecimal value) {
            this.rateofint = value;
        }

        /**
         * Gets the value of the expirydate property.
         *
         * @return possible object is
         * {@link XMLGregorianCalendar }
         */
        public XMLGregorianCalendar getEXPIRYDATE() {
            return expirydate;
        }

        /**
         * Sets the value of the expirydate property.
         *
         * @param value allowed object is
         *              {@link XMLGregorianCalendar }
         */
        public void setEXPIRYDATE(XMLGregorianCalendar value) {
            this.expirydate = value;
        }

        /**
         * Gets the value of the poolid property.
         *
         * @return possible object is
         * {@link BigDecimal }
         */
        public BigDecimal getPOOLID() {
            return poolid;
        }

        /**
         * Sets the value of the poolid property.
         *
         * @param value allowed object is
         *              {@link BigDecimal }
         */
        public void setPOOLID(BigDecimal value) {
            this.poolid = value;
        }

        /**
         * Gets the value of the orderno property.
         *
         * @return possible object is
         * {@link BigDecimal }
         */
        public BigDecimal getORDERNO() {
            return orderno;
        }

        /**
         * Sets the value of the orderno property.
         *
         * @param value allowed object is
         *              {@link BigDecimal }
         */
        public void setORDERNO(BigDecimal value) {
            this.orderno = value;
        }

        /**
         * Gets the value of the type property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getTYPE() {
            return type;
        }

        /**
         * Sets the value of the type property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setTYPE(String value) {
            this.type = value;
        }

        /**
         * Gets the value of the branchcode property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getBRANCHCODE() {
            return branchcode;
        }

        /**
         * Sets the value of the branchcode property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setBRANCHCODE(String value) {
            this.branchcode = value;
        }

        /**
         * Gets the value of the collateralcode property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getCOLLATERALCODE() {
            return collateralcode;
        }

        /**
         * Sets the value of the collateralcode property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setCOLLATERALCODE(String value) {
            this.collateralcode = value;
        }

        /**
         * Gets the value of the collateraldescription property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getCOLLATERALDESCRIPTION() {
            return collateraldescription;
        }

        /**
         * Sets the value of the collateraldescription property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setCOLLATERALDESCRIPTION(String value) {
            this.collateraldescription = value;
        }

        /**
         * Gets the value of the sharingpercentage property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getSHARINGPERCENTAGE() {
            return sharingpercentage;
        }

        /**
         * Sets the value of the sharingpercentage property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setSHARINGPERCENTAGE(String value) {
            this.sharingpercentage = value;
        }

        /**
         * Gets the value of the limitcontribution property.
         *
         * @return possible object is
         * {@link BigDecimal }
         */
        public BigDecimal getLIMITCONTRIBUTION() {
            return limitcontribution;
        }

        /**
         * Sets the value of the limitcontribution property.
         *
         * @param value allowed object is
         *              {@link BigDecimal }
         */
        public void setLIMITCONTRIBUTION(BigDecimal value) {
            this.limitcontribution = value;
        }

        /**
         * Gets the value of the collateraltype property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getCOLLATERALTYPE() {
            return collateraltype;
        }

        /**
         * Sets the value of the collateraltype property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setCOLLATERALTYPE(String value) {
            this.collateraltype = value;
        }

    }

}
